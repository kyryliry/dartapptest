# TemplateRedraw

Drawing app, implementation of my thesis.
The app has two parts: Spring boot backend and Flutter frontend. Also, app use PostgreSQL database.

There is two ways for running app: run all parts locally or use prepared built flutter app in pair with hosted backend. But second way has several limitations.

## Before you start
There is some information, that needs explanation before app is running.

For testing purposes and better user experience, server app generates admin account and one template.
Admin account can be created only by another admin account, and this role has more available functions than user role.
So, if you want to check admins functions, use:

- username: ```admin```
- password: ```admin```

If you delete autogenerated account or template, the system will regenerate them on server restart.

## Local run

This repository is showing the version of code that is running locally.
PostgreSQL database is required to install and prepare before app running.
All the files that are generated by the app, will be saved to your PC.

First, you will need to run server side, because frontend app sends requests to server right after start of run.

### Backend

#### PostgreSQL database
For running server you will need to install postgreSQL driver and create local database.

For Windows, you can download the installer from https://www.postgresql.org/download/windows/.

For macOS, you can use Homebrew for install and run services:
```
brew install postgresql
brew services start postgresql
```
After that, you will need to create database, you can use PgAdmin4, IntellijIdea interface for databases, command line etc.

As you can see in file ```application.properties``` in paintappserver/src/main/resources package, server app using database named **paintapptest**, username **postgres** and password **0000**. If you want to use different names and password, you need to change these data into
```application.properties``` file.

#### Spring boot app
Spring Boot app is placed in ```paintappserver``` package.
You can run server app in Intellij Idea IDE. If you want to, just run it with run button in right top corner. App don't require preparing any configurations.
Or you can run app simply in terminal and for this you will need to use maven for java version 17.
If you don't have maven, you can install it from https://maven.apache.org/download.cgi.
Or for MacOS, you can use Homebrew if you like:
```
brew install maven
```

After that, go to package with spring boot app: ```../dartapptest/paintappserver``` and in this package open terminal and run
```
mvn spring:boot run
```

App will run on ```http://localhost:8080```

Note: If you encounter an error during the build process such as: ```java: java.lang.NoSuchFieldError: Class com.sun.tools.javac.tree.JCTree$JCImport does not have member field 'com.sun.tools.javac.tree.JCTree qualid```. This error is most likely caused by using an incompatible version of Java. The project is designed to use Java 17 and dependencies that are compatible with Java 17.

### Flutter frontend app

#### Installation and Run

Installing Flutter SDK:

https://docs.flutter.dev/get-started/install/windows

This project requires SDK version >=3.1.2 & <4.0.0

*Note for macOS: you can use homebrew or Rosetta 2 for installing Flutter SDK on macOS, but I personally don't know any 100% working guides for this, so I can't advise you to use or not to use any of these options.* 

After installation run command in console window in Flutter directory to check if you need to install some extra dependencies:
```
flutter doctor
```
Next, stay in project directory and run command:
```
flutter pub get
```
This command will fetch all project dependencies, that are required for project run.
Run is depend on which system do you want to test the app. For now on, usable are web and desktop versions: macOS and windows (app was not tested on linux, but it supposed to work on it as well) 
but for every of them you will need to install required environment. The requirements are described in output of *flutter doctor* command.

In project directory ```paintappdart``` run this command in console window to start app:

```
flutter run 
```
After execution, you will be asked to choose run device: web, desktop (masOC/windows)

Or if you want you may execute 
```
flutter run -d *device*
```
where device may be: chrome, macOS, windows, linux etc.

## Built flutter app with deployed server

In package ```builtVersions``` in root package you will find two built versions of the client app (in zip archives):
- Windows
- web

Extract the version you would like to use.

For Windows version just go to its package and run ```paintappdart``` file.

For web there is no any deployed on server version, so if you want to use web version, you will need to start local server. 

For example to start Python3 HTTP Server, you need to install python3 and in package ```../builtVersions/web``` run command in terminal
```
python -m http.server 8000
```
After that, open in browser ```http://localhost:8000``` and use the app.

After the app started, you will se loading circle and some other elements of Library page. It means, that
 client app sent request to server, and waits for response.

As was said in thesis document, there are some host limitations for server app. So, because server is stops if there are no requests for 10+ minutes, first request will "wake up" server, and it will load in 2-3 minutes.
And, because free host don't save permanently files, all data (files and database entries) will be accessed only while server is online. After it turns off, all the data will be deleted. But of course, after restart, system will automatically generate admin account and test template from assets.

Unfortunately, there is no built version for macOS, because inside whole attachment it was too huge and didn't fit the max size of attachment in KOS. But, it is available on public fel gitlab repository (in builtVersions package), where is placed copy of this attachment: https://gitlab.fel.cvut.cz/kyryliry/dartapptest
