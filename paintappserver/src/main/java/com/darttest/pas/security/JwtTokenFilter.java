package com.darttest.pas.security;

import com.darttest.pas.security.SecurityUtils;
import com.darttest.pas.security.model.AuthenticationToken;
import com.darttest.pas.security.model.UserDetails;
import com.darttest.pas.security.service.UserDetailsService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.List;

import static com.darttest.pas.utils.Constants.DEFAULT_KEY;

public class JwtTokenFilter extends OncePerRequestFilter {

    private final UserDetailsService userDetailsService;

    public JwtTokenFilter(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer ")) {
            String token = header.substring(7);
            try {
                Key SECRET_KEY = Keys.hmacShaKeyFor(DEFAULT_KEY.getBytes(StandardCharsets.UTF_8));
                Jws<Claims> claimsJws = Jwts.parserBuilder()
                        .setSigningKey(SECRET_KEY)
                        .build()
                        .parseClaimsJws(token);

                Claims claims = claimsJws.getBody();
                String username = claims.getSubject();
                List<String> authorities = claims.get("authorities", List.class);

                UserDetails userDetails = (UserDetails) userDetailsService.loadUserByUsername(username); // Load UserDetails
                AuthenticationToken authenticationToken = new AuthenticationToken(
                        userDetails.getAuthorities(), userDetails);

                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } catch (JwtException e) {
                SecurityContextHolder.clearContext();
                e.printStackTrace();
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}
