package com.darttest.pas.security.service;

import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.User;
import com.darttest.pas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final UserRepository userDao;

    @Autowired
    public UserDetailsService(UserRepository userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userDao.findByUsername(username);
        if (user == null) {
            throw new NotFoundException("User with username " + username + " not found.");
        }
        return new com.darttest.pas.security.model.UserDetails(user);
    }

}

