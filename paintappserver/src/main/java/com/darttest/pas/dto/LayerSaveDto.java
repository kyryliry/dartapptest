package com.darttest.pas.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class LayerSaveDto implements Serializable {
    public int strokeSize;
    public String color;
    public String showColor;
    public int number;
    public MultipartFile fileL;
}
