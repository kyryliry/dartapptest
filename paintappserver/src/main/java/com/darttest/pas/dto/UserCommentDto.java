package com.darttest.pas.dto;

import com.darttest.pas.model.enums.CommentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserCommentDto implements Serializable {
    private Long id;
    private String text;
    private LocalDateTime date;
    private Long subjectId;
    private CommentType type;
    private Long authorId;
    private List<Long> likesIds;
    private List<Long> dislikesIds;
    private List<Long> repliesIds;
}
