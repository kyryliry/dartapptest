package com.darttest.pas.dto;

import com.darttest.pas.model.enums.TemplateState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TemplateSaveDto implements Serializable {
    private String templateName;
    private MultipartFile file;
    private String categories;
    private Long creatorId;
    private TemplateState state;
    private List<LayerSaveDto> layers;
    private Double maxScore;
}
