package com.darttest.pas.dto;

import com.darttest.pas.model.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserDto implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String email;
    private Boolean isBlocked;
    private Double globalPlayerRating;
    private Double globalCreatorRating;
    private List<TemplateDto> ownedTemplates;
    private List<TemplateUsageDto> playedTemplates;
    private long playerTemplatesNum;
    private int followers;
    private int following;
    private UserRole role;
    private String newToken;
}
