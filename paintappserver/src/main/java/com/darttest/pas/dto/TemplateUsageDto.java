package com.darttest.pas.dto;

import com.darttest.pas.model.enums.UsageState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TemplateUsageDto implements Serializable {
    private Long id;
    private Double actualScore;
    private LocalDateTime dateOfLastPlay;
    private Long playerId;
    private Long parentId;
    private UsageState state;
    private Long unfinishedUsageId;
    private String newToken;
}
