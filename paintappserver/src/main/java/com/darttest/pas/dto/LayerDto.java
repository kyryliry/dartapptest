package com.darttest.pas.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LayerDto implements Serializable {

    private Long templateId;
    private Long id;
    private Long attachmentId;


    private Integer strokeSize;


    private String color;


    private String showColor;


    private Integer number;


    private String points;
// оця річ важить 9 мегабайт коли я замальовую 60-70% площі


    private MultipartFile file;


}
