package com.darttest.pas.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UnfinishedUsageDto implements Serializable {
    private Long id;
    private Double actualScore;
    private Long usageId;
    private MultipartFile actualLayerAtch;
    private MultipartFile finishedLayerAtch;
    private Long actualLayerId;
}
