package com.darttest.pas.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CategoryDto implements Serializable {
    private String name;
    private Long id;
    private String newToken;
}
