package com.darttest.pas.dto;

import com.darttest.pas.model.enums.CommentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ReportDto implements Serializable {
    private Long id;
    private String text;
    private LocalDateTime date;
    private Long subjectId;
    private CommentType type;
    private Long authorId;
    private String authorUsername;
    private Long reviewerId;
    private String reviewerUsername;
    private Boolean isOpen;
    private String closeReason;
    private LocalDateTime closeDate;
}
