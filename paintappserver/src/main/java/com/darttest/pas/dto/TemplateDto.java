package com.darttest.pas.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TemplateDto {

    private Long id;

    private String templateName;

    private MultipartFile file;

    private List<CategoryDto> categories;
    private Long creatorId;

}
