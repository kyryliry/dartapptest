package com.darttest.pas.dto;

import com.darttest.pas.model.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ShortUserDto implements Serializable {
    private Long id;
    private String username;
    private String email;
    private UserRole role;
    private Boolean isBlocked;
    private String newToken;
}
