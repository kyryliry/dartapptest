package com.darttest.pas.dto;

import lombok.Getter;
import lombok.Setter;

public class PointDto {
    @Getter
    @Setter
    private double x;

    @Getter
    @Setter
    private double y;
}
