package com.darttest.pas.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GalleryImageDto implements Serializable {
    private Long id;
    private LocalDateTime uploadDate;
    private Long authorId;
    private Long usageId;
    private String description;
    private MultipartFile image;
    private String imageData;
    private String authorName;
    private double actualScore;
    private List<Long> likes;
    private List<Long> dislikes;

    private String newToken;
}
