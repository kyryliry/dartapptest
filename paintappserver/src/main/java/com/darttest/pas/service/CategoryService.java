package com.darttest.pas.service;

import com.darttest.pas.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface CategoryService {
    void persist(Category category);
    void update(Category category);
    void delete(Category category);
    void deleteById(Long id);
    Category findById(Long id);
    List<Category> findAll();
    Boolean isExist(String name);
    Category findByName(String name);
    Page<Category> findAllPageable(Pageable pageable);
}
