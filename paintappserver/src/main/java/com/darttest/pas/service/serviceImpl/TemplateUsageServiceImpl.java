package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.model.enums.UsageState;
import com.darttest.pas.repository.TemplateUsageRepository;
import com.darttest.pas.service.TemplateUsageService;
import com.darttest.pas.service.UnfinishedUsageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class TemplateUsageServiceImpl implements TemplateUsageService {
    private final TemplateUsageRepository templateUsageRepository;
    private final UnfinishedUsageService unfinishedUsageService;
    private final GalleryImageServiceImpl galleryImageService;
    private final ReportServiceImpl reportService;

    @Override
    @Transactional
    public void persist(TemplateUsage templateUsage) {
        templateUsageRepository.save(templateUsage);
    }

    @Override
    @Transactional
    public TemplateUsage createTemplateUsage(User player, Template parent) {
        if(findTemplateUsageByPlayerAndParent(player, parent)!=null){
            throw new ExistsException("template usage for given parameters already exists");
        }
        TemplateUsage templateUsage = new TemplateUsage(null, LocalDateTime.now(), player, parent, null, UsageState.INPROGRESS);
        persist(templateUsage);
        return templateUsage;
    }

    @Override
    @Transactional
    public void update(TemplateUsage templateUsage) {
        for(GalleryImage i: galleryImageService.findGalleryImagesByUsage(templateUsage)){
            i.setUsage(templateUsage);
            galleryImageService.update(i);
        }
        templateUsageRepository.save(templateUsage);
    }

    @Override
    public TemplateUsage findById(Long id) {
        return templateUsageRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void delete(TemplateUsage templateUsage) {
        if(templateUsage.getUnfinishedDetails()!=null) {
            unfinishedUsageService.delete(templateUsage.getUnfinishedDetails());
        }
        List<GalleryImage> images = galleryImageService.findGalleryImagesByUsage(templateUsage);
        while(!images.isEmpty()){
            GalleryImage img = images.get(images.size()-1);
            List<Report> reports = reportService.findReportsBySubjectIdAndType(img.getId(), CommentType.IMAGE);
            for(Report r: reports){
                reportService.closeReportBySystem(r);
            }
            galleryImageService.delete(img);
            images.remove(img);
        }
        templateUsageRepository.delete(templateUsage);
    }

    @Override
    public List<TemplateUsage> findTemplateUsagesByPlayer(User player) {
        return templateUsageRepository.findTemplateUsagesByPlayer(player);
    }

    @Override
    public Map<String, List<Template>> findAllSortedTemplatesByPlayer(User player){
        List<TemplateUsage> allTemplateUsagesByPlayer = findTemplateUsagesByPlayer(player);
        List<Template> unfinishedTemplates = new ArrayList<>();
        List<Template> finishedTemplates = new ArrayList<>();
        List<Template> inProgressNullScoreTemplates = new ArrayList<>();
        List<Template> inProgressNotNullScoreTemplates = new ArrayList<>();
        for(TemplateUsage t: allTemplateUsagesByPlayer){
            if(t.getState()==UsageState.UNFINISHED){
                unfinishedTemplates.add(t.getParent());
            }else if(t.getState()==UsageState.FINISHED){
                finishedTemplates.add(t.getParent());
            }else if(t.getState()==UsageState.INPROGRESS){
                if(t.getActualScore()==null){
                    inProgressNullScoreTemplates.add(t.getParent());
                }else{
                    inProgressNotNullScoreTemplates.add(t.getParent());
                }
            }
        }
        Map<String, List<Template>> temps = new HashMap<>();
        temps.put("unfinished", unfinishedTemplates);
        temps.put("finished", finishedTemplates);
        temps.put("nullProgress", inProgressNullScoreTemplates);
        temps.put("notNullProgress", inProgressNotNullScoreTemplates);
        return temps;
    }

    @Override
    public List<TemplateUsage> findTemplateUsagesByParent(Template parent) {
        return templateUsageRepository.findTemplateUsagesByParent(parent);
    }


    @Override
    @Transactional
    public TemplateUsage actualizeUsage(TemplateUsage templateUsage) {
        assert templateUsage!=null;
        templateUsage.setDateOfLastPlay(LocalDateTime.now());
        if(templateUsage.getState()==UsageState.FINISHED){
            templateUsage.setState(UsageState.INPROGRESS);
        }
        update(templateUsage);
        return templateUsage;
    }

    @Override
    @Transactional
    public TemplateUsage finish(TemplateUsage templateUsage) {
        templateUsage.setDateOfLastPlay(LocalDateTime.now());
        templateUsage.setState(UsageState.FINISHED);
        if(templateUsage.getUnfinishedDetails()!=null){
            UnfinishedUsage tmp = templateUsage.getUnfinishedDetails();
            templateUsage.setUnfinishedDetails(null);
            unfinishedUsageService.delete(tmp);
        }
        update(templateUsage);
        return templateUsage;
    }

    @Override
    @Transactional
    public TemplateUsage saveAsUnfinished(TemplateUsage templateUsage, UnfinishedUsage unfinishedUsage) {
        templateUsage.setDateOfLastPlay(LocalDateTime.now());
        templateUsage.setState(UsageState.UNFINISHED);
        if(templateUsage.getUnfinishedDetails()!=null){
                templateUsage.getUnfinishedDetails().setActualLayer(unfinishedUsage.getActualLayer());
                templateUsage.getUnfinishedDetails().setActualLayerAtch(unfinishedUsage.getActualLayerAtch());
                templateUsage.getUnfinishedDetails().setFinishedLayerAtch(unfinishedUsage.getFinishedLayerAtch());
                unfinishedUsageService.update(templateUsage.getUnfinishedDetails());
        }else {
            unfinishedUsage.setUsage(templateUsage);
            unfinishedUsageService.persist(unfinishedUsage);
            templateUsage.setUnfinishedDetails(unfinishedUsage);
        }
        templateUsageRepository.save(templateUsage);
        return templateUsage;
    }

    @Override
    public TemplateUsage findTemplateUsageByPlayerAndParent(User player, Template parent) {
        return templateUsageRepository.findTemplateUsageByPlayerAndParent(player, parent);
    }

    @Override
    @Transactional
    public GalleryImage createGalleryImage(Long id, GalleryImage galleryImage) {
        return null;
    }

    @Override
    public void changeUnfinishedTemplateUsageName(TemplateUsage templateUsage, String newName, String oldName) {
        unfinishedUsageService.changeUnfinishedTemplateUsageName(templateUsage.getUnfinishedDetails(), newName, oldName);
    }
}
