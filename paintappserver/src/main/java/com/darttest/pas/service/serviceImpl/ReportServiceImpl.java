package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.Report;
import com.darttest.pas.model.User;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReportServiceImpl extends CommentServiceImpl<Report> {
    private final ReportRepository reportRepository;
    public ReportServiceImpl(CommentRepository<Report, Long> commentRepository, ReportRepository reportRepository) {
        super(commentRepository);
        this.reportRepository = reportRepository;
    }

    public List<Report> findByReviewer(User reviewer){
        return reportRepository.findAllByReviewer(reviewer);
    }

    public Page<Report> findAllPaged(Pageable pageable){
        return reportRepository.findAllReports(pageable);
    }

    public Page<Report> findAllOpenedPageable(Boolean isOpen, Pageable pageable){
        return reportRepository.findByIsOpen(isOpen, pageable);
    }

    public Report findReportById(Long id){
        return (Report) reportRepository.findById(id).orElse(null);
    }

    @Transactional
    public void closeReport(Report report){
        if(!report.getIsOpen()){
            throw new ExistsException("report is already closed");
        }
        if(report.getCloseReason().equals("")){
            throw new NotFoundException("close reason can not be empty");
        }
        report.setIsOpen(false);
        update(report);
    }

    @Transactional
    public void closeReportBySystem(Report report){
        if(!report.getIsOpen()){
            throw new ExistsException("report is already closed");
        }
        report.setReviewer(null);
        report.setCloseDate(LocalDateTime.now());
        report.setCloseReason("automatically closed by system");
        report.setIsOpen(false);
        update(report);
    }

    @Transactional
    public Report reopenReport(Report report){
        if(report.getIsOpen()){
            throw new ExistsException("report is not closed");
        }
        report.setIsOpen(true);
        update(report);
        return report;
    }

    @Transactional
    public void createReport(Report report){
        if(reportRepository.findByAuthorAndSubjectIdAndType(report.getAuthor(), report.getSubjectId(), report.getType())!=null){
            throw new ExistsException("this user has already reported this object");
        }
        if(report.getText().equals("")){
            throw new NotFoundException("report text can not be empty");
        }
        persist(report);
    }

    List<Report> findReportsBySubjectIdAndType(Long subjectId, CommentType type){
        return reportRepository.findReportBySubjectIdAndType(subjectId, type);
    }

    List<Report> findAllByReporter(User author){
        return reportRepository.findAllByAuthor(author);
    }
}
