package com.darttest.pas.service;

import com.darttest.pas.model.Attachment;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface AttachmentService {
    Attachment saveNewLayerAttachment(MultipartFile file, String templateName, Integer number, String directory) throws IOException;

    Attachment findById(Long id);

    byte[] downloadImage(Long id) throws IOException;
    void delete(Attachment attachment);
    void update(Attachment attachment);
}
