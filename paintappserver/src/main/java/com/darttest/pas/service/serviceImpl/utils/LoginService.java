package com.darttest.pas.service.serviceImpl.utils;
import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.security.AuthenticationSuccess;
import com.darttest.pas.security.DefaultAuthenticationProvider;
import com.darttest.pas.security.SecurityUtils;
import com.darttest.pas.security.model.UserDetails;
import com.darttest.pas.security.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Service
public class LoginService {

    private final DefaultAuthenticationProvider provider;
    private final UserDetailsService userDetailsService;
    @Autowired
    private AuthenticationSuccess authenticationSuccessHandler;

    @Autowired
    public LoginService(DefaultAuthenticationProvider provider, UserDetailsService userDetailsService) {
        this.provider = provider;
        this.userDetailsService = userDetailsService;
    }

    @Transactional
    public void loginUser (String username, String password, HttpServletRequest request, HttpServletResponse response) throws ExistsException, IOException {
//        if (SecurityUtils.getCurrentUserDetails() != null) {
//            throw new ExistsException("You are already login." + SecurityUtils.getCurrentUserDetails().getUsername());
//        }
        Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);
        authentication = provider.authenticate(authentication);

        if (authentication.isAuthenticated()) {
            UserDetails userDetails = (UserDetails) userDetailsService.loadUserByUsername(username);
            SecurityUtils.setCurrentUser(userDetails);
            authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
        }
    }
}

