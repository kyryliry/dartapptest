package com.darttest.pas.service;

import com.darttest.pas.model.TemplateImage;

import java.util.List;

public interface TemplateImageService {
    void persist(TemplateImage templateImage);
    TemplateImage findById(Long id);

    List<TemplateImage> findAll();
    void update(TemplateImage templateImage);
    void delete(TemplateImage templateImage);

}
