package com.darttest.pas.service;

import com.darttest.pas.model.Comment;
import com.darttest.pas.model.enums.CommentType;

import java.util.List;

public interface CommentService {
    void persist(Comment comment);
    void update(Comment comment);
    Comment findById(Long id);
    void delete(Comment comment);
    List<Comment> findBySubjectIdAndType(Long subjectId, CommentType type);
}
