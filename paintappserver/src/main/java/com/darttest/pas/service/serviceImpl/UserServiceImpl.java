package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.dto.UserDto;
import com.darttest.pas.exceptions.AccountException;
import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.model.enums.UserRole;
import com.darttest.pas.repository.UserRepository;
import com.darttest.pas.service.TemplateService;
import com.darttest.pas.service.TemplateUsageService;
import com.darttest.pas.service.UserService;
import com.darttest.pas.service.serviceImpl.utils.EmailService;
import com.darttest.pas.utils.Constants;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final TemplateService templateService;
    private final PasswordEncoder encoder;
    private final GalleryImageServiceImpl galleryImageService;
    private final TemplateUsageService templateUsageService;
    private final ReportServiceImpl reportService;
    private final UserCommentServiceImpl userCommentService;
    private final EmailService emailService;

    /*
     * ---------------------------------------------------------------------------------------
     *                          START OF USER METHODS SECTION
     * ---------------------------------------------------------------------------------------
     */
    @Override
    @Transactional
    public void persist(User user) {
        if (this.findByUsername(user.getUsername()) != null) {
            throw new ExistsException("username already exists");
        }
        if (this.findByEmail(user.getEmail()) != null) {
            throw new ExistsException("email already exists");
        }
        if (user.getPassword().equals("") || user.getPassword() == null) {
            throw new AccountException("password can not be empty");
        }
        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        if (!patternMatches(user.getEmail(), regexPattern)) {
            throw new ExistsException("email is not valid");
        }
        sendAppropriateEmail("create", user.getEmail(), user);
        user.encodePassword(encoder);
        userRepository.save(user);
    }

    public void sendAppropriateEmail(String reason, String email, User user){
        switch (reason) {
            case "create" -> emailService.sendSimpleEmail(email, "Your account data", "You have a new account in the TemplateRedraw app, here are your data:\n" +
                    "username: " + user.getUsername() + ",\n" +
                    "password: " + user.getPassword() + "\n");
            case "updateUsername" -> emailService.sendSimpleEmail(email, "Your account data", "Your account has been changed in the TemplateRedraw app, here are your data:\n" +
                        "username: " + user.getUsername() + "\n");
            case "updatePass" -> emailService.sendSimpleEmail(email, "Your account data", "Your password has been changed in the TemplateRedraw app, here are your new data:\n" +
                    "password: " + user.getPassword() + "\n");
        }

    }

    public static boolean patternMatches(String emailAddress, String regexPattern) {
        return Pattern.compile(regexPattern)
                .matcher(emailAddress)
                .matches();
    }


    @Override
    @Transactional
    public void update(User user) {
        userRepository.save(user);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findAllByRole(UserRole role) {
        return userRepository.findAllByRole(role);
    }

    @Override
    public Page<User> findAllByRolePageable(UserRole role, Pageable pageable) {
        return userRepository.findAllByRole(role, pageable);
    }

    @Override
    public Page<User> findAllByIsBlockedPageable(Boolean isBlocked, Pageable pageable){
        return userRepository.findAllByIsBlocked(isBlocked, pageable);
    }

    @Override
    public Page<User> findFollowersById(Long userId, Pageable pageable) {
        return userRepository.findFollowersById(userId, pageable);
    }

    @Override
    public Page<User> findFollowingById(Long userId, Pageable pageable) {
        return userRepository.findFollowingById(userId, pageable);
    }

    @Override
    public Page<User> findAllSortedByPlayerRating(Pageable pageable){
        return userRepository.findAllByOrderByGlobalPlayerRatingDesc(pageable);
    }

    @Override
    public Page<User> findAllSortedByCreatorRating(Pageable pageable){
        return userRepository.findAllByOrderByGlobalCreatorRatingDesc(pageable);
    }

    @Override
    @Transactional
    public void changePassword(User user, String password) {
        user.setPassword(password);
        sendAppropriateEmail("updatePass", user.getEmail(), user);
        user.encodePassword(encoder);
        update(user);
    }

    @Override
    @Transactional
    public void changeEmail(User user, String email) {
        if (findByEmail(email) != null) {
            throw new ExistsException("username already exists");
        }
        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        if (!patternMatches(email, regexPattern)) {
            throw new ExistsException("email is not valid");
        }

        user.setEmail(email);
        update(user);
    }

    @Override
    @Transactional
    public void updateUser(UserDto dto, Long id) {
        User user = findById(id);
        Objects.requireNonNull(user);
        if (!dto.getEmail().equals(user.getEmail())) {
            changeEmail(user, dto.getEmail());
        }
        if (!dto.getPassword().equals("") && !encoder.matches(dto.getPassword(), user.getPassword())) {
            changePassword(user, dto.getPassword());
        }
        if (!dto.getUsername().equals(user.getUsername())) {
            changeUsername(user, dto.getUsername());
        }
        if(!dto.getRole().equals(user.getRole())){
            user.setRole(dto.getRole());
            update(user);
        }
    }

    @Override
    @Transactional
    public void changeUsername(User user, String username) {
        String old = user.getUsername();
        if (findByUsername(username) != null) {
            throw new ExistsException("username already exists");
        }
        user.setUsername(username);
        sendAppropriateEmail("updateUsername", user.getEmail(), user);
        changeAllTemplatesNames(user, username, old);
        update(user);
    }

    public void changeAllTemplatesNames(User user, String username, String old){
        for(Template t: user.getOwnedTemplates()){
            String newName = username + "_" + t.getTemplateName().split("_")[1];
            templateService.changeTemplateName(t, newName);
        }
        for(TemplateUsage tu: templateUsageService.findTemplateUsagesByPlayer(user)){
            if(tu.getUnfinishedDetails()!=null) {
                String newName = username;
                templateUsageService.changeUnfinishedTemplateUsageName(tu, newName, old);
            }
        }
    }

    @Transactional
    public void follow(User follower, User following){
        if(Objects.equals(follower.getId(), following.getId())){
            throw new AccountException("You can't subscribe yourself!");
        }
        follower.getFollowing().add(following);
        following.getFollowers().add(follower);
        update(following);
        update(follower);
    }

    @Transactional
    public void unfollow(User follower, User following){
        if(Objects.equals(follower.getId(), following.getId())){
            throw new AccountException("You can't unsubscribe yourself!");
        }
        if (following.getFollowers().contains(follower) && follower.getFollowing().contains(following)) {
            follower.getFollowing().remove(following);
            following.getFollowers().remove(follower);
            update(follower);
            update(following);
        }else{
            throw new AccountException("You arent subscribe!");
        }
    }


    @Override
    @Transactional
    public void delete(User user) {
        List<Report> reports = reportService.findReportsBySubjectIdAndType(user.getId(), CommentType.USER);
        for(Report r: reports){
            reportService.closeReportBySystem(r);
        }

        List<Report> reported = reportService.findAllByReporter(user);
        for(Report r: reported){
            r.setAuthor(null);
            reportService.update(r);
        }

        List<Report> reviewed = reportService.findByReviewer(user);
        for(Report r: reviewed){
            r.setReviewer(null);
            reportService.update(r);
        }

        List<UserComment> comments = userCommentService.findAllByAuthor(user);
        for(UserComment c: comments){
            c.setAuthor(null);
            userCommentService.update(c);
        }

        while(user.getPlayedTemplates().size()!=0){
            TemplateUsage t = user.getPlayedTemplates().get(user.getPlayedTemplates().size()-1);
            user.getPlayedTemplates().remove(t);
            templateService.removeGalleryImagesByUsage(t);
            templateUsageService.delete(t);
        }
        if(user.getPlayedTemplates().isEmpty()){
            File file = new File(Constants.UNF_USAGE_DIRECTORY +"/"+user.getUsername());
            boolean deleted = file.delete();
            if(!deleted){
                throw new NotFoundException("file not found");
            }
        }


        if(user.getFollowing()!=null) {
            for (User following : user.getFollowing()) {
                following.getFollowers().remove(user);
                update(following);
            }
        }
        if(user.getFollowers()!=null) {
            for (User subscriber : user.getFollowers()) {
                subscriber.getFollowing().remove(user);
                update(subscriber);
            }
        }

        List<GalleryImage> likedG = galleryImageService.findByLikes_Id(user.getId());
        List<GalleryImage> dislikedG = galleryImageService.findByDislikes_Id(user.getId());
        for(GalleryImage t: likedG){
            t.getLikes().remove(user);
            galleryImageService.update(t);
        }
        for(GalleryImage t: dislikedG){
            t.getDislikes().remove(user);
            galleryImageService.update(t);
        }
        List<Template> likedT = templateService.findByLikes_Id(user.getId());
        List<Template> dislikedT = templateService.findByDislikes_Id(user.getId());
        for(Template t: likedT){
            t.getLikes().remove(user);
            templateService.update(t);
        }
        for(Template t: dislikedT){
            t.getDislikes().remove(user);
            templateService.update(t);
        }

        for(Template t: user.getOwnedTemplates()){
            t.setCreator(null);
            templateService.changeTemplateName(t, "[deleteduser"+user.getId()+"]"+t.getTemplateName());
        }

        userRepository.delete(user);
    }

    @Override
    @Transactional
    public void block(User user) {
        if(user.getIsBlocked()){
            throw new ExistsException("user is already blocked");
        }
        user.setIsBlocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unblock(User user) {
        if(!user.getIsBlocked()){
            throw new NotFoundException("user is not blocked");
        }
        user.setIsBlocked(false);
        update(user);
    }

    /*
     * ---------------------------------------------------------------------------------------
     *                        START OF TEMPLATE USAGE METHODS SECTION
     * ---------------------------------------------------------------------------------------
     */

    @Override
    @Transactional
    public void addTemplateUsage(User user, Template template) {
        TemplateUsage templateUsage = templateUsageService.createTemplateUsage(user, template);
        user.getPlayedTemplates().add(templateUsage);
        update(user);
        template.setDownloadsCount(template.getDownloadsCount()+1);
        templateService.update(template);
    }

    @Override
    @Transactional
    public void finishTemplateUsage(TemplateUsage templateUsage) {
        TemplateUsage newUsage = templateUsageService.finish(templateUsage);
        updateTemplateUsageList(templateUsage.getPlayer(), templateUsage, newUsage);
    }

    @Override
    @Transactional
    public void actualizeTemplateUsage(TemplateUsage templateUsage) {
        TemplateUsage newUsage = templateUsageService.actualizeUsage(templateUsage);
        updateTemplateUsageList(templateUsage.getPlayer(), templateUsage, newUsage);
    }

    @Override
    @Transactional
    public void saveAsUnfinished(TemplateUsage templateUsage, UnfinishedUsage unfinishedUsage) {
        TemplateUsage newUsage = templateUsageService.saveAsUnfinished(templateUsage, unfinishedUsage);
        updateTemplateUsageList(templateUsage.getPlayer(), templateUsage, newUsage);
    }

    @Override
    @Transactional
    public void removeTemplateUsage(TemplateUsage templateUsage) {
        User user = templateUsage.getPlayer();
        user.setGlobalPlayerRating(user.getGlobalPlayerRating()-templateUsage.getActualScore());
        user.getPlayedTemplates().remove(templateUsage);
        templateService.removeGalleryImagesByUsage(templateUsage);
        templateUsageService.delete(templateUsage);
        update(user);
    }

    public void updateTemplateUsageList(User user, TemplateUsage oldT, TemplateUsage newUsage){
        TemplateUsage old = user.getPlayedTemplates().stream().filter(c->c.getId().equals(oldT.getId())).findFirst().orElse(null);;
        user.getPlayedTemplates().remove(old);
        user.getPlayedTemplates().add(newUsage);
        update(user);
    }


}
