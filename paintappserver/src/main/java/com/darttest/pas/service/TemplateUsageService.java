package com.darttest.pas.service;

import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.UsageState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TemplateUsageService {
    void persist(TemplateUsage templateUsage);
    TemplateUsage createTemplateUsage(User player, Template parent);
    void update(TemplateUsage templateUsage);
    TemplateUsage findById(Long id);
    void delete(TemplateUsage templateUsage);
    List<TemplateUsage> findTemplateUsagesByPlayer(User player);
    List<TemplateUsage> findTemplateUsagesByParent(Template parent);
    TemplateUsage actualizeUsage(TemplateUsage templateUsage);
    TemplateUsage finish(TemplateUsage templateUsage);
    TemplateUsage saveAsUnfinished(TemplateUsage templateUsage, UnfinishedUsage unfinishedUsage);
    TemplateUsage findTemplateUsageByPlayerAndParent(User player, Template parent);
    GalleryImage createGalleryImage(Long id, GalleryImage galleryImage);
    Map<String, List<Template>> findAllSortedTemplatesByPlayer(User player);
    void changeUnfinishedTemplateUsageName(TemplateUsage templateUsage, String newName, String oldName);
}
