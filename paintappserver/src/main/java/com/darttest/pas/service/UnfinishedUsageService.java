package com.darttest.pas.service;

import com.darttest.pas.model.TemplateUsage;
import com.darttest.pas.model.UnfinishedUsage;

public interface UnfinishedUsageService {
    void persist(UnfinishedUsage unfinishedUsage);
    void update(UnfinishedUsage unfinishedUsage);
    UnfinishedUsage findById(Long id);
    void delete(UnfinishedUsage unfinishedUsage);
    void changeUnfinishedTemplateUsageName(UnfinishedUsage unfinishedUsage, String newName, String oldName);
}
