package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.model.Comment;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.repository.CommentRepository;
import com.darttest.pas.repository.GalleryImageRepository;
import com.darttest.pas.repository.TemplateRepository;
import com.darttest.pas.repository.UserRepository;
import com.darttest.pas.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public abstract class CommentServiceImpl<T extends Comment> implements CommentService {
    private final CommentRepository<T, Long> commentRepository;

    @Override
    @Transactional
    public void persist(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void update(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Comment comment) {
        commentRepository.delete(comment);
    }

    @Override
    public List<Comment> findBySubjectIdAndType(Long subjectId, CommentType type){
        return commentRepository.findBySubjectIdAndType(subjectId, type);
    }
}
