package com.darttest.pas.service;

import com.darttest.pas.dto.UserDto;
import com.darttest.pas.model.Template;
import com.darttest.pas.model.TemplateUsage;
import com.darttest.pas.model.UnfinishedUsage;
import com.darttest.pas.model.User;
import com.darttest.pas.model.enums.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    void persist(User user);
    void update(User user);
    User findById(Long id);
    User findByUsername(String name);
    User findByEmail(String email);
    List<User> findAllByRole(UserRole role);
    Page<User> findAllByRolePageable(UserRole role, Pageable pageable);
    Page<User> findAllByIsBlockedPageable(Boolean isBlocked, Pageable pageable);
    Page<User> findFollowersById(Long userId, Pageable pageable);
    Page<User> findFollowingById(Long userId, Pageable pageable);
    Page<User> findAllSortedByPlayerRating(Pageable pageable);
    Page<User> findAllSortedByCreatorRating(Pageable pageable);
    void changePassword(User user, String password);
    void changeEmail(User user, String email);
    void updateUser(UserDto dto, Long id);
    void changeUsername(User user, String username);
    void follow(User follower, User following);
    void unfollow(User follower, User following);
    void delete(User user);
    void block(User user);
    void unblock(User user);
    void addTemplateUsage(User user, Template template);
    void finishTemplateUsage(TemplateUsage templateUsage);
    void actualizeTemplateUsage(TemplateUsage templateUsage);
    void saveAsUnfinished(TemplateUsage templateUsage, UnfinishedUsage unfinishedUsage);
    void removeTemplateUsage(TemplateUsage templateUsage);
}
