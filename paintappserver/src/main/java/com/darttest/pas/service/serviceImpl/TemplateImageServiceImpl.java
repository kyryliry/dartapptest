package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.model.TemplateImage;
import com.darttest.pas.repository.TemplateImageRepository;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.service.TemplateImageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor

public abstract class TemplateImageServiceImpl<T extends TemplateImage> implements TemplateImageService {
    private final TemplateImageRepository<T, Long> templateImageRepository;
    private final AttachmentService attachmentService;

    @Override
    @Transactional
    public void persist(TemplateImage templateImage) {
        templateImageRepository.save(templateImage);
    }

    @Override
    public List<TemplateImage> findAll() {
        return templateImageRepository.findAll();
    }

    @Override
    public TemplateImage findById(Long id) {
        return templateImageRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void update(TemplateImage templateImage) {
        templateImageRepository.save(templateImage);
    }

    @Override
    @Transactional
    public void delete(TemplateImage templateImage) {
        attachmentService.delete(templateImage.getImageAtch());
        templateImageRepository.delete(templateImage);
    }
}
