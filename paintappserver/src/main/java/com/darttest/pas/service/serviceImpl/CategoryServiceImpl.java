package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.Category;
import com.darttest.pas.model.Template;
import com.darttest.pas.repository.CategoryRepository;
import com.darttest.pas.service.CategoryService;
import com.darttest.pas.service.TemplateService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final TemplateService templateService;

    @Transactional
    @Override
    public void persist(Category category) {
        if(!isExist(category.getName())) {
            categoryRepository.save(category);
        }else{
            throw new ExistsException("category with this name already exists");
        }
    }
    @Transactional
    @Override
    public void update(Category category) {
        categoryRepository.save(category);
        for(Template t: templateService.findTemplatesByCategoryId(category.getId())){
            Category old = t.getCategories().stream().filter(c->c.getId().equals(category.getId())).findFirst().orElse(null);
            if(old!=null) {
                t.getCategories().remove(old);
                t.getCategories().add(category);
                templateService.update(t);
            }

        }

    }

    @Transactional
    @Override
    public void delete(Category category) {
        for(Template t: templateService.findTemplatesByCategoryId(category.getId())){
            Category old = t.getCategories().stream().filter(c->c.getId().equals(category.getId())).findFirst().orElse(null);
            if(old!=null) {
                t.getCategories().remove(old);
                templateService.update(t);
            }
        }
        categoryRepository.delete(category);
    }
    @Transactional
    @Override
    public void deleteById(Long id) {
        delete(categoryRepository.findById(id).orElseThrow());
    }

    @Override
    public Category findById(Long id) {
        return categoryRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public Boolean isExist(String name) {
        return findByName(name) != null;
    }

    @Override
    public Page<Category> findAllPageable(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }
}
