package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.SampleImage;
import com.darttest.pas.repository.SampleImageRepository;
import com.darttest.pas.repository.TemplateImageRepository;
import com.darttest.pas.service.AttachmentService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;

@Service

public class SampleImageServiceImpl extends TemplateImageServiceImpl<SampleImage> {
    final private SampleImageRepository sampleImageRepository;
    private final AttachmentService attachmentService;

    public SampleImageServiceImpl(TemplateImageRepository<SampleImage, Long> templateImageRepository, AttachmentService attachmentService, SampleImageRepository sampleImageRepository) {
        super(templateImageRepository, attachmentService);
        this.sampleImageRepository = sampleImageRepository;
        this.attachmentService = attachmentService;
    }

    @Transactional
    public void deleteSampleImage(SampleImage sampleImage){
        attachmentService.delete(sampleImage.getImageAtch());
        String filePath = sampleImage.getImageAtch().getDownloadLink();
        File file = new File(filePath);
        String directoryPath = file.getParent();
        File directory = new File(directoryPath);
        boolean deleted = directory.delete();
        if(!deleted){
            throw new ExistsException("directory still contains some files");
        }
        sampleImageRepository.delete(sampleImage);
    }

}
