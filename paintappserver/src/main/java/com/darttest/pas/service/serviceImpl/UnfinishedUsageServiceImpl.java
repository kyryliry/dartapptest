package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.Attachment;
import com.darttest.pas.model.TemplateUsage;
import com.darttest.pas.model.UnfinishedUsage;
import com.darttest.pas.repository.UnfinishedUsageRepository;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.service.UnfinishedUsageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
@AllArgsConstructor
public class UnfinishedUsageServiceImpl implements UnfinishedUsageService {
    private final UnfinishedUsageRepository unfinishedUsageRepository;
    private final AttachmentService attachmentService;
    @Override
    @Transactional
    public void persist(UnfinishedUsage unfinishedUsage) {
        unfinishedUsageRepository.save(unfinishedUsage);
    }

    @Override
    @Transactional
    public void update(UnfinishedUsage unfinishedUsage) {
        unfinishedUsageRepository.save(unfinishedUsage);
    }

    @Override
    public UnfinishedUsage findById(Long id) {
        return unfinishedUsageRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void delete(UnfinishedUsage unfinishedUsage) {
        String filePath = unfinishedUsage.getActualLayerAtch().getDownloadLink();
        attachmentService.delete(unfinishedUsage.getActualLayerAtch());
        attachmentService.delete(unfinishedUsage.getFinishedLayerAtch());
        File file = new File(filePath);
        String directoryPath = file.getParent();
        File directory = new File(directoryPath);
        boolean deleted = directory.delete();
        if(!deleted){
            throw new ExistsException("directory still contains some files");
        }
        unfinishedUsageRepository.delete(unfinishedUsage);
    }

    private static String getOsDependentRegex() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            // Windows-based regex
            return "(\\\\unfinishedUsages\\\\)([^\\\\/]+)([\\\\/].*)";
        } else {
            // macOS/Linux-based regex
            return "(/unfinishedUsages/)([^/]+/)([^/]+_[^/]+)(/.*)";
        }
    }

    @Override
    @Transactional
    public void changeUnfinishedTemplateUsageName(UnfinishedUsage unfinishedUsage, String newName, String oldName) {
        Attachment actual = unfinishedUsage.getActualLayerAtch();
        if(actual!=null) {
            String oldPathL = actual.getDownloadLink();
            String patternL = getOsDependentRegex();
            String replacementL = "$1" + newName + "/$3$4";
            String modifiedPathL = oldPathL.replaceFirst(patternL, replacementL);
            actual.setDownloadLink(modifiedPathL);
            renameDirectory(oldPathL, oldName, newName);
            attachmentService.update(actual);
        }

        Attachment finish = unfinishedUsage.getFinishedLayerAtch();
        if(finish!=null) {
            String oldPathF = finish.getDownloadLink();
            String patternF = getOsDependentRegex();
            String replacementF = "$1" + newName + "/$3$4";
            String modifiedPathF = oldPathF.replaceFirst(patternF, replacementF);
            finish.setDownloadLink(modifiedPathF);
            if(actual==null) {
                renameDirectory(oldPathF, oldName, newName);
            }
            attachmentService.update(finish);
        }
    }

    public void renameDirectory(String originalPathString, String templateName, String newWholeName) {
        try {
            Path originalPath = Paths.get(originalPathString);
            Path parentPath = originalPath.getParent().getParent();
            Path deepestDir = parentPath.getFileName();

            if (deepestDir != null && deepestDir.toString().matches(templateName)) {
                Path newPath = parentPath.resolveSibling(newWholeName);
                Files.move(parentPath, newPath, StandardCopyOption.REPLACE_EXISTING);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
