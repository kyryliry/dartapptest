package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.Attachment;
import com.darttest.pas.repository.AttachmentRepository;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.utils.Constants;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

@Service
@AllArgsConstructor
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;

    @Override
    @Transactional
    public Attachment saveNewLayerAttachment(MultipartFile file, String templateName, Integer number, String directory) throws IOException {

        File dir = new File(directory+"/"+templateName);
        if(!dir.exists()){
            dir.mkdirs();
        }

        String filePath=dir+"/"+file.getOriginalFilename();
        if(Objects.equals(directory, Constants.GALLERY_DIRECTORY)){
            filePath = filePath.substring(0, filePath.length() - 4);
            filePath+=number.toString()+".png";
        }
//        else if(Objects.equals(directory, Constants.UPLOAD_DIRECTORY)){
//            filePath=dir+"/"+file.getOriginalFilename();
//        }

        Attachment attachment = attachmentRepository.save(Attachment.builder()
                .extension("png")
                .downloadLink(filePath).build());

        file.transferTo(new File(filePath));

        attachmentRepository.save(attachment);
        return attachment;
    }

    @Override
    public Attachment findById(Long id) {
        return attachmentRepository.findById(id).orElse(null);
    }

    public byte[] downloadImage(Long id) throws IOException {
        Attachment fileData = attachmentRepository.findById(id).orElse(null);
        assert fileData != null;
        String filePath=fileData.getDownloadLink();
        byte[] images = Files.readAllBytes(new File(filePath).toPath());
        return images;
    }

    @Override
    @Transactional
    public void delete(Attachment attachment){
        File file = new File(attachment.getDownloadLink());
        boolean deleted = file.delete();
        if(!deleted){
            throw new NotFoundException("file not found");
        }
        attachmentRepository.delete(attachment);
    }

    @Override
    @Transactional
    public void update(Attachment attachment){
        attachmentRepository.save(attachment);
    }
}
