package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.repository.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserCommentServiceImpl extends CommentServiceImpl<UserComment> {
    private final UserCommentRepository userCommentRepository;
    public UserCommentServiceImpl(CommentRepository<UserComment, Long> commentRepository, UserCommentRepository userCommentRepository) {
        super(commentRepository);
        this.userCommentRepository = userCommentRepository;
    }

    public List<UserComment> getNonParentedCommentsForTemplate(Template template){
        return userCommentRepository.findUserCommentBySubjectIdAndType(template.getId(), CommentType.TEMPLATE);
    }

    public List<UserComment> getNonParentedCommentsForImage(GalleryImage galleryImage){
        return userCommentRepository.findUserCommentBySubjectIdAndType(galleryImage.getId(), CommentType.IMAGE);
    }

    @Transactional
    public void createReply(UserComment userComment, Long parentId){
        UserComment parent = (UserComment) findById(parentId);
        assert parent!=null;
        userComment.setSubjectId(parent.getId());
        userComment.setType(CommentType.COMMENT);
        persist(userComment);
        parent.getReplies().add(userComment);
        update(parent);
    }

    public List<UserComment> findAllByAuthor(User author){
        return userCommentRepository.findAllByAuthor(author);
    }

    public UserComment deleteComment(UserComment comment){
        comment.setAuthor(null);
        comment.setText("Comment has been deleted.");
        update(comment);
        return comment;
    }
    @Transactional
    public Template deleteTemplateCommentWithReplies(UserComment comment, Template template){
        if(!comment.getReplies().isEmpty()){
            List<UserComment> commentsToDelete = new ArrayList<>(comment.getReplies());
            comment.getReplies().clear();
            while(!commentsToDelete.isEmpty()){
                UserComment c = commentsToDelete.get(commentsToDelete.size()-1);
                deleteTemplateCommentWithReplies(c, template);
                commentsToDelete.remove(c);
            }
        }
        template.getComments().remove(comment);
        delete(comment);
        return template;
    }

    @Transactional
    public UserComment addLike(User user, UserComment comment) {
        if(comment.getLikes().contains(user)){
            throw new ExistsException("user has already liked this comment");
        }
        comment.getLikes().add(user);
        update(comment);
        return comment;
    }

    @Transactional
    public UserComment removeLike(User user, UserComment comment) {
        if(!comment.getLikes().contains(user)){
            throw new ExistsException("comment does not contain looked like");
        }
        comment.getLikes().remove(user);
        update(comment);
        return comment;
    }

    @Transactional
    public UserComment addDislike(User user, UserComment comment) {
        if(comment.getDislikes().contains(user)){
            throw new ExistsException("user has already disliked this template");
        }
        comment.getDislikes().add(user);
        update(comment);
        return comment;
    }

    @Transactional
    public UserComment removeDislike(User user, UserComment comment) {
        if(!comment.getDislikes().contains(user)){
            throw new ExistsException("template does not contain looked dislike");
        }
        comment.getDislikes().remove(user);
        update(comment);
        return comment;
    }

    public GalleryImage deleteImageCommentWithReplies(UserComment comment, GalleryImage galleryImage){
        if(!comment.getReplies().isEmpty()){
            List<UserComment> commentsToDelete = new ArrayList<>(comment.getReplies());
            while(!commentsToDelete.isEmpty()){
                UserComment c = commentsToDelete.get(commentsToDelete.size()-1);
                deleteImageCommentWithReplies(c, galleryImage);
                commentsToDelete.remove(c);
            }
        }
        galleryImage.getComments().remove(comment);
        delete(comment);
        return galleryImage;
    }
}
