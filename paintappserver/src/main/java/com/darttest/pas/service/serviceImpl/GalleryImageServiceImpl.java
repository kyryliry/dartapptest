package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.*;
import com.darttest.pas.repository.GalleryImageRepository;
import com.darttest.pas.repository.TemplateImageRepository;
import com.darttest.pas.service.AttachmentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GalleryImageServiceImpl extends TemplateImageServiceImpl<GalleryImage>{
    private final GalleryImageRepository galleryImageRepository;
    private final UserCommentServiceImpl userCommentService;
    public GalleryImageServiceImpl(TemplateImageRepository<GalleryImage, Long> templateImageRepository, AttachmentService attachmentService, GalleryImageRepository galleryImageRepository, UserCommentServiceImpl userCommentService) {
        super(templateImageRepository, attachmentService);
        this.galleryImageRepository = galleryImageRepository;
        this.userCommentService = userCommentService;
    }

    public List<GalleryImage> findGalleryImagesByUsage(TemplateUsage usage){
        return galleryImageRepository.findGalleryImagesByUsage(usage);
    }

    public GalleryImage addLike(User user, GalleryImage image){
        if(image.getLikes().contains(user)){
            throw new ExistsException("user has already liked this image");
        }
        image.getLikes().add(user);
        update(image);
        return image;
    }

    public GalleryImage removeLike(User user, GalleryImage image){
        if(!image.getLikes().contains(user)){
            throw new ExistsException("image does not contain looked like");
        }
        image.getLikes().remove(user);
        update(image);
        return image;
    }

    public GalleryImage addDislike(User user, GalleryImage image){
        if(image.getDislikes().contains(user)){
            throw new ExistsException("user has already disliked this image");
        }
        image.getDislikes().add(user);
        update(image);
        return image;
    }

    public GalleryImage removeDislike(User user, GalleryImage image){
        if(!image.getDislikes().contains(user)){
            throw new ExistsException("image does not contain looked dislike");
        }
        image.getDislikes().remove(user);
        update(image);
        return image;
    }

    public void addComment(UserComment comment, GalleryImage galleryImage){
        userCommentService.persist(comment);
        galleryImage.getComments().add(comment);
        update(galleryImage);
    }

    public void addReply(UserComment comment, Long parentId, GalleryImage galleryImage){
        userCommentService.createReply(comment, parentId);
        galleryImage.getComments().add(comment);
        update(galleryImage);
    }

    public GalleryImage removeComment(UserComment comment, GalleryImage galleryImage){
        userCommentService.deleteComment(comment);
        return galleryImage;
    }

    public List<GalleryImage> findByLikes_Id(Long userId){
        return galleryImageRepository.findByLikes_Id(userId);
    }

    public List<GalleryImage> findByDislikes_Id(Long userId){
        return galleryImageRepository.findByDislikes_Id(userId);
    }
}
