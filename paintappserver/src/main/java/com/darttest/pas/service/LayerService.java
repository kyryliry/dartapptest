package com.darttest.pas.service;

import com.darttest.pas.model.Layer;
import com.darttest.pas.model.Template;

import java.util.List;

public interface LayerService {
    void persist(Layer layer);

    Layer findById(Long id);

    List<Layer> findAllByTemplateId(Long id);
    void delete(Layer layer);


}
