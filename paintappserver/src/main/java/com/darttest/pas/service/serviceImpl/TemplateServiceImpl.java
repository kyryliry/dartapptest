package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.model.enums.TemplateState;
import com.darttest.pas.repository.TemplateRepository;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.service.LayerService;
import com.darttest.pas.service.TemplateService;
import com.darttest.pas.service.TemplateUsageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.*;

@Service
@AllArgsConstructor
public class TemplateServiceImpl implements TemplateService {

    private final TemplateRepository templateRepository;
    private final GalleryImageServiceImpl galleryImageService;
    private final TemplateUsageService templateUsageService;
    private final SampleImageServiceImpl sampleImageService;
    private final LayerService layerService;
    private final ReportServiceImpl reportService;
    private final UserCommentServiceImpl userCommentService;
    private final AttachmentService attachmentService;
    @Override
    @Transactional
    public void persist(Template template) {
        template.getCreator().getOwnedTemplates().add(template);
        templateRepository.save(template);
    }

    @Override
    public Template findTemplateById(Long id) {
        return templateRepository.findById(id).orElse(null);
    }

    @Override
    public Template findByName(String name) {
        Template t = templateRepository.findByTemplateName(name);
        return templateRepository.findByTemplateName(name);
    }

    @Override
    @Transactional
    public void update(Template template) {
        templateRepository.save(template);
    }

    @Transactional
    @Override
    public void delete(Template template) {

        List<Layer> layers = template.getLayers();
        String filePath = "";
        if(!layers.isEmpty()){
            filePath = layers.get(0).getLayerImage().getDownloadLink();
        }
        while(!layers.isEmpty()){
            Layer t = layers.get(layers.size()-1);
            layerService.delete(t);
            layers.remove(t);
        }

        List<Report> reports = reportService.findReportsBySubjectIdAndType(template.getId(), CommentType.TEMPLATE);
        for(Report r: reports){
            reportService.closeReportBySystem(r);
        }

        List<UserComment> comments = new ArrayList<>(template.getComments().stream().filter(d -> d.getType() != CommentType.COMMENT && Objects.equals(d.getSubjectId(), template.getId())).toList());
        while(!comments.isEmpty()){
            UserComment t = comments.get(comments.size()-1);
            userCommentService.deleteTemplateCommentWithReplies(t, template);
            comments.remove(t);
        }

        template.getLikes().clear();
        template.getDislikes().clear();
        update(template);

        List<TemplateUsage> usages = templateUsageService.findTemplateUsagesByParent(template);
        while(!usages.isEmpty()){
            TemplateUsage t = usages.get(usages.size()-1);
            removeGalleryImagesByUsage(t);
            templateUsageService.delete(t);
            usages.remove(t);
        }
        if(template.getSampleImage()!=null) {
            sampleImageService.deleteSampleImage(template.getSampleImage());
        }

        if(!Objects.equals(filePath, "")) {
            File file = new File(filePath);
            String directoryPath = file.getParent();
            File directory = new File(directoryPath);
            boolean deleted = directory.delete();
            if (!deleted) {
                throw new ExistsException("directory still contains some files");
            }
        }
        templateRepository.delete(template);
    }

    @Override
    public List<Template> findAll(){
        return templateRepository.findAll();
    }

    @Override
    public Map<String, List<Template>> findAllSortedByTemplateState(User user){
        List<Template> allTemplateSortedByState = user.getOwnedTemplates();
        List<Template> publishedTemplates = new ArrayList<>();
        List<Template> draftTemplates = new ArrayList<>();
        List<Template> verifiedTemplates = new ArrayList<>();
        List<Template> blockedTemplates = new ArrayList<>();
        for(Template t: allTemplateSortedByState){
            if(t.getState()== TemplateState.PUBLISHED){
                publishedTemplates.add(t);
            }else if(t.getState()== TemplateState.DRAFT){
                draftTemplates.add(t);
            }else if(t.getState()== TemplateState.VERIFIED){
                verifiedTemplates.add(t);
            }else if(t.getState()== TemplateState.BLOCKED){
                blockedTemplates.add(t);
            }
        }
        Map<String, List<Template>> temps = new HashMap<>();
        publishedTemplates.sort(Comparator.comparing(Template::getUploadDate).reversed());
        draftTemplates.sort(Comparator.comparing(Template::getUploadDate).reversed());
        verifiedTemplates.sort(Comparator.comparing(Template::getUploadDate).reversed());
        blockedTemplates.sort(Comparator.comparing(Template::getUploadDate).reversed());
        temps.put("published", publishedTemplates);
        temps.put("draft", draftTemplates);
        temps.put("verified", verifiedTemplates);
        temps.put("blocked", blockedTemplates);
        return temps;
    }

    @Override
    public List<Template> findFollowingReleaseForWeek(User user) {
        List<Template> release = new ArrayList<>();
        Set<User> following = user.getFollowing();
        for(User u: following){
            for(Template t: u.getOwnedTemplates()){
                if(t.getUploadDate().isAfter(LocalDateTime.now().minusDays(7))){
                    release.add(t);
                }
            }
        }
        release.sort(Comparator.comparing(Template::getUploadDate).reversed());
        return release;
    }

    @Override
    public List<Template> findTemplatesByCategory(Category category) {
        List<Template> templates = new ArrayList<>();
        for(Template t: templateRepository.findAll()){
            if(t.getCategories().contains(category)) {
                templates.add(t);
            }
        }
        return templates;
    }

    @Override
    public List<Template> findTemplatesByCategoryId(Long id) {
        List<Template> templates = new ArrayList<>();
        for(Template t: templateRepository.findAll()){
            if(t.getCategories().stream().map(Category::getId).toList().contains(id)) {
                templates.add(t);
            }
        }
        return templates;
    }

    @Override
    @Transactional
    public void addCategory(Template template, Category category) {
        template.getCategories().add(category);
        templateRepository.save(template);
    }

    @Override
    @Transactional
    public void removeCategory(Template template, Category category) {
        if(template.getCategories().contains(category)){
            template.getCategories().remove(category);
            templateRepository.save(template);
        }
    }

    @Override
    @Transactional
    public void removeGalleryImagesByUsage(TemplateUsage templateUsage) {
        String filePath = "";
        if(!templateUsage.getParent().getImages().isEmpty()) {
            filePath = templateUsage.getParent().getImages().get(0).getImageAtch().getDownloadLink();
        }
        List<GalleryImage> images = galleryImageService.findGalleryImagesByUsage(templateUsage);
        templateUsage.getParent().getImages().removeAll(images);
        templateUsage.getParent().setDownloadsCount(templateUsage.getParent().getDownloadsCount()-1);
        update(templateUsage.getParent());

        for(GalleryImage i: images){
            List<UserComment> comments = new ArrayList<>(i.getComments().stream().filter(d -> d.getType() != CommentType.COMMENT && Objects.equals(d.getSubjectId(), i.getId())).toList());
            while(!comments.isEmpty()){
                UserComment t = comments.get(comments.size()-1);
                userCommentService.deleteImageCommentWithReplies(t, i);
                comments.remove(t);
            }

            i.getLikes().clear();
            i.getDislikes().clear();
            galleryImageService.update(i);
            List<Report> reports = reportService.findReportsBySubjectIdAndType(i.getId(), CommentType.IMAGE);
            for(Report r: reports){
                reportService.closeReportBySystem(r);
            }
            galleryImageService.delete(i);
        }
        if(templateUsage.getParent().getImages().isEmpty() && !Objects.equals(filePath, "")) {
            File file = new File(filePath);
            String directoryPath = file.getParent();
            File directory = new File(directoryPath);
            boolean deleted = directory.delete();
            if (!deleted) {
                throw new ExistsException("directory still contains some files");
            }
        }
    }

    @Override
    @Transactional
    public void removeGalleryImage(GalleryImage galleryImage) {
        String filePath = galleryImage.getImageAtch().getDownloadLink();
        Template template = galleryImage.getUsage().getParent();
        template.getImages().remove(galleryImage);
        update(template);
        List<Report> reports = reportService.findReportsBySubjectIdAndType(galleryImage.getId(), CommentType.IMAGE);
        for(Report r: reports){
            reportService.closeReportBySystem(r);
        }
        List<UserComment> comments = new ArrayList<>(galleryImage.getComments().stream().filter(d -> d.getType() != CommentType.COMMENT && Objects.equals(d.getSubjectId(), galleryImage.getId())).toList());
        while(!comments.isEmpty()){
            UserComment t = comments.get(comments.size()-1);
            userCommentService.deleteImageCommentWithReplies(t, galleryImage);
            comments.remove(t);
        }
        galleryImage.getLikes().clear();
        galleryImage.getDislikes().clear();
        galleryImageService.update(galleryImage);
        galleryImageService.delete(galleryImage);
        if(template.getImages().isEmpty()) {
            File file = new File(filePath);
            String directoryPath = file.getParent();
            File directory = new File(directoryPath);
            boolean deleted = directory.delete();
            if (!deleted) {
                throw new ExistsException("directory still contains some files");
            }
        }
    }

    @Override
    public Set<Template> findAllByCategories(Set<Category> categories){
        return templateRepository.findAllByCategories(categories);
    }

    @Override
    public Set<Template> findAllBySearchText(String searchText) {
        List<Template> templates = findAll();
        String[] words = searchText.split(" ");
        Set<Template> sortedTemps = new HashSet<>();
        for(Template t: templates){
            String[] tWords = t.getTemplateName().split(" ");
            Set<String> tWordsSet = new HashSet<>(Arrays.asList(tWords));
            for (String word : words) {
                if (tWordsSet.contains(word)) {
                    sortedTemps.add(t);
                    break;
                }
            }
        }

        return sortedTemps;
    }

    @Override
    @Transactional
    public Template addLike(User user, Template template) {
        if(template.getLikes().contains(user)){
            throw new ExistsException("user has already liked this template");
        }
        template.getLikes().add(user);
        update(template);
        return template;
    }

    @Override
    @Transactional
    public Template removeLike(User user, Template template) {
        if(!template.getLikes().contains(user)){
            throw new ExistsException("template does not contain looked like");
        }
        template.getLikes().remove(user);
        update(template);
        return template;
    }

    @Override
    @Transactional
    public Template addDislike(User user, Template template) {
        if(template.getDislikes().contains(user)){
            throw new ExistsException("user has already disliked this template");
        }
        template.getDislikes().add(user);
        update(template);
        return template;
    }

    @Override
    @Transactional
    public Template removeDislike(User user, Template template) {
        if(!template.getDislikes().contains(user)){
            throw new ExistsException("template does not contain looked dislike");
        }
        template.getDislikes().remove(user);
        update(template);
        return template;
    }

    @Override
    public void addComment(UserComment comment, Template template){
        userCommentService.persist(comment);
        template.getComments().add(comment);
        update(template);
    }

    @Override
    public void addReply(UserComment comment, Long parentId, Template template){
        userCommentService.createReply(comment, parentId);
        template.getComments().add(comment);
        update(template);
    }
    @Override
    public Template removeComment(UserComment comment, Template template){
        userCommentService.deleteComment(comment);
        return template;
    }

    @Override
    public Template removeCommentWithReplies(UserComment comment, Template template){
        Template res = userCommentService.deleteTemplateCommentWithReplies(comment, template);
        update(res);
        return res;
    }

    @Override
    public List<Template> findByLikes_Id(Long userId) {
        return templateRepository.findByLikes_Id(userId);
    }

    @Override
    public List<Template> findByDislikes_Id(Long userId) {
        return templateRepository.findByDislikes_Id(userId);
    }

    @Override
    @Transactional
    public void changeTemplateName(Template template, String newName){
        String oldName = template.getTemplateName();
        template.setTemplateName(newName);
        Attachment a = template.getSampleImage().getImageAtch();
        String oldPath = a.getDownloadLink();
        String pattern = getOsDependentRegex("templatesSample");

        String replacement = "$1" + newName + "$3";
        String modifiedPath = oldPath.replaceFirst(pattern, replacement);
        a.setDownloadLink(modifiedPath);
        renameDirectory(oldPath, oldName, newName);
        attachmentService.update(a);

        int i=0;
        for(Layer l: template.getLayers()){
            Attachment lay = l.getLayerImage();
            String oldPathL = lay.getDownloadLink();
            String patternL = getOsDependentRegex("uploads");
            String replacementL = "$1"+ newName+"$3";
            String modifiedPathL = oldPathL.replaceFirst(patternL, replacementL);
            lay.setDownloadLink(modifiedPathL);
            if(i==0) {
                renameDirectory(oldPathL, oldName, newName);
            }
            attachmentService.update(lay);
            i++;
        }
        i=0;
        for(GalleryImage l: template.getImages()){
            Attachment lay = l.getImageAtch();
            String oldPathL = lay.getDownloadLink();
            String patternL = getOsDependentRegex("galleryImages");
            String replacementL = "$1"+ newName+"$3";
            String modifiedPathL = oldPathL.replaceFirst(patternL, replacementL);
            lay.setDownloadLink(modifiedPathL);
            if(i==0) {
                renameDirectory(oldPathL, oldName, newName);
            }
            attachmentService.update(lay);
            i++;
        }

        List<TemplateUsage> usages = templateUsageService.findTemplateUsagesByParent(template);
        for(TemplateUsage t: usages){
            if(t.getUnfinishedDetails()!=null){
                Attachment actual = t.getUnfinishedDetails().getActualLayerAtch();
                if(actual!=null) {
                    String oldPathL = actual.getDownloadLink();
                    String patternL = getOsDependentRegex("unfinishedUsages");
                    String replacementL = "$1$2" + newName + "$4";
                    String modifiedPathL = oldPathL.replaceFirst(patternL, replacementL);
                    actual.setDownloadLink(modifiedPathL);
                    renameDirectory(oldPathL, oldName, newName);
                    attachmentService.update(actual);
                }

                Attachment finish = t.getUnfinishedDetails().getFinishedLayerAtch();
                if(finish!=null) {
                    String oldPathF = finish.getDownloadLink();
                    String patternF = getOsDependentRegex("unfinishedUsages");
                    String replacementF = "$1$2" + newName + "$4";
                    String modifiedPathF = oldPathF.replaceFirst(patternF, replacementF);
                    finish.setDownloadLink(modifiedPathF);
                    if(actual==null) {
                        renameDirectory(oldPathF, oldName, newName);
                    }
                    attachmentService.update(finish);
                }
            }
        }

        update(template);
    }

    private static String getOsDependentRegex(String type) {
        String os = System.getProperty("os.name").toLowerCase();
        switch (type) {
            case "templatesSample" -> {
                if (os.contains("win")) {
                    // Windows-based regex
                    return "(\\\\templatesSample\\\\)([^\\\\/]+_[^\\\\/]+)([\\\\/].*)";
                } else {
                    // macOS/Linux-based regex
                    return "(/templatesSample/)([^/]+_[^/]+)(/.*)";
                }
            }
            case "uploads" -> {
                if (os.contains("win")) {
                    // Windows-based regex
                    return "(\\\\uploads\\\\)([^\\\\/]+_[^\\\\/]+)([\\\\/].*)";
                } else {
                    // macOS/Linux-based regex
                    return "(/uploads/)([^/]+_[^/]+)(/.*)";
                }
            }
            case "galleryImages" -> {
                if (os.contains("win")) {
                    // Windows-based regex
                    return "(\\\\galleryImages\\\\)([^\\\\/]+_[^\\\\/]+)([\\\\/].*)";
                } else {
                    // macOS/Linux-based regex
                    return "(/galleryImages/)([^/]+_[^/]+)(/.*)";
                }
            }
            case "unfinishedUsages" -> {
                if (os.contains("win")) {
                    // Windows-based regex
                    return "(\\\\unfinishedUsages\\\\)([^\\\\/]+)([\\\\/].*)";
                } else {
                    // macOS/Linux-based regex
                    return "(/unfinishedUsages/)([^/]+/)([^/]+_[^/]+)(/.*)";
                }
            }
        }
        return "";
    }

    public void renameDirectory(String originalPathString, String templateName, String newWholeName) {
        try {
            Path originalPath = Paths.get(originalPathString);
            Path parentPath = originalPath.getParent();
            Path deepestDir = parentPath.getFileName();

            if (deepestDir != null && deepestDir.toString().matches(templateName)) {
                Path newPath = parentPath.resolveSibling(newWholeName);
                Files.move(parentPath, newPath, StandardCopyOption.REPLACE_EXISTING);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
