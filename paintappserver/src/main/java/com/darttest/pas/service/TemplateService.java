package com.darttest.pas.service;

import com.darttest.pas.model.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface TemplateService {
    void persist(Template template);

    Template findTemplateById(Long id);

    Template findByName(String name);

    void update(Template template);

    void delete(Template template);

    List<Template> findAll();

    List<Template> findTemplatesByCategory(Category category);

    List<Template> findTemplatesByCategoryId(Long id);

    void addCategory(Template template, Category category);

    void removeCategory(Template template, Category category);

    void removeGalleryImagesByUsage(TemplateUsage templateUsage);

    void removeGalleryImage(GalleryImage galleryImage);

    Map<String, List<Template>> findAllSortedByTemplateState(User user);
    List<Template> findFollowingReleaseForWeek(User user);

    Set<Template> findAllByCategories(Set<Category> categories);

    Set<Template> findAllBySearchText(String searchText);

    Template addLike(User user, Template template);
    Template removeLike(User user, Template template);
    Template addDislike(User user, Template template);
    Template removeDislike(User user, Template template);
    void addComment(UserComment comment, Template template);


    void addReply(UserComment comment, Long parentId, Template template);

    Template removeComment(UserComment comment, Template template);

    Template removeCommentWithReplies(UserComment comment, Template template);

    List<Template> findByLikes_Id(Long userId);

    List<Template> findByDislikes_Id(Long userId);

    void changeTemplateName(Template template, String newName);
}
