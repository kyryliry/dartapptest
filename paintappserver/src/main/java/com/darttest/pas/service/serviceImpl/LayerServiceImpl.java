package com.darttest.pas.service.serviceImpl;

import com.darttest.pas.model.Layer;
import com.darttest.pas.repository.LayerRepository;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.service.LayerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class LayerServiceImpl implements LayerService {

    private final LayerRepository layerRepository;
    private final AttachmentService attachmentService;

    @Override
    @Transactional
    public void persist(Layer layer) {
        layerRepository.save(layer);
    }

    @Override
    public Layer findById(Long id) {
        return layerRepository.findById(id).orElse(null);
    }

    @Override
    public List<Layer> findAllByTemplateId(Long id) {
        return layerRepository.findByTemplateId(id);
    }

    @Override
    @Transactional
    public void delete(Layer layer) {
        attachmentService.delete(layer.getLayerImage());
        layerRepository.delete(layer);
    }
}
