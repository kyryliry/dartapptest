package com.darttest.pas.controller;

import com.darttest.pas.dto.TemplateUsageDto;
import com.darttest.pas.dto.UnfinishedUsageDto;
import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.UsageState;
import com.darttest.pas.service.*;
import com.darttest.pas.utils.Constants;
import com.darttest.pas.utils.TokenUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@AllArgsConstructor
public class TemplateUsageController {
    private static final Logger LOG = LoggerFactory.getLogger(TemplateUsageController.class);
    @Autowired
    private final TemplateUsageService templateUsageService;
    private final TemplateService templateService;
    private final UserService userService;
    private final AttachmentService attachmentService;
    private final UnfinishedUsageService unfinishedUsageService;
    private final LayerService layerService;

    @GetMapping(value = "api/users/usage/checkExist/{playerId}/{parentId}")
    public ResponseEntity<?> getUsageByParentAndPlayer(@PathVariable Long playerId, @PathVariable Long parentId){
        try{
            User user = userService.findById(playerId);
            Template template = templateService.findTemplateById(parentId);
            TemplateUsage t = templateUsageService.findTemplateUsageByPlayerAndParent(user, template);
            if(t==null){
                return null;
            }
            String newToken = TokenUtil.regenerateToken();
            TemplateUsageDto d = TemplateUsageDto.builder().id(t.getId()).playerId(user.getId())
                    .actualScore(t.getActualScore())
                    .dateOfLastPlay(t.getDateOfLastPlay())
                    .state(t.getState())
                    .parentId(t.getParent().getId())
                    .unfinishedUsageId(t.getUnfinishedDetails()!=null?t.getUnfinishedDetails().getId():null)
                    .newToken(newToken)
                    .build();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(d);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/users/usage/unfinished/{id}")
    public ResponseEntity<?> getUnfinishedUsageById(@PathVariable Long id){
        try{
            UnfinishedUsage t = unfinishedUsageService.findById(id);
            byte[] actualLayerAtch = attachmentService.downloadImage(t.getActualLayerAtch().getId());
            String actualLayerString = Base64Utils.encodeToString(actualLayerAtch);
            byte[] finishedLayerAtch = attachmentService.downloadImage(t.getFinishedLayerAtch().getId());
            String finishedLayerString = Base64Utils.encodeToString(finishedLayerAtch);
            Map<String, Object> response = new HashMap<>();
            response.put("id", t.getId());
            response.put("usageId", t.getUsage().getId());
            response.put("actualScore", t.getUsage().getActualScore());
            response.put("actualLayerAtch", actualLayerString);
            response.put("finishedLayerAtch", finishedLayerString);
            String newToken = TokenUtil.regenerateToken();
            if(t.getActualLayer()!=null) {
                response.put("actualLayerId", t.getActualLayer().getId());
            }
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred");
        }
    }

    @PostMapping(value = "api/users/usage/new")
    public ResponseEntity<?> addNewTemplateUsage(@RequestBody TemplateUsageDto templateUsageDto){
        try{
            User user = userService.findById(templateUsageDto.getPlayerId());
            Template template = templateService.findTemplateById(templateUsageDto.getParentId());
            userService.addTemplateUsage(user, template);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("TemplateUsage successfully created");
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (NullPointerException | ExistsException e){
            LOG.info("TemplateUsage creating caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PutMapping(value = "api/users/usage/finish")
    public ResponseEntity<?> finishTemplateUsage(@RequestBody TemplateUsageDto templateUsageDto){
        try{
            TemplateUsage templateUsage = templateUsageService.findById(templateUsageDto.getId());
            if(templateUsageDto.getActualScore()!=null && templateUsage.getState()!= UsageState.UNFINISHED) {
                User player = templateUsage.getPlayer();
                if((templateUsage.getActualScore()!=null && templateUsage.getActualScore()< templateUsageDto.getActualScore())){
                    double currentScore = templateUsage.getActualScore();
                    templateUsage.setActualScore(templateUsageDto.getActualScore());
                    player.setGlobalPlayerRating(player.getGlobalPlayerRating()-currentScore);
                    player.setGlobalPlayerRating(player.getGlobalPlayerRating()+templateUsageDto.getActualScore());
                }else if(templateUsage.getActualScore()==null){
                    templateUsage.setActualScore(templateUsageDto.getActualScore());
                    player.setGlobalPlayerRating(player.getGlobalPlayerRating()+templateUsageDto.getActualScore());
                }
                userService.update(player);
            }else if(templateUsageDto.getActualScore()!=null && templateUsage.getState()== UsageState.UNFINISHED){
                templateUsage.setActualScore(templateUsageDto.getActualScore());
            }
            userService.finishTemplateUsage(templateUsage);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("TemplateUsage finishing caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PutMapping(value = "api/users/usage/actualize")
    public ResponseEntity<?> actualizeTemplateUsage(@RequestBody TemplateUsageDto templateUsageDto){
        try{
            TemplateUsage templateUsage = templateUsageService.findById(templateUsageDto.getId());
            if(templateUsageDto.getActualScore()!=null) {
                if((templateUsage.getActualScore()!=null && templateUsage.getActualScore()< templateUsageDto.getActualScore())){
                    templateUsage.setActualScore(templateUsageDto.getActualScore());
                }else if(templateUsage.getActualScore()==null){
                    templateUsage.setActualScore(templateUsageDto.getActualScore());
                }
            }
            userService.actualizeTemplateUsage(templateUsage);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("TemplateUsage actualizing caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PutMapping(value = "api/users/usage/saveUnfinished")
    public ResponseEntity<?> saveUnfinishedTemplateUsage(@ModelAttribute UnfinishedUsageDto unfinishedUsageDto){
        try{
            TemplateUsage templateUsage = templateUsageService.findById(unfinishedUsageDto.getUsageId());
            User user = templateUsage.getPlayer();
            if(unfinishedUsageDto.getActualScore()!=null) {
                templateUsage.setActualScore(unfinishedUsageDto.getActualScore());
            }
            Attachment finishAtch = attachmentService.saveNewLayerAttachment(unfinishedUsageDto.getFinishedLayerAtch(), templateUsage.getParent().getTemplateName(), 0, Constants.UNF_USAGE_DIRECTORY+"/"+user.getUsername());
            Attachment actualAtch = attachmentService.saveNewLayerAttachment(unfinishedUsageDto.getActualLayerAtch(), templateUsage.getParent().getTemplateName(), 0, Constants.UNF_USAGE_DIRECTORY+"/"+user.getUsername());
            UnfinishedUsage unfinishedUsage = UnfinishedUsage.builder()
                    .usage(templateUsage)
                    .actualLayer(unfinishedUsageDto.getActualLayerId()!=null?layerService.findById(unfinishedUsageDto.getActualLayerId()):null)
                    .actualLayerAtch(actualAtch)
                    .finishedLayerAtch(finishAtch).build();
            if(templateUsage.getUnfinishedDetails()!=null && templateUsage.getUnfinishedDetails().getActualLayerAtch()!=null && templateUsage.getUnfinishedDetails().getFinishedLayerAtch()!=null && (!templateUsage.getUnfinishedDetails().getActualLayerAtch().getDownloadLink().equals(
                    unfinishedUsage.getActualLayerAtch().getDownloadLink()) ||

                    !templateUsage.getUnfinishedDetails().getFinishedLayerAtch().getDownloadLink().equals(
                            unfinishedUsage.getFinishedLayerAtch().getDownloadLink()))) {
                UnfinishedUsage u = templateUsage.getUnfinishedDetails();
                Attachment oldA = u.getActualLayerAtch();
                Attachment oldF = u.getFinishedLayerAtch();
                u.setActualLayerAtch(null);
                u.setFinishedLayerAtch(null);
                attachmentService.delete(oldA);
                attachmentService.delete(oldF);
                unfinishedUsageService.update(u);
            }
            userService.saveAsUnfinished(templateUsage, unfinishedUsage);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("UnfinishedUsage {} successfully created", unfinishedUsage.getId());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (NullPointerException | ExistsException | IOException e){
            LOG.info("UnfinishedUsage creating caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value="api/users/usage/remove/{id}")
    public ResponseEntity<?> removeTemplateUsage(@PathVariable Long id){
        try{
            TemplateUsage templateUsage = templateUsageService.findById(id);
            userService.removeTemplateUsage(templateUsage);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("TemplateUsage {} successfully deleted", id);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (NullPointerException | ExistsException e){
            LOG.info("TemplateUsage {} deleting caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
