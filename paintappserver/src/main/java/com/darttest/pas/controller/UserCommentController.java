package com.darttest.pas.controller;

import com.darttest.pas.dto.UserCommentDto;
import com.darttest.pas.exceptions.AccountException;
import com.darttest.pas.model.GalleryImage;
import com.darttest.pas.model.Template;
import com.darttest.pas.model.User;
import com.darttest.pas.model.UserComment;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.model.enums.UserRole;
import com.darttest.pas.security.model.AuthenticationToken;
import com.darttest.pas.service.TemplateService;
import com.darttest.pas.service.UserService;
import com.darttest.pas.service.serviceImpl.GalleryImageServiceImpl;
import com.darttest.pas.service.serviceImpl.UserCommentServiceImpl;
import com.darttest.pas.utils.TokenUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@AllArgsConstructor
public class UserCommentController {
    private static final Logger LOG = LoggerFactory.getLogger(UserCommentController.class);
    @Autowired
    private final UserCommentServiceImpl userCommentService;
    private final UserService userService;
    private final TemplateService templateService;
    private final GalleryImageServiceImpl galleryImageService;

    @PostMapping(value = "api/{id}/comments/new")
    private ResponseEntity<?> addComment(Principal principal, @RequestBody UserCommentDto dto, @PathVariable Long id){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User author = userService.findById(userId);
            UserComment comment = new UserComment(
                    dto.getText(),
                    LocalDateTime.now(),
                    id,
                    dto.getType(),
                    author,
                    new ArrayList<>(),
                    new ArrayList<>(),
                    new ArrayList<>()
            );
            if(dto.getType()==CommentType.TEMPLATE) {
                Template template = templateService.findTemplateById(id);
                templateService.addComment(comment, template);
            }else if(dto.getType()==CommentType.IMAGE){
                GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
                galleryImageService.addComment(comment, galleryImage);
            }
            Map<String, Object> commentDetails = new HashMap<>();
            commentDetails.put("id", comment.getId());
            commentDetails.put("text", comment.getText());
            commentDetails.put("authorId", comment.getAuthor().getId());
            commentDetails.put("authorUsername", comment.getAuthor().getUsername());
            commentDetails.put("date", comment.getDate());
            List<Long> repliesIds = new ArrayList<>(comment.getReplies().stream().map(UserComment::getId).toList());
            commentDetails.put("repliesIds", repliesIds);
            commentDetails.put("subjectId", comment.getSubjectId());
            commentDetails.put("type", comment.getType());
            List<Long> likesIds = new ArrayList<>(comment.getLikes().stream().map(User::getId).toList());
            commentDetails.put("likes", likesIds);
            List<Long> dislikesIds = new ArrayList<>(comment.getDislikes().stream().map(User::getId).toList());
            commentDetails.put("dislikes", dislikesIds);
            String newToken = TokenUtil.regenerateToken();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(commentDetails);
        }catch (Exception e){
            LOG.info("UserComment creating caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/comments/{id}/replies")
    public ResponseEntity<?> getAllRepliesToComment(@PathVariable Long id){
        try {
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            List<Map<String, Object>> commentDetailsList = new ArrayList<>();
            UserComment comment = (UserComment) userCommentService.findById(id);
            if (!comment.getReplies().isEmpty()) {
                comment.getReplies().sort(Comparator.comparing(UserComment::getDate));
                for (UserComment c : comment.getReplies()) {
                    Map<String, Object> commentDetails = new HashMap<>();
                    commentDetails.put("id", c.getId());
                    commentDetails.put("text", c.getText());
                    commentDetails.put("authorId", c.getAuthor()!=null ? c.getAuthor().getId() : null);
                    commentDetails.put("authorUsername", c.getAuthor()!=null ? c.getAuthor().getUsername() : null);
                    commentDetails.put("date", c.getDate());
                    List<Long> repliesIds = new ArrayList<>(c.getReplies().stream().map(UserComment::getId).toList());
                    commentDetails.put("repliesIds", repliesIds);
                    commentDetails.put("subjectId", c.getSubjectId());
                    commentDetails.put("type", c.getType());
                    List<Long> likesIds = new ArrayList<>(c.getLikes().stream().map(User::getId).toList());
                    commentDetails.put("likes", likesIds);
                    List<Long> dislikesIds = new ArrayList<>(c.getDislikes().stream().map(User::getId).toList());
                    commentDetails.put("dislikes", dislikesIds);
                    commentDetailsList.add(commentDetails);
                }
            }
            response.put("comments", commentDetailsList);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/templates/images/{id}/comments/all")
    public ResponseEntity<?> getImageComments(@PathVariable Long id){
        try {
            Map<String, Object> response = new HashMap<>();
            GalleryImage image = (GalleryImage) galleryImageService.findById(id);
            List<Map<String, Object>> commentDetailsList = new ArrayList<>();
            if (!image.getComments().isEmpty()) {
                image.getComments().sort(Comparator.comparing(UserComment::getDate));
                List<UserComment> commentWithParentNull = userCommentService.getNonParentedCommentsForImage(image);
                for (UserComment c : commentWithParentNull) {
                    Map<String, Object> commentDetails = new HashMap<>();
                    commentDetails.put("id", c.getId());
                    commentDetails.put("text", c.getText());
                    commentDetails.put("authorId", c.getAuthor() != null ? c.getAuthor().getId() : null);
                    commentDetails.put("authorUsername", c.getAuthor() != null ? c.getAuthor().getUsername() : null);
                    commentDetails.put("date", c.getDate());
                    List<Long> repliesIds = new ArrayList<>(c.getReplies().stream().map(UserComment::getId).toList());
                    commentDetails.put("repliesIds", repliesIds);
                    commentDetails.put("subjectId", c.getSubjectId());
                    commentDetails.put("type", c.getType());
                    List<Long> likesIds = new ArrayList<>(c.getLikes().stream().map(User::getId).toList());
                    commentDetails.put("likes", likesIds);
                    List<Long> dislikesIds = new ArrayList<>(c.getDislikes().stream().map(User::getId).toList());
                    commentDetails.put("dislikes", dislikesIds);
                    commentDetailsList.add(commentDetails);
                }
            }
            response.put("comments", commentDetailsList);
            String newToken = TokenUtil.regenerateToken();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }


    @PostMapping(value = "api/templates/{id}/comments/newReply")
    public ResponseEntity<?> addReplyToTemplateComment(Principal principal, @PathVariable Long id, @RequestBody UserCommentDto dto){
        return addReply(principal, dto, CommentType.TEMPLATE, id);
    }

    @PostMapping(value = "api/templates/images/{id}/comments/newReply")
    public ResponseEntity<?> addReplyToImageComment(Principal principal, @PathVariable Long id, @RequestBody UserCommentDto dto){
        return addReply(principal, dto, CommentType.IMAGE, id);
    }

    private ResponseEntity<?> addReply(Principal principal, UserCommentDto dto, CommentType type, Long id){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User author = userService.findById(userId);
            UserComment comment = new UserComment(
                    dto.getText(),
                    LocalDateTime.now(),
                    dto.getSubjectId(),
                    CommentType.COMMENT,
                    author,
                    new ArrayList<>(),
                    new ArrayList<>(),
                    new ArrayList<>()
            );
            if(type==CommentType.TEMPLATE) {
                Template template = templateService.findTemplateById(id);
                templateService.addReply(comment, dto.getSubjectId(), template);
            }else if(type==CommentType.IMAGE){
                GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
                galleryImageService.addReply(comment, dto.getSubjectId(), galleryImage);
            }
            Map<String, Object> response = new HashMap<>();
            response.put("id", comment.getId());
            response.put("date", comment.getDate());
            String newToken = TokenUtil.regenerateToken();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        }catch (Exception e){
            LOG.info("UserComment creating reply caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "api/templates/{id}/comments/{commentId}/delete")
    public ResponseEntity<?> deleteCommentFromTemplate(Principal principal, @PathVariable Long id, @PathVariable Long commentId) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            UserComment comment = (UserComment) userCommentService.findById(commentId);
            if(userId.equals(comment.getAuthor().getId()) || user.getRole()== UserRole.ADMIN || user.getRole()==UserRole.MODER) {
                Template template = templateService.findTemplateById(id);
                templateService.removeComment(comment, template);
            }else{
                throw new AccountException("you are not allowed to delete this comment");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.info("UserComment {} deleting from template {} reply caused error {}", commentId, id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "api/templates/images/{id}/comments/{commentId}/delete")
    public ResponseEntity<?> deleteCommentFromImage(Principal principal, @PathVariable Long id, @PathVariable Long commentId) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            UserComment comment = (UserComment) userCommentService.findById(commentId);
            if(userId.equals(comment.getAuthor().getId()) || user.getRole()== UserRole.ADMIN || user.getRole()==UserRole.MODER) {
                GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
                galleryImageService.removeComment(comment, galleryImage);
            }else{
                throw new AccountException("you are not allowed to delete this comment");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.info("UserComment {} deleting from image {} reply caused error {}", commentId, id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "api/templates/{id}/comments/deleteAll")
    public ResponseEntity<?> deleteAllTemplateComments(Principal principal, @PathVariable Long id){
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            Template template = templateService.findTemplateById(id);
            if(userId.equals(template.getCreator().getId()) || user.getRole()==UserRole.ADMIN || user.getRole()==UserRole.MODER) {
                List<UserComment> comments = new ArrayList<>(template.getComments().stream().filter(d -> d.getType() != CommentType.COMMENT && Objects.equals(d.getSubjectId(), template.getId())).toList());
                while(!comments.isEmpty()){
                    UserComment t = comments.get(comments.size()-1);
                    userCommentService.deleteTemplateCommentWithReplies(t, template);
                    comments.remove(t);
                }
            }else{
                throw new AccountException("you are not allowed to delete this comment");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("All comments deleting from template {} reply caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "api/templates/images/{id}/comments/deleteAll")
    public ResponseEntity<?> deleteAllImageComments(Principal principal, @PathVariable Long id){
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
            if(userId.equals(galleryImage.getAuthor().getId()) || user.getRole()==UserRole.ADMIN || user.getRole()==UserRole.MODER) {
                List<UserComment> comments = new ArrayList<>(galleryImage.getComments().stream().filter(d -> d.getType() != CommentType.COMMENT && Objects.equals(d.getSubjectId(), galleryImage.getId())).toList());
                while(!comments.isEmpty()){
                    UserComment t = comments.get(comments.size()-1);
                    userCommentService.deleteImageCommentWithReplies(t, galleryImage);
                    comments.remove(t);
                }
            }else{
                throw new AccountException("you are not allowed to delete this comment");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("All comments deleting from image {} reply caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "api/templates/{id}/comments/{commentId}/delete/withReplies")
    public ResponseEntity<?> deleteTemplateCommentWithReplies(Principal principal, @PathVariable Long id, @PathVariable Long commentId) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            UserComment comment = (UserComment) userCommentService.findById(commentId);
            if(userId.equals(comment.getAuthor().getId()) || user.getRole()==UserRole.ADMIN || user.getRole()==UserRole.MODER) {
                Template template = templateService.findTemplateById(id);
                templateService.removeCommentWithReplies(comment, template);
            }else{
                throw new AccountException("you are not allowed to delete this comment");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.info("Comment with replies deleting from template {} reply caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

}
