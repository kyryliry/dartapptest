package com.darttest.pas.controller;

import com.darttest.pas.dto.GalleryImageDto;
import com.darttest.pas.exceptions.AccountException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.UserRole;
import com.darttest.pas.security.model.AuthenticationToken;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.service.TemplateService;
import com.darttest.pas.service.TemplateUsageService;
import com.darttest.pas.service.UserService;
import com.darttest.pas.service.serviceImpl.GalleryImageServiceImpl;
import com.darttest.pas.utils.Constants;
import com.darttest.pas.utils.TokenUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@AllArgsConstructor
public class GalleryImageController {
    private static final Logger LOG = LoggerFactory.getLogger(GalleryImageController.class);
    @Autowired
    private final GalleryImageServiceImpl galleryImageService;
    private final TemplateService templateService;
    private final UserService userService;
    private final TemplateUsageService templateUsageService;
    private final AttachmentService attachmentService;

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PostMapping(value = "api/templates/{id}/images/new")
    public ResponseEntity<?> addNewGalleryImage(@PathVariable Long id, @ModelAttribute GalleryImageDto galleryImageDto){
        try {
            Template template = templateService.findTemplateById(id);
            assert template != null;
            int count = template.getImages().size()+1;
            User user = userService.findById(galleryImageDto.getAuthorId());
            Attachment attachment = attachmentService.saveNewLayerAttachment(galleryImageDto.getImage(), template.getTemplateName(), count, Constants.GALLERY_DIRECTORY);
            GalleryImage galleryImage = new GalleryImage(galleryImageDto.getDescription(), attachment, LocalDateTime.now(), user, new ArrayList<>(), new ArrayList<>(), templateUsageService.findById(galleryImageDto.getUsageId()), galleryImageDto.getActualScore());
            galleryImageService.persist(galleryImage);
            template.getImages().add(galleryImage);
            templateService.update(template);
            byte[] imageData = attachmentService.downloadImage(galleryImage.getImageAtch().getId());
            String base64Image = Base64Utils.encodeToString(imageData);
            Map<String, Object> galleryImageDetails = new HashMap<>();
            galleryImageDetails.put("id", galleryImage.getId());
            galleryImageDetails.put("description", galleryImage.getDescription()!=null? galleryImage.getDescription(): "");
            galleryImageDetails.put("usageId", galleryImage.getUsage().getId());
            galleryImageDetails.put("authorName", galleryImage.getAuthor().getUsername());
            galleryImageDetails.put("uploadDate", galleryImage.getUploadDate());
            galleryImageDetails.put("authorId", galleryImage.getAuthor().getId());
            galleryImageDetails.put("image", base64Image);
            galleryImageDetails.put("actualScore", galleryImage.getActualScore());
            List<Long> likesIds = new ArrayList<>(galleryImage.getLikes().stream().map(User::getId).toList());
            galleryImageDetails.put("likes", likesIds);
            List<Long> dislikesIds = new ArrayList<>(galleryImage.getDislikes().stream().map(User::getId).toList());
            galleryImageDetails.put("dislikes", dislikesIds);
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Gallery Image {} successfully created", galleryImage.getId());
            return new ResponseEntity<>(galleryImageDetails, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Gallery Image {} creating caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/templates/{id}/images")
    public ResponseEntity<?> getAllGalleryImagesForTemplate(@PathVariable Long id){
        try{
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            Template template = templateService.findTemplateById(id);
            List<Map<String, Object>> galleryImageDetailsList = new ArrayList<>();
            for(GalleryImage i: template.getImages()){
                byte[] imageData = attachmentService.downloadImage(i.getImageAtch().getId());
                String base64Image = Base64Utils.encodeToString(imageData);
                Map<String, Object> galleryImageDetails = new HashMap<>();
                galleryImageDetails.put("id", i.getId());
                galleryImageDetails.put("description", i.getDescription()!=null? i.getDescription(): "");
                galleryImageDetails.put("usageId", i.getUsage().getId());
                galleryImageDetails.put("authorName", i.getAuthor().getUsername());
                galleryImageDetails.put("uploadDate", i.getUploadDate());
                galleryImageDetails.put("authorId", i.getAuthor().getId());
                galleryImageDetails.put("image", base64Image);
                galleryImageDetails.put("actualScore", i.getActualScore());
                List<Long> likesIds = new ArrayList<>(i.getLikes().stream().map(User::getId).toList());
                galleryImageDetails.put("likes", likesIds);
                List<Long> dislikesIds = new ArrayList<>(i.getDislikes().stream().map(User::getId).toList());
                galleryImageDetails.put("dislikes", dislikesIds);
                galleryImageDetailsList.add(galleryImageDetails);
            }
            response.put("images", galleryImageDetailsList);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping("api/templates/images/checkExist/{id}")
    public ResponseEntity<?> checkIfImageExist(@PathVariable Long id){
        try{
            GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            if(galleryImage==null){
                response.put("exist", "false");
            }else{
                response.put("exist", "true");
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Gallery Image {} exist check caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/images/{id}/edit")
    public ResponseEntity<?> updateGalleryImageDescription(Principal principal, @PathVariable Long id, @RequestBody Map<String, String> response){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
            assert galleryImage!=null;
            if(Objects.equals(userId, galleryImage.getAuthor().getId())) {
                galleryImage.setDescription(response.get("description"));
                galleryImageService.update(galleryImage);
                Map<String, Object> response1 = new HashMap<>();
                String newToken = TokenUtil.regenerateToken();
                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", newToken);
                LOG.info("Gallery Image {} successfully updated", galleryImage.getId());
                return new ResponseEntity<>(response1, headers, HttpStatus.OK);
            }else{
                throw new AccountException("you are not allowed to update this image");
            }
        }catch (Exception e){
            LOG.info("Gallery Image {} updating caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/templates/images/{id}")
    public ResponseEntity<?> getGalleryImage(@PathVariable Long id){
        try{
            GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
            assert galleryImage!=null;
            byte[] imageData = attachmentService.downloadImage(galleryImage.getImageAtch().getId());
            String base64Image = Base64Utils.encodeToString(imageData);
            List<Long> likesIds = new ArrayList<>(galleryImage.getLikes().stream().map(User::getId).toList());

            List<Long> dislikesIds = new ArrayList<>(galleryImage.getDislikes().stream().map(User::getId).toList());
            String newToken = TokenUtil.regenerateToken();
            GalleryImageDto d = GalleryImageDto.builder()
                    .id(id)
                    .imageData(base64Image)
                    .usageId(galleryImage.getUsage().getId())
                    .uploadDate(galleryImage.getUploadDate())
                    .description(galleryImage.getDescription())
                    .authorId(galleryImage.getAuthor().getId())
                    .authorName(galleryImage.getAuthor().getUsername())
                    .actualScore(galleryImage.getActualScore())
                    .likes(likesIds)
                    .dislikes(dislikesIds)
                    .newToken(newToken)
                    .build();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(d);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @DeleteMapping(value = "api/templates/images/{id}/delete")
    public ResponseEntity<?> deleteGalleryImage(Principal principal, @PathVariable Long id){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            assert user!=null;
            GalleryImage galleryImage = (GalleryImage) galleryImageService.findById(id);
            assert galleryImage!=null;
            if(Objects.equals(userId, galleryImage.getAuthor().getId()) || user.getRole()== UserRole.ADMIN || user.getRole()==UserRole.MODER) {
                templateService.removeGalleryImage(galleryImage);
                Map<String, Object> response = new HashMap<>();
                String newToken = TokenUtil.regenerateToken();
                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", newToken);
                LOG.info("Gallery Image {} successfully deleted", galleryImage.getId());
                return new ResponseEntity<>(response, headers, HttpStatus.OK);
            }else{
                throw new AccountException("you are not allowed to delete this image");
            }
        }catch (Exception e){
            LOG.info("Gallery Image {} deleting caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

}
