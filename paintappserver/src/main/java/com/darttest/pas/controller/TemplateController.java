package com.darttest.pas.controller;

import com.darttest.pas.exceptions.AccountException;
import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.TemplateState;
import com.darttest.pas.model.enums.UserRole;
import com.darttest.pas.security.model.AuthenticationToken;
import com.darttest.pas.service.*;
import com.darttest.pas.service.serviceImpl.GalleryImageServiceImpl;
import com.darttest.pas.service.serviceImpl.SampleImageServiceImpl;
import com.darttest.pas.service.serviceImpl.UserCommentServiceImpl;
import com.darttest.pas.utils.Constants;
import com.darttest.pas.utils.TokenUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.darttest.pas.dto.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;

@RestController
@AllArgsConstructor
public class TemplateController {
    private static final Logger LOG = LoggerFactory.getLogger(TemplateController.class);
    @Autowired
    private final TemplateService templateService;
    private final LayerService layerService;
    private final AttachmentService attachmentService;
    private final SampleImageServiceImpl sampleImageService;
    private final CategoryService categoryService;
    private final UserService userService;
    private final TemplateUsageService templateUsageService;
    private final GalleryImageServiceImpl galleryImageService;
    private final UserCommentServiceImpl userCommentService;

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{id}/publish")
    public ResponseEntity<?> publishDraftTemplate(Principal principal, @PathVariable Long id) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            Template template = templateService.findTemplateById(id);
            if(template.getCreator()==null){
                throw new AccountException("you are not allowed to publish this template");
            }
            if (userId.equals(template.getCreator().getId())) {
                template.setState(TemplateState.PUBLISHED);
                templateService.update(template);
                Map<String, Object> response = new HashMap<>();
                String newToken = TokenUtil.regenerateToken();
                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", newToken);
                LOG.info("Template {} successfully published", id);
                return new ResponseEntity<>(response, headers, HttpStatus.OK);
            } else {
                throw new AccountException("you are not allowed to publish this template");
            }
        } catch (Exception e) {
            LOG.info("Template {} publishing caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{id}/edit")
    public ResponseEntity<?> changeTemplateName(Principal principal, @PathVariable Long id, @RequestBody Map<String, String> body) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            Template template = templateService.findTemplateById(id);
            assert template != null;
            Template other = templateService.findByName(body.get("templateName"));
            if (other == null && template.getCreator()!=null && Objects.equals(userId, template.getCreator().getId())) {
                template.setTemplateName(body.get("templateName"));
                templateService.update(template);
            } else {
                throw new ExistsException("this template name already exists or you are not allowed to change its name");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Template {} name successfully changed", id);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.info("Template {} name changing caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @DeleteMapping(value = "api/templates/{id}/delete")
    public ResponseEntity<?> deleteTemplate(Principal principal, @PathVariable Long id) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User user = userService.findById(userId);
            Template template = templateService.findTemplateById(id);
            if (user.getRole() == UserRole.ADMIN || user.getRole() == UserRole.MODER || (user.getRole() == UserRole.USER && (template.getCreator() == user || template.getState() == TemplateState.DRAFT))) {
                user.getOwnedTemplates().remove(template);
                user.setGlobalCreatorRating((double) user.getOwnedTemplates().size());
                userService.update(user);
                templateService.delete(template);
            } else {
                throw new AccountException("you dont have access to delete this template");
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Template {} successfully deleted", id);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.info("Template {} deleting caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/templates/{name}")
    public ResponseEntity<?> getTemplateByName(@PathVariable String name) {
        Map<String, Object> response = new HashMap<>();
        if (templateService.findByName(name) != null) {
            response.put("id", templateService.findByName(name).getId());

        } else {
            response.put("id", -1);
        }
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @PostMapping(value = "api/template/createWithLayers")
    public ResponseEntity<?> createTemplateWithLayers(@ModelAttribute TemplateSaveDto templateDto) {
        try {
            User user = userService.findById(templateDto.getCreatorId());
            Template template = Template.builder().templateName(user.getUsername()+"_"+templateDto.getTemplateName()).maxScore(templateDto.getMaxScore()).uploadDate(LocalDateTime.now()).creator(user).layers(new ArrayList<>()).state(templateDto.getState()).build();
            Attachment attachment = attachmentService.saveNewLayerAttachment(templateDto.getFile(), template.getTemplateName(), 0, Constants.SAMPLE_DIRECTORY);
            templateService.persist(template);
            user.setGlobalCreatorRating((double) user.getOwnedTemplates().size());
            userService.update(user);
            SampleImage sampleImage = new SampleImage("", attachment, template);
            template.setSampleImage(sampleImage);
            sampleImageService.persist(sampleImage);
            templateService.update(template);

            ObjectMapper objectMapper = new ObjectMapper();
            List<CategoryDto> categories = objectMapper.readValue(templateDto.getCategories(), new TypeReference<List<CategoryDto>>() {
            });

            for (CategoryDto c : categories) {
                templateService.addCategory(template, categoryService.findById(c.getId()));
            }

            for (LayerSaveDto layerDto : templateDto.getLayers()) {
                Attachment attachment1 = attachmentService.saveNewLayerAttachment(layerDto.getFileL(), template.getTemplateName(), layerDto.getNumber(), Constants.UPLOAD_DIRECTORY);
                Layer lay = Layer.builder()
                        .template(template)
                        .brushColor(layerDto.getColor())
                        .showingColor(layerDto.getShowColor())
                        .number(layerDto.getNumber())
                        .brushSize(layerDto.getStrokeSize())
                        .layerImage(attachment1).build();
                layerService.persist(lay);
                template.getLayers().add(lay);

            }
            templateService.update(template);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Template {} successfully created", template.getId());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.info("Template creating caused error {}", e.getMessage());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping("api/templates/get/{id}")
    public ResponseEntity<?> downloadLayerImage(@PathVariable Long id) throws IOException {
        byte[] imageData = attachmentService.downloadImage(id);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }

    @GetMapping("api/templates/checkExist/{id}")
    public ResponseEntity<?> checkIfTemplateExist(@PathVariable Long id){
        try{
            Template template = templateService.findTemplateById(id);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            if(template==null){
                response.put("exist", "false");
            }else{
                response.put("exist", "true");
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Template {} check exist caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    @GetMapping("api/comments/checkExist/{id}")
    public ResponseEntity<?> checkIfCommentExist(@PathVariable Long id){
        try{
            UserComment comment = (UserComment) userCommentService.findById(id);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            if(comment==null){
                response.put("exist", "false");
            }else{
                response.put("exist", "true");
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Comment {} check exist caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    @GetMapping("api/templates/getTwSI/{id}")
    public ResponseEntity<?> getTemplateWithSampleImage(@PathVariable Long id) throws IOException {
        try {

            Template template = templateService.findTemplateById(id);
            byte[] imageData = attachmentService.downloadImage(template.getSampleImage().getImageAtch().getId());
            String base64Image = Base64Utils.encodeToString(imageData);
            Map<String, Object> response = new HashMap<>();
            response.put("id", template.getId());
            response.put("name", template.getTemplateName());
            response.put("image", base64Image);
            response.put("creatorId", template.getCreator()!=null ? template.getCreator().getId(): null);
            response.put("layersAmount", template.getLayers().size());
            response.put("downloadsCount", template.getDownloadsCount());
            response.put("creatorName", template.getCreator()!=null? template.getCreator().getUsername(): "");
            response.put("uploadDate", template.getUploadDate());
            response.put("sampleDescription", template.getSampleImage().getDescription());
            response.put("maxScore", template.getMaxScore());
            List<Long> likes = new ArrayList<>(template.getLikes().stream().map(User::getId).toList());
            response.put("likes", likes);
            List<Long> dislikes = new ArrayList<>(template.getDislikes().stream().map(User::getId).toList());
            response.put("dislikes", dislikes);
            List<CategoryDto> cats = new ArrayList<>();
            for (Category c : template.getCategories()) {
                cats.add(CategoryDto.builder().name(c.getName()).id(c.getId()).build());
            }
            ObjectMapper objectMapper = new ObjectMapper();
            String categoriesJson = objectMapper.writeValueAsString(cats);
            response.put("categories", categoriesJson);
            List<Map<String, Object>> galleryImageDetailsList = new ArrayList<>();
            if (!template.getImages().isEmpty()) {
                for (GalleryImage i : template.getImages().subList(0, Math.min(template.getImages().size(), 5))) {
                    byte[] iData = attachmentService.downloadImage(i.getImageAtch().getId());
                    String base64I = Base64Utils.encodeToString(iData);
                    Map<String, Object> galleryImageDetails = new HashMap<>();
                    galleryImageDetails.put("id", i.getId());
                    galleryImageDetails.put("description", i.getDescription() != null ? i.getDescription() : "");
                    galleryImageDetails.put("uploadDate", i.getUploadDate());
                    galleryImageDetails.put("authorName", i.getAuthor().getUsername());
                    galleryImageDetails.put("authorId", i.getAuthor().getId());
                    galleryImageDetails.put("usageId", i.getUsage().getId());
                    galleryImageDetails.put("image", base64I);
                    galleryImageDetails.put("actualScore", i.getActualScore());
                    List<Long> likesIds = new ArrayList<>(i.getLikes().stream().map(User::getId).toList());
                    galleryImageDetails.put("likes", likesIds);
                    List<Long> dislikesIds = new ArrayList<>(i.getDislikes().stream().map(User::getId).toList());
                    galleryImageDetails.put("dislikes", dislikesIds);
                    galleryImageDetailsList.add(galleryImageDetails);
                }
            }
            response.put("images", galleryImageDetailsList);
            List<Map<String, Object>> commentDetailsList = new ArrayList<>();
            if(!template.getComments().isEmpty()){
                template.getComments().sort(Comparator.comparing(UserComment::getDate));
                List<UserComment> commentWithParentNull = userCommentService.getNonParentedCommentsForTemplate(template);
                for(UserComment c: commentWithParentNull){
                    Map<String, Object> commentDetails = new HashMap<>();
                    commentDetails.put("id", c.getId());
                    commentDetails.put("text", c.getText());
                    commentDetails.put("authorId", c.getAuthor()!=null ? c.getAuthor().getId() : null);
                    commentDetails.put("authorUsername", c.getAuthor()!=null ? c.getAuthor().getUsername() : null);
                    commentDetails.put("date", c.getDate());
                    List<Long> repliesIds = new ArrayList<>(c.getReplies().stream().map(UserComment::getId).toList());
                    commentDetails.put("repliesIds", repliesIds);
                    commentDetails.put("subjectId", c.getSubjectId());
                    commentDetails.put("type", c.getType());
                    List<Long> likesIds = new ArrayList<>(c.getLikes().stream().map(User::getId).toList());
                    commentDetails.put("likes", likesIds);
                    List<Long> dislikesIds = new ArrayList<>(c.getDislikes().stream().map(User::getId).toList());
                    commentDetails.put("dislikes", dislikesIds);
                    commentDetailsList.add(commentDetails);
                }
            }
            response.put("comments", commentDetailsList);
            String newToken = TokenUtil.regenerateToken();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred");
        }
    }

    @GetMapping("api/templates/getTwSI/all")
    public ResponseEntity<?> getAllTemplatesWithSampleImage(){
        try {
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            List<Template> temps = templateService.findAll();
            temps.sort(Comparator.comparing(Template::getUploadDate).reversed());
            List<Map<String, Object>> fullResult = prepareListOfTemplatesWithSI(temps);
            if (fullResult.isEmpty()) {
                response.put("result", null);
                ResponseEntity
                        .status(HttpStatus.OK)
                        .header("Authorization", newToken)
                        .body(response);
            }
            response.put("result", fullResult);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Error occurred");
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @GetMapping(value = "api/templates/{name}/getallatts")
    public ResponseEntity<?> getAllAttachmentsIdByTemplateName(@PathVariable String name) {
        Long id = templateService.findByName(name).getId();
        List<Long> ids = new ArrayList<>();
        List<Layer> lays = layerService.findAllByTemplateId(id);
        lays.sort(Comparator.comparing(Layer::getNumber));
        for (Layer layer : lays) {
            ids.add(layer.getLayerImage().getId());
        }
        Map<String, Object> response = new HashMap<>();
        String newToken = TokenUtil.regenerateToken();
        response.put("ids", ids);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @GetMapping(value = "api/templates/{name}/getlayersinfo")
    public ResponseEntity<?> getLayersInfo(@PathVariable String name) {
        Template template = templateService.findByName(name);
        Long id = template.getId();
        List<Layer> lays = layerService.findAllByTemplateId(id);
        lays.sort(Comparator.comparing(Layer::getNumber));
        List<LayerDto> dtos = new ArrayList<>();
        for (Layer layer : lays) {
            dtos.add(new LayerDto(id, layer.getId(), layer.getLayerImage().getId(), layer.getBrushSize(), layer.getBrushColor(), layer.getShowingColor(), layer.getNumber(), null, null));
        }
        Map<String, Object> response = new HashMap<>();
        String newToken = TokenUtil.regenerateToken();
        response.put("maxScore", template.getMaxScore());
        response.put("layers", dtos);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{id}/sampleDesc/edit")
    public ResponseEntity<?> updateSampleImageDescription(Principal principal, @PathVariable Long id, @RequestBody Map<String, String> response) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            Template template = templateService.findTemplateById(id);
            assert template != null;
            if(template.getCreator()==null){
                throw new AccountException("you are not allowed to update this template");
            }
            if (Objects.equals(userId, template.getCreator().getId())) {
                template.getSampleImage().setDescription(response.get("description"));
                sampleImageService.update(template.getSampleImage());
                Map<String, Object> response1 = new HashMap<>();
                String newToken = TokenUtil.regenerateToken();
                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", newToken);
                LOG.info("Template {} description successfully updated", template.getId());
                return new ResponseEntity<>(response1, headers, HttpStatus.OK);
            } else {
                throw new AccountException("you are not allowed to update this template");
            }
        } catch (Exception e) {
            LOG.info("Template {} description updating caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{type}/{id}/addLike")
    public ResponseEntity<?> addLike(Principal principal, @PathVariable Long id, @PathVariable String type) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        Long userId = auth.getPrincipal().getUser().getId();
        if (userId == null) {
            throw new NotFoundException("user not found");
        }
        try {
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            User user = userService.findById(userId);
            if (Objects.equals(type, "image")) {
                GalleryImage image = (GalleryImage) galleryImageService.findById(id);
                GalleryImage result = galleryImageService.addLike(user, image);
                if (result.getDislikes().contains(user)) {
                    result = galleryImageService.removeDislike(user, result);
                    List<Long> dislikesIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                    response.put("dislikes", dislikesIds);
                }
                List<Long> userIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                response.put("likes", userIds);
            } else if (type.equals("template")) {
                Template template = templateService.findTemplateById(id);
                Template result = templateService.addLike(user, template);
                if (result.getDislikes().contains(user)) {
                    result = templateService.removeDislike(user, result);
                    List<Long> dislikesIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                    response.put("dislikes", dislikesIds);
                }
                List<Long> userIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                response.put("likes", userIds);
            } else if(type.equals("comment")){
                UserComment comment = (UserComment) userCommentService.findById(id);
                UserComment result = userCommentService.addLike(user, comment);
                if (result.getDislikes().contains(user)) {
                    result = userCommentService.removeDislike(user, result);
                    List<Long> dislikesIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                    response.put("dislikes", dislikesIds);
                }
                List<Long> userIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                response.put("likes", userIds);
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            LOG.info("Like on {} {} adding caused error {}", type, id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{type}/{id}/removeLike")
    public ResponseEntity<?> removeLike(Principal principal, @PathVariable Long id, @PathVariable String type) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        Long userId = auth.getPrincipal().getUser().getId();
        Map<String, Object> response = new HashMap<>();
        String newToken = TokenUtil.regenerateToken();
        if (userId == null) {
            throw new NotFoundException("user not found");
        }
        try {
            List<Long> userIds = new ArrayList<>();
            User user = userService.findById(userId);
            if (Objects.equals(type, "image")) {
                GalleryImage image = (GalleryImage) galleryImageService.findById(id);
                GalleryImage result = galleryImageService.removeLike(user, image);
                userIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                response.put("likes", userIds);
            } else if (type.equals("template")) {
                Template template = templateService.findTemplateById(id);
                Template result = templateService.removeLike(user, template);
                userIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                response.put("likes", userIds);
            } else if(type.equals("comment")){
                UserComment comment = (UserComment) userCommentService.findById(id);
                UserComment result = userCommentService.removeLike(user, comment);
                userIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                response.put("likes", userIds);
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            LOG.info("Like on {} {} removing caused error {}", type, id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{type}/{id}/addDislike")
    public ResponseEntity<?> addDislike(Principal principal, @PathVariable Long id, @PathVariable String type) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        Long userId = auth.getPrincipal().getUser().getId();
        if (userId == null) {
            throw new NotFoundException("user not found");
        }
        try {
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            User user = userService.findById(userId);
            if (Objects.equals(type, "image")) {
                GalleryImage image = (GalleryImage) galleryImageService.findById(id);
                GalleryImage result = galleryImageService.addDislike(user, image);
                if (result.getLikes().contains(user)) {
                    result = galleryImageService.removeLike(user, result);
                    List<Long> likesIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                    response.put("likes", likesIds);
                }
                List<Long> userIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                response.put("dislikes", userIds);
            } else if (type.equals("template")) {
                Template template = templateService.findTemplateById(id);
                Template result = templateService.addDislike(user, template);
                if (result.getLikes().contains(user)) {
                    result = templateService.removeLike(user, result);
                    List<Long> likesIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                    response.put("likes", likesIds);
                }
                List<Long> userIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                response.put("dislikes", userIds);
            }else if(type.equals("comment")){
                UserComment comment = (UserComment) userCommentService.findById(id);
                UserComment result = userCommentService.addDislike(user, comment);
                if (result.getLikes().contains(user)) {
                    result = userCommentService.removeLike(user, result);
                    List<Long> likesIds = new ArrayList<>(result.getLikes().stream().map(User::getId).toList());
                    response.put("likes", likesIds);
                }
                List<Long> userIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                response.put("dislikes", userIds);
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            LOG.info("Dislike on {} {} adding caused error {}", type, id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/templates/{type}/{id}/removeDislike")
    public ResponseEntity<?> removeDislike(Principal principal, @PathVariable Long id, @PathVariable String type) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        Long userId = auth.getPrincipal().getUser().getId();

        if (userId == null) {
            throw new NotFoundException("user not found");
        }
        try {
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            List<Long> userIds = new ArrayList<>();
            User user = userService.findById(userId);
            if (Objects.equals(type, "image")) {
                GalleryImage image = (GalleryImage) galleryImageService.findById(id);
                GalleryImage result = galleryImageService.removeDislike(user, image);
                userIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                response.put("dislikes", userIds);
            } else if (type.equals("template")) {
                Template template = templateService.findTemplateById(id);
                Template result = templateService.removeDislike(user, template);
                userIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                response.put("dislikes", userIds);
            } else if (type.equals("comment")) {
                UserComment comment = (UserComment) userCommentService.findById(id);
                UserComment result = userCommentService.removeDislike(user, comment);
                userIds = new ArrayList<>(result.getDislikes().stream().map(User::getId).toList());
                response.put("dislikes", userIds);
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            LOG.info("Dislike on {} {} removing caused error {}", type, id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN', 'ROLE_MODER')")
    @GetMapping(value = "api/templates/allUser")
    public ResponseEntity<?> getAllTemplatesForUser(Principal principal) {
        try {
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            Map<String, Object> response = new HashMap<>();
            User user = userService.findById(userId);
            Map<String, List<Template>> temps = templateUsageService.findAllSortedTemplatesByPlayer(user);
            List<Template> srTemps = templateService.findFollowingReleaseForWeek(user);
            List<Map<String, Object>> srTempsResult = prepareListOfTemplatesWithSI(srTemps);
            response.put("subRelease", srTempsResult);
            List<Map<String, Object>> unfTempsResult = prepareListOfTemplatesWithSI(temps.get("unfinished"));
            response.put("unfinished", unfTempsResult);
            List<Map<String, Object>> finTempsResult = prepareListOfTemplatesWithSI(temps.get("finished"));
            response.put("finished", finTempsResult);
            List<Map<String, Object>> nullTempsResult = prepareListOfTemplatesWithSI(temps.get("nullProgress"));
            response.put("nullProgress", nullTempsResult);
            List<Map<String, Object>> notNullTempsResult = prepareListOfTemplatesWithSI(temps.get("notNullProgress"));
            response.put("notNullProgress", notNullTempsResult);

            Map<String, List<Template>> tempsOwned = templateService.findAllSortedByTemplateState(user);
            List<Map<String, Object>> publishedTempsResult = prepareListOfTemplatesWithSI(tempsOwned.get("published"));
            response.put("published", publishedTempsResult);
            List<Map<String, Object>> draftTempsResult = prepareListOfTemplatesWithSI(tempsOwned.get("draft"));
            response.put("draft", draftTempsResult);
            List<Map<String, Object>> blockedTempsResult = prepareListOfTemplatesWithSI(tempsOwned.get("blocked"));
            response.put("blocked", blockedTempsResult);
            List<Map<String, Object>> verifiedTempsResult = prepareListOfTemplatesWithSI(tempsOwned.get("verified"));
            response.put("verified", verifiedTempsResult);
            String newToken = TokenUtil.regenerateToken();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }


    @GetMapping(value = "api/templates/allUser/simplified/{id}")
    public ResponseEntity<?> getAllTemplatesForUserSimplified(@PathVariable Long id) {
        try {
            Map<String, Object> response = new HashMap<>();
            User user = userService.findById(id);
            Map<String, List<Template>> temps = templateUsageService.findAllSortedTemplatesByPlayer(user);
            List<Map<String, Object>> finTempsResult = prepareListOfTemplatesWithSI(temps.get("finished"));
            response.put("finished", finTempsResult);

            Map<String, List<Template>> tempsOwned = templateService.findAllSortedByTemplateState(user);
            List<Map<String, Object>> publishedTempsResult = prepareListOfTemplatesWithSI(tempsOwned.get("published"));
            response.put("published", publishedTempsResult);
            String newToken = TokenUtil.regenerateToken();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PostMapping(value = "api/templates/getByCategories")
    public ResponseEntity<?> getAllTemplatesByCategories(@RequestBody SearchDto searchDto) {
        try {
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            Set<Template> allFound = new HashSet<>();
            if (!Objects.equals(searchDto.getSearchText(), "")) {
                Set<Template> searchedTemps = templateService.findAllBySearchText(searchDto.getSearchText());
                if (!searchedTemps.isEmpty()) {
                    allFound.addAll(searchedTemps);
                }
            }

            if (!searchDto.getCategories().isEmpty()) {
                Set<Category> categories = new HashSet<>();
                for (CategoryDto c : searchDto.getCategories()) {
                    categories.add(categoryService.findById(c.getId()));
                }
                Set<Template> templates = templateService.findAllByCategories(categories);
                if (!templates.isEmpty()) {
                    allFound.addAll(templates);
                }
            }

            if (allFound.isEmpty()) {
                response.put("result", null);
                ResponseEntity
                        .status(HttpStatus.OK)
                        .header("Authorization", newToken)
                        .body(response);
            }
            List<Map<String, Object>> tempsResult = prepareListOfTemplatesWithSI(allFound.stream().toList());
            response.put("result", tempsResult);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", newToken)
                    .body(response);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
    public List<Map<String, Object>> prepareListOfTemplatesWithSI(List<Template> templates) {
        try {
            List<Map<String, Object>> fullResult = new ArrayList<>();
            for (Template template : templates) {
                byte[] imageData = attachmentService.downloadImage(template.getSampleImage().getImageAtch().getId());
                String base64Image = Base64Utils.encodeToString(imageData);
                Map<String, Object> response = new HashMap<>();
                response.put("id", template.getId());
                response.put("name", template.getTemplateName());
                response.put("image", base64Image);
                response.put("downloadsCount", template.getDownloadsCount());
                response.put("creatorName", template.getCreator()!=null?template.getCreator().getUsername():"");
                response.put("uploadDate", template.getUploadDate());
                fullResult.add(response);
            }
            return fullResult;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Map<String, Object>>();
        }
    }
}
