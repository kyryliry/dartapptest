package com.darttest.pas.controller;

import com.darttest.pas.dto.CategoryDto;
import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.model.Category;
import com.darttest.pas.service.CategoryService;
import com.darttest.pas.utils.PaginatedResponse;
import com.darttest.pas.utils.TokenUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@AllArgsConstructor
public class CategoryController {

    private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);
    @Autowired
    private final CategoryService categoryService;

    @GetMapping(value = "api/categories")
    public ResponseEntity<?> getAllCategories(){
        List<CategoryDto> cats = new ArrayList<>();
        for(Category c: categoryService.findAll()){
            cats.add(CategoryDto.builder().id(c.getId()).name(c.getName()).build());
        }
        cats.sort(Comparator.comparing(CategoryDto::getName));
        Map<String, Object> response = new HashMap<>();
        String newToken = TokenUtil.regenerateToken();
        response.put("categories", cats);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @GetMapping(value = "api/categories/paged")
    public ResponseEntity<PaginatedResponse<CategoryDto>> getAllCategoriesPaged(@RequestParam(defaultValue = "0") int page,
                                                              @RequestParam(defaultValue = "15") int size){
        Pageable pageable = PageRequest.of(page, size, Sort.by("name"));
        Page<Category> catsPage = categoryService.findAllPageable(pageable);
        String newToken = TokenUtil.regenerateToken();
        Page<CategoryDto> pages = catsPage.map(cat -> CategoryDto.builder()
                .id(cat.getId())
                .name(cat.getName())
                .build());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<CategoryDto> response = new PaginatedResponse<>(pages, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @GetMapping(value = "api/categories/get/{id}")
    public ResponseEntity<?> getCategory(@PathVariable Long id){
        try {
            Category category = categoryService.findById(id);
            String newToken = TokenUtil.regenerateToken();
            CategoryDto d = CategoryDto.builder().id(category.getId()).name(category.getName()).newToken(newToken).build();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(d);
        }catch (NullPointerException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "api/categories/new")
    public ResponseEntity<?> createCategory(@RequestBody CategoryDto categoryDto){
        try {
            Category category = Category.builder().name(categoryDto.getName()).build();
            categoryService.persist(category);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Category {} successfully created", categoryDto.getName());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (ExistsException e){
            LOG.info("Category {} creating caused error {}", categoryDto.getName(), e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "api/categories/update/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable Long id, @RequestBody CategoryDto categoryDto){
        try{
            Category category = categoryService.findById(id);
            category.setName(categoryDto.getName());
            categoryService.update(category);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            LOG.info("Category {} successfully updated", categoryDto.getName());
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (NullPointerException e){
            LOG.info("Category {} updating caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "api/categories/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Long id){
        try{
            categoryService.deleteById(id);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Category {} successfully deleted", id);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (NullPointerException e){
            LOG.info("Category {} deleting caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "api/categories/delete/set")
    public ResponseEntity<?> deleteCategoriesSet(@RequestBody Set<CategoryDto> categoryDtoSet){
        try{
            for(CategoryDto c: categoryDtoSet){
                categoryService.deleteById(c.getId());
                LOG.info("Category {} successfully deleted", c.getName());
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (NullPointerException e){
            LOG.info("Delete categories set caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
