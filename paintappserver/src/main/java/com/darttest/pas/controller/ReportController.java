package com.darttest.pas.controller;

import com.darttest.pas.dto.ReportDto;
import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.Report;
import com.darttest.pas.model.User;
import com.darttest.pas.model.UserComment;
import com.darttest.pas.model.enums.CommentType;
import com.darttest.pas.security.model.AuthenticationToken;
import com.darttest.pas.service.UserService;
import com.darttest.pas.service.serviceImpl.ReportServiceImpl;
import com.darttest.pas.service.serviceImpl.UserCommentServiceImpl;
import com.darttest.pas.utils.PaginatedResponse;
import com.darttest.pas.utils.TokenUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@AllArgsConstructor
public class ReportController {
    private static final Logger LOG = LoggerFactory.getLogger(ReportController.class);
    @Autowired
    private final ReportServiceImpl reportService;
    private final UserService userService;
    private final UserCommentServiceImpl userCommentService;

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_MODER', 'ROLE_ADMIN')")
    @PostMapping(value = "api/reports/create")
    public ResponseEntity<?> createReport(Principal principal, @RequestBody ReportDto reportDto){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User author = userService.findById(userId);
            Report report = new Report(
                    reportDto.getText(),
                    LocalDateTime.now(),
                    reportDto.getSubjectId(),
                    reportDto.getType(),
                    author, true, "", null,
                    author.getUsername(), null
            );
            reportService.createReport(report);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Report {} successfully created", report.getId());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Report creating caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_MODER', 'ROLE_ADMIN')")
    @GetMapping(value = "api/reports/get/open/paged")
    public ResponseEntity<PaginatedResponse<ReportDto>> getAllOpenedPaged(@RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "15") int size){
        Pageable pageable = PageRequest.of(page, size, Sort.by("date").descending());
        Page<Report> reports = reportService.findAllOpenedPageable(true, pageable);
        return reportsPageMapper(reports);
    }

    @PreAuthorize("hasAnyRole('ROLE_MODER', 'ROLE_ADMIN')")
    @GetMapping(value = "api/reports/get/closed/paged")
    public ResponseEntity<PaginatedResponse<ReportDto>> getAllClosedPaged(@RequestParam(defaultValue = "0") int page,
                                             @RequestParam(defaultValue = "15") int size){
        Pageable pageable = PageRequest.of(page, size, Sort.by("date").descending());
        Page<Report> reports = reportService.findAllOpenedPageable(false, pageable);
        return reportsPageMapper(reports);
    }

    @PreAuthorize("hasAnyRole('ROLE_MODER', 'ROLE_ADMIN')")
    @GetMapping(value = "api/reports/get/all/paged")
    public ResponseEntity<PaginatedResponse<ReportDto>> getAllPaged(@RequestParam(defaultValue = "0") int page,
                                             @RequestParam(defaultValue = "15") int size){
        Pageable pageable = PageRequest.of(page, size, Sort.by("date").descending());
        Page<Report> reports = reportService.findAllPaged(pageable);
        return reportsPageMapper(reports);
    }

    @PutMapping(value = "api/reports/close")
    public ResponseEntity<?> closeReport(Principal principal, @RequestBody ReportDto reportDto){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            User reviewer = userService.findById(userId);
            Report report = reportService.findReportById(reportDto.getId());
            assert report!=null;
            report.setCloseReason(reportDto.getCloseReason());
            report.setReviewer(reviewer);
            report.setCloseDate(LocalDateTime.now());
            reportService.closeReport(report);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Report {} successfully closed", report.getId());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Report {} closing caused error {}", reportDto.getId(), e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_MODER', 'ROLE_ADMIN')")
    @PutMapping(value = "api/reports/reopen/{id}")
    public ResponseEntity<?> reopenReport(@PathVariable Long id){
        try{
            Report report = reportService.findReportById(id);
            assert report!=null;
            reportService.reopenReport(report);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Report {} successfully reopened", report.getId());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Report {} reopening caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_MODER', 'ROLE_ADMIN')")
    @GetMapping(value = "api/reports/{id}/userComment/subParent")
    public ResponseEntity<?> getGrandParentOfUserComment(@PathVariable Long id){
        try{
            String newToken = TokenUtil.regenerateToken();
            Report report = reportService.findReportById(id);
            assert report != null;
            if(report.getType()!= CommentType.COMMENT){
                throw new NotFoundException("this report type is not valid");
            }
            Pattern pattern = Pattern.compile("^\\{([A-Za-z]+)(\\d+)\\}");
            Matcher matcher = pattern.matcher(report.getText());
            String letter = "TEMPLATE";
            Long objectId = null;
            if (matcher.find()) {
                letter = matcher.group(1);
                String number = matcher.group(2);
                objectId = Long.parseLong(number);
            }
            UserComment userComment = (UserComment) userCommentService.findById(report.getSubjectId());
            if(userComment.getType()!=CommentType.COMMENT){
                return ResponseEntity.status(HttpStatus.OK).header("Authorization", newToken).body(prepareUserComment(userComment, letter, objectId,0));
            }else{
                UserComment parent = (UserComment) userCommentService.findById(userComment.getSubjectId());
                if(parent.getType()!=CommentType.COMMENT){
                    return ResponseEntity.status(HttpStatus.OK).header("Authorization", newToken).body(prepareUserComment(parent, letter, objectId, 1));
                }else{
                    UserComment grandParent = (UserComment) userCommentService.findById(parent.getSubjectId());
                    return ResponseEntity.status(HttpStatus.OK).header("Authorization", newToken).body(prepareUserComment(grandParent, letter, objectId, 2, parent.getId()));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    private Map<String, Object> prepareUserComment(UserComment comment, String letter, Long objectId, int injection, Long parentId){
        Map<String, Object> commentDetails = new HashMap<>(prepareUserComment(comment,letter,objectId,injection));
        commentDetails.put("parentId", parentId);
        return commentDetails;
    }

    private Map<String, Object> prepareUserComment(UserComment comment, String letter, Long objectId, int injection){
        Map<String, Object> commentDetails = new HashMap<>();
        commentDetails.put("id", comment.getId());
        commentDetails.put("text", comment.getText());
        commentDetails.put("authorId", comment.getAuthor()!=null ? comment.getAuthor().getId(): null);
        commentDetails.put("authorUsername", comment.getAuthor()!=null ? comment.getAuthor().getUsername() : null);
        commentDetails.put("date", comment.getDate());
        List<Long> repliesIds = new ArrayList<>(comment.getReplies().stream().map(UserComment::getId).toList());
        commentDetails.put("repliesIds", repliesIds);
        commentDetails.put("subjectId", comment.getSubjectId());
        commentDetails.put("type", comment.getType());
        List<Long> likesIds = new ArrayList<>(comment.getLikes().stream().map(User::getId).toList());
        commentDetails.put("likes", likesIds);
        List<Long> dislikesIds = new ArrayList<>(comment.getDislikes().stream().map(User::getId).toList());
        commentDetails.put("dislikes", dislikesIds);
        commentDetails.put("objectId", objectId);
        commentDetails.put("objectType", CommentType.valueOf(letter));
        commentDetails.put("injection", injection);
        return commentDetails;
    }

    private ResponseEntity<PaginatedResponse<ReportDto>> reportsPageMapper(Page<Report> reports){
        String newToken = null;
        if(SecurityContextHolder.getContext().getAuthentication()!=null) {
            newToken = TokenUtil.regenerateToken();
        }

        Page<ReportDto> reportDtos = reports.map(report -> ReportDto.builder()
                .id(report.getId())
                .text(report.getText())
                .date(report.getDate())
                .subjectId(report.getSubjectId())
                .type(report.getType())
                .authorId(report.getAuthor()!=null?report.getAuthor().getId():null)
                .authorUsername(report.getAuthorUsername())
                .reviewerId(report.getReviewer()!=null?report.getReviewer().getId():null)
                .reviewerUsername(report.getReviewer()!=null?report.getReviewer().getUsername():"")
                .isOpen(report.getIsOpen())
                .closeReason(report.getCloseReason()!=null? report.getCloseReason() : "")
                .closeDate(report.getCloseDate()!=null?report.getCloseDate():null)
                .build());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<ReportDto> response = new PaginatedResponse<>(reportDtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

}
