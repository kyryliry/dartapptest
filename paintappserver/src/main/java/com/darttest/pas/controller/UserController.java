package com.darttest.pas.controller;

import com.darttest.pas.dto.*;
import com.darttest.pas.exceptions.ExistsException;
import com.darttest.pas.exceptions.NotFoundException;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.UserRole;
import com.darttest.pas.security.model.AuthenticationToken;
import com.darttest.pas.service.*;
import com.darttest.pas.utils.PaginatedResponse;
import com.darttest.pas.utils.TokenUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;
import java.util.*;

@RestController
@AllArgsConstructor
public class UserController {
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private final UserService userService;

    @PreAuthorize("isAnonymous() || hasAnyRole('ROLE_ADMIN')")
    @PostMapping(value = "api/users/new")
    public ResponseEntity<?> registerUser(@RequestBody UserDto userDto){
        try{
            User user = User.builder().username(userDto.getUsername())
                    .following(new HashSet<>())
                    .followers(new HashSet<>())
                    .ownedTemplates(new ArrayList<>())
                    .playedTemplates(new ArrayList<>())
                    .role(UserRole.USER)
                    .isBlocked(false)
                    .globalPlayerRating(0.0)
                    .globalCreatorRating(0.0)
                    .password(userDto.getPassword())
                    .email(userDto.getEmail()).build();
            userService.persist(user);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("User {} successfully registered", userDto.getUsername());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (ExistsException | NotFoundException e){
            LOG.info("User {} creating caused error {}", userDto.getUsername(), e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "api/users/admins/new")
    public ResponseEntity<?> registerAdmin(@RequestBody UserDto userDto){
        try{
            User user = User.builder().username(userDto.getUsername())
                    .following(new HashSet<>())
                    .followers(new HashSet<>())
                    .ownedTemplates(new ArrayList<>())
                    .playedTemplates(new ArrayList<>())
                    .role(UserRole.ADMIN)
                    .isBlocked(false)
                    .globalPlayerRating(0.0)
                    .globalCreatorRating(0.0)
                    .password(userDto.getPassword())
                    .email(userDto.getEmail()).build();
            userService.persist(user);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Admin {} successfully registered", userDto.getUsername());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (ExistsException | NotFoundException e){
            LOG.info("Admin {} creating caused error {}", userDto.getUsername(), e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "api/users/moders/new")
    public ResponseEntity<?> registerModer(@RequestBody UserDto userDto){
        try{
            User user = User.builder().username(userDto.getUsername())
                    .following(new HashSet<>())
                    .followers(new HashSet<>())
                    .ownedTemplates(new ArrayList<>())
                    .playedTemplates(new ArrayList<>())
                    .role(UserRole.MODER)
                    .isBlocked(false)
                    .globalPlayerRating(0.0)
                    .globalCreatorRating(0.0)
                    .password(userDto.getPassword())
                    .email(userDto.getEmail()).build();
            userService.persist(user);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("Moder {} successfully registered", userDto.getUsername());
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (ExistsException | NotFoundException e){
            LOG.info("Moder {} creating caused error {}", userDto.getUsername(), e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "api/users/admins/changePassword/{id}")
    public ResponseEntity<?> adminChangePassword(@PathVariable Long id, @RequestBody HashMap<String, String> data){
        try {
            User user = userService.findById(id);
            userService.changePassword(user, data.get("password"));
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Change pass by admin caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "api/users/admins/changeRole/{id}")
    public ResponseEntity<?> changeUserRole(@PathVariable Long id, @RequestBody HashMap<String, String> role){
        try{
            User user = userService.findById(id);
            UserRole newRole = UserRole.valueOf(role.get("role"));
            user.setRole(newRole);
            userService.update(user);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Change role by admin caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "api/users/admins/edit/{id}")
    public ResponseEntity<?> editUser(@PathVariable Long id, @RequestBody UserDto userDto){
        try{
            userService.updateUser(userDto, id);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Edit user {} by admin caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_MODER')")
    @PutMapping(value = "api/users/edit")
    public ResponseEntity<?> editCurrentUser(Principal principal, @RequestBody UserDto userDto){
        final AuthenticationToken auth = (AuthenticationToken) principal;
        Long loggedId = auth.getPrincipal().getUser().getId();
        try{
            userService.updateUser(userDto, loggedId);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Edit user {} caused error {}", loggedId, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id){
        try {
            User user = userService.findById(id);
            HttpHeaders headers = new HttpHeaders();
            String newToken = TokenUtil.regenerateToken();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(userDtoMapper(user));
        }catch (NullPointerException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/users/short/{id}")
    public ResponseEntity<?> getShortUserDataById(@PathVariable Long id){
        try {
            User user = userService.findById(id);
            String newToken = TokenUtil.regenerateToken();
            ShortUserDto dto = ShortUserDto.builder().id(user.getId()).username(user.getUsername())
                    .role(user.getRole()).newToken(newToken).build();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(dto);
        }catch (NullPointerException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/users/get/short/{name}")
    public ResponseEntity<?> getShortUserDataByUsername(@PathVariable String name){
        try {
            User user = userService.findByUsername(name);
            String newToken = TokenUtil.regenerateToken();
            ShortUserDto dto = ShortUserDto.builder().id(user.getId()).username(user.getUsername())
                    .role(user.getRole()).newToken(newToken).build();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(dto);
        }catch (NullPointerException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "api/users/get/{name}")
    public ResponseEntity<?> getUserByName(@PathVariable String name){
        try {
            User user = userService.findByUsername(name);
            HttpHeaders headers = new HttpHeaders();
            String newToken = TokenUtil.regenerateToken();
            headers.add("Authorization", newToken);
            return ResponseEntity.ok().headers(headers).body(userDtoMapper(user));
        }catch (NullPointerException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODER', 'ROLE_USER')")
    @GetMapping(value = "api/users/checkFollow/{profileId}")
    public ResponseEntity<?> checkIfUserFollows(Principal principal, @PathVariable Long profileId){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long loggedId = auth.getPrincipal().getUser().getId();
            User follower = userService.findById(loggedId);
            User following = userService.findById(profileId);
            Map<String, Object> response = new HashMap<>();
            Boolean result = following.getFollowers().contains(follower) && follower.getFollowing().contains(following);
            response.put("follow", result.toString());
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Check follow caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODER', 'ROLE_USER')")
    @PutMapping(value = "api/users/follow/{id}")
    public ResponseEntity<?> follow(Principal principal, @PathVariable Long id){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long loggedId = auth.getPrincipal().getUser().getId();
            User follower = userService.findById(loggedId);

            User following = userService.findById(id);

            userService.follow(follower, following);

            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Following {} caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODER', 'ROLE_USER')")
    @PutMapping(value = "api/users/unfollow/{id}")
    public ResponseEntity<?> unfollow(Principal principal, @PathVariable Long id){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long loggedId = auth.getPrincipal().getUser().getId();
            User follower = userService.findById(loggedId);

            User following = userService.findById(id);

            userService.unfollow(follower, following);

            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Unfollowing {} caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "api/users/get/users/paged")
    public ResponseEntity<PaginatedResponse<UserDto>> getAllUsersSimplified(@RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "15") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username"));
        Page<User> usersPage = userService.findAllByRolePageable(UserRole.USER, pageable);

        Page<UserDto> dtos = usersPage.map(user -> UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<UserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "api/users/get/moders/paged")
    public ResponseEntity<PaginatedResponse<UserDto>> getAllModerators(@RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "15") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username"));
        Page<User> usersPage = userService.findAllByRolePageable(UserRole.MODER, pageable);

        Page<UserDto> dtos =  usersPage.map(user -> UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<UserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "api/users/get/admins/paged")
    public ResponseEntity<PaginatedResponse<UserDto>> getAllAdmins(@RequestParam(defaultValue = "0") int page,
                                          @RequestParam(defaultValue = "15") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username"));
        Page<User> usersPage = userService.findAllByRolePageable(UserRole.ADMIN, pageable);

        Page<UserDto> dtos = usersPage.map(user -> UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<UserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "api/users/get/blockedUsers/paged")
    public ResponseEntity<PaginatedResponse<UserDto>> getAllBlockedUsers(@RequestParam(defaultValue = "0") int page,
                                      @RequestParam(defaultValue = "15") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username"));
        Page<User> usersPage = userService.findAllByIsBlockedPageable(true, pageable);
        Page<UserDto> dtos = usersPage.map(user -> UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<UserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @GetMapping("api/users/checkExist/{id}")
    public ResponseEntity<?> checkIfUserExist(@PathVariable Long id){
        try{
            User user = userService.findById(id);
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            if(user==null){
                response.put("exist", "false");
            }else{
                response.put("exist", "true");
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Check exist user {} caused error {}", id, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    @GetMapping(value = "api/users/get/rating/player/paged")
    public ResponseEntity<PaginatedResponse<UserDto>> getAllPlayersRating(@RequestParam(defaultValue = "0") int page,
                                                                           @RequestParam(defaultValue = "15") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> usersPage = userService.findAllSortedByPlayerRating(pageable);
        Page<UserDto> dtos = usersPage.map(user -> UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .globalPlayerRating(user.getGlobalPlayerRating())
                .globalCreatorRating(user.getGlobalCreatorRating())
                .playerTemplatesNum(user.getPlayedTemplates().size())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<UserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @GetMapping(value = "api/users/get/rating/creator/paged")
    public ResponseEntity<PaginatedResponse<UserDto>> getAllCreatorsRating(@RequestParam(defaultValue = "0") int page,
                                                                               @RequestParam(defaultValue = "15") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> usersPage = userService.findAllSortedByCreatorRating(pageable);
        Page<UserDto> dtos = usersPage.map(user -> UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .globalPlayerRating(user.getGlobalPlayerRating())
                .globalCreatorRating(user.getGlobalCreatorRating())
                .playerTemplatesNum(user.getOwnedTemplates().size())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<UserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @GetMapping(value = "api/users/get/followers/{id}/paged")
    public ResponseEntity<PaginatedResponse<ShortUserDto>> getAllFollowers(@RequestParam(defaultValue = "0") int page,
                                            @RequestParam(defaultValue = "15") int size,
                                              @PathVariable Long id) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username"));
        Page<User> usersPage = userService.findFollowersById(id, pageable);
        Page<ShortUserDto> dtos = usersPage.map(user -> ShortUserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<ShortUserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @GetMapping(value = "api/users/get/following/{id}/paged")
    public ResponseEntity<PaginatedResponse<ShortUserDto>> getAllFollowing(@RequestParam(defaultValue = "0") int page,
                                              @RequestParam(defaultValue = "15") int size,
                                              @PathVariable Long id) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username"));
        Page<User> usersPage = userService.findFollowingById(id, pageable);
        Page<ShortUserDto> dtos = usersPage.map(user -> ShortUserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .isBlocked(user.getIsBlocked())
                .role(user.getRole())
                .build());
        String newToken = TokenUtil.regenerateToken();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", newToken);
        PaginatedResponse<ShortUserDto> response = new PaginatedResponse<>(dtos, newToken);
        return ResponseEntity.ok().headers(headers).body(response);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "api/users/delete/set")
    public ResponseEntity<?> deleteUsersSet(@RequestBody Set<UserDto> userDtoSet){
        try{
            for(UserDto u: userDtoSet){
                userService.delete(userService.findById(u.getId()));
                LOG.info("User {} successfully deleted", u.getId());
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Deleting user set caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODER', 'ROLE_USER')")
    @DeleteMapping(value = "api/users/delete")
    public ResponseEntity<?> deleteCurrentUser(Principal principal){
        try{
            final AuthenticationToken auth = (AuthenticationToken) principal;
            Long userId = auth.getPrincipal().getUser().getId();
            userService.delete(userService.findById(userId));
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            LOG.info("User {} successfully deleted", userId);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Deleting user caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/users/block/set")
    public ResponseEntity<?> blockUsersSet(@RequestBody Set<UserDto> userDtoSet){
        try{
            for(UserDto u: userDtoSet){
                userService.block(userService.findById(u.getId()));
                LOG.info("User {} successfully blocked", u.getId());
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Blocking user set caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODER')")
    @PutMapping(value = "api/users/unblock/set")
    public ResponseEntity<?> unblockUsersSet(@RequestBody Set<UserDto> userDtoSet){
        try{
            for(UserDto u: userDtoSet){
                userService.unblock(userService.findById(u.getId()));
                LOG.info("User {} successfully unblocked", u.getId());
            }
            Map<String, Object> response = new HashMap<>();
            String newToken = TokenUtil.regenerateToken();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", newToken);
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        }catch (Exception e){
            LOG.info("Unblocking user set caused error {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }



    public UserDto userDtoMapper(User user){
        List<TemplateDto> ownedTemplates = new ArrayList<>();
        for(Template t: user.getOwnedTemplates()){
            ownedTemplates.add(new TemplateDto(t.getId(), t.getTemplateName(), null, new ArrayList<>(), user.getId()));
        }
        List<TemplateUsageDto> playerTemplates = new ArrayList<>();
        for(TemplateUsage t: user.getPlayedTemplates()){
            playerTemplates.add(TemplateUsageDto.builder().id(t.getId()).playerId(user.getId())
                            .actualScore(t.getActualScore())
                            .dateOfLastPlay(t.getDateOfLastPlay())
                            .state(t.getState())
                            .parentId(t.getParent().getId())
                            .unfinishedUsageId( t.getUnfinishedDetails()!=null?t.getUnfinishedDetails().getId():null
                            )
                    .build());
        }
        return UserDto.builder().id(user.getId())
                .username(user.getUsername())
                .followers(user.getFollowers().size())
                .following(user.getFollowing().size())
                .ownedTemplates(ownedTemplates)
                .playedTemplates(playerTemplates)
                .role(user.getRole())
                .isBlocked(user.getIsBlocked())
                .globalPlayerRating(user.getGlobalPlayerRating())
                .globalCreatorRating(user.getGlobalCreatorRating())
                .playerTemplatesNum(user.getPlayedTemplates().size())
                .password(user.getPassword())
                .email(user.getEmail()).build();
    }
}
