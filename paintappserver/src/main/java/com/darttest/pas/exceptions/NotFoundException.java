package com.darttest.pas.exceptions;

public class NotFoundException extends AppException{
    public NotFoundException(String message) {
        super(message);
    }
}
