package com.darttest.pas.exceptions;

public class ExistsException extends AppException {

    public ExistsException(String message) {
        super(message);
    }
}

