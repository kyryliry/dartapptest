package com.darttest.pas.exceptions;

public class AccountException extends AppException {
    public AccountException(String message) {
        super(message);
    }
}

