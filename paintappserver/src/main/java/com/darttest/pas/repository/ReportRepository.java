package com.darttest.pas.repository;

import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.CommentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReportRepository extends CommentRepository<Report, Long> {
    List<Report> findAllByReviewer(User reviewer);
    List<Report> findAllByAuthor(User author);
    List<Report> findAllByIsOpen(Boolean isOpen);
    Page<Report> findByIsOpen(Boolean isOpen, Pageable pageable);
    @Query("SELECT r FROM Report r WHERE TYPE(r) = Report")
    Page<Report> findAllReports(Pageable pageable);
    @Query("SELECT r FROM Report r WHERE TYPE(r) = Report")
    List<Report> findAllReports();
    Report findByAuthorAndSubjectIdAndType(User author, Long subjectId, CommentType type);
    List<Report> findReportBySubjectIdAndType(Long subjectId, CommentType type);
}
