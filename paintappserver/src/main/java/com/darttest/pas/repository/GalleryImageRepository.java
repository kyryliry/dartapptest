package com.darttest.pas.repository;

import com.darttest.pas.model.GalleryImage;
import com.darttest.pas.model.Template;
import com.darttest.pas.model.TemplateUsage;
import com.darttest.pas.model.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface GalleryImageRepository extends TemplateImageRepository<GalleryImage, Long> {
    List<GalleryImage> findGalleryImagesByAuthor(User author);
    List<GalleryImage> findGalleryImagesByUsage(TemplateUsage usage);

    List<GalleryImage> findByLikes_Id(Long userId);

    List<GalleryImage> findByDislikes_Id(Long userId);
}
