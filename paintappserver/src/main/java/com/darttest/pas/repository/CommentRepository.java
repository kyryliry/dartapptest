package com.darttest.pas.repository;

import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.CommentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository<T, Long> extends JpaRepository<Comment, Long> {
    @Query("SELECT c FROM Comment c WHERE TYPE(c) = Report")
    List<Report> findAllReports();

    @Query("SELECT c FROM Comment c WHERE TYPE(c) = UserComment")
    List<UserComment> findAllUserComments();
    List<Comment> findBySubjectIdAndType(Long subjectId, CommentType type);
}
