package com.darttest.pas.repository;

import com.darttest.pas.model.Category;
import com.darttest.pas.model.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TemplateRepository extends JpaRepository<Template, Long> {
    public Template findByTemplateName(String templateName);
    @Query("SELECT t FROM Template t JOIN t.categories c WHERE c IN :categories")
    public Set<Template> findAllByCategories(@Param("categories") Set<Category> categories);

    List<Template> findByLikes_Id(Long userId);

    List<Template> findByDislikes_Id(Long userId);
}
