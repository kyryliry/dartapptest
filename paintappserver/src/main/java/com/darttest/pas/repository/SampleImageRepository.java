package com.darttest.pas.repository;

import com.darttest.pas.model.SampleImage;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleImageRepository extends TemplateImageRepository<SampleImage, Long> {
}
