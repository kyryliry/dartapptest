package com.darttest.pas.repository;

import com.darttest.pas.model.TemplateImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateImageRepository<T, Long> extends JpaRepository<TemplateImage, Long> {
}
