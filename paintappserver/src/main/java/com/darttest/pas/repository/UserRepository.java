package com.darttest.pas.repository;

import com.darttest.pas.model.User;
import com.darttest.pas.model.enums.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    List<User> findAllByRole(UserRole role);
    Page<User> findAllByRole(UserRole role, Pageable pageable);
    Page<User> findAllByIsBlocked(Boolean isBlocked, Pageable pageable);
    @Query("SELECT f FROM User u JOIN u.followers f WHERE u.id = :userId")
    Page<User> findFollowersById(@Param("userId") Long userId, Pageable pageable);
    @Query("SELECT f FROM User u JOIN u.following f WHERE u.id = :userId")
    Page<User> findFollowingById(@Param("userId") Long userId, Pageable pageable);
    Page<User> findAllByOrderByGlobalPlayerRatingDesc(Pageable pageable);
    Page<User> findAllByOrderByGlobalCreatorRatingDesc(Pageable pageable);
}
