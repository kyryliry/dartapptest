package com.darttest.pas.repository;

import com.darttest.pas.model.Report;
import com.darttest.pas.model.Template;
import com.darttest.pas.model.User;
import com.darttest.pas.model.UserComment;
import com.darttest.pas.model.enums.CommentType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCommentRepository extends CommentRepository<UserComment, Long> {

    List<UserComment> findUserCommentBySubjectIdAndType(Long subjectId, CommentType type);

    List<UserComment> findAllByAuthor(User author);
}
