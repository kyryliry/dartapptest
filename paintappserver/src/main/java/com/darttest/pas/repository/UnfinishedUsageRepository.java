package com.darttest.pas.repository;

import com.darttest.pas.model.UnfinishedUsage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnfinishedUsageRepository extends JpaRepository<UnfinishedUsage, Long> {
}
