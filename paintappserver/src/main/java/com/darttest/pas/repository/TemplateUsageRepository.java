package com.darttest.pas.repository;

import com.darttest.pas.model.GalleryImage;
import com.darttest.pas.model.Template;
import com.darttest.pas.model.TemplateUsage;
import com.darttest.pas.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TemplateUsageRepository extends JpaRepository<TemplateUsage, Long> {
    List<TemplateUsage> findTemplateUsagesByPlayer(User player);

    List<TemplateUsage> findTemplateUsagesByParent(Template parent);
    TemplateUsage findTemplateUsageByPlayerAndParent(User player, Template parent);
}
