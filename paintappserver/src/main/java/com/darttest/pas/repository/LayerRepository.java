package com.darttest.pas.repository;

import com.darttest.pas.model.Layer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LayerRepository extends JpaRepository<Layer, Long> {

    public List<Layer> findByTemplateId(Long templateId);
}
