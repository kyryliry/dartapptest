package com.darttest.pas.model.enums;

public enum CommentType {
    TEMPLATE("TEMPLATE"), IMAGE("IMAGE"), USER("USER"), COMMENT("COMMENT");

    private final String name;

    CommentType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
