package com.darttest.pas.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "attachments")
public class Attachment extends AbstractEntity{
    private String extension;
    private String downloadLink;
}
