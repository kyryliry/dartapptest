package com.darttest.pas.model;

import com.darttest.pas.model.enums.CommentType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true, exclude = {"replies", "likes", "dislikes"})
@NoArgsConstructor
@Entity
@Data
@ToString(exclude = {"replies", "likes", "dislikes"})
@Table(name = "userComments")
@DiscriminatorValue(value = "USER")
public class UserComment extends Comment{
    @OneToMany
    private List<UserComment> replies;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "userComment_likes",
            joinColumns = @JoinColumn(name = "userComment_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> likes;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "userComment_dislikes",
            joinColumns = @JoinColumn(name = "userComment_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> dislikes;

    public UserComment(String text, LocalDateTime date, Long subjectId, CommentType type, User author, List<UserComment> replies, List<User> likes, List<User> dislikes) {
        super(text, date, subjectId, type, author);
        this.replies = replies;
        this.likes = likes;
        this.dislikes = dislikes;
    }
}
