package com.darttest.pas.model;

import com.darttest.pas.model.enums.CommentType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true, exclude = {"reviewer"})
@NoArgsConstructor
@Entity
@Data
@ToString(exclude = {"reviewer"})
@Table(name = "reports")
@DiscriminatorValue(value = "REPORT")
public class Report extends Comment{
    private Boolean isOpen;
    private String closeReason;
    private LocalDateTime closeDate;
    private String authorUsername;
    @ManyToOne
    private User reviewer;
    public Report(String text, LocalDateTime date, Long subjectId, CommentType type, User author, Boolean isOpen, String closeReason, LocalDateTime closeDate, String authorUsername, User reviewer) {
        super(text, date, subjectId, type, author);
        this.isOpen = isOpen;
        this.closeReason = closeReason;
        this.closeDate = closeDate;
        this.authorUsername = authorUsername;
        this.reviewer = reviewer;
    }
}
