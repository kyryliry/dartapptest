package com.darttest.pas.model.enums;

public enum TemplateState {
    DRAFT("DRAFT"), PUBLISHED("PUBLISHED"), VERIFIED("VERIFIED"), BLOCKED("BLOCKED"), DELETED("DELETED");

    private final String name;

    TemplateState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
