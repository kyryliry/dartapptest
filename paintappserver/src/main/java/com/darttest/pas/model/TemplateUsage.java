package com.darttest.pas.model;

import com.darttest.pas.model.enums.UsageState;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true, exclude = {"unfinishedDetails", "player", "parent"})
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "templateUsages")
public class TemplateUsage extends AbstractEntity{

    private Double actualScore;
    private LocalDateTime dateOfLastPlay;
    @ManyToOne(cascade = CascadeType.MERGE)
    @ToString.Exclude
    private User player;
    @ManyToOne(cascade = CascadeType.MERGE)
    @ToString.Exclude
    private Template parent;
    @OneToOne(mappedBy = "usage", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private UnfinishedUsage unfinishedDetails;
    private UsageState state;
}
