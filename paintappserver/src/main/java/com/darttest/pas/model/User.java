package com.darttest.pas.model;

import com.darttest.pas.model.enums.UserRole;
import lombok.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true, exclude = {"playedTemplates", "ownedTemplates", "followers", "following"})
@ToString(exclude = {"playedTemplates", "ownedTemplates", "followers", "following"})
@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User extends AbstractEntity{
    private String username;
    private String password;
    private String email;
    private Boolean isBlocked;
    private Double globalPlayerRating;
    private Double globalCreatorRating;
    @OneToMany(mappedBy = "creator")
    private List<Template> ownedTemplates;
    @ManyToMany
    @JoinTable(
            name = "users_followers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "followers_id")
    )
    private Set<User> followers;

    @ManyToMany
    @JoinTable(
            name = "users_following",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "following_id")
    )
    private Set<User> following;
    @OneToMany(mappedBy = "player")
    private List<TemplateUsage> playedTemplates;
    private UserRole role;
    public void encodePassword(PasswordEncoder passwordEncoder) {
        password = passwordEncoder.encode(password);
    }

    public void erasePassword() {
        this.password = null;
    }

}
