package com.darttest.pas.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
@ToString(callSuper = true)
@Entity
@Table(name = "galleryImages")
@DiscriminatorValue(value = "GALLERY")
public class GalleryImage extends TemplateImage{
    private LocalDateTime uploadDate;
    @ManyToOne(cascade = CascadeType.MERGE)
    private User author;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "galleryimage_likes",
            joinColumns = @JoinColumn(name = "galleryimage_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> likes;

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "galleryimage_dislikes",
            joinColumns = @JoinColumn(name = "galleryimage_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> dislikes;
    @ManyToOne
    private TemplateUsage usage;
    private double actualScore;
    @OneToMany
    private List<UserComment> comments;

    public GalleryImage(String description, Attachment imageAtch, LocalDateTime uploadDate, User author, List<User> likes, List<User> dislikes, TemplateUsage usage, double actualScore) {
        super(description, imageAtch);
        this.uploadDate = uploadDate;
        this.author = author;
        this.likes = likes;
        this.dislikes = dislikes;
        this.usage = usage;
        this.actualScore = actualScore;
    }
}
