package com.darttest.pas.model;

import javax.persistence.*;

import com.darttest.pas.model.enums.Difficulty;
import com.darttest.pas.model.enums.TemplateState;
import lombok.*;
import com.fasterxml.jackson.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true, exclude = {"images", "sampleImage", "layers", "categories", "creator", "likes", "dislikes", "comments", "downloads"})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "templates")

public class Template extends AbstractEntity{

    private String templateName;
    private double maxScore;
    private LocalDateTime uploadDate;
    @OneToMany
    private List<GalleryImage> images;
    @OneToOne
    @ToString.Exclude
    private SampleImage sampleImage;
    @OneToMany(mappedBy = "template")
    @JsonIgnore
    private List<Layer> layers;
    private TemplateState state;
    @ManyToMany
    private Set<Category> categories;
    @ManyToOne(cascade = {CascadeType.MERGE})
    private User creator;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "template_likes",
            joinColumns = @JoinColumn(name = "template_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> likes;

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "template_dislikes",
            joinColumns = @JoinColumn(name = "template_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> dislikes;
    @OneToMany
    private List<UserComment> comments;
    private Difficulty difficulty;
    private long downloadsCount;

}
