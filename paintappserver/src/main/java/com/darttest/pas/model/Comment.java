package com.darttest.pas.model;

import com.darttest.pas.model.enums.CommentType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true, exclude = {"author"})
@ToString(exclude = {"author"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "comments")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING)

public abstract class Comment extends AbstractEntity{
    private String text;
    private LocalDateTime date;
    private Long subjectId;
    private CommentType type;
    @ManyToOne(cascade = CascadeType.MERGE)
    private User author;
}
