package com.darttest.pas.model.enums;

public enum UserRole {
    GUEST("ROLE_GUEST"), USER("ROLE_USER"), MODER("ROLE_MODER"), ADMIN("ROLE_ADMIN");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
