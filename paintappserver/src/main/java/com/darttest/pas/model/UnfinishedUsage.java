package com.darttest.pas.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true, exclude={"actualLayerAtch", "finishedLayerAtch", "actualLayer", "usage"})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "unfinishedUsages")
public class UnfinishedUsage extends AbstractEntity{
    @OneToOne
    private Attachment actualLayerAtch;
    @OneToOne
    private Attachment finishedLayerAtch;
    @OneToOne
    private Layer actualLayer;
    @OneToOne(optional = false)
    @ToString.Exclude
    private TemplateUsage usage;
}
