package com.darttest.pas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;


@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Data
@ToString(callSuper = true)
@Table(name = "sampleImages")
@DiscriminatorValue(value = "SAMPLE")
public class SampleImage extends TemplateImage{
    @OneToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnore
    @ToString.Exclude
    private Template template;

    public SampleImage(String description, Attachment imageAtch, Template template) {
        super(description, imageAtch);
        this.template = template;
    }
}
