package com.darttest.pas.model;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "templateImages")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING)

public abstract class TemplateImage extends AbstractEntity{

    private String description;
    @OneToOne
    private Attachment imageAtch;
}
