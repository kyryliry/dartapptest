package com.darttest.pas.model.enums;

public enum UsageState {
    FINISHED("FINISHED"), UNFINISHED("UNFINISHED"), INPROGRESS("INPROGRESS");

    private final String name;

    UsageState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
