package com.darttest.pas.model;
import javax.persistence.*;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "layers")
public class Layer extends AbstractEntity{
    private Integer brushSize;
    private String brushColor;
    private String showingColor;
    private Integer number;
    @OneToOne
    private Attachment layerImage;
    @ManyToOne(optional = false)
    private Template template;
}
