package com.darttest.pas.utils;

import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import io.jsonwebtoken.Jwts;
import java.util.stream.Collectors;

import static com.darttest.pas.utils.Constants.DEFAULT_KEY;

public class TokenUtil {

    public static String regenerateToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            Date now = new Date();
            Date expiryDate = new Date(now.getTime() + 3_600_000); // Token expires in 1 hour
            Key SECRET_KEY = Keys.hmacShaKeyFor(DEFAULT_KEY.getBytes(StandardCharsets.UTF_8));
            return Jwts.builder()
                    .setSubject(userDetails.getUsername())
                    .claim("authorities", authentication.getAuthorities().stream()
                            .map(grantedAuthority -> grantedAuthority.getAuthority())
                            .collect(Collectors.toList()))
                    .setIssuedAt(now)
                    .setExpiration(expiryDate)
                    .signWith(SECRET_KEY)
                    .compact();
        }
        return null;
    }
}

