package com.darttest.pas.utils;

import org.springframework.data.domain.Page;

public class PaginatedResponse<T> {
    private Page<T> page;
    private String newToken;

    public PaginatedResponse(Page<T> page, String newToken) {
        this.page = page;
        this.newToken = newToken;
    }

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }

    public String getToken() {
        return newToken;
    }

    public void setToken(String newToken) {
        this.newToken = newToken;
    }
}

