package com.darttest.pas.utils;

public class Constants {
    public static String UPLOAD_DIRECTORY = System.getProperty("user.dir") + "/uploads";
    public static String SAMPLE_DIRECTORY = System.getProperty("user.dir") + "/templatesSample";
    public static String UNF_USAGE_DIRECTORY = System.getProperty("user.dir") + "/unfinishedUsages";
    public static String GALLERY_DIRECTORY = System.getProperty("user.dir") + "/galleryImages";
    public static String DEFAULT_KEY = "default-application-key-for-signing";
}
