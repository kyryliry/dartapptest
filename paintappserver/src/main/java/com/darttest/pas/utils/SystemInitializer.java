package com.darttest.pas.utils;

import com.darttest.pas.controller.CategoryController;
import com.darttest.pas.dto.CategoryDto;
import com.darttest.pas.dto.LayerSaveDto;
import com.darttest.pas.model.*;
import com.darttest.pas.model.enums.TemplateState;
import com.darttest.pas.model.enums.UserRole;
import com.darttest.pas.service.AttachmentService;
import com.darttest.pas.service.LayerService;
import com.darttest.pas.service.TemplateService;
import com.darttest.pas.service.UserService;
import com.darttest.pas.service.serviceImpl.SampleImageServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Component
public class SystemInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInitializer.class);
    private final UserService userService;
    private final TemplateService templateService;
    private final AttachmentService attachmentService;
    private final LayerService layerService;
    private final SampleImageServiceImpl sampleImageService;
    private final PlatformTransactionManager txManager;
    @Autowired
    public SystemInitializer(UserService userService, TemplateService templateService, AttachmentService attachmentService, LayerService layerService, SampleImageServiceImpl sampleImageService, PlatformTransactionManager txManager) {
        this.userService = userService;
        this.templateService = templateService;
        this.attachmentService = attachmentService;
        this.layerService = layerService;
        this.sampleImageService = sampleImageService;
        this.txManager = txManager;
    }




    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            generateAdmin();
            generateTemplate();
            return null;
        });
    }

    private void generateAdmin() {
        if (userService.findByUsername("admin")!=null) {
            return;
        }
        User user = User.builder().username("admin")
                .following(new HashSet<>())
                .followers(new HashSet<>())
                .ownedTemplates(new ArrayList<>())
                .playedTemplates(new ArrayList<>())
                .role(UserRole.ADMIN)
                .isBlocked(false)
                .globalPlayerRating(0.0)
                .globalCreatorRating(0.0)
                .password("admin")
                .email("admin@admin.admin").build();
        userService.persist(user);
        LOG.info("System init admin successfully generated");
    }

    private void generateTemplate(){
        try {
            if(templateService.findByName("admin_template")!=null){
                return;
            }
            Map<Integer, String> brushColors = new HashMap<>();
            brushColors.put(0, "#fffaf1e7");
            brushColors.put(1, "#ff857661");
            brushColors.put(2, "#fffba3d6");
            brushColors.put(3, "#ffffffff");
            brushColors.put(4, "#ff000000");
            Map<Integer, String> showingColors = new HashMap<>();
            showingColors.put(0, "#ff1c4b80");
            showingColors.put(1, "#ff7a899e");
            showingColors.put(2, "#ff045c29");
            showingColors.put(3, "#ff919add");
            showingColors.put(4, "#ffd26464");
            User user = userService.findByUsername("admin");
            Template template = Template.builder().templateName(user.getUsername() + "_" + "template").maxScore(50).uploadDate(LocalDateTime.now()).creator(user).layers(new ArrayList<>()).state(TemplateState.PUBLISHED).build();

            ClassPathResource classPathResource = new ClassPathResource("assets/template.png");
            MultipartFile sampleFile = new FileResourceMultipartFile(classPathResource, "image/png");


            Attachment attachment = attachmentService.saveNewLayerAttachment(sampleFile, template.getTemplateName(), 0, Constants.SAMPLE_DIRECTORY);
            templateService.persist(template);
            user.setGlobalCreatorRating((double) user.getOwnedTemplates().size());
            userService.update(user);
            SampleImage sampleImage = new SampleImage("", attachment, template);
            template.setSampleImage(sampleImage);
            sampleImageService.persist(sampleImage);
            templateService.update(template);

            for(int i=0; i<5; i++){
                ClassPathResource layerPath = new ClassPathResource("assets/template"+i+".png");
                MultipartFile layerFile = new FileResourceMultipartFile(layerPath, "image/png");
                Attachment attachment1 = attachmentService.saveNewLayerAttachment(layerFile, template.getTemplateName(), i, Constants.UPLOAD_DIRECTORY);
                Layer lay = Layer.builder()
                        .template(template)
                        .brushColor(brushColors.get(i))
                        .showingColor(showingColors.get(i))
                        .number(i)
                        .brushSize(10)
                        .layerImage(attachment1).build();
                layerService.persist(lay);
                template.getLayers().add(lay);

            }
            templateService.update(template);
            LOG.info("System init template successfully generated");
        }catch (IOException e){
            LOG.info("System init template generation caused error {}", e.getMessage());
        }
    }
}


