import 'package:perfect_freehand/perfect_freehand.dart';

class Stroke {
  final List<Point> points;
  final bool isEraser;
  
  const Stroke(this.points, this.isEraser);
}
