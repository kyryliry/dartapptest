import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:paintappdart/templatesLibrary/main_page.dart';
import 'package:paintappdart/templatesLibrary/ratings_page.dart';
import 'package:paintappdart/utils/session_manager.dart';
import 'package:paintappdart/model/user_model.dart';

import '../templatesLibrary/categories_page.dart';
import '../templatesLibrary/library_main_page.dart';
import '../templatesLibrary/login_page.dart';
import '../templatesLibrary/profile_page.dart';
import '../templatesLibrary/reports_page.dart';
import '../templatesLibrary/roles_page.dart';
import '../templatesLibrary/subscriptions_page.dart';
import '../templatesLibrary/user_library_page.dart';

class AppBarWithBack extends StatefulWidget {
  UserModel? userDto;
  bool withBack = true;
  AppBarWithBack({Key? key, this.userDto, this.withBack=true}) : super(key: key);
  @override
  AppBarWithBackState createState() => AppBarWithBackState();
}

class AppBarWithBackState extends State<AppBarWithBack> {
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if(widget.withBack)
            GestureDetector(
              onTap:(){
                Navigator.pop(context);
              },
              child: const Icon(
                  Icons.arrow_back_ios_new
              ),
            ),
            TextButton(
              onPressed: () => {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => MainPage(userDto: widget.userDto)),
                      (Route<dynamic> route) => false,
                ),
              },
              child: const Text('Main Page'),
            ),
            TextButton(
              onPressed: () => {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LibraryMainPage(name: widget.userDto!=null? widget.userDto!.userName: null)),
                      (Route<dynamic> route) => false,
                ),
                 },
              child: const Text('Library'),
            ),
            TextButton(
              onPressed: () => {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => RatingsPage(userDto: widget.userDto)),
                      (Route<dynamic> route) => false,
                ),
            },
              child: const Text('Rating'),
            ),
            // TextButton(
            //   onPressed: () => {},
            //   child: const Text('Search'),
            // ),
            // TextButton(
            //   onPressed: () => {},
            //   child: const Text('FAQ & Help'),
            // ),
            GestureDetector(
              onTap: () {
                if(widget.userDto!=null) {
                  showMenu(
                    context: context,
                    position: RelativeRect.fromLTRB(
                      screenSize.width,
                      60,
                      0,
                      screenSize.height,
                    ),
                    items: [
                      PopupMenuItem<String>(
                        value: 'Profile',
                        child: const Text('Profile'),
                          onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(userDto: widget.userDto, profileId: widget.userDto!.id,)));}
                      ),
                      PopupMenuItem<String>(
                          value: 'My library',
                          child: const Text('My library'),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => UserLibraryPage(userDto: widget.userDto!)),
                            );}
                      ),
                      PopupMenuItem<String>(
                        value: 'Subscriptions',
                        child: const Text('Subscriptions'),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => SubscriptionsPage(userDto: widget.userDto, profileId: widget.userDto!.id)));
                        },
                      ),
                      if(widget.userDto!.role=="MODER" || widget.userDto!.role=="ADMIN")
                        const PopupMenuItem<String>(
                          value: 'Verifications',
                          child: Text('Verifications'),
                        ),
                      if(widget.userDto!.role=="MODER" || widget.userDto!.role=="ADMIN")
                        PopupMenuItem<String>(
                          value: 'Reports',
                          child: const Text('Reports'),
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ReportsPage(userDto: widget.userDto!)));
                          },
                        ),
                      if(widget.userDto!.role=="ADMIN")
                        PopupMenuItem<String>(
                            value: 'Categories',
                            child: const Text('Categories'),
                            onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => CategoriesPage(userDto: widget.userDto!)));}
                        ),
                      if(widget.userDto!.role=="ADMIN")
                        PopupMenuItem<String>(
                            value: 'Roles',
                            child: const Text('Roles'),
                            onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => RolesPage(userDto: widget.userDto!)));}
                        ),
                      PopupMenuItem<String>(
                        value: 'Logout',
                        child: const Text('Logout'),
                        onTap: () {
                          logout();
                        },
                      ),
                    ],
                    elevation: 8.0,
                  );
                }else{
                  showMenu(
                    context: context,
                    position: RelativeRect.fromLTRB(
                      screenSize.width,
                      60,
                      0,
                      screenSize.height,
                    ),
                    items: [
                      PopupMenuItem<String>(
                        value: 'Login',
                        child: const Text('Login'),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (
                                context) => const LoginPage()),
                          );
                        },
                      ),
                    ],
                  );
                }
              },
              child: CircleAvatar(
                backgroundColor: Colors.deepPurple.shade100,
                radius: 30.0,
                child: const Text('Me'),
              ),
            ),
          ],
        );
  }

  Future<void> logout() async {
    String? jsessionId = SessionManager().getJsessionId();
    final temps = await get(Uri.parse("http://localhost:8080/logout"),
        headers: {
          'Authorization': jsessionId!,
          'Content-Type': 'application/json'
        });

    if(temps.statusCode==200){
      SessionManager().setJsessionId("");
      if(mounted) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (
              context) => LibraryMainPage()),
              (Route<
              dynamic> route) => false,
        );
      }
    }
  }
}