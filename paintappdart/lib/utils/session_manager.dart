class SessionManager {
  static final SessionManager _instance = SessionManager._internal();
  String? jsessionId;

  factory SessionManager() {
    return _instance;
  }

  SessionManager._internal();

  void setJsessionId(String id) {
    jsessionId = id;
  }

  String? getJsessionId() {
    return jsessionId;
  }
}

