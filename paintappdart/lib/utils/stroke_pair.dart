import 'package:paintappdart/utils/stroke.dart';
import 'package:paintappdart/utils/stroke_options.dart';

class StrokePair {
  final Stroke stroke;
  final StrokeOptions options;

  const StrokePair(this.stroke, this.options);
}