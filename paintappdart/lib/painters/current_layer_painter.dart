import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:perfect_freehand/perfect_freehand.dart';
import '../utils/stroke.dart';
import '../utils/stroke_options.dart';
import 'package:flutter/material.dart' hide Image;

class CurrentLayerPainter extends CustomPainter {
  final List<Stroke> lines;
  final StrokeOptions options;
  final List<StrokeOptions> optionList;
  final List<Color> colors;
  Color currentColor = Colors.black;
  final ui.Image? backgroundImage;
  final double? resizeFactor;

  CurrentLayerPainter({required this.currentColor, required this.optionList, required this.lines, required this.options, required this.colors, this.backgroundImage, this.resizeFactor});

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    Paint paint = Paint();
    canvas.saveLayer(null, paint);
    if (backgroundImage != null && resizeFactor!=null) {
      paint.blendMode = BlendMode.srcOver;
      canvas.drawImageRect(backgroundImage!, Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble(), backgroundImage!.height.toDouble()), Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble()*resizeFactor!, backgroundImage!.height.toDouble()*resizeFactor!), paint);

    }
    paint.color = currentColor;

    for (int i = 0; i < lines.length; ++i) {
      if(colors.length==lines.length){
        paint.color = colors[i];
      }
      if (lines[i].isEraser) {
        paint.blendMode = BlendMode.clear;
      } else {
        paint.blendMode = BlendMode.srcOver;
      }
      final outlinePoints = getStroke(
        lines[i].points,
        size: optionList[i].size,
        thinning: optionList[i].thinning,
        smoothing: optionList[i].smoothing,
        streamline: optionList[i].streamline,
        taperStart: optionList[i].taperStart,
        capStart: optionList[i].capStart,
        taperEnd: optionList[i].taperEnd,
        capEnd: optionList[i].capEnd,
        simulatePressure: optionList[i].simulatePressure,
        isComplete: optionList[i].isComplete,
      );

      final path = Path();

      if (outlinePoints.isEmpty) {
        return;
      } else if (outlinePoints.length < 2) {
        path.addOval(Rect.fromCircle(
            center: Offset(outlinePoints[0].x, outlinePoints[0].y),
            radius: 1));
      } else {
        path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

        for (int i = 1; i < outlinePoints.length - 1; ++i) {
          final p0 = outlinePoints[i];

          final p1 = outlinePoints[i + 1];
          path.quadraticBezierTo(
              p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
        }
      }

      canvas.drawPath(path, paint);
    }

    canvas.restore();
  }

  @override
  bool shouldRepaint(CurrentLayerPainter oldDelegate) {
    return true;
  }
}
