import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:perfect_freehand/perfect_freehand.dart';
import '../utils/stroke.dart';
import '../utils/stroke_options.dart';
import 'package:flutter/material.dart' hide Image;

class SingleLinePainter extends CustomPainter {
  final Stroke? line;
  final StrokeOptions options;
  Color currentColor = Colors.black;

  SingleLinePainter(
      {required this.currentColor, required this.line, required this.options});

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    if(line==null){
      return;
    }
    Paint paint = Paint();
    var _blendMode = BlendMode.srcOver;

    if (line!.isEraser) {
      canvas.saveLayer(null, Paint());

      _blendMode = BlendMode.clear;
    }
    paint.blendMode = _blendMode;
    paint.color = currentColor;
    final outlinePoints = getStroke(
      line!.points,
      size: options.size,
      thinning: options.thinning,
      smoothing: options.smoothing,
      streamline: options.streamline,
      taperStart: options.taperStart,
      capStart: options.capStart,
      taperEnd: options.taperEnd,
      capEnd: options.capEnd,
      simulatePressure: options.simulatePressure,
      isComplete: options.isComplete,
    );

    final path = Path();

    if (outlinePoints.isEmpty) {
      return;
    } else if (outlinePoints.length < 2) {
      path.addOval(Rect.fromCircle(
          center: Offset(outlinePoints[0].x, outlinePoints[0].y), radius: 1));
    } else {
      path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

      for (int i = 1; i < outlinePoints.length - 1; ++i) {
        final p0 = outlinePoints[i];

        final p1 = outlinePoints[i + 1];
        path.quadraticBezierTo(
            p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
      }
    }

    canvas.drawPath(path, paint);
    if (line!.isEraser) {
      canvas.restore();
    }

  }

  @override
  bool shouldRepaint(SingleLinePainter oldDelegate) {
    return true;
  }
}
