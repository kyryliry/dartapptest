
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:paintappdart/utils/stroke_pair.dart';
import 'package:perfect_freehand/perfect_freehand.dart';
import 'package:paintappdart/utils/stroke_options.dart';

import '../model/layer_model.dart';

class SavePaint{
  final StrokeOptions options;
  final List<List<StrokePair>> everyLineHasOptionListOfAll;
  int id = 0;
  List<String> pathes = <String>[];
  List<Uint8List> pathesFile = <Uint8List>[];
  List<List<Offset>> allPixels = <List<Offset>>[];
  final List<Color> layersColors;
  Color currentColor = Colors.black;
  Map<LayerModel, ui.Image> layers = <LayerModel, ui.Image>{};
  Map<int, LayerModel> layersData = <int, LayerModel>{};
  final Function(Map<LayerModel, ui.Image>, ui.Image) onMapGenerated;
  final double resizeFactor;

  SavePaint({required this.everyLineHasOptionListOfAll, required this.options, required this.layersColors, required this.onMapGenerated, required this.layersData, required this.resizeFactor});

  Color invert(Color color) {
    if(color==Colors.black){
      return Color.fromARGB((color.opacity * 255).round(), 210, 100, 100);
    }
    final r = 255 - color.red;
    final g = 255 - color.green;
    final b = 255 - color.blue;

    return Color.fromARGB((color.opacity * 255).round(), r, g, b);
  }

  Future<void> save(double sizex, double sizey, double pixelRatio) async {
    PictureRecorder pR = PictureRecorder();
    Canvas wholeCanvas = Canvas(pR);
    wholeCanvas.drawColor(Colors.transparent, BlendMode.src);

    for(int i=0; i< everyLineHasOptionListOfAll.length; ++i){
        PictureRecorder pictureRecorder = PictureRecorder();

        Canvas canvas = Canvas(pictureRecorder);
        canvas.drawColor(Colors.transparent, BlendMode.src);
        Paint paint = Paint();
        if(layersColors.length==everyLineHasOptionListOfAll.length) {
          paint.color = layersColors[i];
        }else{
          paint.color = currentColor;
        }
      for(int j=0; j<everyLineHasOptionListOfAll[i].length; ++j){
          if(everyLineHasOptionListOfAll[i][j].stroke.isEraser){
            paint.blendMode = BlendMode.clear;
          }else{
            if(layersColors.length==everyLineHasOptionListOfAll.length) {
              paint.color = layersColors[i];
            }else{
              paint.color = currentColor;
            }
            paint.blendMode = BlendMode.srcOver;
          }
          final outlinePoints = getStroke(
            everyLineHasOptionListOfAll[i][j].stroke.points,
            size:everyLineHasOptionListOfAll[i][j].options.size,
            thinning: everyLineHasOptionListOfAll[i][j].options.thinning,
            smoothing: everyLineHasOptionListOfAll[i][j].options.smoothing,
            streamline: everyLineHasOptionListOfAll[i][j].options.streamline,
            taperStart: everyLineHasOptionListOfAll[i][j].options.taperStart,
            capStart: everyLineHasOptionListOfAll[i][j].options.capStart,
            taperEnd: everyLineHasOptionListOfAll[i][j].options.taperEnd,
            capEnd: everyLineHasOptionListOfAll[i][j].options.capEnd,
            simulatePressure: everyLineHasOptionListOfAll[i][j].options.simulatePressure,
            isComplete: everyLineHasOptionListOfAll[i][j].options.isComplete,
          );

          final path = Path();

          if (outlinePoints.isEmpty) {
            return;
          } else if (outlinePoints.length < 2) {
            path.addOval(Rect.fromCircle(
                center: Offset(outlinePoints[0].x, outlinePoints[0].y),
                radius: 1));
          } else {
            path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

            for (int i = 1; i < outlinePoints.length - 1; ++i) {
              final p0 = outlinePoints[i];
              final p1 = outlinePoints[i + 1];
              path.quadraticBezierTo(
                  p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
            }
          }
          canvas.drawPath(path, paint);
          wholeCanvas.drawPath(path, paint);
      }

        Picture picture = pictureRecorder.endRecording();
        ui.Image image = await picture.toImage(
            (sizex).toInt(),
            (sizey).toInt());

        ui.PictureRecorder recorder = ui.PictureRecorder();
        canvas = Canvas(recorder);
      Rect srcRect = Offset.zero & Size(image.width.toDouble(), image.height.toDouble());
      Rect dstRect = Offset.zero & Size(sizex * pixelRatio, sizey * pixelRatio);
      canvas.drawImageRect(image, srcRect, dstRect, Paint());

      picture = recorder.endRecording();
      ui.Image croppedImage = await picture.toImage(
          (sizex * pixelRatio).toInt(),
          (sizey * pixelRatio).toInt());

        if(resizeFactor!=1) {
        final ui.PictureRecorder recorder = ui.PictureRecorder();
          canvas = Canvas(recorder);
          paint = Paint();

          Rect srcRect1 = Offset.zero & Size(image.width.toDouble(), image.height.toDouble());
          Rect dstRect1 = Offset.zero & Size((sizex * pixelRatio)/resizeFactor, (sizey * pixelRatio)/resizeFactor);

          canvas.drawImageRect(image, srcRect1, dstRect1, Paint());
          picture = recorder.endRecording();

          croppedImage = await picture.toImage(
              (sizex * pixelRatio)~/resizeFactor,
              (sizey * pixelRatio)~/resizeFactor);
        }
        var showColor = '#${invert(layersColors[i]).value.toRadixString(16)}';
        var strokeSize = everyLineHasOptionListOfAll[i].last.options.size.toInt();
        if(layersData.isNotEmpty) {

          if (layersData.containsKey(i + 1)) {
          LayerModel value = layersData[i + 1]!;
            showColor = value.showColor;
            strokeSize = value.strokeSize;
          }
        }

        layers.putIfAbsent(LayerModel(templateId: 0, strokeSize: strokeSize, color: '#${layersColors[i].value.toRadixString(16)}', showColor: showColor, number: i+1), () => croppedImage);
      }


    Picture p = pR.endRecording();

    ui.Image image = await p.toImage(
        (sizex).toInt(),
        (sizey).toInt());
    ui.PictureRecorder recorder = ui.PictureRecorder();
    wholeCanvas = Canvas(recorder);

    Rect srcRect = Offset.zero & Size(image.width.toDouble(), image.height.toDouble());
    Rect dstRect = Offset.zero & Size(sizex * pixelRatio, sizey * pixelRatio);

    wholeCanvas.drawImageRect(image, srcRect, dstRect, Paint());

    p = recorder.endRecording();
    ui.Image sampleImage = await p.toImage(
        (sizex * pixelRatio).toInt(),
        (sizey * pixelRatio).toInt());

    if(resizeFactor!=1) {
      final ui.PictureRecorder recorder = ui.PictureRecorder();
      wholeCanvas = Canvas(recorder);

      Paint paint = Paint();
      Rect srcRect1 = Offset.zero & Size(image.width.toDouble(), image.height.toDouble());
      Rect dstRect1 = Offset.zero & Size((sizex * pixelRatio)/resizeFactor, (sizey * pixelRatio)/resizeFactor);

      wholeCanvas.drawImageRect(image, srcRect1, dstRect1, Paint());
      p = recorder.endRecording();

      sampleImage = await p.toImage(
          (sizex * pixelRatio)~/resizeFactor,
          (sizey * pixelRatio)~/resizeFactor);
    }
    onMapGenerated(layers, sampleImage);
    }
}