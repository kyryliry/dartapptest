
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/utils/stroke_pair.dart';
import 'package:paintappdart/model/unfinished_usage_model.dart';
import 'package:perfect_freehand/perfect_freehand.dart';
import 'package:paintappdart/utils/stroke_options.dart';

import '../utils/session_manager.dart';
import '../utils/stroke.dart';

class SaveUnfinishedPainter{
  final logger = Logger(
    printer: SimplePrinter(),
  );
  final List<List<StrokePair>> everyLineHasOptionListOfAll;
  final List<Color> layersColors;
  Color currentColor = Colors.black;
  final double resizeFactor;
  final List<Stroke> lines;
  final StrokeOptions options;
  final List<StrokeOptions> optionList;
  final List<Color> colors;
  final UnfinishedUsageModel unfinishedUsageDto;
  final String templateName;
  final ui.Image? backgroundImage;
  final ui.Image? oldFinImage;
  final bool? isSameLayer;
  final int? layerNumber;
  final double accuracy;

  SaveUnfinishedPainter({required this.everyLineHasOptionListOfAll,
    required this.layersColors, required this.resizeFactor,
    required this.currentColor, required this.optionList, required this.lines,
    required this.options, required this.colors, required this.unfinishedUsageDto,
    required this.templateName, this.backgroundImage, this.oldFinImage, this.isSameLayer, this.layerNumber, required this.accuracy});

  Future<void> save(double sizex, double sizey, double pixelRatio) async {
    ui.Image? finishedLayerAtch = oldFinImage;
    if(isSameLayer==false) {
      PictureRecorder recorderFinished = PictureRecorder();
      Canvas canvasFinished = Canvas(recorderFinished);
      canvasFinished.drawColor(Colors.transparent, BlendMode.src);

      for (int i = 0; i < everyLineHasOptionListOfAll.length; ++i) {
        Paint paint = Paint();
        if (layersColors.length == everyLineHasOptionListOfAll.length) {
          paint.color = layersColors[i];
        } else {
          paint.color = currentColor;
        }
        for (int j = 0; j < everyLineHasOptionListOfAll[i].length; ++j) {
          if (oldFinImage != null && layerNumber != null &&
              layerNumber! >= i) {
            paint.blendMode = BlendMode.srcOver;

            canvasFinished.drawImageRect(oldFinImage!, Rect.fromLTRB(
                0, 0, oldFinImage!.width.toDouble(),
                oldFinImage!.height.toDouble()), Rect.fromLTRB(
                0, 0, oldFinImage!.width.toDouble() * resizeFactor!,
                oldFinImage!.height.toDouble() * resizeFactor!), paint);
          }
          if (backgroundImage != null && layerNumber != null &&
              (layerNumber! >= i || layerNumber==0)) {
            paint.blendMode = BlendMode.srcOver;
            canvasFinished.drawImageRect(backgroundImage!, Rect.fromLTRB(
                0, 0, backgroundImage!.width.toDouble(),
                backgroundImage!.height.toDouble()), Rect.fromLTRB(
                0, 0, backgroundImage!.width.toDouble() * resizeFactor!,
                backgroundImage!.height.toDouble() * resizeFactor!), paint);
          }
          if (everyLineHasOptionListOfAll[i][j].stroke.isEraser) {
            paint.blendMode = BlendMode.clear;
          } else {
            if (layersColors.length == everyLineHasOptionListOfAll.length) {
              paint.color = layersColors[i];
            } else {
              paint.color = currentColor;
            }
            paint.blendMode = BlendMode.srcOver;
          }
          final outlinePoints = getStroke(
            everyLineHasOptionListOfAll[i][j].stroke.points,
            size: everyLineHasOptionListOfAll[i][j].options.size,
            thinning: everyLineHasOptionListOfAll[i][j].options.thinning,
            smoothing: everyLineHasOptionListOfAll[i][j].options.smoothing,
            streamline: everyLineHasOptionListOfAll[i][j].options.streamline,
            taperStart: everyLineHasOptionListOfAll[i][j].options.taperStart,
            capStart: everyLineHasOptionListOfAll[i][j].options.capStart,
            taperEnd: everyLineHasOptionListOfAll[i][j].options.taperEnd,
            capEnd: everyLineHasOptionListOfAll[i][j].options.capEnd,
            simulatePressure: everyLineHasOptionListOfAll[i][j].options
                .simulatePressure,
            isComplete: everyLineHasOptionListOfAll[i][j].options.isComplete,
          );

          final path = Path();

          if (outlinePoints.isEmpty) {
            return;
          } else if (outlinePoints.length < 2) {
            path.addOval(Rect.fromCircle(
                center: Offset(outlinePoints[0].x, outlinePoints[0].y),
                radius: 1));
          } else {
            path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

            for (int i = 1; i < outlinePoints.length - 1; ++i) {
              final p0 = outlinePoints[i];
              final p1 = outlinePoints[i + 1];
              path.quadraticBezierTo(
                  p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
            }
          }
          canvasFinished.drawPath(path, paint);
        }
      }

      Picture p = recorderFinished.endRecording();
      ui.Image image = await p.toImage(
          (sizex).toInt(),
          (sizey).toInt());
      ui.PictureRecorder recorder = ui.PictureRecorder();
      canvasFinished = Canvas(recorder);
      Rect srcRect = Offset.zero & Size(
          image.width.toDouble(), image.height.toDouble());
      Rect dstRect = Offset.zero & Size(
          (sizex) / resizeFactor, (sizey) / resizeFactor);
      canvasFinished.drawImageRect(image, srcRect, dstRect, Paint());

      p = recorder.endRecording();
      finishedLayerAtch = await p.toImage(
          (sizex) ~/ resizeFactor,
          (sizey) ~/ resizeFactor);
    }


    PictureRecorder recorderActual = PictureRecorder();
    Canvas canvasActual = Canvas(recorderActual);
    canvasActual.drawColor(Colors.transparent, BlendMode.src);
    Paint paint = Paint();
    paint.color = currentColor;
    if(isSameLayer==true){
      if (backgroundImage != null) {
        paint.blendMode = BlendMode.srcOver;
        canvasActual.drawImageRect(backgroundImage!, Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble(), backgroundImage!.height.toDouble()), Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble()*resizeFactor!, backgroundImage!.height.toDouble()*resizeFactor!), paint);
      }
    }

    for (int i = 0; i < lines.length; ++i) {
      if(colors.length==lines.length){
        paint.color = colors[i];
      }
      if (lines[i].isEraser) {
        paint.blendMode = BlendMode.clear;
      } else {
        paint.blendMode = BlendMode.srcOver;
      }
      final outlinePoints = getStroke(
        lines[i].points,
        size: optionList[i].size,
        thinning: optionList[i].thinning,
        smoothing: optionList[i].smoothing,
        streamline: optionList[i].streamline,
        taperStart: optionList[i].taperStart,
        capStart: optionList[i].capStart,
        taperEnd: optionList[i].taperEnd,
        capEnd: optionList[i].capEnd,
        simulatePressure: optionList[i].simulatePressure,
        isComplete: optionList[i].isComplete,
      );

      final path = Path();

      if (outlinePoints.isEmpty) {
        return;
      } else if (outlinePoints.length < 2) {
        path.addOval(Rect.fromCircle(
            center: Offset(outlinePoints[0].x, outlinePoints[0].y),
            radius: 1));
      } else {
        path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

        for (int i = 1; i < outlinePoints.length - 1; ++i) {
          final p0 = outlinePoints[i];

          final p1 = outlinePoints[i + 1];
          path.quadraticBezierTo(
              p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
        }
      }

      canvasActual.drawPath(path, paint);
    }


    Picture p2 = recorderActual.endRecording();
    ui.Image image2 = await p2.toImage(
        (sizex).toInt(),
        (sizey).toInt());
    ui.PictureRecorder recorder2 = ui.PictureRecorder();
    canvasActual = Canvas(recorder2);
    Rect srcRect2 = Offset.zero & Size(image2.width.toDouble(), image2.height.toDouble());
    Rect dstRect2 = Offset.zero & Size((sizex)/resizeFactor, (sizey)/resizeFactor);
    canvasActual.drawImageRect(image2, srcRect2, dstRect2, Paint());

    p2 = recorder2.endRecording();
    ui.Image actualLayerAtch = await p2.toImage(
        (sizex)~/resizeFactor,
        (sizey)~/resizeFactor);

    saveUnfinished(unfinishedUsageDto, finishedLayerAtch, actualLayerAtch);
  }

  Future<void> saveUnfinished(UnfinishedUsageModel unfinishedUsageDto, ui.Image? finishedLayerAtch, ui.Image? actualLayerAtch) async {
    String formattedName = templateName?.replaceAll(RegExp(r'\s'), '_') ?? 'default';
    Map<String, dynamic> formDataFields = {
      'id': unfinishedUsageDto.id,
      'usageId': unfinishedUsageDto.usageId,
      'actualScore': accuracy,
    };

    if (finishedLayerAtch != null) {
      File finishedFile = File("${formattedName}_finished_layer_atch.png");
      final finishedData = await finishedLayerAtch.toByteData(format: ui.ImageByteFormat.png);
      Uint8List? finishedBytes = finishedData?.buffer.asUint8List();
      if (finishedBytes != null) {
        formDataFields['finishedLayerAtch'] = MultipartFile.fromBytes(finishedBytes, filename: finishedFile.path);
      }
    }

    if (actualLayerAtch != null) {
      File actualFile = File("${formattedName}_actual_layer_atch.png");
      final actualData = await actualLayerAtch.toByteData(format: ui.ImageByteFormat.png);
      Uint8List? actualBytes = actualData?.buffer.asUint8List();
      if (actualBytes != null) {
        formDataFields['actualLayerAtch'] = MultipartFile.fromBytes(actualBytes, filename: actualFile.path);
      }
    }

    if (unfinishedUsageDto.actualLayerId != null) {
      formDataFields['actualLayerId'] = unfinishedUsageDto.actualLayerId;
    }
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/users/usage/saveUnfinished");
    var formData = FormData.fromMap(formDataFields);

    var response = await Dio().put(apiUrl.toString(), data: formData, options: Options(headers: headers));
    if(response.statusCode==200){
      if(response.headers.value('authorization')!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers.value('authorization')}');
      }
    }else{
      logger.i('saving unfinished usage caused error: ${response.data}');
    }
  }
}