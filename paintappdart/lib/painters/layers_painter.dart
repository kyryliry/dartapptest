import 'dart:async';
import 'dart:ui';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:paintappdart/utils/stroke_pair.dart';
import 'package:perfect_freehand/perfect_freehand.dart';
import 'package:flutter/material.dart' hide Image;

class LayersPainter extends CustomPainter {
  final List<List<StrokePair>> everyLineHasOptionListOfAll;
  Color currentColor = Colors.black;
  final List<Color> layersColors;
  List<Picture> layers = <Picture>[];
  final ui.Image? backgroundImage;
  final double? resizeFactor;
  final int? layerNumber;


  LayersPainter({required this.layersColors, required this.everyLineHasOptionListOfAll, this.backgroundImage, this.resizeFactor, this.layerNumber});

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    for(int i=0; i< everyLineHasOptionListOfAll.length; ++i){
      canvas.saveLayer(null, Paint());
      Paint paint = Paint();
      if(layersColors.length==everyLineHasOptionListOfAll.length) {
        paint.color = layersColors[i];
      }else{
        paint.color = currentColor;
      }
      for(int j=0; j<everyLineHasOptionListOfAll[i].length; ++j){
        if (backgroundImage != null && resizeFactor!=null && layerNumber!=null && (layerNumber!>=i || layerNumber==0)) {
          paint.blendMode = BlendMode.srcOver;
          canvas.drawImageRect(backgroundImage!, Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble(), backgroundImage!.height.toDouble()), Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble()*resizeFactor!, backgroundImage!.height.toDouble()*resizeFactor!), paint);
        }
          if(everyLineHasOptionListOfAll[i][j].stroke.isEraser){
            paint.color = Color.fromARGB(0, currentColor.red, currentColor.green, currentColor.blue);
            paint.blendMode = BlendMode.clear;
          }else{
            if(layersColors.length==everyLineHasOptionListOfAll.length) {
              paint.color = layersColors[i];
            }else{
              paint.color = currentColor;
            }
            paint.blendMode = BlendMode.srcOver;
          }
          final outlinePoints = getStroke(
            everyLineHasOptionListOfAll[i][j].stroke.points,
            size:everyLineHasOptionListOfAll[i][j].options.size,
            thinning: everyLineHasOptionListOfAll[i][j].options.thinning,
            smoothing: everyLineHasOptionListOfAll[i][j].options.smoothing,
            streamline: everyLineHasOptionListOfAll[i][j].options.streamline,
            taperStart: everyLineHasOptionListOfAll[i][j].options.taperStart,
            capStart: everyLineHasOptionListOfAll[i][j].options.capStart,
            taperEnd: everyLineHasOptionListOfAll[i][j].options.taperEnd,
            capEnd: everyLineHasOptionListOfAll[i][j].options.capEnd,
            simulatePressure: everyLineHasOptionListOfAll[i][j].options.simulatePressure,
            isComplete: everyLineHasOptionListOfAll[i][j].options.isComplete,
          );

          final path = Path();

          if (outlinePoints.isEmpty) {
            return;
          } else if (outlinePoints.length < 2) {
            path.addOval(Rect.fromCircle(
                center: Offset(outlinePoints[0].x, outlinePoints[0].y),
                radius: 1));
          } else {
            path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

            for (int i = 1; i < outlinePoints.length - 1; ++i) {
              final p0 = outlinePoints[i];
              final p1 = outlinePoints[i + 1];
              path.quadraticBezierTo(
                  p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
            }
          }

          canvas.drawPath(path, paint);
      }
      canvas.restore();
    }

  }

  @override
  bool shouldRepaint(LayersPainter oldDelegate) {
    return true;
  }
}
