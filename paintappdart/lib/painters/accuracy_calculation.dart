import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:perfect_freehand/perfect_freehand.dart';
import 'package:tuple/tuple.dart';
import '../utils/stroke.dart';
import '../utils/stroke_options.dart';
import 'package:flutter/material.dart' hide Image;

class AccuracyCalculator{
  final List<Stroke> lines;
  final StrokeOptions options;
  final List<StrokeOptions> optionList;
  final List<Color> colors;
  Color currentColor = Colors.black;
  final ui.Image? backgroundImage;
  double? resizeFactor;
  Map<int, List<Offset>>? allPixels;
  int? count;
  int? len;
  final ui.Image? img;
  bool isCancelled = false;

  AccuracyCalculator({required this.currentColor, required this.optionList, required this.lines, required this.options, required this.colors, this.backgroundImage, this.resizeFactor, this.allPixels, this.count, this.len, this.img, required this.isCancelled});

  void cancel(){
    isCancelled = true;
  }

  Future<double?> calculate(double sizex, double sizey, double pixelRatio) async {
    if(lines.isEmpty && backgroundImage==null){
      return null;
    }
    if (isCancelled) {
      return null;
    }
    ui.PictureRecorder pictureRecorder = ui.PictureRecorder();

     Canvas canvas = Canvas(pictureRecorder);
    canvas.drawColor(Colors.transparent, BlendMode.src);
    Paint paint = Paint();
    canvas.saveLayer(null, paint);
    if (backgroundImage != null && resizeFactor!=null) {
        paint.blendMode = BlendMode.srcOver; // Ensure normal blending mode
        canvas.drawImageRect(backgroundImage!, Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble(), backgroundImage!.height.toDouble()), Rect.fromLTRB(0, 0, backgroundImage!.width.toDouble()*resizeFactor!, backgroundImage!.height.toDouble()*resizeFactor!), paint);
    }
      paint.color = currentColor;

      for (int i = 0; i < lines.length; ++i) {
        if(colors.length==lines.length){
          paint.color = colors[i];
        }
        if (lines[i].isEraser) {
          paint.blendMode = BlendMode.clear;
        } else {
          paint.blendMode = BlendMode.srcOver;
        }
        final outlinePoints = getStroke(
          lines[i].points,
          size: optionList[i].size,
          thinning: optionList[i].thinning,
          smoothing: optionList[i].smoothing,
          streamline: optionList[i].streamline,
          taperStart: optionList[i].taperStart,
          capStart: optionList[i].capStart,
          taperEnd: optionList[i].taperEnd,
          capEnd: optionList[i].capEnd,
          simulatePressure: optionList[i].simulatePressure,
          isComplete: optionList[i].isComplete,
        );

        final path = Path();

        if (outlinePoints.isEmpty) {
          return null;
        } else if (outlinePoints.length < 2) {
          path.addOval(Rect.fromCircle(
              center: Offset(outlinePoints[0].x, outlinePoints[0].y),
              radius: 1));
        } else {
          path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

          for (int i = 1; i < outlinePoints.length - 1; ++i) {
            final p0 = outlinePoints[i];

            final p1 = outlinePoints[i + 1];
            path.quadraticBezierTo(
                p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
          }
        }

        canvas.drawPath(path, paint);
        canvas.drawPath(path, paint);
      }

      canvas.restore();
    canvas.restore();
    ui.Picture picture = pictureRecorder.endRecording();

    ui.Image image = await picture.toImage(
        (sizex).toInt(),
        (sizey).toInt());
    ui.PictureRecorder recorder2 = ui.PictureRecorder();
    Canvas canvasActual = Canvas(recorder2);
    Rect srcRect2 = Offset.zero & Size(image.width.toDouble(), image.height.toDouble());
    Rect dstRect2 = Offset.zero & Size((sizex)/resizeFactor!, (sizey)/resizeFactor!);
    canvasActual.drawImageRect(image, srcRect2, dstRect2, Paint());

    picture = recorder2.endRecording();
    ui.Image actualLayerAtch = await picture.toImage(
        (sizex)~/resizeFactor!,
        (sizey)~/resizeFactor!);
    Future<double?> accuracy = calculateAccuracyForLayer(actualLayerAtch);
    return accuracy;
  }

  Future<double?> calculateAccuracyForLayer(ui.Image image) async {
    if (isCancelled) {
      return null;
    }
    int originalPoints = 0;
    int currentDrawPoints = 0;
    Set<Offset> match = {};
    ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.rawRgba);
    Uint8List? pixels1 = byteData?.buffer.asUint8List();
    ByteData? byteData2 = await img!.toByteData(format: ui.ImageByteFormat.rawRgba);
    Uint8List? pixels2 = byteData2?.buffer.asUint8List();
    List<bool> isMatched = List<bool>.filled(pixels1!.length ~/ 4, false);

    for (int y = 0; y < image.height; y++) {
      for (int x = 0; x < image.width; x++) {
        if (isCancelled) {
          return null;
        }
        int idx = (y * image.width + x);
        int index = (y * image.width + x) * 4;

        int alpha1 = pixels2![index + 3];
        int alpha2 = pixels1![index + 3];

        if(alpha1!=0){
          originalPoints++;
        }
        if(alpha2!=0){
          currentDrawPoints++;
        }
        if (alpha2!= 0 && alpha1!=0) {
          Offset i = Offset(x.toDouble(), y.toDouble());
          match.add(i);
          isMatched[idx] = true;
          Tuple2<Set<Offset>, List<bool>> result = calculateIndexes(x, y, image.width, image.height, pixels1, match, isMatched);
          match = result.item1;
          isMatched = result.item2;
        }
      }
    }
    int right = match.length;
    int wrong = currentDrawPoints-match.length;
    double accuracy = right-wrong>=0 ? ((right-wrong)/originalPoints) : 0;
    if(accuracy>1){
      accuracy = 1;
    }
    return accuracy*100;
  }

  Tuple2<Set<Offset>, List<bool>> calculateIndexes(int x, int y, int width, int height, Uint8List pixels, Set<Offset> match, List<bool> isMatched){
    for(int i=1; i<4; i++){
      int idxYPS = ((y+i) * width + x);
      int idxYP = ((y+i) * width + x) * 4;
      if(idxYP>=0 && idxYP<pixels.length && pixels[idxYP+3]!=0 && !isMatched[idxYPS]){
        Offset o = Offset(x.toDouble(), (y+i).toDouble());
        match.add(o);
        isMatched[idxYPS] = true;
      }
      int idxYMS = ((y-i) * width + x);
      int idxYM = ((y-i) * width + x) * 4;
      if(idxYM>=0 && idxYM<pixels.length && pixels[idxYM+3]!=0 && !isMatched[idxYMS]){
        Offset o = Offset(x.toDouble(), (y-i).toDouble());
        match.add(o);
        isMatched[idxYMS] = true;
      }
      int idxXPS = (y * width + (x+i));
      int idxXP = (y * width + (x+i)) * 4;
      if(idxXP>=0 && idxXP<pixels.length && pixels[idxXP+3]!=0 && !isMatched[idxXPS]){
        Offset o = Offset((x+i).toDouble(), y.toDouble());
        match.add(o);
        isMatched[idxXPS] = true;
      }
      int idxXMS = (y * width + (x-i));
      int idxXM = (y * width + (x-i)) * 4;
      if(idxXM>=0 && idxXM<pixels.length && pixels[idxXM+3]!=0 && !isMatched[idxXMS]){
        Offset o = Offset((x-i).toDouble(), y.toDouble());
        match.add(o);
        isMatched[idxXMS] = true;
      }
    }
    return Tuple2(match, isMatched);
  }

}
