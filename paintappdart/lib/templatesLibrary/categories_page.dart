import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/utils/app_bar_with_back.dart';
import 'package:paintappdart/model/user_model.dart';

import '../model/category_model.dart';
import '../utils/session_manager.dart';

class CategoriesPage extends StatefulWidget {
  final UserModel userDto;

  CategoriesPage({Key? key, required this.userDto}) : super(key: key);

  @override
  CategoriesPageState createState() => CategoriesPageState();
}

class CategoriesPageState extends State<CategoriesPage> {

  List<CategoryModel> categories = <CategoryModel>[];
  Set<CategoryModel> selectedCategories = <CategoryModel>{};
  int selectedIndex = -1;
  String newCatName = "";
  Map<int, TextEditingController> _controllers = {};
  int totalPages = 0;
  int amountPerPage = 10;
  int currentPage = 1;
  final logger = Logger(
    printer: SimplePrinter(),
  );

  @override
  void initState(){
    super.initState();
    getCategories(0, amountPerPage);
  }

  @override
  void dispose(){
    _controllers.forEach((_, controller) => controller.dispose());
    super.dispose();
  }

  Future<void> getCategories(int page, int size) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/categories/paged?page=$page&size=$size");

    final cats = await get(apiUrl, headers: headers);
    if (cats.statusCode == 200) {
      final Map<String, dynamic> item = json.decode(cats.body);
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      final data = item['page'];
      List<dynamic> cts = data['content'] as List;
      List<CategoryModel> tmpCats = <CategoryModel>[];
      selectedCategories.clear();
      for (var c in cts) {
        tmpCats.add(CategoryModel.fromJson(c));
      }
      setState(() {
        categories.clear();
        categories = List.of(categories)..addAll(tmpCats);
        totalPages = data['totalPages'];
      });
    } else {
      logger.i('get categories caused error: ${cats.body}');
    }
  }

  Future<void> deleteCategories() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/categories/delete/set");

    List<Map<String, dynamic>> listOfObjects = [];

    for (CategoryModel category in selectedCategories) {
      listOfObjects.add(category.toJson());
    }
    final cats = await delete(
      url,
      headers: headers,
      body: jsonEncode(listOfObjects),
    );
    if (cats.statusCode == 200) {
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      setState(() {
        for(var c in selectedCategories){
          categories.remove(c);
        }
        selectedCategories.clear();
      });
    } else {
      logger.i('delete category caused error ${cats.body}');
    }
  }

  Future<void> changeCategoryName(CategoryModel category, int index) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/categories/update/${category.id}");
    final Map<String, dynamic> jsonFields = {
      'id': category.id,
      'name': category.name,
    };
    final createResponse = await put(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
    }else{
      logger.i('change category name caused error: ${createResponse.body}');
    }
  }

  Future<void> createCategory() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/categories/new");
    final Map<String, dynamic> jsonFields = {
      'name': newCatName,
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      newCatName="";
      categories.clear();
      selectedCategories.clear();
      getCategories(0, amountPerPage);
    }else{
      logger.i('create category caused error: ${createResponse.body}');
    }
  }

  Widget buildPagination(int currentPage, int totalPages) {
    const int maxVisiblePages = 5;
    List<Widget> pageButtons = [];

    int startPage = max(currentPage - maxVisiblePages ~/ 2, 1);
    int endPage = min(startPage + maxVisiblePages - 1, totalPages);
    startPage = max(endPage - maxVisiblePages + 1, 1);

    if (startPage > 1) {
      pageButtons.add(buildPageButton(1));
    }

    if (startPage > 2) {
      pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
    }

    for (int i = startPage; i <= endPage; i++) {
      pageButtons.add(buildPageButton(i, isCurrent: i == currentPage));
    }

    if (totalPages > endPage) {
      if (endPage < totalPages - 1) {
        pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
      }
      pageButtons.add(buildPageButton(totalPages - 1));
      pageButtons.add(buildPageButton(totalPages));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: pageButtons,
    );
  }

  Widget buildPageButton(int page, {bool isCurrent = false}) {
    return Padding(padding: const EdgeInsets.only(left: 2, right: 2),
        child:
        TextButton(
          onPressed: () {
            setState(() {
              currentPage = page;
            });
            getCategories(page-1, amountPerPage);
          },
          style: TextButton.styleFrom(
            backgroundColor: isCurrent ? Colors.deepPurple : Colors.deepPurple.shade50,
            foregroundColor: isCurrent ? Colors.white : null,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          ),
          child: Text((page).toString()),
        ));
  }

  @override
  Widget build(BuildContext context) {
      return SelectionArea(child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(
            userDto: widget.userDto,
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child:
            Column(
              children: [
                const Text('Categories:', textScaler: TextScaler.linear(2),),
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  margin: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  child:
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 5),
                          child:
                          TextField(
                            onChanged: (value) {
                              setState(() {
                                newCatName = value;
                              });
                            },
                            decoration: const InputDecoration(
                              hintText: "Enter new category...",
                              border: OutlineInputBorder(),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                          ),
                        ),),
                      ElevatedButton(
                        onPressed: ()
                        {
                          if(newCatName!="") {
                            createCategory();
                          }
                          },
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          backgroundColor: Colors.deepPurple.shade100,
                          foregroundColor: Colors.white,
                        ),
                        child:
                        const Text('Add Category'),),
                    ],
                  ),
                ),
                //     if(categories.isEmpty)
                // const Expanded(child: Center(
                //   child:
                //   SizedBox(
                //     width: 50,
                //     height: 50,
                //     child: CircularProgressIndicator(),
                //   ),
                // ),),
                if(categories.isNotEmpty)
                Expanded(
                  child:
                  ListView.builder(
                      itemCount:categories.length,
                      itemBuilder: (context, index) {
                        bool isSelected = selectedIndex == index;
                        bool isChecked = selectedCategories.contains(categories[index]) ? true:false;
                        _controllers[index] = _controllers[index] ?? TextEditingController(text: categories[index].name);
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          child:
                          Row(
                          children: [
                            Checkbox(
                                value: isChecked,
                                onChanged: (newValue){
                                  setState(() {
                                    isChecked = newValue!;
                                    if(isChecked){
                                      selectedCategories.add(categories[index]);
                                    }else if(!isChecked && selectedCategories.contains(categories[index])){
                                      selectedCategories.remove(categories[index]);
                                    }
                                  });
                                }),
                            if(!isSelected)
                              Row(
                                children: [
                                  Text(
                                    categories[index].name,
                                    textScaler: const TextScaler.linear(1.5),
                                  ),
                                  IconButton(onPressed: (){
                                    if(!isSelected && selectedIndex==-1) {
                                      isSelected = true;
                                      setState(() {
                                        selectedIndex = index;
                                      });
                                    }
                                  },
                                      icon: const Icon(Icons.edit)),
                                ],),
                            if(isSelected)
                              Container(
                                width: MediaQuery.of(context).size.width-60,
                                margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
                                child:
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Expanded(
                                    child: TextField(
                                      controller: _controllers[index],
                                      onChanged: (value) {
                                        setState(() {
                                          categories[index].name = value;
                                        });
                                      },
                                      decoration: const InputDecoration(
                                        hintText: "Enter new category name",
                                        border: OutlineInputBorder(),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                      onPressed: (){
                                        setState(() {
                                          isSelected = false;
                                          selectedIndex = -1;
                                        });
                                        changeCategoryName(categories[index], index);
                                      },
                                      icon: Icon(Icons.save)),
                                  IconButton(
                                      onPressed: (){
                                        setState(() {
                                          isSelected = false;
                                          selectedIndex = -1;
                                        });
                                      },
                                      icon: Icon(Icons.cancel))
                                ],
                              ),),
                          ],
                        ),);
                      }
                  ),
                ),
                if(selectedCategories.isNotEmpty)
                  Container(
                    height: 60,
                    padding: const EdgeInsets.only(bottom: 10),
                    child:
                    ElevatedButton(
                      onPressed: (){
                        deleteCategories();
                      },
                      child: const Text('Delete selected categories'),
                    ),
                  ),
                Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(bottom: 10),
                  child: buildPagination(currentPage, totalPages),
                ),
              ],
            )),
          ],
        ),
      ));
  }
}
