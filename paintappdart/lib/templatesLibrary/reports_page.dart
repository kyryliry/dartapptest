import 'dart:convert';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/gallery_image_page.dart';
import 'package:paintappdart/templatesLibrary/profile_page.dart';
import 'package:paintappdart/templatesLibrary/template_page.dart';
import 'package:paintappdart/model/comment_model.dart';
import 'package:paintappdart/model/gallery_image_model.dart';
import 'package:paintappdart/model/report_model.dart';
import 'package:tuple/tuple.dart';

import '../utils/app_bar_with_back.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class ReportsPage extends StatefulWidget {
  final UserModel userDto;

  ReportsPage({Key? key, required this.userDto}) : super(key: key);

  @override
  ReportsPageState createState() => ReportsPageState();
}

class ReportsPageState extends State<ReportsPage> with SingleTickerProviderStateMixin {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<ReportModel> open = <ReportModel>[];
  List<ReportModel> closed = <ReportModel>[];
  List<ReportModel> all = <ReportModel>[];
  Map<String, int> totalPages = <String, int>{};
  Map<String, int> currentPage = <String, int>{};
  int amountPerPage = 10;
  String reportType = "open";
  bool fetched = false;
  Map<int, TextEditingController> _controllers = {};
  int selectedIndex = -1;
  String tmpCloseReason = "";

  TabController? _tabController;
  @override
  void initState(){
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    totalPages.putIfAbsent('open', () => 0);
    totalPages.putIfAbsent('closed', () => 0);
    totalPages.putIfAbsent('all', () => 0);
    currentPage.putIfAbsent('open', () => 1);
    currentPage.putIfAbsent('closed', () => 1);
    currentPage.putIfAbsent('all', () => 1);
    fetchReports(0, amountPerPage, "open");
    fetchReports(0, amountPerPage, "closed");
    fetchReports(0, amountPerPage, "all");
  }

  @override
  void dispose() {
    _controllers.forEach((_, controller) => controller.dispose());
    _tabController?.dispose();
    super.dispose();
  }

  Future<void> switchToTab(int tabIndex, String type) async {
    _tabController?.animateTo(tabIndex);
  }

  Future<void> fetchReports(int page, int size, String type) async {
    final String url = 'http://localhost:8080/api/reports/get/$type/paged?page=$page&size=$size';

    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await get(Uri.parse(url),
        headers: headers);

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final page = data['page'];
      List<dynamic> reports = page['content'] as List;
      List<ReportModel> tmpReports = <ReportModel>[];
      for (var report in reports) {
        tmpReports.add(ReportModel(
            id: report['id'], text: report['text'], date: report['date'],
            authorId: report['authorId'], authorUsername: report['authorUsername'], subjectId: report['subjectId'],
        type: report['type'], reviewerId: report['reviewerId'], reviewerUsername: report['reviewerUsername'],
            isOpen: report['isOpen'], closeReason: report['closeReason'], closeDate: report['closeDate']));
      }
      setState(() {
        if(type=="open"){
          open.clear();
          open = List.of(open)..addAll(tmpReports);
          totalPages['open'] = page['totalPages'];
        }else if(type=="closed"){
          closed.clear();
          closed = List.of(closed)..addAll(tmpReports);
          totalPages['closed'] = page['totalPages'];
        }else if(type=="all"){
          all.clear();
          all = List.of(all)..addAll(tmpReports);
          totalPages['all'] = page['totalPages'];
        }
        fetched = true;
      });
    } else {
      logger.i('fetch reports caused error: ${response.body}');
    }
  }

  Future<void> closeReport(ReportModel reportDto) async {
    var url = Uri.parse("http://localhost:8080/api/reports/close");
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {"Access-Control-Allow-Origin": "*",
      'Content-Type': 'application/json',
      'Accept': '*/*'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final Map<String, dynamic> jsonFields = {
      'id': reportDto.id,
      'closeReason': reportDto.closeReason,
    };
    try {
      var closeResponse = await put(
          Uri.parse("http://localhost:8080/api/reports/close"),
        body: jsonEncode(jsonFields),
        headers: {
          'Authorization': jsessionId!,
          'Content-Type': 'application/json'
        },
      );
      if(closeResponse.statusCode==200){
        if(closeResponse.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${closeResponse.headers['authorization']}');
        }
        fetchReports(0, amountPerPage, "open");
        fetchReports(0, amountPerPage, "closed");
        fetchReports(0, amountPerPage, "all");
      }else{
        logger.i('close report caused error: ${closeResponse.body}');
      }
    } catch (e) {
      logger.i('Error with: $e');
    }
  }

  Future<void> reopenReport(ReportModel reportDto) async {
    var url = Uri.parse("http://localhost:8080/api/reports/reopen/${reportDto.id}");
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final closeResponse = await put(
      url, headers: headers
    );
    if(closeResponse.statusCode==200){
      if(closeResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${closeResponse.headers['authorization']}');
      }
      fetchReports(0, amountPerPage, "open");
      fetchReports(0, amountPerPage, "closed");
      fetchReports(0, amountPerPage, "all");
    }else{
      logger.i('reopen report caused error: ${closeResponse.body}');
    }
  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Report subject was deleted from system',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> getReportSubject(ReportModel reportDto) async {
    if(reportDto.type=="TEMPLATE"){
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse("http://localhost:8080/api/templates/checkExist/${reportDto.subjectId}");
      final response = await get(url, headers: headers);
      if(response.statusCode==200){
        final item = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }
        if(item['exist']=="true"){
          if(mounted) {
            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                TemplatePage(
                    templateId: reportDto.subjectId, userDto: widget.userDto)));
          }
        }else{
          if(mounted) {
            alertDialog(context);
          }
        }
      }else{
        logger.i('get report subject template caused error: ${response.body}');
      }
    }else if(reportDto.type=="IMAGE"){
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse("http://localhost:8080/api/templates/images/checkExist/${reportDto.subjectId}");
      final response = await get(url,headers: headers);
      if(response.statusCode==200){
        final item = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }
        if(item['exist']=="true"){
          String? jsessionId = SessionManager().getJsessionId();
          Map<String, String>? headers1 = {'Content-Type': 'application/json'};
          if(jsessionId!=null){
            headers1.putIfAbsent('Authorization', () => jsessionId);
          }
          var urlI = Uri.parse("http://localhost:8080/api/templates/images/${reportDto.subjectId}");
          final responseI = await get(urlI, headers: headers1);
          if(responseI.statusCode==200){
            final io = json.decode(responseI.body);
            if(responseI.headers['authorization']!=null) {
              SessionManager().setJsessionId('Bearer ${responseI.headers['authorization']}');
            }
            String i = io['imageData'];
            var iBytes = base64Decode(i);
            ui.Codec codec1 = await ui.instantiateImageCodec(iBytes);
            ui.FrameInfo frame1 = await codec1.getNextFrame();
            List<dynamic> likes = io['likes'] as List;
            List<dynamic> dislikes = io['dislikes'] as List;
            GalleryImageModel imageDto = GalleryImageModel(id: io['id'],
                description: io['description'],
                usageId: io['usageId'],
                authorId: io['authorId'],
                authorName: io['authorName'],
                image: frame1.image,
                uploadDate: io['uploadDate'],
                actualScore: io['actualScore'],
                likes: likes.cast<int>(),
                dislikes: dislikes.cast<int>());
            if(mounted) {
            Navigator.push(context, MaterialPageRoute(builder: (context) =>
            GalleryImagePage(
            galleryImageDto: imageDto, userDto: widget.userDto, templateId: reportDto.subjectId, fromTemplate: true,)));
            }
          }else{
            logger.i('get report subject image caused error: ${responseI.body}');
          }
        }else{
          if(mounted) {
            alertDialog(context);
          }
        }
      }else{
        logger.i('get report subject image caused error: ${response.body}');
      }
    } else if(reportDto.type=="USER"){
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse("http://localhost:8080/api/users/checkExist/${reportDto.subjectId}");
      final response = await get(url, headers: headers);
      if(response.statusCode==200){
        final item = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }
        if(item['exist']=="true"){
          if(mounted) {
            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                ProfilePage(
                    userDto: widget.userDto, profileId: reportDto.subjectId,)));
          }
        }else{
          if(mounted) {
            alertDialog(context);
          }
        }
      }else{
        logger.i('get report subject user caused error: ${response.body}');
      }
    }else if(reportDto.type=="COMMENT"){
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse("http://localhost:8080/api/comments/checkExist/${reportDto.subjectId}");
      final response = await get(url, headers: headers);
      if(response.statusCode==200){
        final item = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }
        if(item['exist']=="true"){
          String? jsessionId = SessionManager().getJsessionId();
          Map<String, String>? headers1 = {'Content-Type': 'application/json'};
          if(jsessionId!=null){
            headers1.putIfAbsent('Authorization', () => jsessionId);
          }
          var url1 = Uri.parse("http://localhost:8080/api/reports/${reportDto.id}/userComment/subParent");
          final comment = await get(url1, headers: headers1);
          if(comment.statusCode==200){
            final io = json.decode(comment.body);
            if(comment.headers['authorization']!=null) {
              SessionManager().setJsessionId('Bearer ${comment.headers['authorization']}');
            }
            List<dynamic> likes = io['likes'] as List;
            List<dynamic> dislikes = io['dislikes'] as List;
            List<dynamic> replies = io['repliesIds'] as List;
            final objectType = io['objectType'];
            final objectId = io['objectId'];
            final injection = io['injection'];
            var parentId = 0;
            if(io['parentId']!=null){
              parentId = io['parentId'];
            }
            CommentModel commentDto = CommentModel(id: io['id'],
                text: io['text'],
                authorId: io['authorId'],
                authorUsername: io['authorUsername'],
                date: io['date'],
                subjectId: io['subjectId'],
                type: io['type'],
                replies: replies.cast<int>(),
                likes: likes.cast<int>(),
                dislikes: dislikes.cast<int>());
            if(mounted){
              if(objectType=="TEMPLATE") {
                String? jsessionId = SessionManager().getJsessionId();
                Map<String, String>? headers2 = {'Content-Type': 'application/json'};
                if(jsessionId!=null){
                  headers2.putIfAbsent('Authorization', () => jsessionId);
                }
                var url = Uri.parse("http://localhost:8080/api/templates/checkExist/${objectId}");
                final response = await get(url, headers: headers2);
                if(response.statusCode==200){
                  final item = json.decode(response.body);
                  if(response.headers['authorization']!=null) {
                    SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
                  }
                  if(item['exist']=="true"){
                    if(mounted) {
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>
                          TemplatePage(
                              templateId: objectId, userDto: widget.userDto, reportedCommentParent: commentDto, reportedComment: Tuple3(reportDto.subjectId, injection, parentId),)));
                    }
                  }else{
                    if(mounted) {
                      alertDialog(context);
                    }
                  }
                }else{
                  logger.i('get report subject comment on template caused error: ${response.body}');
                }
              }else if(objectType=="IMAGE"){
                String? jsessionId = SessionManager().getJsessionId();
                Map<String, String>? headers2 = {'Content-Type': 'application/json'};
                if(jsessionId!=null){
                  headers2.putIfAbsent('Authorization', () => jsessionId);
                }
                var url = Uri.parse("http://localhost:8080/api/templates/images/checkExist/${objectId}");
                final response = await get(url, headers: headers2);
                if(response.statusCode==200){
                  final item = json.decode(response.body);
                  if(response.headers['authorization']!=null) {
                    SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
                  }
                  if(item['exist']=="true"){
                    String? jsessionId = SessionManager().getJsessionId();
                    Map<String, String>? headers3 = {'Content-Type': 'application/json'};
                    if(jsessionId!=null){
                      headers3.putIfAbsent('Authorization', () => jsessionId);
                    }
                    var urlI = Uri.parse("http://localhost:8080/api/templates/images/${objectId}");
                    final responseI = await get(urlI, headers: headers3);
                    if(responseI.statusCode==200){
                      final io = json.decode(responseI.body);
                      if(responseI.headers['authorization']!=null) {
                        SessionManager().setJsessionId('Bearer ${responseI.headers['authorization']}');
                      }
                      String i = io['imageData'];
                      var iBytes = base64Decode(i);
                      ui.Codec codec1 = await ui.instantiateImageCodec(iBytes);
                      ui.FrameInfo frame1 = await codec1.getNextFrame();
                      List<dynamic> likes = io['likes'] as List;
                      List<dynamic> dislikes = io['dislikes'] as List;
                      GalleryImageModel imageDto = GalleryImageModel(id: io['id'],
                          description: io['description'],
                          usageId: io['usageId'],
                          authorId: io['authorId'],
                          authorName: io['authorName'],
                          image: frame1.image,
                          uploadDate: io['uploadDate'],
                          actualScore: io['actualScore'],
                          likes: likes.cast<int>(),
                          dislikes: dislikes.cast<int>());
                      if(mounted) {
                        Navigator.push(context, MaterialPageRoute(builder: (context) =>
                            GalleryImagePage(
                                galleryImageDto: imageDto, userDto: widget.userDto, templateId: objectId, fromTemplate: true, reportedCommentParent: commentDto, reportedComment: Tuple3(reportDto.subjectId, injection, parentId),)));
                      }
                    }else{
                      logger.i('get report subject comment on image caused error: ${responseI.body}');
                    }
                  }else{
                    if(mounted) {
                      alertDialog(context);
                    }
                  }
                }else{
                  logger.i('get report subject comment on image caused error: ${response.body}');
                }
              }
            }
            }else{
            logger.i('get report subParent caused error: ${comment.body}');
          }

        }else{
          if(mounted) {
            alertDialog(context);
          }
        }
    }

  }
  }

  Widget buildPagination(int currentPage, int totalPages, String type) {
    const int maxVisiblePages = 5;
    List<Widget> pageButtons = [];

    int startPage = max(currentPage - maxVisiblePages ~/ 2, 1);
    int endPage = min(startPage + maxVisiblePages - 1, totalPages);
    startPage = max(endPage - maxVisiblePages + 1, 1);

    if (startPage > 1) {
      pageButtons.add(buildPageButton(1, type));
    }

    if (startPage > 2) {
      pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
    }

    for (int i = startPage; i <= endPage; i++) {
      pageButtons.add(buildPageButton(i, type, isCurrent: i == currentPage));
    }

    if (totalPages > endPage) {
      if (endPage < totalPages - 1) {
        pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
      }
      pageButtons.add(buildPageButton(totalPages - 1, type));
      pageButtons.add(buildPageButton(totalPages, type));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: pageButtons,
    );
  }

  Widget buildPageButton(int page, String type, {bool isCurrent = false}) {
    return Padding(padding: const EdgeInsets.only(left: 2, right: 2),
        child:
        TextButton(
          onPressed: () {
            setState(() {
              currentPage[type] = page;
            });
            fetchReports(page-1, amountPerPage, type);
          },
          style: TextButton.styleFrom(
            backgroundColor: isCurrent ? Colors.deepPurple : Colors.deepPurple.shade50,
            foregroundColor: isCurrent ? Colors.white : null,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          ),
          child: Text((page).toString()),
        ));
  }

  Widget buildReportList(List<ReportModel> reports, String type) {
    return Column(
        children:[
          Expanded(child:
          ListView.builder(
              itemCount: reports.length,
              itemBuilder: (context, index) {
                bool isSelected = selectedIndex == index;
                _controllers[index] = _controllers[index] ?? TextEditingController();
                return Padding(padding: const EdgeInsets.all(10),
                    child:
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                      ),
                      width: MediaQuery.of(context).size.width,
                      child:
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                const SizedBox(width: 5,),
                                Text(reports[index].text, textScaler: const TextScaler.linear(1.3),
                                  textAlign: TextAlign.start,),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text('Report type: ${reports[index].type}'),
                                Text('Reporter: ${reports[index].authorUsername}'),
                                Text('Date: ${DateFormat('dd.MM.yyyy HH:mm').format(DateTime.parse(reports[index].date))}'),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                    onPressed: (){
                                      getReportSubject(reports[index]);
                                    },
                                    child: const Text('Open subject'),),
                                if(reports[index].authorId!=null)
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(userDto: widget.userDto, profileId: reports[index].authorId!)));
                                    },
                                    child: const Text('Open user profile')),
                                if(reports[index].authorId==null)
                                  const Text('Profile deleted'),
                                if(reports[index].isOpen)
                                  ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: isSelected
                                            ? Colors.grey.shade300
                                            : null,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          if(selectedIndex!=index) {
                                            selectedIndex = index;
                                          }
                                        });
                                      },
                                      child: const Text('Close')),
                                if(!reports[index].isOpen)
                                  ElevatedButton(
                                      onPressed: () {
                                        reopenReport(reports[index]);
                                      },
                                      child: const Text('Reopen')),
                              ],
                            ),
                            const SizedBox(height: 5,),
                            if(isSelected)
                              Container(
                                width: MediaQuery.of(context).size.width-60,
                                margin: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                                child:
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        controller: _controllers[index],
                                        onChanged: (value) {
                                          setState(() {
                                            tmpCloseReason = value;
                                          });
                                        },
                                        decoration: const InputDecoration(
                                          hintText: "Enter close reason",
                                          border: OutlineInputBorder(),
                                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                        ),
                                      ),
                                    ),
                                    IconButton(
                                        onPressed: (){
                                          setState(() {
                                            isSelected = false;
                                            selectedIndex = -1;
                                            reports[index].closeReason = tmpCloseReason;
                                          });
                                          closeReport(reports[index]);
                                        },
                                        icon: const Icon(Icons.check_circle_outline)),
                                    IconButton(
                                        onPressed: (){
                                          setState(() {
                                            isSelected = false;
                                            selectedIndex = -1;
                                            tmpCloseReason = "";
                                          });
                                        },
                                        icon: const Icon(Icons.cancel_outlined))
                                  ],
                                ),),
                              if(reports[index].closeDate!=null)
                                Row(
                                  children: [
                                    const SizedBox(width: 5,),
                                    Text('Date: ${DateFormat('dd.MM.yyyy HH:mm').format(DateTime.parse(reports[index].closeDate!))}'),
                                    const SizedBox(width: 30,),
                                    Text('Reviewer: ${reports[index].reviewerUsername ?? "profile deleted"}')
                                  ],
                                ),
                            if(reports[index].closeDate!=null)
                            Row(
                              children:[
                            const SizedBox(width: 5,),
                                Text('Old reason: ${reports[index].closeReason}'),
                              ]),
                          ],
                        ),
                      ));
              }
          ),),
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(bottom: 10),
            child: buildPagination(currentPage[type]!, totalPages[type]!, type),
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    if(!fetched){
      return const CircularProgressIndicator();
    }else {
      return SelectionArea(child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(
            userDto: widget.userDto,
          ),
        ),
        body: Stack(
            children: [
              Column(
                children: [
                  TabBar(
                    controller: _tabController,
                    tabs: const [
                      Tab(text: 'Open'),
                      Tab(text: 'Closed'),
                      Tab(text: 'All'),
                    ],
                  ),
                  Expanded(child:
                  TabBarView(
                    controller: _tabController,
                    children: [
                      buildReportList(open, "open"),
                      buildReportList(closed, "closed"),
                      buildReportList(all, "all"),
                    ],
                  ),),
                ],
              ),
            ]
        ),
      ));
    }
  }

}