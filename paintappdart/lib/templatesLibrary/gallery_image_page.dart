import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/template_page.dart';
import 'package:tuple/tuple.dart';

import '../utils/app_bar_with_back.dart';
import '../model/comment_model.dart';
import '../model/gallery_image_model.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class GalleryImagePage extends StatefulWidget{
  final GalleryImageModel galleryImageDto;
  final UserModel? userDto;
  final int templateId;
  final CommentModel? reportedCommentParent;
  final Tuple3<int, int, int>? reportedComment;
  final bool fromTemplate;
  const GalleryImagePage({Key? key, required this.galleryImageDto, required this.userDto, required this.templateId, required this.fromTemplate,
  this.reportedCommentParent, this.reportedComment}) : super(key: key);

  @override
  GalleryImagePageState createState() => GalleryImagePageState();
}

class GalleryImagePageState extends State<GalleryImagePage> {

  final logger = Logger(
    printer: SimplePrinter(),
  );
  GalleryImageModel? galleryImageDto;
  Size? size;
  UserModel? userDto;
  bool isEdit = false;
  String? newDesc;
  String? newComment;
  String? newReply;
  int? selectedId;
  bool isFetchingComments = false;
  DateTime dateValue = DateTime.now();
  final TextEditingController textFieldController = TextEditingController();
  Set<int> expandedComments = <int>{};
  Map<int, bool> loadingStates = {};
  Map<int, int> recursionLevel = {};
  CommentModel? singleCommentThread;
  final TextEditingController commentFieldController = TextEditingController();


  @override
  void initState() {
    super.initState();
    galleryImageDto = widget.galleryImageDto;
    fetchComments();
    userDto = widget.userDto;
    dateValue = DateTime.parse(galleryImageDto!.uploadDate);
    if(widget.reportedCommentParent!=null && widget.reportedComment!=null && widget.reportedComment!.item2==2){
      CommentModel com = widget.reportedCommentParent!;
      fetchReplies(com);
      recursionLevel.clear();
      expandedComments.clear();
      singleCommentThread = com;
    }
  }

  Future<void> fetchComments() async {
    setState(() {
      isFetchingComments = true;
    });
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/images/${galleryImageDto!.id}/comments/all");
    final finishResponse = await get(
        url, headers: headers
    );
    if(finishResponse.statusCode!=200){
      logger.i('fetching comments caused error: ${finishResponse.body}');
    }
    List<CommentModel> comments = <CommentModel>[];
    final item = json.decode(finishResponse.body);
    if(finishResponse.headers['authorization']!=null) {
      SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
    }
    final commentsList = item['comments'] as List;
    CommentModel? existCom;
    for (var io in commentsList) {
      List<dynamic> likes = io['likes'] as List;
      List<dynamic> dislikes = io['dislikes'] as List;
      List<dynamic> replies = io['repliesIds'] as List;
      if(widget.reportedCommentParent!=null && widget.reportedComment!.item2!=2 &&
          io['id']==widget.reportedCommentParent!.id){
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }
      comments.add(CommentModel(id: io['id'],
          text: io['text'],
          authorId: io['authorId'],
          authorUsername: io['authorUsername'],
          date: io['date'],
          subjectId: io['subjectId'],
          type: io['type'],
          replies: replies.cast<int>(),
          likes: likes.cast<int>(),
          dislikes: dislikes.cast<int>()));
    }
    if(existCom!=null){
      comments.insert(0, existCom);
    }
    setState(() {
      galleryImageDto!.comments = comments;
      isFetchingComments = false;
    });
  }

  Future<void> addCommentLike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/addLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'] as List;
      setState(() {
        commentDto.likes = jsonList.cast<int>();
      });
      if(item['dislikes']!=null){
        List<dynamic> jsonList2 = item['dislikes'] as List;
        setState(() {
          commentDto.dislikes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add like to comment caused error: ${finishResponse.body}');
    }
  }

  Future<void> removeCommentLike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/removeLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'];
      setState(() {
        commentDto.likes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove like from comment caused error: ${finishResponse.body}');
    }
  }

  Future<void> addCommentDislike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/addDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'] as List;
      setState(() {
        commentDto.dislikes = jsonList.cast<int>();
      });
      if(item['likes']!=null){
        List<dynamic> jsonList2 = item['likes'] as List;
        setState(() {
          commentDto.likes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add dislike to comment caused error: ${finishResponse.body}');
    }
  }

  Future<void> removeCommentDislike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/removeDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'];
      setState(() {
        commentDto.dislikes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove dislike from comment caused error: ${finishResponse.body}');
    }
  }

  Future<void> fetchReplies(CommentModel comment) async {
    setState(() {
      loadingStates[comment.id] = true;
    });
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/comments/${comment.id}/replies");
    final finishResponse = await get(
        url, headers: headers
    );
    if(finishResponse.statusCode!=200){
      logger.i('fetching replies caused error: ${finishResponse.body}');
    }
    List<CommentModel> comments = <CommentModel>[];
    final item = json.decode(finishResponse.body);
    if(finishResponse.headers['authorization']!=null) {
      SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
    }
    final commentsList = item['comments'] as List;
    CommentModel? existCom;
    for (var io in commentsList) {
      List<dynamic> likes = io['likes'] as List;
      List<dynamic> dislikes = io['dislikes'] as List;
      List<dynamic> replies = io['repliesIds'] as List;
      if(widget.reportedComment!=null && widget.reportedComment!.item2==2 && widget.reportedComment!.item3!=0 && widget.reportedComment!.item3==io['id']){
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }else if(widget.reportedComment!=null && widget.reportedComment!.item2==2 && widget.reportedComment!.item1==io['id']) {
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }else if(widget.reportedComment!=null && widget.reportedComment!.item2==1 && widget.reportedComment!.item1==io['id']) {
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }
      comments.add(CommentModel(id: io['id'],
          text: io['text'],
          authorId: io['authorId'],
          authorUsername: io['authorUsername'],
          date: io['date'],
          subjectId: io['subjectId'],
          type: io['type'],
          replies: replies.cast<int>(),
          likes: likes.cast<int>(),
          dislikes: dislikes.cast<int>()));

    }
    if(existCom!=null) {
      comments.insert(0, existCom);
    }
    setState(() {
      comment.repliesList = comments;
      loadingStates[comment.id] = false;
      expandedComments.add(comment.id);
    });
  }

  Future<bool> addComment() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/${galleryImageDto!.id}/comments/new");
    final Map<String, dynamic> jsonFields = {
      'text': newComment,
      'type': 'IMAGE',
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      final io = json.decode(createResponse.body);
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      setState(() {
        List<dynamic> likes = io['likes'] as List;
        List<dynamic> dislikes = io['dislikes'] as List;
        List<dynamic> replies = io['repliesIds'] as List;
        galleryImageDto!.comments!.add(CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>()));
      });
      return true;
    }else{
      logger.i('add comment caused error: ${createResponse.body}');
      return false;
    }
  }

  Future<bool> addReply(CommentModel parent) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/templates/images/${galleryImageDto!.id}/comments/newReply");
    final Map<String, dynamic> jsonFields = {
      'text': newReply,
      'subjectId':parent.id,
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      setState(() {
        newReply = null;
        selectedId = null;
      });
      return true;
    }else{
      logger.i('add reply caused error: ${createResponse.body}');
      return false;
    }
  }

  Future<bool> deleteComment(CommentModel comment) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/templates/images/${galleryImageDto!.id}/comments/${comment.id}/delete");
    final deleteResponse = await delete(
        url, headers: headers
    );
    if(deleteResponse.statusCode==200){
      if(deleteResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${deleteResponse.headers['authorization']}');
      }
      setState(() {
        loadingStates[comment.id] = true;
        comment.text = "Comment has been deleted.";
        comment.authorId = null;
        comment.authorUsername = null;
        loadingStates[comment.id] = false;
      });
      return true;
    }else{
      logger.i('delete comment caused error: ${deleteResponse.body}');
      return false;
    }
  }

  Future<bool> createReport(String text, int id, String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/reports/create");
    if(type=="COMMENT"){
      var tmp = "{IMAGE${galleryImageDto!.id}}$text";
      text = tmp;
    }
    final Map<String, dynamic> jsonFields = {
      'text': text,
      'type':type,
      'subjectId':'$id'
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      return true;
    }else{
      logger.i('create report caused error: ${createResponse.body}');
      return false;
    }
  }

  Future<void> showReportDialog(String type, int id) async {
    String selectedOption = "";
    bool own = false;
    String reason = "";
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: const Text('Send report'),
                content: Container(
                  width: 300,
                  child:
                  SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        ListTile(
                          title: const Text('Spam'),
                          leading: Radio<String>(
                            value: 'Spam',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Bullying or harassment'),
                          leading: Radio<String>(
                            value: 'Bullying or harassment',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Nudity or sexual content'),
                          leading: Radio<String>(
                            value: 'Nudity or sexual content',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Hate speech or symbols'),
                          leading: Radio<String>(
                            value: 'Hate speech or symbols',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Scam or fraud'),
                          leading: Radio<String>(
                            value: 'Scam or fraud',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Your own reason'),
                          leading: Radio<String>(
                            value: 'Your own reason',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = true;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        if(own)
                          TextField(
                            onChanged: (value) {
                              setState(() {
                                reason = value;
                              });
                            },
                            controller: textFieldController,
                            decoration:
                            const InputDecoration(hintText: "Enter reason of your report"),
                          ),
                      ],
                    ),
                  ),),
                actions: <Widget>[
                  TextButton(
                    child: const Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    onPressed: () async {
                      if(selectedOption=="Your own reason"){
                        selectedOption = reason;
                      }
                      if(await createReport(selectedOption, id, type)){
                        if(mounted) {
                          Navigator.of(context).pop();
                          alertDialog(context);
                        }
                      }
                    },
                    child: const Text('Send'),
                  ),
                ],
              );
            },);}
    );
  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Thank you for your report!',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> saveNewGIDescription() async {
    setState(() {
      galleryImageDto!.description = newDesc!;
    });

    final Map<String, dynamic> jsonFields = {
      'description': galleryImageDto!.description,
    };
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/images/${galleryImageDto!.id}/edit");
    final finishResponse = await put(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(finishResponse.statusCode==200){
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
    }else{
      logger.i('edit gallery image caused error: ${finishResponse.body}');
    }
  }

  Future<void> deleteGI() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/images/${galleryImageDto!.id}/delete");
    final finishResponse = await delete(
      url, headers: headers
    );
    if(finishResponse.statusCode==200){
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
    }else{
      logger.i('delete gallery image caused error: ${finishResponse.body}');
    }
  }

  Future<void> addLike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/image/${galleryImageDto!.id}/addLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'] as List;
      setState(() {
        galleryImageDto!.likes = jsonList.cast<int>();
      });
      if(item['dislikes']!=null){
        List<dynamic> jsonList2 = item['dislikes'] as List;
        setState(() {
          galleryImageDto!.dislikes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add like to gallery image caused error ${finishResponse.body}');
    }
  }

  Future<void> removeLike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/image/${galleryImageDto!.id}/removeLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'];
      setState(() {
        galleryImageDto!.likes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove like from gallery image caused error ${finishResponse.body}');
    }
  }

  Future<void> addDislike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/image/${galleryImageDto!.id}/addDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'] as List;
      setState(() {
        galleryImageDto!.dislikes = jsonList.cast<int>();
      });
      if(item['likes']!=null){
        List<dynamic> jsonList2 = item['likes'] as List;
        setState(() {
          galleryImageDto!.likes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add dislike to gallery image caused error ${finishResponse.body}');
    }
  }

  Future<void> removeDislike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/image/${galleryImageDto!.id}/removeDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'];
      setState(() {
        galleryImageDto!.dislikes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove dislike from gallery image caused error ${finishResponse.body}');
    }
  }

  Size setImageSize(int width, int height) {
    double resizeFactor = 1;
    double canvasWidth = width.toDouble();
    double canvasHeight = height.toDouble();
    if (width > height &&
        width > (MediaQuery.of(context).size.width - 40) / 2) {
      resizeFactor = ((MediaQuery.of(context).size.width - 40) / 2) / width;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height > width &&
        height > (MediaQuery.of(context).size.height - 40) / 2) {
      resizeFactor = ((MediaQuery.of(context).size.height - 40) / 2) / height;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height == width &&
        (width > (MediaQuery.of(context).size.width - 40) / 2 ||
            height > (MediaQuery.of(context).size.height - 40) / 2)) {
      if ((MediaQuery.of(context).size.width - 40) / 2 >
          (MediaQuery.of(context).size.height - 40) / 2) {
        resizeFactor = ((MediaQuery.of(context).size.height - 40) / 2) / height;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      } else {
        resizeFactor = ((MediaQuery.of(context).size.width - 40) / 2) / width;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      }
    }
    return Size(canvasWidth, canvasHeight);
  }

  Future<void> loginDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('You have to login to add comments!',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Widget buildComment(CommentModel comment) {
    bool isExpanded = expandedComments.contains(comment.id);
    bool isLoading = loadingStates[comment.id] ?? false;
    bool isReplying = selectedId==comment.id;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.deepPurple.shade200),
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListTile(
            title: Row(
              children: [
                Text(
                  comment.authorUsername ?? "[deleted]",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: widget.reportedComment!=null && widget.reportedComment!.item1==comment.id ? Colors.red.shade800: null,
                  ),
                ),
                Padding(padding: const EdgeInsets.only(left:50),
                  child:
                  Text(
                      DateFormat('dd.MM.yyyy HH:mm').format(DateTime.parse(comment.date))
                  ),
                ),
                if(widget.reportedComment!=null && widget.reportedComment!.item1==comment.id)
                  Padding(padding: const EdgeInsets.only(left:50),
                    child:
                    Text('REPORTED',
                      style: TextStyle(
                        color: Colors.red.shade800,
                        fontWeight: FontWeight.bold,
                      ),),
                  ),
              ],
            ),
            subtitle:
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:[
                Text(
                  comment.text,
                  textScaler: const TextScaler.linear(1.3),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    if(comment.replies!=null && comment.replies!.isNotEmpty && isLoading)
                      const CircularProgressIndicator(),
                    if(comment.replies!=null && comment.replies!.isNotEmpty && !isLoading)
                      GestureDetector(
                        child: Icon(
                            isExpanded ? FontAwesomeIcons.minus : FontAwesomeIcons.plus
                        ),
                        onTap: (){
                          if (isExpanded) {
                            setState(() {
                              expandedComments.remove(comment.id);
                              recursionLevel.remove(comment.id);
                            });
                          } else {
                            if ((comment.repliesList==null || comment.repliesList!.isEmpty) && comment.replies!.isNotEmpty) {
                              fetchReplies(comment);
                              setState(() {
                                expandedComments.add(comment.id);
                                if(comment.type=="COMMENT" && recursionLevel.containsKey(comment.subjectId)){
                                  recursionLevel.putIfAbsent(comment.id, ()=>(recursionLevel[comment.subjectId]!+1));
                                  if(recursionLevel[comment.id]!>=10){
                                    recursionLevel.clear();
                                    expandedComments.clear();
                                    singleCommentThread = comment;
                                  }
                                }else if(comment.type=="TEMPLATE" || recursionLevel.isEmpty){
                                  recursionLevel.putIfAbsent(comment.id, () => 1);
                                }
                              });
                            } else {
                              setState(() {
                                expandedComments.add(comment.id);
                                if(comment.type=="COMMENT" && recursionLevel.containsKey(comment.subjectId)){
                                  recursionLevel.putIfAbsent(comment.id, ()=>(recursionLevel[comment.subjectId]!+1));
                                  if(recursionLevel[comment.id]!>=10){
                                    recursionLevel.clear();
                                    expandedComments.clear();
                                    singleCommentThread = comment;
                                  }
                                }else if(comment.type=="TEMPLATE" || recursionLevel.isEmpty){
                                  recursionLevel.putIfAbsent(comment.id, () => 1);
                                }
                              });
                            }
                          }
                        },
                      ),
                    if(comment.replies==null || comment.replies!.isEmpty)
                      const SizedBox(width: 24,),
                    if(userDto!=null)
                      Padding(padding: const EdgeInsets.only(left: 5),
                        child:
                        GestureDetector(
                          child: const Icon(
                              Icons.thumb_up_off_alt_outlined
                          ),
                          onTap: (){
                            if(!comment.likes!.contains(userDto!.id)){
                              addCommentLike(comment);
                            }else{
                              removeCommentLike(comment);
                            }
                          },
                        ),),
                    if(userDto==null)
                      const Padding(padding: EdgeInsets.only(left: 5),
                        child:
                        Icon(
                            Icons.thumb_up_off_alt_outlined
                        ),),
                    Text('${comment.likes!.length}'),
                    if(userDto!=null)
                      Padding(padding: const EdgeInsets.only(left: 5),
                        child:
                        GestureDetector(
                          child: const Icon(
                              Icons.thumb_down_off_alt_outlined
                          ),
                          onTap: (){
                            if(!comment.dislikes!.contains(userDto!.id)){
                              addCommentDislike(comment);
                            }else{
                              removeCommentDislike(comment);
                            }
                          },
                        ),),
                    if(userDto==null)
                      const Padding(padding: EdgeInsets.only(left: 5),
                        child:
                        Icon(
                            Icons.thumb_down_off_alt_outlined
                        ),),
                    Text('${comment.dislikes!.length}'),
                    if(userDto!=null && !isReplying)
                      Padding(padding: const EdgeInsets.only(left: 5),
                          child: ElevatedButton(
                              onPressed: (){
                                setState(() {
                                  selectedId = comment.id;
                                });
                              },
                              child: const Text('Reply'))
                      ),
                    if(userDto!=null)
                      Padding(padding: const EdgeInsets.only(left: 5),
                          child: ElevatedButton(
                              onPressed: (){
                                showReportDialog("COMMENT", comment.id);
                              },
                              child: const Text('Report'))
                      ),
                    if(userDto!=null && userDto!.id==comment.authorId)
                      Padding(padding: const EdgeInsets.only(left: 5),
                          child: ElevatedButton(
                              onPressed: () {
                                deleteComment(comment);
                              },
                              child: const Text('Delete'))
                      ),
                  ],
                ),
                if(isReplying)
                  Row(
                      children:[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child:
                            TextField(
                              onChanged: (value) {
                                if(userDto==null){
                                  loginDialog(context);
                                }else {
                                  setState(() {
                                    newReply = value;
                                  });
                                }
                              },
                              decoration: const InputDecoration(
                                hintText: "Add new reply...",
                                border: OutlineInputBorder(),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                            ),
                          ),),
                        IconButton(
                          onPressed: ()
                          async {
                            if(userDto==null){
                              loginDialog(context);
                            }else {
                              if (newReply != "") {
                                if (await addReply(comment)) {
                                  fetchReplies(comment);
                                }
                              }
                            }
                          },
                          icon: const Icon(Icons.check_circle_outline),),
                        IconButton(
                          onPressed: ()
                          {
                            setState(() {
                              selectedId = null;
                            });
                          },

                          icon: const Icon(Icons.cancel_outlined),),
                      ]),],),
          ),),
        if (isExpanded && comment.repliesList != null && comment.repliesList!.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Column(
              children: comment.repliesList!.map((reply) => buildComment(reply)).toList(),
            ),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (galleryImageDto == null || (galleryImageDto!=null && galleryImageDto!.id == null) || isFetchingComments) {
      return SelectionArea(child:
      Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.deepPurple.shade50,
            toolbarHeight: 60,
            automaticallyImplyLeading: false,
            title: widget.fromTemplate!=false?
            AppBarWithBack(userDto: userDto,) : AppBarWithBack(userDto: userDto, withBack: false),
          ),
          body: const Stack(
              children: [
                Center(
                  child:
                  SizedBox(
                    width: 50,
                    height: 50,
                    child: CircularProgressIndicator(),
                  ),
                ),
              ])));
    } else {
      setState(() {
        size = setImageSize(
            galleryImageDto!.image!.width, galleryImageDto!.image!.height);
      });
      return SelectionArea(child:Scaffold(
        appBar: AppBar(
        backgroundColor: Colors.deepPurple.shade50,
        toolbarHeight: 60,
        automaticallyImplyLeading: false,
          title: widget.fromTemplate!=false?
          AppBarWithBack(userDto: userDto,) : AppBarWithBack(userDto: userDto, withBack: false),

        ),
        body: Stack(
          children: [
            Container(
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ListView(
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: FittedBox(
                                  alignment: Alignment.center,
                                  fit: BoxFit.scaleDown,
                                  child: Container(
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black),
                                      ),
                                      child: RawImage(
                                        width: size!.width,
                                        height: size!.height,
                                        image: galleryImageDto!.image!,
                                      )))),
                          Expanded(
                              child: Container(
                                  height: size!.height,
                                  padding: EdgeInsets.all(15),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Author: ${galleryImageDto!.authorName ?? ""}',
                                        textScaler: const TextScaler.linear(2),
                                      ),
                                      if(!isEdit)
                                      Text(
                                        'Description: ${galleryImageDto!.description ?? ""}',
                                        textScaler: TextScaler.linear(1.5),
                                      ),
                                      if(isEdit)
                                        Row(
                                          children: [
                                            Expanded(
                                              child: TextField(
                                                onChanged: (value) {
                                                  setState(() {
                                                    newDesc = value;
                                                  });
                                                },
                                                decoration: const InputDecoration(
                                                  hintText: "Enter new description",
                                                  border: OutlineInputBorder(),
                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                ),
                                              ),
                                            ),
                                            IconButton(
                                                onPressed: (){
                                                  setState(() {
                                                    isEdit = false;
                                                  });
                                                  saveNewGIDescription();
                                                },
                                                icon: const Icon(Icons.check_circle_outline)),
                                            IconButton(
                                                onPressed: (){
                                                  setState(() {
                                                    isEdit = false;
                                                  });
                                                },
                                                icon: const Icon(Icons.cancel_outlined))
                                          ],
                                        ),
                                      Text(
                                        'Accuracy: ${galleryImageDto!.actualScore}',
                                      ),
                                        Text(DateFormat('dd.MM.yyyy HH:mm').format(dateValue)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          if(userDto!=null)
                                          GestureDetector(
                                            child: const Icon(
                                                Icons.thumb_up_off_alt_outlined
                                            ),
                                            onTap: (){
                                              if(!galleryImageDto!.likes.contains(userDto!.id)){
                                                addLike();
                                              }else{
                                                removeLike();
                                              }
                                            },
                                          ),
                                          if(userDto==null)
                                            const Icon(
                                                Icons.thumb_up_off_alt_outlined
                                            ),
                                          Text('${galleryImageDto!.likes.length}'),
                                          if(userDto!=null)
                                            GestureDetector(
                                              child: const Icon(
                                                  Icons.thumb_down_off_alt_outlined
                                              ),
                                              onTap: (){
                                                if(!galleryImageDto!.dislikes.contains(userDto!.id)){
                                                  addDislike();
                                                }else{
                                                  removeDislike();
                                                }
                                              },
                                            ),
                                          if(userDto==null)
                                            const Icon(
                                                Icons.thumb_down_off_alt_outlined
                                            ),
                                          Text('${galleryImageDto!.dislikes.length}'),
                                        ],
                                      ),
                                      if(userDto!=null)
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          ElevatedButton(
                                            onPressed: ()
                                            {
                                              showReportDialog("IMAGE", galleryImageDto!.id);
                                            },
                                            style: ElevatedButton.styleFrom(
                                              minimumSize: const Size(90, 26),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                              backgroundColor: Colors.deepPurple.shade100,
                                              foregroundColor: Colors.white,
                                            ),
                                            child:
                                            const Text('Report'),),
                                          if(userDto!.id==galleryImageDto!.authorId || userDto!.role=="ADMIN" || userDto!.role=="MODER")
                                          ElevatedButton(
                                            onPressed: ()
                                            {
                                              deleteGI();
                                              // Navigator.pop(context, galleryImageDto!.id);
                                              Navigator.pushAndRemoveUntil(
                                                context,
                                                MaterialPageRoute(builder: (context) => TemplatePage(
                                                    templateId: widget.templateId,
                                                    userDto: userDto, withBack: false)),
                                                    (Route<dynamic> route) => false,
                                              );
                                              },
                                            style: ElevatedButton.styleFrom(
                                              minimumSize: const Size(90, 26),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                              backgroundColor: Colors.deepPurple.shade100,
                                              foregroundColor: Colors.white,
                                            ),
                                            child:
                                            const Text('Delete image'),),
                                          if(userDto!=null && userDto!.id==galleryImageDto!.authorId)
                                          ElevatedButton(
                                            onPressed: ()
                                            {
                                              setState(() {
                                                isEdit = true;
                                              });
                                              },
                                            style: ElevatedButton.styleFrom(
                                              minimumSize: const Size(90, 26),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                              backgroundColor: Colors.deepPurple.shade100,
                                              foregroundColor: Colors.white,
                                            ),
                                            child:
                                            const Text('Edit'),),
                                        ],
                                      ),
                                    ],
                                  ))),
                        ],
                      ),
                      Row(
                          children:[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 5),
                                child:
                                TextField(
                                  onChanged: (value) {
                                    if(userDto==null){
                                      loginDialog(context);
                                    }else {
                                      setState(() {
                                        newComment = value;
                                      });
                                    }
                                  },
                                  controller: commentFieldController,
                                  decoration: const InputDecoration(
                                    hintText: "Add new comment...",
                                    border: OutlineInputBorder(),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                ),
                              ),),
                            ElevatedButton(
                              onPressed: ()
                              async {
                                if(userDto==null){
                                  loginDialog(context);
                                }else {
                                  if (newComment != "") {
                                    if (await addComment()) {
                                      if (mounted) {
                                        setState(() {
                                          newComment = null;
                                          commentFieldController.text = "";
                                          recursionLevel.clear();
                                          expandedComments.clear();
                                          singleCommentThread = null;
                                        });
                                      }
                                    }
                                  }
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                backgroundColor: Colors.deepPurple.shade100,
                                foregroundColor: Colors.white,
                              ),
                              child:
                              const Text('Add'),),
                          ]),
                      if(singleCommentThread==null && galleryImageDto!.comments!=null && galleryImageDto!.comments!.isNotEmpty)
                        ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: galleryImageDto!.comments!.map((comment) => buildComment(comment)).toList(),
                        ),
                      if(singleCommentThread!=null)
                        ElevatedButton(
                            onPressed: () {
                              setState(() {
                                recursionLevel.clear();
                                expandedComments.clear();
                                singleCommentThread = null;
                              });
                            },
                            child: const Text("Return to main comments thread")),
                      if(singleCommentThread!=null && singleCommentThread!.repliesList!=null && singleCommentThread!.repliesList!.isNotEmpty)
                        ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: singleCommentThread!.repliesList!.map((comment) => buildComment(comment)).toList(),
                        ),
                    ]))
          ],
        ),
      ));
    }
  }

}