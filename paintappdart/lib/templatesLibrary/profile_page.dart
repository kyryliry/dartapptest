import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/subscriptions_page.dart';
import 'package:paintappdart/templatesLibrary/template_page.dart';
import 'dart:ui' as ui;

import '../utils/app_bar_with_back.dart';
import '../utils/session_manager.dart';
import '../model/template_model.dart';
import '../model/user_model.dart';
import 'library_main_page.dart';

class ProfilePage extends StatefulWidget{
  final UserModel? userDto;
  final int profileId;
  const ProfilePage({Key? key, required this.userDto, required this.profileId}) : super(key: key);

  @override
  ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  UserModel? userDto;
  UserModel? profile;
  bool isEdit = false;
  Size? size;
  ui.Image? image;
  int playedTemplatesNum = 0;
  List<TemplateModel> published = <TemplateModel>[];
  List<TemplateModel> finished = <TemplateModel>[];
  bool? amIFollow;
  final TextEditingController textFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    userDto = widget.userDto;
    loadImage('assets/images/default_avatar.png');
    getUserData();
  }

  Future<void> getUserData() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final user = await get(Uri.parse("http://localhost:8080/api/users/${widget.profileId}"), headers: headers);
    if (user.statusCode == 200) {
      final userData = json.decode(user.body);
      if(user.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${user.headers['authorization']}');
      }
      setState(() {
        profile = UserModel(id: userData['id'],
            userName: userData['username'],
            role: userData['role'],
            isBlocked: userData['isBlocked'],
            email: userData['email'],
            followersCount: userData['followers'],
            followingCount: userData['following'],
            globalCreatorRating: userData['globalCreatorRating'],
            globalPlayerRating: userData['globalPlayerRating']);
        playedTemplatesNum = userData['playerTemplatesNum'];
        getTemplates();
        if(userDto!=null && userData['id']!=userDto!.id) {
          checkIfUserFollows();
        }
      });
    }else{
      logger.i('get user data caused error: ${user.body}');
    }
  }

  Future<void> checkIfUserFollows() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await get(Uri.parse("http://localhost:8080/api/users/checkFollow/${profile!.id}"), headers: headers);
    if(response.statusCode==200){
      final item = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      if(item['follow']=="true"){
        setState(() {
          amIFollow = true;
        });
      }else{
        setState(() {
          amIFollow = false;
        });
      }
    }else{
      logger.i('check user following caused error: ${response.body}');
    }
  }

  Future<void> getTemplates() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await get(Uri.parse("http://localhost:8080/api/templates/allUser/simplified/${widget.profileId}"), headers: headers);

    if (response.statusCode == 200) {
      final all = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final publishedL = all['published'] as List;
      final finishedL = all['finished'] as List;

      for (var item in publishedL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          published.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }

      for (var item in finishedL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          finished.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }
    } else {
      logger.i('get templates in user profile caused error: ${response.body}');
    }
  }

  Future<void> loadImage(String assetPath) async {
    final ByteData data = await rootBundle.load(assetPath);
    final ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
    final ui.FrameInfo fi = await codec.getNextFrame();
    setState(() {
      image = fi.image;
    });
  }

  Size setImageSize(int width, int height) {
    double resizeFactor = 1;
    double canvasWidth = width.toDouble();
    double canvasHeight = height.toDouble();
    if (width > height &&
        width > (MediaQuery.of(context).size.width - 40) / 3) {
      resizeFactor = ((MediaQuery.of(context).size.width - 40) / 3) / width;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height > width &&
        height > (MediaQuery.of(context).size.height - 40) / 3) {
      resizeFactor = ((MediaQuery.of(context).size.height - 40) / 3) / height;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height == width &&
        (width > (MediaQuery.of(context).size.width - 40) / 3 ||
            height > (MediaQuery.of(context).size.height - 40) / 3)) {
      if ((MediaQuery.of(context).size.width - 40) / 3 >
          (MediaQuery.of(context).size.height - 40) / 3) {
        resizeFactor = ((MediaQuery.of(context).size.height - 40) / 3) / height;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      } else {
        resizeFactor = ((MediaQuery.of(context).size.width - 40) / 3) / width;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      }
    }
    return Size(canvasWidth, canvasHeight);
  }

  Future<bool> updateUser(UserModel user, String password) async {
    if(profile!.id!=userDto!.id) {
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if (jsessionId != null) {
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse(
          "http://localhost:8080/api/users/admins/edit/${user.id}");
      final Map<String, dynamic> jsonFields = {
        'id': user.id,
        'username': user.userName,
        'password': password,
        'email': user.email,
        'role': user.role
      };
      final editResponse = await put(
        url,
        headers: headers,
        body: jsonEncode(jsonFields),
      );
      if (editResponse.statusCode == 200) {
        if(editResponse.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${editResponse.headers['authorization']}');
        }
        return true;
      } else {
        logger.i('user editing by admin in profile page caused error: ${editResponse.body}');
        return false;
      }
    }else{
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if (jsessionId != null) {
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse(
          "http://localhost:8080/api/users/edit");
      final Map<String, dynamic> jsonFields = {
        'id': user.id,
        'username': user.userName,
        'password': password,
        'email': user.email,
        'role': profile!.role
      };
      final editResponse = await put(
        url,
        headers: headers,
        body: jsonEncode(jsonFields),
      );
      if (editResponse.statusCode == 200) {
        final item = json.decode(editResponse.body);
        if(editResponse.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${editResponse.headers['authorization']}');
        }
        setState(() {
          logout();
        });
        return true;
      } else {
        logger.i('user editing in profile page caused error: ${editResponse.body}');
        return false;
      }
    }
  }

  Future<void> showEditDialog(UserModel user) async {
    String oldUserName = user.userName;
    String oldRole = user.role;
    String oldEmail = user.email!;
    String newPassword = "";
    TextEditingController usernameController = TextEditingController(text: oldUserName);
    TextEditingController emailController = TextEditingController(text: oldEmail);
    bool usernameEdit = false;
    bool emailEdit = false;
    bool passwordEdit = false;
    bool roleEdit = false;
    bool roleListOpen = false;
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: const Text('Edit user data'),
                content: Container(
                  width: 300,
                  child:
                  SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        if(!usernameEdit)
                          Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Current username: '),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        usernameEdit = true;
                                      });

                                    },
                                    child: const Text('Edit username'))
                              ],
                            ),),
                        if(!usernameEdit)
                          Text(user.userName, textScaler: const TextScaler.linear(1.5),),
                        if(usernameEdit)
                          Row(
                            children: [
                              Expanded(child:
                              TextField(
                                  controller: usernameController,
                                  onChanged: (value) {
                                    setState(() {
                                      user.userName = value;
                                    });
                                  }),),
                              IconButton(
                                  onPressed: () {
                                    setState( (){
                                      user.userName = oldUserName;
                                      usernameEdit = false;
                                      usernameController.text = oldUserName;
                                    });
                                  },
                                  icon: const Icon(Icons.cancel_outlined))
                            ],
                          ),
                        if(!emailEdit)
                          Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Current email: '),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        emailEdit = true;
                                      });
                                    },
                                    child: const Text('Edit email'))
                              ],
                            ),),
                        if(!emailEdit)
                          Text(user.email!, textScaler: const TextScaler.linear(1.5),),
                        if(emailEdit)
                          Row(
                            children: [
                              Expanded(child:
                              TextField(
                                  controller: emailController,
                                  onChanged: (value) {
                                    setState(() {
                                      user.email = value;
                                    });
                                  }),),
                              IconButton(
                                  onPressed: () {
                                    setState( (){
                                      user.email = oldEmail;
                                      emailEdit = false;
                                      emailController.text = oldEmail;
                                    });
                                  },
                                  icon: const Icon(Icons.cancel_outlined))
                            ],
                          ),
                        if(!passwordEdit)
                          Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        passwordEdit = true;
                                      });
                                    },
                                    child: const Text('Change password'))
                              ],
                            ),),
                        if(passwordEdit)
                          Row(
                            children: [
                              Expanded(child:
                              TextField(
                                  onChanged: (value) {
                                    setState(() {
                                      newPassword = value;
                                    });
                                  }),),
                              IconButton(
                                  onPressed: () {
                                    setState( (){
                                      newPassword = "";
                                      passwordEdit = false;
                                    });
                                  },
                                  icon: const Icon(Icons.cancel_outlined))
                            ],
                          ),
                        if(!roleEdit && userDto!.role!="USER")
                          Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Current role: ${user.role}'),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        roleEdit = true;
                                      });
                                    },
                                    child: const Text('Edit role'))
                              ],
                            ),),
                        if(roleEdit)
                          Row(
                            children: [
                              Expanded(child:
                              ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    roleListOpen = !roleListOpen;
                                  });
                                },
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  backgroundColor: roleListOpen ? Colors.grey.shade300 : null,
                                ),
                                child: PopupMenuButton<String>(
                                  onSelected: (String role) {
                                    setState(() {
                                      user.role = role;
                                    });
                                  },
                                  itemBuilder: (BuildContext context) {
                                    return <PopupMenuEntry<String>>[
                                      PopupMenuItem<String>(
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(maxHeight: 200.0), // For example, 5 items * 40.0px each
                                          child: const SingleChildScrollView(
                                            child: Column(
                                                children: [
                                                  PopupMenuItem<String>(
                                                    value: 'USER',
                                                    child: Text('user'),
                                                  ),
                                                  PopupMenuItem<String>(
                                                    value: 'MODER',
                                                    child: Text('moder'),
                                                  ),
                                                  PopupMenuItem<String>(
                                                    value: 'ADMIN',
                                                    child: Text('admin'),
                                                  ),
                                                ]),
                                          ),
                                        ),
                                      ),
                                    ];
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(user.role),
                                      Icon(Icons.arrow_drop_down),
                                    ],
                                  ),
                                ),
                              ),
                              ),
                              IconButton(
                                  onPressed: () {
                                    setState( (){
                                      roleEdit = false;
                                    });
                                  },
                                  icon: const Icon(Icons.check_circle_outline)),
                              IconButton(
                                  onPressed: () {
                                    setState( (){
                                      user.role = oldRole;
                                      roleEdit = false;
                                    });
                                  },
                                  icon: const Icon(Icons.cancel_outlined))
                            ],
                          ),
                      ],
                    ),
                  ),),
                actions: <Widget>[
                  TextButton(
                    child: const Text('Cancel'),
                    onPressed: () {
                      setState(() {
                        user.userName = oldUserName;
                        user.email = oldEmail;
                        user.role = oldRole;
                        usernameEdit = false;
                        emailEdit = false;
                        passwordEdit = false;
                        roleEdit = false;
                      });
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    onPressed: () async {
                      if(await updateUser(user, newPassword)){
                        usernameEdit = false;
                        emailEdit = false;
                        passwordEdit = false;
                        roleEdit = false;
                        Navigator.of(context).pop();
                      }
                    },
                    child: const Text('Save'),
                  ),
                ],
              );
            },);}
    );
  }

  Future<void> deleteUsers() async {
    if(profile!.id!=userDto!.id) {
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse("http://localhost:8080/api/users/delete/set");
      List<Map<String, dynamic>> listOfObjects = [];
      Map<String, dynamic> u  = {
        'id':profile!.id
      };
      listOfObjects.add(u);
      final userResponse = await delete(
        url,
        headers: headers,
        body: jsonEncode(listOfObjects),
      );
      if (userResponse.statusCode == 200) {
        if(userResponse.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${userResponse.headers['authorization']}');
        }
        setState(() {
            Navigator.pop(context, true);
        });
      } else {
        logger.i('delete user by admin in profile page caused error: ${userResponse.body}');
      }
    }else{
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var url = Uri.parse("http://localhost:8080/api/users/delete");
      final userResponse = await delete(
        url, headers: headers
      );
      if (userResponse.statusCode == 200) {
        if(userResponse.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${userResponse.headers['authorization']}');
        }
        setState(() {
          logout();
        });
      } else {
        logger.i('delete user in profile page caused error: ${userResponse.body}');
      }
    }
  }

  Future<void> logout() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final temps = await get(Uri.parse("http://localhost:8080/logout"), headers: headers);

    if(temps.statusCode==200){
      SessionManager().setJsessionId("");
      if(mounted) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (
              context) => LibraryMainPage()),
              (Route<
              dynamic> route) => false,
        );
      }
    }else{
      logger.i('logout in profile page caused error: ${temps.body}');
    }
  }

  Future<void> blockUsers() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/block/set");

    List<Map<String, dynamic>> listOfObjects = [];
    Map<String, dynamic> u  = {
      'id':profile!.id
    };
    listOfObjects.add(u);
    final cats = await put(
      url,
      headers: headers,
      body: jsonEncode(listOfObjects),
    );
    if (cats.statusCode == 200) {
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      setState(() {
        getUserData();
      });
    } else {
      logger.i('block user in profile page caused error: ${cats.body}');
    }
  }

  Future<void> unblockUsers() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/unblock/set");
    List<Map<String, dynamic>> listOfObjects = [];
    Map<String, dynamic> u  = {
      'id':profile!.id
    };
    listOfObjects.add(u);
    final cats = await put(
      url,
      headers: headers,
      body: jsonEncode(listOfObjects),
    );
    if (cats.statusCode == 200) {
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      setState(() {
        getUserData();
      });
    } else {
      logger.i('unblock user in profile page caused error: ${cats.body}');
    }
  }

  Future<void> follow() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/follow/${profile!.id}");
    final follow = await put(url, headers: headers);
    if(follow.statusCode==200){
      if(follow.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${follow.headers['authorization']}');
      }
      setState(() {
        amIFollow = true;
        profile!.followersCount = profile!.followersCount! + 1;
      });
    }else{
      logger.i('follow user in profile page caused error: ${follow.body}');
    }
  }

  Future<void> unfollow() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/unfollow/${profile!.id}");
    final follow = await put(url, headers: headers);
    if(follow.statusCode==200){
      if(follow.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${follow.headers['authorization']}');
      }
      setState(() {
        amIFollow = false;
        profile!.followersCount = profile!.followersCount! - 1;
      });
    }else{
      logger.i('unfollow user in profile page caused error: ${follow.body}');
    }
  }

  Future<bool> createReport(String text) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/reports/create");
    final Map<String, dynamic> jsonFields = {
      'text': text,
      'type':'USER',
      'subjectId':'${profile!.id}'
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      return true;
    }else{
      logger.i('create report in profile page caused error: ${createResponse.body}');
      return false;
    }
  }

  Future<void> showReportDialog() async {
    String selectedOption = "";
    String reason = "";
    bool own = false;
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: const Text('Send report'),
                content: Container(
                  width: 300,
                  child:
                  SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        ListTile(
                          title: const Text('Spam'),
                          leading: Radio<String>(
                            value: 'Spam',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Bullying or harassment'),
                          leading: Radio<String>(
                            value: 'Bullying or harassment',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Nudity or sexual content'),
                          leading: Radio<String>(
                            value: 'Nudity or sexual content',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Hate speech or symbols'),
                          leading: Radio<String>(
                            value: 'Hate speech or symbols',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Scam or fraud'),
                          leading: Radio<String>(
                            value: 'Scam or fraud',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Your own reason'),
                          leading: Radio<String>(
                            value: 'Your own reason',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = true;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        if(own)
                          TextField(
                            onChanged: (value) {
                              setState(() {
                                reason = value;
                              });
                            },
                            controller: textFieldController,
                            decoration:
                            const InputDecoration(hintText: "Enter reason of your report"),
                          ),
                      ],
                    ),
                  ),),
                actions: <Widget>[
                  TextButton(
                    child: const Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    onPressed: () async {
                      if(selectedOption=="Your own reason"){
                        selectedOption = reason;
                      }
                      if(await createReport(selectedOption)){
                        Navigator.of(context).pop();
                        alertDialog(context);
                      }
                    },
                    child: const Text('Send'),
                  ),
                ],
              );
            },);}
    );
  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Thank you for your report!',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final ScrollController publishedScrollController = ScrollController();
    final ScrollController finishedScrollController = ScrollController();
    if ((userDto == null && widget.userDto!=null) || image==null || profile==null) {
      return const CircularProgressIndicator();
    } else {
      setState(() {
        size = setImageSize(
            image!.width, image!.height);
      });
      return SelectionArea(child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(userDto: userDto,),
        ),
        body: Stack(
          children: [
            Container(
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ListView(
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: FittedBox(
                                  alignment: Alignment.center,
                                  fit: BoxFit.scaleDown,
                                  child: Container(
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black),
                                      ),
                                      child: RawImage(
                                        width: size!.width,
                                        height: size!.height,
                                        image: image,
                                      )))),
                          Expanded(
                              child: Container(
                                  padding: EdgeInsets.all(15),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        profile!.userName ?? "",
                                        textScaler: const TextScaler.linear(1.5),
                                      ),
                                      Text(
                                        'Role: ${profile!.role}',
                                      ),
                                        Text(
                                          'Player rating points: ${profile!.globalPlayerRating!}',
                                        ),
                                      Text(
                                        'Creator rating points: ${profile!.globalCreatorRating!}',
                                      ),
                                      Text(
                                        'Amount of played templates: $playedTemplatesNum',
                                      ),
                                      Text(
                                        'Followers: ${profile!.followersCount!}',
                                      ),
                                      Text(
                                        'Following: ${profile!.followingCount!}',
                                      ),
                                    ],
                                  ))),
                          Expanded(
                              child: Container(
                                  padding: EdgeInsets.all(15),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.end,
                                    children: [
                                      if(widget.userDto!=null)
                                      ElevatedButton(
                                          onPressed: (){
                                            showReportDialog();
                                          },
                                          child: const Text("Report")),
                                      if(widget.userDto!=null && userDto!.id!=widget.profileId && amIFollow==false)
                                      ElevatedButton(
                                          onPressed: (){
                                            follow();
                                          },
                                          child: Text("Follow")),
                                      if(widget.userDto!=null && userDto!.id!=widget.profileId && amIFollow==true)
                                      ElevatedButton(
                                          onPressed: (){
                                            unfollow();
                                          },
                                          child: Text("Unfollow")),
                                      if(widget.userDto!=null && profile!=null && (userDto!.id==widget.profileId || userDto!.role!="USER"))
                                      ElevatedButton(
                                          onPressed: (){
                                            showEditDialog(profile!);
                                          },
                                          child: const Text("Edit profile")),
                                      if(widget.userDto!=null && userDto!.role=="ADMIN" && !profile!.isBlocked!)
                                      ElevatedButton(
                                          onPressed: (){
                                            blockUsers();
                                          },
                                          child: const Text("Block profile")),
                                      if(widget.userDto!=null && userDto!.role=="ADMIN" && profile!.isBlocked!)
                                        ElevatedButton(
                                            onPressed: (){
                                              unblockUsers();
                                            },
                                            child: const Text("Unblock profile")),
                                      if(widget.userDto!=null && (userDto!.role=="ADMIN" || userDto!.id==widget.profileId))
                                        ElevatedButton(
                                            onPressed: (){
                                              deleteUsers();
                                            },
                                            child: const Text("Delete profile")),
                                      ElevatedButton(
                                          onPressed: (){
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => SubscriptionsPage(userDto: widget.userDto, profileId: profile!.id)));
                                          },
                                          child: const Text("Subscriptions")),
                                    ],
                                  ))),
                        ],
                      ),
                      if(published.isNotEmpty)
                        Padding(padding: const EdgeInsets.only(top:15),
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Published plays', textScaler: TextScaler.linear(2),),
                            Container(
                                height: 250,
                                child: Scrollbar(
                                    thumbVisibility: true,
                                    thickness: 6.0,
                                    controller: publishedScrollController,
                                    radius: Radius.circular(5.0),
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: published.length,
                                      controller: publishedScrollController,
                                      itemBuilder: (context, index) {
                                        return
                                          GestureDetector(
                                              onTap: () {
                                                Navigator.push(context, MaterialPageRoute(
                                                    builder: (context) => TemplatePage(
                                                        templateId: published[index].id,
                                                        userDto: userDto!)));
                                              },child:
                                          Container(
                                              width: MediaQuery.of(context).size.width / (MediaQuery.of(context).size.width/250),
                                              padding: const EdgeInsets.all(4),
                                              child:
                                              Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: [
                                                    AspectRatio(
                                                      aspectRatio: 6 / 4,
                                                      child: Container(
                                                        padding: const EdgeInsets.only(top: 15,
                                                            left: 15,
                                                            right: 15,
                                                            bottom: 15),
                                                        child:
                                                        FittedBox(
                                                            fit: BoxFit.scaleDown,
                                                            child: Container(
                                                                height: 150,
                                                                decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      color: Colors.black),
                                                                ),
                                                                child: RawImage(
                                                                  image: published[index]
                                                                      .sampleImage,
                                                                ))),
                                                      ),
                                                    ),
                                                    AspectRatio(
                                                      aspectRatio: 8.0,
                                                      child: Text(published[index].name.split('_').length > 1 ? published[index].name.split('_')[1] : "",
                                                        textAlign: TextAlign.center,),
                                                    ),
                                                    AspectRatio(
                                                      aspectRatio: 8.0,
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment
                                                            .spaceAround,
                                                        children: [
                                                          Text(published[index].creatorName != null
                                                              ? published[index].creatorName!
                                                              : "error",
                                                            textAlign: TextAlign.center,),
                                                          Row(children: [
                                                            const Icon(
                                                                Icons.download
                                                            ),
                                                            Text(published[index].downloadsCount
                                                                .toString(),
                                                              textAlign: TextAlign.center,),
                                                          ],),
                                                        ],
                                                      ),
                                                    ),
                                                  ])));
                                      },
                                    )))
                          ],
                        ),),
                      if(finished.isNotEmpty)
                      Padding(padding: const EdgeInsets.only(top:15),
                      child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Finished plays', textScaler: TextScaler.linear(2),),
                            Container(
                                height: 250,
                                child: Scrollbar(
                                    thumbVisibility: true,
                                    thickness: 6.0,
                                    controller: finishedScrollController,
                                    radius: Radius.circular(5.0),
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: finished.length,
                                      controller: finishedScrollController,
                                      itemBuilder: (context, index) {
                                        return
                                          GestureDetector(
                                              onTap: () {

                                                Navigator.push(context, MaterialPageRoute(
                                                    builder: (context) => TemplatePage(
                                                        templateId: finished[index].id,
                                                        userDto: userDto!)));
                                              },child:
                                          Container(
                                              width: MediaQuery.of(context).size.width / (MediaQuery.of(context).size.width/250),
                                              padding: const EdgeInsets.all(4),
                                              child:
                                              Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: [
                                                    AspectRatio(
                                                      aspectRatio: 6 / 4,
                                                      child: Container(
                                                        padding: const EdgeInsets.only(top: 15,
                                                            left: 15,
                                                            right: 15,
                                                            bottom: 15),
                                                        child:
                                                        FittedBox(
                                                            fit: BoxFit.scaleDown,
                                                            child: Container(
                                                                height: 150,
                                                                decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      color: Colors.black),
                                                                ),
                                                                child: RawImage(
                                                                  image: finished[index]
                                                                      .sampleImage,
                                                                ))),
                                                      ),
                                                    ),
                                                    AspectRatio(
                                                      aspectRatio: 8.0,
                                                      child: Text(finished[index].name.split('_').length > 1 ? finished[index].name.split('_')[1] : "",
                                                        textAlign: TextAlign.center,),
                                                    ),
                                                    AspectRatio(
                                                      aspectRatio: 8.0,
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment
                                                            .spaceAround,
                                                        children: [
                                                          Text(finished[index].creatorName != null
                                                              ? finished[index].creatorName!
                                                              : "error",
                                                            textAlign: TextAlign.center,),
                                                          Row(children: [
                                                            const Icon(
                                                                Icons.download
                                                            ),
                                                            Text(finished[index].downloadsCount
                                                                .toString(),
                                                              textAlign: TextAlign.center,),
                                                          ],),
                                                        ],
                                                      ),
                                                    ),
                                                  ])));
                                      },
                                    )))
                          ],
                        ),),
                    ]))
          ],
        ),
      ));
    }
  }

}