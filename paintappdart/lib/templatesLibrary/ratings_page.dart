import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/profile_page.dart';

import '../utils/app_bar_with_back.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class RatingsPage extends StatefulWidget {
  final UserModel? userDto;

  RatingsPage({Key? key, required this.userDto}) : super(key: key);

  @override
  RatingsPageState createState() => RatingsPageState();
}

class RatingsPageState extends State<RatingsPage>
    with SingleTickerProviderStateMixin {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<UserModel> players = <UserModel>[];
  List<UserModel> creators = <UserModel>[];
  Map<String, int> totalPages = <String, int>{};
  Map<String, int> currentPage = <String, int>{};
  int amountPerPage = 10;
  String userType = "player";
  ui.Image? image;

  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    totalPages.putIfAbsent('player', () => 0);
    totalPages.putIfAbsent('creator', () => 0);
    currentPage.putIfAbsent('player', () => 1);
    currentPage.putIfAbsent('creator', () => 1);
    fetchUsers(0, amountPerPage, "player");
    fetchUsers(0, amountPerPage, "creator");
    loadImage('assets/images/default_avatar.png');
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  Future<void> switchToTab(int tabIndex, String type) async {
    _tabController?.animateTo(tabIndex);
  }

  Future<void> fetchUsers(int page, int size, String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if (jsessionId != null) {
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final String url =
        'http://localhost:8080/api/users/get/rating/$type/paged?page=$page&size=$size';
    final response = await get(Uri.parse(url), headers: headers);

    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      if (response.headers['authorization'] != null) {
        SessionManager()
            .setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final Map<String, dynamic> data = item['page'];
      List<dynamic> users = data['content'] as List;
      List<UserModel> tmpUsers = <UserModel>[];
      for (var user in users) {
        tmpUsers.add(UserModel(
            id: user['id'],
            userName: user['username'],
            role: user['role'],
            isBlocked: user['isBlocked'],
            globalPlayerRating: user['globalPlayerRating'],
            globalCreatorRating: user['globalCreatorRating'],
            numOfTemps: user['playerTemplatesNum']));
      }
      setState(() {
        if (type == "player") {
          players.clear();
          players = List.of(players)..addAll(tmpUsers);
          totalPages['player'] = data['totalPages'];
        } else if (type == "creator") {
          creators.clear();
          creators = List.of(creators)..addAll(tmpUsers);
          totalPages['creator'] = data['totalPages'];
        }
      });
    } else {
      logger.i('load users caused error in ratings: ${response.body}');
    }
  }

  Widget buildPagination(int currentPage, int totalPages, String type) {
    const int maxVisiblePages = 5;
    List<Widget> pageButtons = [];

    int startPage = max(currentPage - maxVisiblePages ~/ 2, 1);
    int endPage = min(startPage + maxVisiblePages - 1, totalPages);
    startPage = max(endPage - maxVisiblePages + 1, 1);

    if (startPage > 1) {
      pageButtons.add(buildPageButton(1, type));
    }

    if (startPage > 2) {
      pageButtons.add(const Padding(
          padding: EdgeInsets.only(left: 5, right: 5),
          child: Text("...", style: TextStyle(color: Colors.black))));
    }

    for (int i = startPage; i <= endPage; i++) {
      pageButtons.add(buildPageButton(i, type, isCurrent: i == currentPage));
    }

    if (totalPages > endPage) {
      if (endPage < totalPages - 1) {
        pageButtons.add(const Padding(
            padding: EdgeInsets.only(left: 5, right: 5),
            child: Text("...", style: TextStyle(color: Colors.black))));
      }
      pageButtons.add(buildPageButton(totalPages - 1, type));
      pageButtons.add(buildPageButton(totalPages, type));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: pageButtons,
    );
  }

  Widget buildPageButton(int page, String type, {bool isCurrent = false}) {
    return Padding(
        padding: const EdgeInsets.only(left: 2, right: 2),
        child: TextButton(
          onPressed: () {
            setState(() {
              currentPage[type] = page;
            });
            fetchUsers(page - 1, amountPerPage, type);
          },
          style: TextButton.styleFrom(
            backgroundColor:
                isCurrent ? Colors.deepPurple : Colors.deepPurple.shade50,
            foregroundColor: isCurrent ? Colors.white : null,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          ),
          child: Text((page).toString()),
        ));
  }

  Future<void> loadImage(String assetPath) async {
    final ByteData data = await rootBundle.load(assetPath);
    final ui.Codec codec =
        await ui.instantiateImageCodec(data.buffer.asUint8List());
    final ui.FrameInfo fi = await codec.getNextFrame();
    setState(() {
      image = fi.image;
    });
  }

  Widget buildUserList(List<UserModel> users, String type) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: (MediaQuery.of(context).size.width - 5) / 6,
                alignment: Alignment.center,
                child: Text("№"),
              ),
              Container(
                width: (MediaQuery.of(context).size.width - 5) / 3,
                alignment: Alignment.center,
                child: Text("User"),
              ),
              Container(
                width: (MediaQuery.of(context).size.width - 5) / 4,
                alignment: Alignment.center,
                child: Text("Total points"),
              ),
              Container(
                width: (MediaQuery.of(context).size.width - 5) / 4,
                alignment: Alignment.center,
                child: Text(type=="player" ? "Played templates" : "Created templates"),
              ),
            ],
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: users.length,
            itemBuilder: (context, index) {
              return Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                  Container(
                  width: (MediaQuery.of(context).size.width - 5) / 6,
              alignment: Alignment.center,
                      child: Text("${index+1}"),
                  ),
              Container(
              width: (MediaQuery.of(context).size.width - 5) / 3,
              alignment: Alignment.center,
              child:
                      GestureDetector(
                        onTap: () async {
                          var result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ProfilePage(
                                userDto: widget.userDto,
                                profileId: users[index].id,
                              ),
                            ),
                          );
                          if (result != null && result == true) {
                            fetchUsers(0, amountPerPage, type);
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                            ),
                            height: 65,
                            width: MediaQuery.of(context).size.width / 3,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      FittedBox(
                                          alignment: Alignment.center,
                                          fit: BoxFit.scaleDown,
                                          child: Container(
                                              alignment: Alignment.center,
                                              child: RawImage(
                                                width: 60,
                                                height: 60,
                                                image: image,
                                              ))),
                                      SizedBox(width: 8),
                                      Text(
                                        users[index].userName,
                                        textScaler:
                                            const TextScaler.linear(1.5),
                                        style: TextStyle(
                                          color: users[index].isBlocked!
                                              ? Colors.red.shade800
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),),
              Container(
              width: (MediaQuery.of(context).size.width - 5) / 4,
              alignment: Alignment.center,
              child:
                      Text(type=="player" ? "${users[index].globalPlayerRating!}" : "${users[index].globalCreatorRating!}"),),
              Container(
              width: (MediaQuery.of(context).size.width - 5) / 4,
              alignment: Alignment.center,
              child:
                      Text("${users[index].numOfTemps!}"),),
                    ],
                  ));
            },
          ),
        ),
        Container(
          height: 40,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.only(bottom: 10),
          child: buildPagination(currentPage[type]!, totalPages[type]!, type),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (image == null) {
      return const CircularProgressIndicator();
    } else {
      return SelectionArea(
          child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(
            userDto: widget.userDto, withBack: false,
          ),
        ),
        body: Stack(children: [
          Column(
            children: [
              TabBar(
                controller: _tabController,
                tabs: const [
                  Tab(text: 'Players Rating'),
                  Tab(text: 'Creators Rating'),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    buildUserList(players, "player"),
                    buildUserList(creators, "creator"),
                  ],
                ),
              ),
            ],
          ),
        ]),
      ));
    }
  }
}
