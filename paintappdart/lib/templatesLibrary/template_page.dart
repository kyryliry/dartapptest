import 'dart:convert';
import 'dart:ui' as ui;

import 'package:cyclop/cyclop.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/all_gallery_images_page.dart';
import 'package:paintappdart/templatesLibrary/gallery_image_page.dart';
import 'package:paintappdart/templatesLibrary/profile_page.dart';
import 'package:paintappdart/utils/app_bar_with_back.dart';
import 'package:paintappdart/model/comment_model.dart';
import 'package:paintappdart/model/gallery_image_model.dart';
import 'package:paintappdart/model/template_model.dart';
import 'package:paintappdart/model/user_model.dart';
import 'package:tuple/tuple.dart';

import '../drawing/redraw_mode_page.dart';
import '../model/category_model.dart';
import '../utils/session_manager.dart';
import 'library_main_page.dart';

class TemplatePage extends StatefulWidget {
  final int templateId;
  final UserModel? userDto;
  final CommentModel? reportedCommentParent;
  final Tuple3<int, int, int>? reportedComment;
  final bool? withBack;

  const TemplatePage({Key? key, required this.templateId, required this.userDto, this.reportedComment, this.reportedCommentParent, this.withBack}) : super(key: key);

  @override
  TemplatePageState createState() => TemplatePageState();
}

class TemplatePageState extends State<TemplatePage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  TemplateModel? templateDto;
  String? creatorName;
  Size? size;
  UserModel? userDto;
  bool isUsageExist = false;
  bool isEdit = false;
  String? newDesc;
  String? newComment;
  String? newReply;
  int? selectedId;
  final TextEditingController textFieldController = TextEditingController();
  Set<int> expandedComments = <int>{};
  Map<int, bool> loadingStates = {};
  Map<int, int> recursionLevel = {};
  CommentModel? singleCommentThread;
  final TextEditingController commentFieldController = TextEditingController();
  bool withBack = true;

  @override
  void initState() {
    super.initState();
    if(widget.withBack!=null){
      withBack = widget.withBack!;
    }else{
      withBack = true;
    }
    getTemplateData();
    if(widget.userDto!=null) {
      userDto = widget.userDto;
    }
    if(widget.reportedCommentParent!=null && widget.reportedComment!=null && widget.reportedComment!.item2==2){
      CommentModel com = widget.reportedCommentParent!;
      fetchReplies(com);
      recursionLevel.clear();
      expandedComments.clear();
      singleCommentThread = com;
    }
  }

  Future<void> getTemplateData() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await get(Uri.parse(
        "http://localhost:8080/api/templates/getTwSI/${widget.templateId}"), headers: headers);

    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final String base64Image = item['image'];
      final id = item['id'];
      final name = item['name'];
      final creatorId = item['creatorId'];
      final amount = item['layersAmount'];
      final dCount = item['downloadsCount'];
      final sDesc = item['sampleDescription'];
      final score = item['maxScore'];
      final date = item['uploadDate'] != "" ? item['uploadDate'] : DateTime
          .now().toString();
      final likes = item['likes'] as List;
      final dislikes = item['dislikes'] as List;
      final cats = item['categories'];
      List<dynamic> jsonList = json.decode(cats);

      List<CategoryModel> categories =
      jsonList.map((jsonItem) => CategoryModel.fromJson(jsonItem)).toList();
      var imageBytes = base64Decode(base64Image);
      ui.Codec codec = await ui.instantiateImageCodec(imageBytes);
      ui.FrameInfo frame = await codec.getNextFrame();

      List<GalleryImageModel> galleryImages = <GalleryImageModel>[];
      final images = item['images'];
      List<dynamic> imagesList = images;
      for (var io in imagesList) {
        String i = io['image'];
        var iBytes = base64Decode(i);
        ui.Codec codec1 = await ui.instantiateImageCodec(iBytes);
        ui.FrameInfo frame1 = await codec1.getNextFrame();
        List<dynamic> likes = io['likes'] as List;
        List<dynamic> dislikes = io['dislikes'] as List;
        galleryImages.add(GalleryImageModel(id: io['id'],
            description: io['description'],
            usageId: io['usageId'],
            authorId: io['authorId'],
            authorName: io['authorName'],
            image: frame1.image,
            uploadDate: io['uploadDate'],
            actualScore: io['actualScore'],
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>()));
      }
      List<CommentModel> comments = <CommentModel>[];
      final comms = item['comments'];
      CommentModel? existCom;
      List<dynamic> commentsList = comms;
      for (var io in commentsList) {
        List<dynamic> likes = io['likes'] as List;
        List<dynamic> dislikes = io['dislikes'] as List;
        List<dynamic> replies = io['repliesIds'] as List;
        if(widget.reportedCommentParent!=null && widget.reportedComment!.item2!=2 &&
          io['id']==widget.reportedCommentParent!.id){
            existCom = CommentModel(id: io['id'],
                text: io['text'],
                authorId: io['authorId'],
                authorUsername: io['authorUsername'],
                date: io['date'],
                subjectId: io['subjectId'],
                type: io['type'],
                replies: replies.cast<int>(),
                likes: likes.cast<int>(),
                dislikes: dislikes.cast<int>());
          continue;
        }
        comments.add(CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>()));
      }
      if(existCom!=null){
        comments.insert(0, existCom);
      }


      setState(() {
        var image = frame.image;
        templateDto = TemplateModel(
            id: id,
            creatorId: creatorId,
            name: name,
            sampleImage: image,
            categories: categories,
            galleryImages: galleryImages,
            layersAmount: amount,
            downloadsCount: dCount,
            uploadDate: date,
            description: sDesc,
        likes: likes.cast<int>(),
        dislikes: dislikes.cast<int>(),
        comments: comments,
        maxScore: score);
        size = setImageSize(
            templateDto!.sampleImage.width, templateDto!.sampleImage.height);
      });

      if (userDto != null) {
        String? jsessionId = SessionManager().getJsessionId();
        Map<String, String>? headers1 = {'Content-Type': 'application/json'};
        if(jsessionId!=null){
          headers1.putIfAbsent('Authorization', () => jsessionId);
        }
        final getUsage = await get(Uri.parse(
            "http://localhost:8080/api/users/usage/checkExist/${userDto!
                .id}/${templateDto!.id}"), headers: headers1);
        if (getUsage.statusCode == 200 && getUsage.body != "") {
          if(getUsage.headers['authorization']!=null) {
            SessionManager().setJsessionId('Bearer ${getUsage.headers['authorization']}');
          }
          setState(() {
            isUsageExist = true;
          });
        }
    }

      if(creatorId!=null){
        String? jsessionId = SessionManager().getJsessionId();
        Map<String, String>? headers1 = {'Content-Type': 'application/json'};
        if(jsessionId!=null){
          headers1.putIfAbsent('Authorization', () => jsessionId);
        }
        final userRequest = await get(Uri.parse(
            "http://localhost:8080/api/users/$creatorId"), headers: headers1);

        if (userRequest.statusCode == 200) {
          final user = json.decode(userRequest.body);
          if(userRequest.headers['authorization']!=null) {
            SessionManager().setJsessionId('Bearer ${userRequest.headers['authorization']}');
          }
          setState(() {
            creatorName = user['username'];
          });
        }
      }


    } else {
      logger.i('get template data caused error: ${response.body}');
    }



  }
  Future<void> openGI(int index) async{
    final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => GalleryImagePage(galleryImageDto: templateDto!.galleryImages![index], userDto: userDto, templateId: templateDto!.id, fromTemplate: true,)));
    if(result!=null){
      GalleryImageModel? galleryImageDto;
      for(var i in templateDto!.galleryImages!){
        if(i.id==result){
          galleryImageDto = i;
        }
      }
      setState(() {
        templateDto!.galleryImages!.remove(galleryImageDto);
      });
    }
  }

  Future<void> createTemplateUsage() async {
    final Map<String, dynamic> jsonFields = {
      'playerId': userDto!.id,
      'parentId': templateDto!.id,
    };
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/users/usage/new");
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if (createResponse.statusCode == 200) {
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
    }else{
      logger.i('create template usage in template page caused error: ${createResponse.body}');
    }
    setState(() {
      isUsageExist = true;
    });
  }

  Future<void> saveNewGIDescription() async {
    setState(() {
      templateDto!.description = newDesc!;
    });

    final Map<String, dynamic> jsonFields = {
      'description': templateDto!.description,
    };
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/${templateDto!.id}/sampleDesc/edit");
    final finishResponse = await put(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(finishResponse.statusCode==200){
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
    }else{
      logger.i('edit sample image description caused error: ${finishResponse.body}');
    }
  }


  Future<void> addLike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/template/${templateDto!.id}/addLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'] as List;
      setState(() {
        templateDto!.likes = jsonList.cast<int>();
      });
      if(item['dislikes']!=null){
        List<dynamic> jsonList2 = item['dislikes'] as List;
        setState(() {
          templateDto!.dislikes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add like to template caused error ${finishResponse.body}');
    }
  }

  Future<void> removeLike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/template/${templateDto!.id}/removeLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'];
      setState(() {
        templateDto!.likes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove like from template caused error ${finishResponse.body}');
    }
  }

  Future<void> addDislike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/template/${templateDto!.id}/addDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
        List<dynamic> jsonList = item['dislikes'] as List;
      setState(() {
        templateDto!.dislikes = jsonList.cast<int>();
      });
      if(item['likes']!=null){
        List<dynamic> jsonList2 = item['likes'] as List;
        setState(() {
          templateDto!.likes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add dislike to template caused error ${finishResponse.body}');
    }
  }

  Future<void> removeDislike() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/template/${templateDto!.id}/removeDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'];
      setState(() {
        templateDto!.dislikes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove dislike from template caused error ${finishResponse.body}');
    }
  }


  Future<void> addCommentLike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/addLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'] as List;
      setState(() {
        commentDto.likes = jsonList.cast<int>();
      });
      if(item['dislikes']!=null){
        List<dynamic> jsonList2 = item['dislikes'] as List;
        setState(() {
          commentDto.dislikes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add like to template comment caused error ${finishResponse.body}');
    }
  }

  Future<void> removeCommentLike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/removeLike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['likes'];
      setState(() {
        commentDto.likes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove like from template comment caused error ${finishResponse.body}');
    }
  }

  Future<void> addCommentDislike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/addDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      var item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'] as List;
      setState(() {
        commentDto.dislikes = jsonList.cast<int>();
      });
      if(item['likes']!=null){
        List<dynamic> jsonList2 = item['likes'] as List;
        setState(() {
          commentDto.likes = jsonList2.cast<int>();
        });
      }
    }else{
      logger.i('add dislike to template comment caused error ${finishResponse.body}');
    }
  }

  Future<void> removeCommentDislike(CommentModel commentDto) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/comment/${commentDto.id}/removeDislike");
    final finishResponse = await put(
        url, headers: headers
    );
    if(finishResponse.statusCode==200){
      final item = json.decode(finishResponse.body);
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
      List<dynamic> jsonList = item['dislikes'];
      setState(() {
        commentDto.dislikes = jsonList.cast<int>();
      });
    }else{
      logger.i('remove dislike from template comment caused error ${finishResponse.body}');
    }
  }

  Future<void> fetchReplies(CommentModel comment) async {
    setState(() {
      loadingStates[comment.id] = true;
    });
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/comments/${comment.id}/replies");
    final finishResponse = await get(
        url, headers: headers
    );
    List<CommentModel> comments = <CommentModel>[];
    final item = json.decode(finishResponse.body);
    if(finishResponse.headers['authorization']!=null) {
      SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
    }
    final commentsList = item['comments'] as List;
    CommentModel? existCom;
    for (var io in commentsList) {
      List<dynamic> likes = io['likes'] as List;
      List<dynamic> dislikes = io['dislikes'] as List;
      List<dynamic> replies = io['repliesIds'] as List;
      if(widget.reportedComment!=null && widget.reportedComment!.item2==2 && widget.reportedComment!.item3!=0 && widget.reportedComment!.item3==io['id']){
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }else if(widget.reportedComment!=null && widget.reportedComment!.item2==2 && widget.reportedComment!.item1==io['id']) {
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }else if(widget.reportedComment!=null && widget.reportedComment!.item2==1 && widget.reportedComment!.item1==io['id']) {
        existCom = CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>());
        continue;
      }
      comments.add(CommentModel(id: io['id'],
          text: io['text'],
          authorId: io['authorId'],
          authorUsername: io['authorUsername'],
          date: io['date'],
          subjectId: io['subjectId'],
          type: io['type'],
          replies: replies.cast<int>(),
          likes: likes.cast<int>(),
          dislikes: dislikes.cast<int>()));

    }
    if(existCom!=null) {
      comments.insert(0, existCom);
    }
    setState(() {
      comment.repliesList = comments;
      loadingStates[comment.id] = false;
      expandedComments.add(comment.id);
    });
  }

  Future<bool> addComment() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/${templateDto!.id}/comments/new");
    final Map<String, dynamic> jsonFields = {
      'text': newComment,
      'type': 'TEMPLATE',
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      final io = json.decode(createResponse.body);
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      setState(() {
        List<dynamic> likes = io['likes'] as List;
        List<dynamic> dislikes = io['dislikes'] as List;
        List<dynamic> replies = io['repliesIds'] as List;
        templateDto!.comments!.add(CommentModel(id: io['id'],
            text: io['text'],
            authorId: io['authorId'],
            authorUsername: io['authorUsername'],
            date: io['date'],
            subjectId: io['subjectId'],
            type: io['type'],
            replies: replies.cast<int>(),
            likes: likes.cast<int>(),
            dislikes: dislikes.cast<int>()));
      });
      return true;
    }else{
      logger.i('add comment to template caused error ${createResponse.body}');
      return false;
    }
  }

  Future<bool> addReply(CommentModel parent) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/templates/${templateDto!.id}/comments/newReply");
    final Map<String, dynamic> jsonFields = {
      'text': newReply,
      'subjectId':parent.id,
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      setState(() {
        newReply = null;
        selectedId = null;
      });
      return true;
    }else{
      logger.i('add reply to template comment caused error ${createResponse.body}');
      return false;
    }
  }

  Future<bool> deleteComment(CommentModel comment) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/templates/${templateDto!.id}/comments/${comment.id}/delete");
    final deleteResponse = await delete(
      url, headers: headers
    );
    if(deleteResponse.statusCode==200){
      if(deleteResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${deleteResponse.headers['authorization']}');
      }
      setState(() {
        loadingStates[comment.id] = true;
        comment.text = "Comment has been deleted.";
        comment.authorId = null;
        comment.authorUsername = null;
        loadingStates[comment.id] = false;
      });
      return true;
    }else{
      logger.i('delete comment from template caused error ${deleteResponse.body}');
      return false;
    }
  }

  Future<bool> createReport(String text, int id, String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/reports/create");
    if(type=="COMMENT"){
      var tmp = "{TEMPLATE${templateDto!.id}}$text";
      text = tmp;
    }
    final Map<String, dynamic> jsonFields = {
      'text': text,
      'type':type,
      'subjectId':'$id'
    };
    final createResponse = await post(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(createResponse.statusCode==200){
      if(createResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
      }
      return true;
    }else{
      logger.i('create report in template caused error ${createResponse.body}');
      return false;
    }
  }

  Future<void> showReportDialog(String type, int id) async {
    String selectedOption = "";
    bool own = false;
    String reason = "";
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: const Text('Send report'),
                content: Container(
                  width: 300,
                  child:
                  SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        ListTile(
                          title: const Text('Spam'),
                          leading: Radio<String>(
                            value: 'Spam',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Bullying or harassment'),
                          leading: Radio<String>(
                            value: 'Bullying or harassment',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Nudity or sexual content'),
                          leading: Radio<String>(
                            value: 'Nudity or sexual content',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Hate speech or symbols'),
                          leading: Radio<String>(
                            value: 'Hate speech or symbols',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Scam or fraud'),
                          leading: Radio<String>(
                            value: 'Scam or fraud',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = false;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Your own reason'),
                          leading: Radio<String>(
                            value: 'Your own reason',
                            groupValue: selectedOption,
                            onChanged: (value) {
                              setState(() {
                                own = true;
                                selectedOption = value!;
                              });
                            },
                          ),
                        ),
                        if(own)
                          TextField(
                            onChanged: (value) {
                              setState(() {
                                reason = value;
                              });
                            },
                            controller: textFieldController,
                            decoration:
                            const InputDecoration(hintText: "Enter reason of your report"),
                          ),
                      ],
                    ),
                  ),),
                actions: <Widget>[
                  TextButton(
                    child: const Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    onPressed: () async {
                      if(selectedOption=="Your own reason"){
                        selectedOption = reason;
                      }
                      if(await createReport(selectedOption, id, type)){
                        if(mounted) {
                          Navigator.of(context).pop();
                          alertDialog(context);
                        }
                      }
                    },
                    child: const Text('Send'),
                  ),
                ],
              );
            },);}
    );
  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Thank you for your report!',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> loginDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('You have to login to add comments!',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Widget buildDesc(BuildContext buildContext){
    return Expanded(
        child: Container(
            height: size!.height,
            padding: const EdgeInsets.only(left:15, right: 15),
            child: ListView(
                children: [Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment:
              CrossAxisAlignment.start,
              children: [
                Text(
                  'Name: ${templateDto!.name.split('_').length > 1 ? templateDto!.name.split('_')[1] : ""}',
                  textScaler: const TextScaler.linear(1.7),
                ),
                Row(
                  children: [
                    const Text(
                      'Author: ',
                      textScaler: TextScaler.linear(1.5),),
                    if(creatorName!=null && templateDto!.creatorId!=null)
                    TextButton(
                      child:
                      Text(
                        creatorName!,
                        textScaler: const TextScaler.linear(1.5),),
                      onPressed: () async {
                        if(creatorName!=null && templateDto!.creatorId!=null){
                          var result = await Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(userDto: userDto, profileId: templateDto!.creatorId!)));
                          if(result!=null && result==true){
                            getTemplateData();
                          }
                        }
                      },
                    ),
                    if(creatorName==null || templateDto!.creatorId==null)
                    const Text('deleted',textScaler: TextScaler.linear(1.5),)
                  ],
                ),
                if(!isEdit)
                  Text(
                    'Description: ${templateDto!.description ?? ""}',
                  ),
                if(userDto!=null && (!isEdit && userDto!.id==templateDto!.creatorId))
                ElevatedButton(
                  onPressed: ()
                  {
                    setState(() {
                      isEdit = true;
                    });
                    },
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(90, 20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    backgroundColor: Colors.deepPurple.shade100,
                    foregroundColor: Colors.white,
                  ),
                  child:
                  const Text('Edit'),),
                if(isEdit)
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              newDesc = value;
                            });
                          },
                          decoration: const InputDecoration(
                            hintText: "Enter new description",
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                        ),
                      ),
                      IconButton(
                          onPressed: (){
                            setState(() {
                              isEdit = false;
                            });
                            saveNewGIDescription();
                          },
                          icon: const Icon(Icons.check_circle_outline)),
                      IconButton(
                          onPressed: (){
                            setState(() {
                              isEdit = false;
                            });
                          },
                          icon: const Icon(Icons.cancel_outlined))
                    ],
                  ),
                    Text(
                      'Max possible score: ${templateDto!.maxScore!}',
                    ),
                    Text(
                      'Layers amount: ${templateDto!.layersAmount!=null ? templateDto!.layersAmount! : ""}',
                    ),
                    const Text(
                      'Difficulty: not verified yet',
                    ),
                if(templateDto!.galleryImages!.isNotEmpty)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Gallery:',textScaler: TextScaler.linear(1.0),),
                    if(templateDto!.galleryImages!.length>4)
                    ElevatedButton(
                      onPressed: ()
                      {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AllGalleryImagesPage(templateDto: templateDto!, userDto: userDto,)));
                      },
                      style: ElevatedButton.styleFrom(
                        minimumSize: const Size(100, 10),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        backgroundColor: Colors.deepPurple.shade100,
                        foregroundColor: Colors.white,
                      ),
                      child:
                      const Text('Show all'),),
                  ],
                ),
                if(size!.height>277 && templateDto!.galleryImages!.isNotEmpty)
                Container(
                    padding: const EdgeInsets.only(top: 5,),
                    // height: 100,
                    child: GridView.count(
                      crossAxisCount: 4,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 4.0,
                      childAspectRatio: 1.0,
                      children: List.generate(templateDto!.galleryImages!.length > 4 ? 4 : templateDto!.galleryImages!.length, (index) {
                        return GestureDetector(
                            onTap: () {
                              openGI(index);
                              },
                            child:
                            AspectRatio(
                                aspectRatio: 6/4,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                  ),
                                  child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black),
                                          ),
                                          child: RawImage(
                                              image: templateDto!.galleryImages![index].image!
                                          ))),
                                )));
                      }),
                    )
                ),
                if(userDto!=null)
                Container(
                    height: 30,
                    // width: size!.width,
                    padding: const EdgeInsets.only(top:2, bottom: 2),
                    child:
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                          onPressed: ()
                          {
                            showReportDialog("TEMPLATE", templateDto!.id);
                            },
                          style: ElevatedButton.styleFrom(
                            minimumSize: const Size(90, 26),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            backgroundColor: Colors.deepPurple.shade100,
                            foregroundColor: Colors.white,
                          ),
                          child:
                          const Text('Report'),),
                        if(!isUsageExist && userDto!.id!=templateDto!.creatorId)
                        ElevatedButton(
                          onPressed: ()
                          {
                            if(userDto!=null) {
                              createTemplateUsage();
                            }
                             },
                          style: ElevatedButton.styleFrom(
                            minimumSize: const Size(90, 26),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            backgroundColor: Colors.deepPurple.shade100,
                            foregroundColor: Colors.white,
                          ),
                          child:
                          const Text('Add to my library'),),
                        ElevatedButton(
                          onPressed: ()
                          {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => EyeDrop( child: RedrawModePage(valueText: templateDto!.name, userDto: userDto!,))));
                          },
                          style: ElevatedButton.styleFrom(
                            minimumSize: const Size(90, 26),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            backgroundColor: Colors.deepPurple.shade100,
                            foregroundColor: Colors.white,
                          ),
                          child:
                          const Text('Redraw now'),),
                      ],
                    )),
              ],
            )])));
  }

  Widget buildImage(BuildContext buildContext){
    return Expanded(
        child: Column(

            children:[ FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.scaleDown,
            child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                ),
                child: RawImage(
                  width: size!.width-30,
                  height: size!.height-30,
                  image: templateDto!.sampleImage,
                ))),
            Container(
              height: 30,
            child:
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    const Icon(
                        Icons.download
                    ),
                    Text(templateDto!.downloadsCount.toString()),
                  ],
                ),
                Row(
                  children: [
                    if(userDto!=null)
                      GestureDetector(
                        child: const Icon(
                            Icons.thumb_up_off_alt_outlined
                        ),
                        onTap: (){
                          if(!templateDto!.likes!.contains(userDto!.id)){
                            addLike();
                          }else{
                            removeLike();
                          }
                        },
                      ),
                    if(userDto==null)
                      const Icon(
                          Icons.thumb_up_off_alt_outlined
                      ),
                    Text('${templateDto!.likes!.length}'),
                  ],
                ),
                Row(
                  children: [
                    if(userDto!=null)
                      GestureDetector(
                        child: const Icon(
                            Icons.thumb_down_off_alt_outlined
                        ),
                        onTap: (){
                          if(!templateDto!.dislikes!.contains(userDto!.id)){
                            addDislike();
                          }else{
                            removeDislike();
                          }
                        },
                      ),
                    if(userDto==null)
                      const Icon(
                          Icons.thumb_down_off_alt_outlined
                      ),
                    Text('${templateDto!.dislikes!.length}'),
                  ],
                ),
                Text(DateFormat('dd.MM.yyyy HH:mm').format(templateDto!.uploadDate!=null ? DateTime.parse(templateDto!.uploadDate!) : DateTime.now())),
              ],
            ))]));
  }

  Size setImageSize(int width, int height) {
    double resizeFactor = 1;
    double canvasWidth = width.toDouble();
    double canvasHeight = height.toDouble();
    if (width > height &&
        width > (MediaQuery.of(context).size.width - 40) / 2) {
      resizeFactor = ((MediaQuery.of(context).size.width - 40) / 2) / width;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height > width &&
        height > (MediaQuery.of(context).size.height - 40) / 2) {
      resizeFactor = ((MediaQuery.of(context).size.height - 40) / 2) / height;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height == width &&
        (width > (MediaQuery.of(context).size.width - 40) / 2 ||
            height > (MediaQuery.of(context).size.height - 40) / 2)) {
      if ((MediaQuery.of(context).size.width - 40) / 2 >
          (MediaQuery.of(context).size.height - 40) / 2) {
        resizeFactor = ((MediaQuery.of(context).size.height - 40) / 2) / height;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      } else {
        resizeFactor = ((MediaQuery.of(context).size.width - 40) / 2) / width;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      }
    }
    return Size(canvasWidth, canvasHeight);
  }

  Widget buildComment(CommentModel comment) {
    bool isExpanded = expandedComments.contains(comment.id);
    bool isLoading = loadingStates[comment.id] ?? false;
    bool isReplying = selectedId==comment.id;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.deepPurple.shade200),
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListTile(
          title: Row(
              children: [
                      Text(
                      comment.authorUsername ?? "[deleted]",
                      textAlign: TextAlign.start,
                        style: TextStyle(
                          color: widget.reportedComment!=null && widget.reportedComment!.item1==comment.id ? Colors.red.shade800: null,
                        ),
                    ),
                    Padding(padding: const EdgeInsets.only(left:50),
                    child:
                    Text(
                        DateFormat('dd.MM.yyyy HH:mm').format(DateTime.parse(comment.date))
                    ),
                    ),
                if(widget.reportedComment!=null && widget.reportedComment!.item1==comment.id)
                  Padding(padding: const EdgeInsets.only(left:50),
                    child:
                    Text('REPORTED',
                      style: TextStyle(
                          color: Colors.red.shade800,
                        fontWeight: FontWeight.bold,
                      ),),
                  ),
                  ],
                ),
          subtitle:
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children:[
          Text(
            comment.text,
            textScaler: const TextScaler.linear(1.3),
          ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if(comment.replies!=null && comment.replies!.isNotEmpty && isLoading)
                    const CircularProgressIndicator(),
                  if(comment.replies!=null && comment.replies!.isNotEmpty && !isLoading)
                      GestureDetector(
                        child: Icon(
                            isExpanded ? FontAwesomeIcons.minus : FontAwesomeIcons.plus
                        ),
                        onTap: (){
                          if (isExpanded) {
                            setState(() {
                              expandedComments.remove(comment.id);
                              recursionLevel.remove(comment.id);
                            });
                          } else {
                            if ((comment.repliesList==null || comment.repliesList!.isEmpty) && comment.replies!.isNotEmpty) {
                              fetchReplies(comment);
                              setState(() {
                                expandedComments.add(comment.id);
                                if(comment.type=="COMMENT" && recursionLevel.containsKey(comment.subjectId)){
                                  recursionLevel.putIfAbsent(comment.id, ()=>(recursionLevel[comment.subjectId]!+1));
                                  if(recursionLevel[comment.id]!>=10){
                                    recursionLevel.clear();
                                    expandedComments.clear();
                                    singleCommentThread = comment;
                                  }
                                }else if(comment.type=="TEMPLATE" || recursionLevel.isEmpty){
                                  recursionLevel.putIfAbsent(comment.id, () => 1);
                                }
                              });
                            } else {
                              setState(() {
                                expandedComments.add(comment.id);
                                if(comment.type=="COMMENT" && recursionLevel.containsKey(comment.subjectId)){
                                  recursionLevel.putIfAbsent(comment.id, ()=>(recursionLevel[comment.subjectId]!+1));
                                  if(recursionLevel[comment.id]!>=10){
                                    recursionLevel.clear();
                                    expandedComments.clear();
                                    singleCommentThread = comment;
                                  }
                                }else if(comment.type=="TEMPLATE" || recursionLevel.isEmpty){
                                  recursionLevel.putIfAbsent(comment.id, () => 1);
                                }
                              });
                            }
                          }
                        },
                      ),
                  if(comment.replies==null || comment.replies!.isEmpty)
                    const SizedBox(width: 24,),
                  if(userDto!=null)
                    Padding(padding: const EdgeInsets.only(left: 5),
                    child:
                    GestureDetector(
                      child: const Icon(
                          Icons.thumb_up_off_alt_outlined
                      ),
                      onTap: (){
                        if(!comment.likes!.contains(userDto!.id)){
                          addCommentLike(comment);
                        }else{
                          removeCommentLike(comment);
                        }
                      },
                    ),),
                  if(userDto==null)
                    const Padding(padding: EdgeInsets.only(left: 5),
                        child:
                    Icon(
                        Icons.thumb_up_off_alt_outlined
                    ),),
                  Text('${comment.likes!.length}'),
                  if(userDto!=null)
                    Padding(padding: const EdgeInsets.only(left: 5),
                        child:
                    GestureDetector(
                      child: const Icon(
                          Icons.thumb_down_off_alt_outlined
                      ),
                      onTap: (){
                        if(!comment.dislikes!.contains(userDto!.id)){
                          addCommentDislike(comment);
                        }else{
                          removeCommentDislike(comment);
                        }
                      },
                    ),),
                  if(userDto==null)
                    const Padding(padding: EdgeInsets.only(left: 5),
                      child:
                    Icon(
                        Icons.thumb_down_off_alt_outlined
                    ),),
                  Text('${comment.dislikes!.length}'),
                  if(userDto!=null && !isReplying)
                    Padding(padding: const EdgeInsets.only(left: 5),
                      child: ElevatedButton(
                          onPressed: (){
                            setState(() {
                              selectedId = comment.id;
                            });
                          },
                          child: const Text('Reply'))
                    ),
                  if(userDto!=null)
                    Padding(padding: const EdgeInsets.only(left: 5),
                        child: ElevatedButton(
                            onPressed: (){
                              showReportDialog("COMMENT", comment.id);
                            },
                            child: const Text('Report'))
                    ),
                  if(userDto!=null && userDto!.id==comment.authorId)
                    Padding(padding: const EdgeInsets.only(left: 5),
                        child: ElevatedButton(
                            onPressed: () {
                              deleteComment(comment);
                            },
                            child: const Text('Delete'))
                    ),
                ],
              ),
                  if(isReplying)
                    Row(
                        children:[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child:
                              TextField(
                                onChanged: (value) {
                                  if(userDto==null){
                                    loginDialog(context);
                                  }else {
                                    setState(() {
                                      newReply = value;
                                    });
                                  }
                                },
                                decoration: const InputDecoration(
                                  hintText: "Add new reply...",
                                  border: OutlineInputBorder(),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                              ),
                            ),),
                          IconButton(
                            onPressed: ()
                            async {
                              if(userDto==null){
                                loginDialog(context);
                              }else {
                                if (newReply != "") {
                                  if (await addReply(comment)) {
                                    fetchReplies(comment);
                                  }
                                }
                              }
                            },
                              icon: const Icon(Icons.check_circle_outline),),
                          IconButton(
                            onPressed: ()
                            {
                              setState(() {
                                selectedId = null;
                              });
                            },

                              icon: const Icon(Icons.cancel_outlined),),
                        ]),],),
        ),),
        if (isExpanded && comment.repliesList != null && comment.repliesList!.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Column(
              children: comment.repliesList!.map((reply) => buildComment(reply)).toList(),
            ),
          ),
        ],
    );
  }


  @override
  Widget build(BuildContext context) {
    if (templateDto == null || size == null) {
     return SelectionArea(child:
      Scaffold(
        appBar: AppBar(
        backgroundColor: Colors.deepPurple.shade50,
        toolbarHeight: 60,
        automaticallyImplyLeading: false,
        title: AppBarWithBack(userDto: userDto, withBack: withBack,),
      ),
    body: const Stack(
    children: [
        Center(
          child:
          SizedBox(
            width: 50,
            height: 50,
            child: CircularProgressIndicator(),
          ),
        ),
      ])));
    } else {
      setState(() {
        size = setImageSize(
            templateDto!.sampleImage.width, templateDto!.sampleImage.height);
      });
      return SelectionArea(child:
        Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(userDto: userDto, withBack: withBack,),
        ),
        body: Stack(
          children: [
            Container(
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child:
                  ListView(
                children:[

                      Row(
                        children: [
                          buildImage(context),
                          buildDesc(context),
                        ],
                      ),

                                const Text('Categories: '),
                                Container(
                                    height: 50,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount:
                                            templateDto!.categories.length,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 5),
                                                child: TextButton(
                                                  style: ButtonStyle(
                                                    backgroundColor:
                                                        MaterialStateProperty
                                                            .resolveWith<
                                                                Color?>(
                                                      (Set<MaterialState>
                                                          states) {
                                                        return Colors
                                                            .deepPurple.shade50;
                                                      },
                                                    ),
                                                  ),
                                                  child: Text(templateDto!
                                                      .categories[index].name),
                                                  onPressed: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context) => LibraryMainPage(userDto: userDto!=null ? userDto! : null, category: templateDto!.categories[index])));
                                                },
                                                ),
                                              )
                                            ],
                                          );
                                        }),
                                  ),
                  Row(
                    children:[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child:
                      TextField(
                        onChanged: (value) {
                          if(userDto==null){
                            loginDialog(context);
                          }else {
                            setState(() {
                              newComment = value;
                            });
                          }
                        },
                        controller: commentFieldController,
                        decoration: const InputDecoration(
                          hintText: "Add new comment...",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                      ),
                    ),),
                      ElevatedButton(
                        onPressed: ()
                        async {
                          if(userDto==null){
                            loginDialog(context);
                          }else {
                            if (newComment != "") {
                              if (await addComment()) {
                                if (mounted) {
                                  setState(() {
                                    newComment = null;
                                    commentFieldController.text = "";
                                    recursionLevel.clear();
                                    expandedComments.clear();
                                    singleCommentThread = null;
                                  });
                                }
                              }
                            }
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          backgroundColor: Colors.deepPurple.shade100,
                          foregroundColor: Colors.white,
                        ),
                        child:
                        const Text('Add'),),
                    ]),
                  if(singleCommentThread==null && templateDto!.comments!=null && templateDto!.comments!.isNotEmpty)
                  ListView(
                    physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                    children: templateDto!.comments!.map((comment) => buildComment(comment)).toList(),
                  ),
                  if(singleCommentThread!=null)
                    ElevatedButton(
                        onPressed: () {
                          setState(() {
                            recursionLevel.clear();
                            expandedComments.clear();
                            singleCommentThread = null;
                          });
                        },
                        child: const Text("Return to main comments thread")),
                  if(singleCommentThread!=null && singleCommentThread!.repliesList!=null && singleCommentThread!.repliesList!.isNotEmpty)
                    ListView(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: singleCommentThread!.repliesList!.map((comment) => buildComment(comment)).toList(),
                    ),
                    ]),),
          ],
        ),
      ));
    }
  }
}
