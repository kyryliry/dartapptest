import 'package:flutter/material.dart';

import '../model/user_model.dart';
import '../utils/app_bar_with_back.dart';

class MainPage extends StatefulWidget {
  final UserModel? userDto;

  MainPage({Key? key, required this.userDto}) : super(key: key);

  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return SelectionArea(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.deepPurple.shade50,
              toolbarHeight: 60,
              automaticallyImplyLeading: false,
              title: AppBarWithBack(
                userDto: widget.userDto,
                withBack: false,
              ),
            ),
            body: const SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      'Welcome to Template Redraw App!',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 8.0),
                    child: Text(
                      'This app allows you to redraw images according to templates. Below you will find some important information about this app.',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.bookmark_outline_sharp, size: 24.0), // Icon with a specific size
                        SizedBox(width: 10), // Space between icon and text
                        Expanded(
                          child: Text('Library is the storage of app content. All the templates created by users are stored in this section. Pick one and try to redraw it according to the template!',
                            style: TextStyle(fontSize: 16),),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.bookmark_outline_sharp, size: 24.0), // Icon with a specific size
                        SizedBox(width: 10), // Space between icon and text
                        Expanded(
                          child: Text('When you start the redrawing process, you may wonder why there is no possibility to choose any other color for drawing. '
                              'Don\'t worry; this is not an error. The idea of redrawing is that the creator of the template chose the color of the brush when creating his template, and you have to use the defined color.',
                          style: TextStyle(fontSize: 16), // Customize the font size as needed
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.bookmark_outline_sharp, size: 24.0), // Icon with a specific size
                        SizedBox(width: 10), // Space between icon and text
                        Expanded(
                          child: Text(
                            'After you redraw all the layers that the template originally has, and click to load the next layer, you will see a strange layer number, like "6/5".  '
                                'It means that you have reached the end of the template, and now you have your own layer where you can draw whatever you want with any desired colors. This custom layer doesn\'t affect accuracy points.',
                            style: TextStyle(fontSize: 16), // Customize the font size as needed
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.bookmark_outline_sharp, size: 24.0), // Icon with a specific size
                        SizedBox(width: 10), // Space between icon and text
                        Expanded(
                          child: Text(
                            'It is highly recommended to try redrawing mode before entering template creation mode because as a creator of a template, you will better understand what users will see when they redraw your template.',
                            style: TextStyle(fontSize: 16), // Customize the font size as needed
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.bookmark_outline_sharp, size: 24.0), // Icon with a specific size
                        SizedBox(width: 10), // Space between icon and text
                        Expanded(
                          child: Text(
                            'If you want to create your own template, go to the "My Library" section in the dropdown menu and click the "Create New Template" button. Now, you can draw a template in the app or load PNG images as layers. '
                                'But note that you may use only one color per layer. Then, you can choose appropriate showing colors; when redrawing, users will see the goal of each layer as a background drawn in the showing color. '
                                'Also, when saving, you need to choose a unique name for the template among all the templates that you have created.',
                            style: TextStyle(fontSize: 16), // Customize the font size as needed
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.bookmark_outline_sharp, size: 24.0), // Icon with a specific size
                        SizedBox(width: 10), // Space between icon and text
                        Expanded(
                          child: Text(
                            'Note that you will be able to delete your created template only if it has 0 plays (downloads). Even "Add to library" increases the download count, and your (as the creator\'s) download also counts.' ,
                            style: TextStyle(fontSize: 16), // Customize the font size as needed
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      'Hope you will enjoy this app!',
                      style:
                          TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
              ),
            )));
  }
}
