import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/profile_page.dart';

import '../utils/app_bar_with_back.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class SubscriptionsPage extends StatefulWidget {
  final UserModel? userDto;
  final int profileId;

  SubscriptionsPage({Key? key, required this.userDto, required this.profileId}) : super(key: key);

  @override
  SubscriptionsPageState createState() => SubscriptionsPageState();
}

class SubscriptionsPageState extends State<SubscriptionsPage> with SingleTickerProviderStateMixin {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<UserModel> followers = <UserModel>[];
  List<UserModel> following = <UserModel>[];
  Map<String, int> totalPages = <String, int>{};
  Map<String, int> currentPage = <String, int>{};
  int amountPerPage = 21;
  String userType = "followers";
  ui.Image? image;

  TabController? _tabController;
  @override
  void initState(){
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    totalPages.putIfAbsent('followers', () => 0);
    totalPages.putIfAbsent('following', () => 0);
    currentPage.putIfAbsent('followers', () => 1);
    currentPage.putIfAbsent('following', () => 1);
    fetchUsers(0, amountPerPage, "followers");
    fetchUsers(0, amountPerPage, "following");
    loadImage('assets/images/default_avatar.png');
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  Future<void> switchToTab(int tabIndex, String type) async {
    _tabController?.animateTo(tabIndex);
  }

  Future<void> fetchUsers(int page, int size, String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final String url = 'http://localhost:8080/api/users/get/$type/${widget.profileId}/paged?page=$page&size=$size';
    final response = await get(Uri.parse(url), headers: headers);

    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final Map<String, dynamic> data = item['page'];
      List<dynamic> users = data['content'] as List;
      List<UserModel> tmpUsers = <UserModel>[];
      for (var user in users) {
        tmpUsers.add(UserModel(
            id: user['id'], userName: user['username'], role: user['role'], isBlocked: user['isBlocked']));
      }
      setState(() {
        if(type=="followers"){
          followers.clear();
          followers = List.of(followers)..addAll(tmpUsers);
          totalPages['followers'] = data['totalPages'];
        }else if(type=="following"){
          following.clear();
          following = List.of(following)..addAll(tmpUsers);
          totalPages['following'] = data['totalPages'];
        }
      });
    } else {
      logger.i('fetch subscriptions caused error: ${response.body}');
    }
  }

  Future<void> unfollow(int id) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/unfollow/$id");
    final follow = await put(url, headers: headers);
    if(follow.statusCode==200){
      if(follow.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${follow.headers['authorization']}');
      }
    }else{
      logger.i('unfollow caused error: ${follow.body}');
    }
  }

  Widget buildPagination(int currentPage, int totalPages, String type) {
    const int maxVisiblePages = 5;
    List<Widget> pageButtons = [];

    int startPage = max(currentPage - maxVisiblePages ~/ 2, 1);
    int endPage = min(startPage + maxVisiblePages - 1, totalPages);
    startPage = max(endPage - maxVisiblePages + 1, 1);

    if (startPage > 1) {
      pageButtons.add(buildPageButton(1, type));
    }

    if (startPage > 2) {
      pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
    }

    for (int i = startPage; i <= endPage; i++) {
      pageButtons.add(buildPageButton(i, type, isCurrent: i == currentPage));
    }

    if (totalPages > endPage) {
      if (endPage < totalPages - 1) {
        pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
      }
      pageButtons.add(buildPageButton(totalPages - 1, type));
      pageButtons.add(buildPageButton(totalPages, type));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: pageButtons,
    );
  }

  Widget buildPageButton(int page, String type, {bool isCurrent = false}) {
    return Padding(padding: const EdgeInsets.only(left: 2, right: 2),
        child:
        TextButton(
          onPressed: () {
            setState(() {
              currentPage[type] = page;
            });
            fetchUsers(page-1, amountPerPage, type);
          },
          style: TextButton.styleFrom(
            backgroundColor: isCurrent ? Colors.deepPurple : Colors.deepPurple.shade50,
            foregroundColor: isCurrent ? Colors.white : null,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          ),
          child: Text((page).toString()),
        ));
  }

  Future<void> loadImage(String assetPath) async {
    final ByteData data = await rootBundle.load(assetPath);
    final ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
    final ui.FrameInfo fi = await codec.getNextFrame();
    setState(() {
      image = fi.image;
    });
  }

  Widget buildUserList(List<UserModel> users, String type) {
    return Column(
      children: [
        Expanded(
          child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 0,
              mainAxisSpacing: 0,
              childAspectRatio: 4.5
            ),
            itemCount: users.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () async {
                  var result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfilePage(
                        userDto: widget.userDto,
                        profileId: users[index].id,
                      ),
                    ),
                  );
                  if (result != null && result == true) {
                    fetchUsers(0, amountPerPage, type);
                  }
                },
                child: Padding(padding: const EdgeInsets.all(5),
                  child:
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                        ),
              height: 65,
              child:
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child:
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:[
                    FittedBox(
                        alignment: Alignment.center,
                        fit: BoxFit.scaleDown,
                        child: Container(
                            alignment: Alignment.center,
                            child: RawImage(
                              width: 60,
                              height: 60,
                              image: image,
                            ))),
                    SizedBox(width: 8),
                    Text(
                      users[index].userName,
                      textScaler: const TextScaler.linear(1.5),
                      style: TextStyle(
                        color: users[index].isBlocked! ? Colors.red.shade800 : Colors.black,
                      ),
                    ),],),),
                    if (widget.userDto != null && widget.profileId == widget.userDto!.id && type == "following")
                      SizedBox(width: 8),
                    if (widget.userDto != null && widget.profileId == widget.userDto!.id && type == "following")
                      GestureDetector(
                        onTap: () {
                          unfollow(users[index].id);
                          setState(() {
                            users.removeAt(index);
                          });
                        },
                        child: const Icon(Icons.cancel_outlined),
                      ),
                  ],
                ),),),
              );
            },
          ),
        ),
        Container(
          height: 40,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.only(bottom: 10),
          child: buildPagination(currentPage[type]!, totalPages[type]!, type),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if(image==null){
      return const CircularProgressIndicator();
    }else {
      return SelectionArea(child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(
            userDto: widget.userDto,
          ),
        ),
        body: Stack(
            children: [
              Column(
                children: [
                  TabBar(
                    controller: _tabController,
                    tabs: const [
                      Tab(text: 'Followers'),
                      Tab(text: 'Following'),
                    ],
                  ),
                  Expanded(child:
                  TabBarView(
                    controller: _tabController,
                    children: [
                      buildUserList(followers, "followers"),
                      buildUserList(following, "following"),
                    ],
                  ),),
                ],
              ),
            ]
        ),
      ));
    }
  }

}