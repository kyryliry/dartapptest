import 'dart:convert';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/template_page.dart';
import 'package:paintappdart/utils/app_bar_with_back.dart';
import 'package:paintappdart/model/category_model.dart';
import 'package:paintappdart/model/template_model.dart';

import '../utils/session_manager.dart';
import '../model/user_model.dart';

class LibraryMainPage extends StatefulWidget {
  String? name;
  UserModel? userDto;
  CategoryModel? category;
  LibraryMainPage({Key? key, this.name, this.category, this.userDto}) : super(key: key);

  @override
  LibraryMainPageState createState() => LibraryMainPageState();
}

class LibraryMainPageState extends State<LibraryMainPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<TemplateModel> templates = <TemplateModel>[];
  int? templateId;
  UserModel? userDto;
  bool libIsEmpty = false;
  List<CategoryModel> categories = <CategoryModel>[];
  String searchText = "";
  List<CategoryModel> selectedCategories = <CategoryModel>[];
  bool categoriesListOpen = false;
  CategoryModel? selectedCategory;

  @override
  void initState() {
    super.initState();
    getUserData();
    if(widget.category!=null){
      getCategories();
      categories.remove(widget.category!);
      selectedCategories.add(widget.category!);
      getTemplatesByCategories();
    }else{
      _getImage;
    }
  }

  Future<void> getUserData() async {
    if(widget.userDto!=null){
      userDto = widget.userDto!;
      return;
    }
    if(widget.name!=null) {
      var value = widget.name!;
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      final user = await http
          .get(Uri.parse("http://localhost:8080/api/users/get/short/${value}"), headers: headers);
      if (user.statusCode == 200) {
        final userData = json.decode(user.body);
        if(user.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${user.headers['authorization']}');
        }
        setState(() {
          userDto = UserModel(id: userData['id'],
              userName: userData['username'],
              role: userData['role']);
        });
      }
    }
  }

  Future<void> getTemplatesByCategories() async {
    List<Map<String, dynamic>> listOfObjects = [];

    for (CategoryModel category in selectedCategories) {
      listOfObjects.add(category.toJson());
    }
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }

    final temps = await http.post(Uri.parse("http://localhost:8080/api/templates/getByCategories"),
          headers: headers,
          body: jsonEncode({
                "searchText": searchText,
                "categories":listOfObjects,
              }));

    if(temps.statusCode==200){
      final item = json.decode(temps.body);
      if(temps.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${temps.headers['authorization']}');
      }
      final jsonResponse = item['result'] as List;
      if(item['result']=="" || item['result']==null || jsonResponse.isEmpty){
        setState(() {
          libIsEmpty = true;

          templates.clear();
        });
      }else {
        templates.clear();
        for (var item in jsonResponse) {
          final String base64Image = item['image'];
          final id = item['id'];
          final name = item['name'];
          final dCount = item['downloadsCount'];
          final uName = item['creatorName'];
          var imageBytes = base64Decode(base64Image);
          ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
          ui.FrameInfo frame = await codec.getNextFrame();
          setState(() {
            var image = frame.image;
            templates.add(TemplateModel(id: id,
                name: name,
                sampleImage: image,
                categories: [],
                downloadsCount: dCount,
                creatorName: uName));
          });
        }
      }
    }else{
      logger.i('get templates by categories caused error: ${temps.body}');
    }
  }

  Future<void> getCategories() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/categories");

    final cats = await http
        .get(apiUrl, headers: headers);
    if (cats.statusCode == 200) {
      setState(() {
        final item = json.decode(cats.body);
        if(cats.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
        }

        var list = item['categories'] as List;
        for(var item in list){
          categories.add(CategoryModel.fromJson(item));
        }
      });
    } else {
      logger.i('get categories caused error: ${cats.body}');
    }
  }

  Future<void> get _getImage async {
    getCategories();


    List<ui.Image> images = <ui.Image>[];
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await http
        .get(Uri.parse("http://localhost:8080/api/templates/getTwSI/all"), headers: headers);

    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      final jsonResponse = item['result'] as List;
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      if(item['result']=="" || item['result']==null || jsonResponse.isEmpty){
        setState(() {
          libIsEmpty = true;
        });
      }else {
        final jsonResponse = item['result'] as List;
        for (var item in jsonResponse) {
          final String base64Image = item['image'];
          final id = item['id'];
          final name = item['name'];
          final dCount = item['downloadsCount'];
          final uName = item['creatorName'];
          var imageBytes = base64Decode(base64Image);
          ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
          ui.FrameInfo frame = await codec.getNextFrame();
          if(mounted) {
            setState(() {
              var image = frame.image;
              templates.add(TemplateModel(id: id,
                  name: name,
                  sampleImage: image,
                  categories: [],
                  downloadsCount: dCount,
                  creatorName: uName));
            });
          }
        }
      }
    } else {
      logger.i('get all templates caused error: ${response.body}');
    }
  }

  @override
  Widget build(BuildContext context) {
    int colsNum = 4;
    if(600<MediaQuery.of(context).size.width && MediaQuery.of(context).size.width<850){
      colsNum = 3;
    }else if(MediaQuery.of(context).size.width<600){
      colsNum = 2;
    }else if(MediaQuery.of(context).size.width>850){
      colsNum = 4;
    }

      return SelectionArea(child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: widget.category!=null?
          AppBarWithBack(userDto: userDto,) : AppBarWithBack(userDto: userDto, withBack: false),
        ),
        body:
        Stack(
          children: [
            if(templates.isEmpty && !libIsEmpty)
              const Center(
                child:
                  SizedBox(
                    width: 50,
                    height: 50,
                    child: CircularProgressIndicator(),
                  ),
              ),
            if(templates.isNotEmpty || libIsEmpty)
            ListView(
              children: [
            Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                ),
              margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Padding(
                padding: const EdgeInsets.all(15),
              child:
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child:
                        TextField(
                          onChanged: (value) {
                            setState(() {
                              searchText = value;
                            });
                          },
                          decoration: const InputDecoration(
                            hintText: "Input key words...",
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                        ),
                      ),),
                      ElevatedButton(
                        onPressed: ()
                        {
                          if(selectedCategories.isNotEmpty || searchText!="") {
                            getTemplatesByCategories();
                          }else{
                            templates.clear();
                            _getImage;
                          }
                          },
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          backgroundColor: Colors.deepPurple.shade100,
                          foregroundColor: Colors.white,
                        ),
                        child:
                        const Text('Search'),),
                    ],
                  ),
                  Container(
                    child:
                    Column(
                      children: [
                        Row(
                          children: [
                            const Text('Categories: '),
                            Expanded(
                              child: Container(
                                height: 50,
                                child:
                                ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: selectedCategories.length,
                                    itemBuilder: (context, index) {
                                      return Row(
                                        children: [
                                          Text(selectedCategories[index].name),
                                          IconButton(
                                              onPressed: (){
                                                setState(() {
                                                  CategoryModel tmp = selectedCategories[index];
                                                  selectedCategories.remove(tmp);
                                                  categories.add(tmp);
                                                  categories.sort((a, b) => a.name.compareTo(b.name));
                                                });

                                              },
                                              icon: const Icon(Icons.highlight_remove_outlined)
                                          ),
                                        ],
                                      );
                                    }),),)
                          ],
                        ),
                        Row(
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    categoriesListOpen = !categoriesListOpen;
                                  });
                                },
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  backgroundColor: categoriesListOpen ? Colors.grey.shade300 : null,
                                ),
                                child: PopupMenuButton<CategoryModel>(
                                  onSelected: (CategoryModel category) {
                                    setState(() {
                                      selectedCategory = category;
                                    });
                                  },
                                  itemBuilder: (BuildContext context) {
                                    return <PopupMenuEntry<CategoryModel>>[
                                      PopupMenuItem<CategoryModel>(
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(maxHeight: 200.0), // For example, 5 items * 40.0px each
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: categories.map((CategoryModel category) {
                                                return PopupMenuItem<CategoryModel>(
                                                  value: category,
                                                  child: Text(category.name),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ];
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(selectedCategory?.name ?? 'select category'),
                                      Icon(Icons.arrow_drop_down),
                                    ],
                                  ),
                                ),
                              ),

                              ElevatedButton(
                                onPressed: ()
                                {
                                  if(selectedCategory!=null){
                                    setState(() {
                                      selectedCategories.add(selectedCategory!);
                                      categories.remove(selectedCategory!);
                                      selectedCategory = null;
                                    });

                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  backgroundColor: Colors.deepPurple.shade100,
                                  foregroundColor: Colors.white,
                                ),
                                child:
                                const Text('add category'),),
                            ]),
                      ],
                    ),
                  ),
                ],
              )),
            ),

            if(templates.isEmpty && libIsEmpty)
          Container(),
            if(templates.isNotEmpty)
            GridView.builder(
              physics: const NeverScrollableScrollPhysics(), // to disable GridView's scrolling
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: colsNum, // Number of columns
                childAspectRatio: 1.0, // Aspect ratio for each item
                crossAxisSpacing: 4.0, // Spacing between items horizontally
                mainAxisSpacing: 4.0, // Spacing between items vertically
              ),
              itemCount: templates.length,
              itemBuilder: (context, index) {
                return
                  GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => TemplatePage(
                                templateId: templates[index].id,
                                userDto: userDto)));
                      },
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 15, left: 15, right: 15, bottom: 15),
                          child:
                          Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AspectRatio(
                                  aspectRatio: 6 / 4,
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 15,
                                        left: 15,
                                        right: 15,
                                        bottom: 15),
                                    child:
                                    FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Container(
                                            height: 150,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.black),
                                            ),
                                            child: RawImage(
                                              image: templates[index]
                                                  .sampleImage,
                                            ))),
                                  ),
                                ),
                                AspectRatio(
                                  aspectRatio: 8.0,
                                  child: Text(templates[index].name.split('_').length > 1 ? templates[index].name.split('_')[1] : "",
                                    textAlign: TextAlign.center,),
                                ),
                                AspectRatio(
                                  aspectRatio: 8.0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceAround,
                                    children: [
                                      Text(templates[index].creatorName != null
                                          ? templates[index].creatorName!
                                          : "error",
                                        textAlign: TextAlign.center,),
                                      Row(children: [
                                        const Icon(
                                            Icons.download
                                        ),
                                        Text(templates[index].downloadsCount
                                            .toString(),
                                          textAlign: TextAlign.center,),
                                      ],)
                                    ],
                                  ),
                                ),
                              ])));
              },
            )],),
          ],
        ),
      ));
  }
}
