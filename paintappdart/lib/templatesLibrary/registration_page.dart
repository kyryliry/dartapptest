import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);
  @override
  RegistrationPageState createState() => RegistrationPageState();
}

class RegistrationPageState extends State<RegistrationPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  final TextEditingController _textFieldController = TextEditingController();
  final TextEditingController _passFieldController = TextEditingController();
  final TextEditingController _spassFieldController = TextEditingController();
  final TextEditingController _emailFieldController = TextEditingController();
  String? valueText;
  String? password;
  String? spassword;
  String? email;
  bool firstPasswordVisible = false;
  bool secondPasswordVisible = false;
  String errorMessage = "";

  Future<bool> register() async {
    final temps = await post(Uri.parse("http://localhost:8080/api/users/new"),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode({
          "username": valueText!,
          "password":password!,
          "email": email!,
        }));

    if(temps.statusCode==200){
      if(mounted) {
        Navigator.pop(context);
        return true;
      }
    }else{
      logger.i('register caused error: ${temps.body}');
      setState(() {
        errorMessage = temps.body;
      });
      return false;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return SelectionArea(child:Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple.shade50,
        title: const Text('Registration'),
      ),
      body: Center(
          child:
          Container(
            width: MediaQuery.of(context).size.width/2,
            height: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if(errorMessage!="")
                  Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                    child:
                    Text(errorMessage,),
                  ),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                  child:TextField(
                    onChanged: (value) {
                      setState(() {
                        valueText = value;
                      });
                    },
                    controller: _textFieldController,
                    decoration:
                    const InputDecoration(hintText: "username"),
                  ),),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                  child:TextField(
                    onChanged: (value) {
                      setState(() {
                        email = value;
                      });
                    },
                    controller: _emailFieldController,
                    decoration:
                    const InputDecoration(hintText: "email"),
                  ),),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                  child:
                  TextField(
                    obscureText: !firstPasswordVisible,
                    onChanged: (value) {
                      setState(() {
                        password = value;
                      });
                    },
                    controller: _passFieldController,
                    decoration: InputDecoration(
                      hintText: "password",
                      suffixIcon: IconButton(
                        icon: Icon(
                          firstPasswordVisible ? Icons.visibility : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            firstPasswordVisible = !firstPasswordVisible;
                          });
                        },
                      ),
                    ),
                  ),),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                  child:
                  TextField(
                    obscureText: !secondPasswordVisible,
                    onChanged: (value) {
                      setState(() {
                        spassword = value;
                      });
                    },
                    controller: _spassFieldController,
                    decoration: InputDecoration(
                      hintText: "repeat password",
                      suffixIcon: IconButton(
                        icon: Icon(
                          secondPasswordVisible ? Icons.visibility : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            secondPasswordVisible = !secondPasswordVisible;
                          });
                        },
                      ),
                    ),
                  ),),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                  child:
                  MaterialButton(
                    color: Colors.deepPurple.shade300,
                    textColor: Colors.white,
                    child: const Text('Register'),
                    onPressed: () async {
                      if(spassword!=password){
                        setState(() {
                          errorMessage = "Provided passwords don't match!";
                        });
                      }else {
                        // setState(() async {
                          await register();
                        // });
                      }
                    },
                  ),),
              ],
            ),
          )
      ),
    ));
  }
}