import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/library_main_page.dart';
import 'package:paintappdart/templatesLibrary/registration_page.dart';

import '../utils/session_manager.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  final TextEditingController _textFieldController = TextEditingController();
  final TextEditingController _passFieldController = TextEditingController();
  String? valueText;
  String? password;
  String errorMessage = "";
  bool _passwordVisible = false;

  Future<void> login() async {
    final temps = await post(Uri.parse("http://localhost:8080/auth/login"),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode({
          "username": valueText!,
          "password":password!,
        })
    );

    if(temps.statusCode==200){
      var responseBody = json.decode(temps.body);
      if (responseBody['loggedIn']) {
        String token = responseBody['jsonToken'];
        SessionManager().setJsessionId("Bearer $token");
      }
      if(mounted) {
        Navigator.push(context, MaterialPageRoute(
            builder: (context) => LibraryMainPage(name: valueText,)));
      }
    }else{
      logger.i('login caused error: ${temps.body}');
      setState(() {
        errorMessage = temps.body;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SelectionArea(child:Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple.shade50,
        title: const Text('Login'),
      ),
      body: Center(
        child:
          Container(
            width: MediaQuery.of(context).size.width>600? MediaQuery.of(context).size.width/2:MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if(errorMessage!="")
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                    child:
                    Text(errorMessage,),
                ),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                child:TextField(
                  onChanged: (value) {
                    setState(() {
                      valueText = value;
                    });
                  },
                  controller: _textFieldController,
                  decoration:
                  const InputDecoration(hintText: "username"),
                ),),
                Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                child:
                TextField(
                  obscureText: !_passwordVisible,
                  onChanged: (value) {
                    setState(() {
                      password = value;
                    });
                  },
                  controller: _passFieldController,
                  decoration: InputDecoration(
                    hintText: "password",
                    suffixIcon: IconButton(
                      icon: Icon(
                        _passwordVisible ? Icons.visibility : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                  ),
                ),),
        Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                child:
                MaterialButton(
                  color: Colors.deepPurple.shade300,
                  textColor: Colors.white,
                  child: const Text('Log in'),
                  onPressed: () {
                    setState(() {
                      login();
                      });
                  },
                ),),
        Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                child:
                MaterialButton(
                  color: Colors.deepPurple.shade300,
                  textColor: Colors.white,
                  child: const Text('Registration'),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrationPage()));
                  },
                ),),
              ],
            ),
          )
      ),
    ));
  }
}