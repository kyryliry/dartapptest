import 'dart:convert';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:paintappdart/drawing/load_png_page.dart';
import 'package:paintappdart/templatesLibrary/template_page.dart';
import 'package:paintappdart/utils/app_bar_with_back.dart';
import 'package:paintappdart/model/template_model.dart';
import 'package:paintappdart/model/template_usage_model.dart';

import '../drawing/canvas_size_alert_dialog.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class UserLibraryPage extends StatefulWidget {
  final UserModel userDto;
  const UserLibraryPage({required this.userDto, Key? key}) : super(key: key);

  @override
  UserLibraryPageState createState() => UserLibraryPageState();
}

class UserLibraryPageState extends State<UserLibraryPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<TemplateModel> published = <TemplateModel>[];
  List<TemplateModel> unfinished = <TemplateModel>[];
  List<TemplateModel> finished = <TemplateModel>[];
  List<TemplateModel> nullProgress = <TemplateModel>[];
  List<TemplateModel> notNullProgress = <TemplateModel>[];
  List<TemplateModel> subRelease = <TemplateModel>[];
  int? templateId;
  UserModel? userDto;

  @override
  void initState() {
    super.initState();
    userDto = widget.userDto;
    _getImage;
  }

  Future<void> get _getImage async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await http
        .get(Uri.parse("http://localhost:8080/api/templates/allUser"), headers: headers);

    if (response.statusCode == 200) {
      final all = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final publishedL = all['published'] as List;
      final unfinishedL = all['unfinished'] as List;
      final finishedL = all['finished'] as List;
      final nullProgressL = all['nullProgress'] as List;
      final notNullProgressL = all['notNullProgress'] as List;
      final subReleaseL = all['subRelease'] as List;

      for (var item in subReleaseL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          subRelease.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }

      for (var item in publishedL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          published.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }

      for (var item in unfinishedL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          unfinished.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }

      for (var item in nullProgressL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          nullProgress.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }

      for (var item in notNullProgressL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          notNullProgress.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }

      for (var item in finishedL) {
        final String base64Image = item['image'];
        final id = item['id'];
        final name = item['name'];
        final dCount = item['downloadsCount'];
        final uName = item['creatorName'];
        var imageBytes = base64Decode(base64Image);
        ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
        ui.FrameInfo frame = await codec.getNextFrame();
        setState(() {
          var image = frame.image;
          finished.add(TemplateModel(id: id, name: name, sampleImage: image, categories: [], downloadsCount: dCount, creatorName: uName));
        });
      }
    } else {
      logger.i('load templates in user library caused error: ${response.body}');
    }
  }

  Future<void> deleteUsage(TemplateModel templateDto, List<TemplateModel> templates) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse(
        "http://localhost:8080/api/users/usage/checkExist/${userDto!.id}/${templateDto.id}");
    final response = await http
        .get(apiUrl, headers: headers);
    if(response.statusCode==200 && response.body!=null){
      var item = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      TemplateUsageModel templateUsageDto = TemplateUsageModel.fromJson(item);
      String? jsessionId1 = SessionManager().getJsessionId();
      Map<String, String>? headers1 = {'Content-Type': 'application/json'};
      if(jsessionId1!=null){
        headers1.putIfAbsent('Authorization', () => jsessionId1);
      }
      var url = Uri.parse(
          "http://localhost:8080/api/users/usage/remove/${templateUsageDto.id}");
      final deleteResponse = await http.delete(
        url, headers: headers1
      );
      if(deleteResponse.statusCode==200){
        if(deleteResponse.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${deleteResponse.headers['authorization']}');
        }
        setState(() {
          templates.remove(templateDto);
        });
      }else{
        logger.i('delete template usage caused error: ${deleteResponse.body}');
      }
    }
  }

  Future<void> deleteTemplate(TemplateModel templateDto, List<TemplateModel> templates) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/templates/${templateDto.id}/delete");
    final deleteResponse = await http.delete(
        url, headers: headers
    );
    if(deleteResponse.statusCode==200){
      if(deleteResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${deleteResponse.headers['authorization']}');
      }
      setState(() {
        templates.remove(templateDto);
      });
    }else{
      logger.i('delete template caused error: ${deleteResponse.body}');
    }
  }

  Future<void> choiceDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Enter new template name'),
          actions: <Widget>[
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        backgroundColor: Colors.deepPurple.shade100,
                        foregroundColor: Colors.white,
                      ),
                      child: const Text('Draw in app'),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => CanvasSizeAlertDialog(userDto: userDto!)));
                      },
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        backgroundColor: Colors.deepPurple.shade100,
                        foregroundColor: Colors.white,
                      ),
                      child: const Text('Load png images'),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => LoadPngPage(userDto: userDto!)));
                      },
                    ),
                  ],
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    backgroundColor: Colors.deepPurple.shade100,
                    foregroundColor: Colors.white,
                  ),
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )
          ],
        );
      },
    );
  }

  Widget buildTemplateList(List<TemplateModel> templates, ScrollController scrollController, String text, String type){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(text, textScaler: TextScaler.linear(2),),
        Container(
            height: 250,
            child: Scrollbar(
                thumbVisibility: true,
                thickness: 6.0,
                controller: scrollController,
                radius: Radius.circular(5.0),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: templates.length,
                  controller: scrollController,
                  itemBuilder: (context, index) {
                    return
                      GestureDetector(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => TemplatePage(
                                    templateId: templates[index].id,
                                    userDto: userDto!)));
                          },child:
                      Container(
                          width: MediaQuery.of(context).size.width / (MediaQuery.of(context).size.width/250), // Set the width to 1/4th of the screen width to show 4 items at a time
                          padding: const EdgeInsets.all(4),
                          child:
                          Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AspectRatio(
                                  aspectRatio: 6 / 4,
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 15,
                                        left: 15,
                                        right: 15,
                                        bottom: 15),
                                    child:
                                    FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Container(
                                            height: 150,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.black),
                                            ),
                                            child: RawImage(
                                              image: templates[index]
                                                  .sampleImage,
                                            ))),
                                  ),
                                ),
                                AspectRatio(
                                  aspectRatio: 8.0,
                                  child: Text(templates[index].name.split('_').length > 1 ? templates[index].name.split('_')[1] : "",
                                    textAlign: TextAlign.center,),
                                ),
                                AspectRatio(
                                  aspectRatio: 8.0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceAround,
                                    children: [
                                      Text(templates[index].creatorName != null
                                          ? templates[index].creatorName!
                                          : "",
                                        textAlign: TextAlign.center,),
                                      Row(children: [
                                        const Icon(
                                            Icons.download
                                        ),
                                        Text(templates[index].downloadsCount
                                            .toString(),
                                          textAlign: TextAlign.center,),
                                      ],),
                                      if(type=="published" && templates[index].downloadsCount==0)
                                        GestureDetector(
                                          onTap: () {
                                            deleteTemplate(templates[index], templates);
                                          },
                                          child: const Icon(
                                              Icons.delete
                                          ),),
                                      if(type!="published" && type!="subRelease")
                                      GestureDetector(
                                        onTap: () {
                                          deleteUsage(templates[index], templates);
                                        },
                                        child: const Icon(
                                            Icons.delete
                                        ),),
                                    ],
                                  ),
                                ),
                              ])));
                  },
                )))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final ScrollController publishedScrollController = ScrollController();
    final ScrollController finishedScrollController = ScrollController();
    final ScrollController unfinishedScrollController = ScrollController();
    final ScrollController nullProgressScrollController = ScrollController();
    final ScrollController notNullProgressScrollController = ScrollController();
    final ScrollController subReleaseScrollController = ScrollController();
      return SelectionArea(child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(userDto: userDto!),
        ),
        body: Stack(
          children: [
        ListView(
          padding: const EdgeInsets.all(5),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('My templates', textScaler: TextScaler.linear(2.5),),
              ElevatedButton(
                onPressed: ()
                {
                  choiceDialog();
                  },
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  backgroundColor: Colors.deepPurple.shade100,
                  foregroundColor: Colors.white,
                ),
                child:
                const Text('Create new template'),),
            ],
          ),
            Column(
              children: [
                if(subRelease.isNotEmpty)
                  buildTemplateList(subRelease, subReleaseScrollController, "Following release list", "subRelease"),
                if(published.isNotEmpty)
                  buildTemplateList(published, publishedScrollController, "Published plays", "published"),
                if(unfinished.isNotEmpty)
                  buildTemplateList(unfinished, unfinishedScrollController, "Unfinished plays", "unfinished"),
                if(nullProgress.isNotEmpty)
                  buildTemplateList(nullProgress, nullProgressScrollController, "Not started plays", "nullProgress"),
                if(notNullProgress.isNotEmpty)
                  buildTemplateList(notNullProgress, notNullProgressScrollController, "Reattempted plays", "notNullProgress"),
                if(finished.isNotEmpty)
                  buildTemplateList(finished, finishedScrollController, "Finished plays", "finished"),
              ],
            )])
          ],
        ),
      ));
    }
}
