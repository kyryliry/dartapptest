import 'dart:convert';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/gallery_image_page.dart';
import 'package:paintappdart/utils/app_bar_with_back.dart';
import 'package:paintappdart/model/gallery_image_model.dart';

import '../utils/session_manager.dart';
import '../model/template_model.dart';
import '../model/user_model.dart';

class AllGalleryImagesPage extends StatefulWidget{
  final TemplateModel templateDto;
  final UserModel? userDto;
  AllGalleryImagesPage({Key? key, required this.templateDto, required this.userDto}) : super(key: key);

  @override
  AllGalleryImagesPageState createState() => AllGalleryImagesPageState();
}
class AllGalleryImagesPageState extends State<AllGalleryImagesPage> {

  List<GalleryImageModel> images = <GalleryImageModel>[];
  final logger = Logger(
    printer: SimplePrinter(),
  );

  @override
  void initState() {
    super.initState();
    getAllImages();
  }

  Future<void> getAllImages() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final response = await get(Uri.parse(
        "http://localhost:8080/api/templates/${widget.templateDto.id}/images"), headers: headers);

    if (response.statusCode == 200) {
      final items = json.decode(response.body);
      final imgs = items['images'] as List;
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      for(var io in imgs){
        String i = io['image'];
        var iBytes = base64Decode(i);
        ui.Codec codec1 = await ui.instantiateImageCodec(iBytes);
        ui.FrameInfo frame1 = await codec1.getNextFrame();
        List<dynamic> likes = io['likes'] as List;
        List<dynamic> dislikes = io['dislikes'] as List;
        setState(() {
        images.add(GalleryImageModel(id: io['id'], description: io['description'], usageId: io['usageId'], authorId: io['authorId'],
            authorName: io['authorName'], image: frame1.image, uploadDate: io['uploadDate'], actualScore: io['actualScore'], likes: likes.cast<int>(), dislikes: dislikes.cast<int>()));
        });
      }

    }else{
      logger.i('get all gallery images caused error: ${response.body}');
    }
  }

  @override
  Widget build(BuildContext context) {
    int colsNum = 4;
    if(600<MediaQuery.of(context).size.width && MediaQuery.of(context).size.width<850){
      colsNum = 3;
    }else if(MediaQuery.of(context).size.width<600){
      colsNum = 2;
    }else if(MediaQuery.of(context).size.width>850){
      colsNum = 4;
    }
    if(images.isEmpty){
      return const CircularProgressIndicator();
    }else {
      return SelectionArea(child:
        Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(userDto: widget.userDto,),
        ),
        body: Stack(
          children: [
              GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: colsNum, // Number of columns
                  childAspectRatio: 1.0, // Aspect ratio for each item
                  crossAxisSpacing: 4.0, // Spacing between items horizontally
                  mainAxisSpacing: 4.0, // Spacing between items vertically
                ),
                itemCount: images.length,
                itemBuilder: (context, index) {
                  return
                    GestureDetector(
                        onTap: () async {
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => GalleryImagePage(
                                galleryImageDto: images[index],
                                userDto: widget.userDto,
                                templateId: widget.templateDto.id,
                                fromTemplate: true,
                              )));
                        },
                        child: Container(
                            padding: const EdgeInsets.only(
                                top: 15, left: 15, right: 15, bottom: 15),
                            child:
                            Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  AspectRatio(
                                    aspectRatio: 6 / 4,
                                    child: Container(
                                      padding: const EdgeInsets.only(top: 15,
                                          left: 15,
                                          right: 15,
                                          bottom: 15),
                                      child:
                                      FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Container(
                                              height: 150,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.black),
                                              ),
                                              child: RawImage(
                                                image: images[index]
                                                    .image,
                                              ))),
                                    ),
                                  ),
                                  AspectRatio(
                                    aspectRatio: 8.0,
                                    child: Text(images[index].authorName ?? "",
                                      textAlign: TextAlign.center,),
                                  ),
                                  AspectRatio(
                                    aspectRatio: 8.0,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .spaceAround,
                                      children: [
                                        Row(
                                          children: [
                                            const Text('Score: '),
                                            Text(images[index].actualScore.toString(),
                                              textAlign: TextAlign.center,),
                                          ],
                                        ),
                                        const Row(children: [
                                          Icon(
                                              Icons.thumb_up_off_alt_outlined
                                          ),
                                          Text('0',
                                            textAlign: TextAlign.center,),
                                          Icon(
                                              Icons.thumb_down_off_alt_outlined
                                          ),
                                          Text('0',
                                            textAlign: TextAlign.center,),
                                        ],)
                                      ],
                                    ),
                                  ),
                                ])));
                },
              )
          ],
        ),
      ));
    }
  }

}