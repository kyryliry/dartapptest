import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/profile_page.dart';

import '../utils/app_bar_with_back.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class RolesPage extends StatefulWidget {
  final UserModel userDto;

  RolesPage({Key? key, required this.userDto}) : super(key: key);

  @override
  RolesPageState createState() => RolesPageState();
}

class RolesPageState extends State<RolesPage> with SingleTickerProviderStateMixin {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<UserModel> players = <UserModel>[];
  List<UserModel> moders = <UserModel>[];
  List<UserModel> admins = <UserModel>[];
  List<UserModel> blockedUsers = <UserModel>[];
  List<UserModel> selectedUsers = <UserModel>[];
  Map<String, int> totalPages = <String, int>{};
  Map<String, int> currentPage = <String, int>{};
  int amountPerPage = 21;
  TextEditingController _textFieldController = TextEditingController();
  TextEditingController _passFieldController = TextEditingController();
  TextEditingController _emailFieldController = TextEditingController();
  String? valueText;
  String? password;
  String? email;
  bool rolesOpen = false;
  String userType = "users";
  ui.Image? image;

  TabController? _tabController;
  @override
  void initState(){
    super.initState();
    _tabController = TabController(vsync: this, length: 5);
    _tabController!.addListener(_handleTabSelection);
    totalPages.putIfAbsent('users', () => 0);
    totalPages.putIfAbsent('moders', () => 0);
    totalPages.putIfAbsent('admins', () => 0);
    totalPages.putIfAbsent('blockedUsers', () => 0);
    currentPage.putIfAbsent('users', () => 1);
    currentPage.putIfAbsent('moders', () => 1);
    currentPage.putIfAbsent('admins', () => 1);
    currentPage.putIfAbsent('blockedUsers', () => 1);
    fetchUsers(0, amountPerPage, "users");
    fetchUsers(0, amountPerPage, "moders");
    fetchUsers(0, amountPerPage, "admins");
    fetchUsers(0, amountPerPage, "blockedUsers");
    loadImage('assets/images/default_avatar.png');
  }

  Future<void> loadImage(String assetPath) async {
    final ByteData data = await rootBundle.load(assetPath);
    final ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
    final ui.FrameInfo fi = await codec.getNextFrame();
    setState(() {
      image = fi.image;
    });
  }

  void _handleTabSelection() {
    if (_tabController!.indexIsChanging) {
      setState(() {
        selectedUsers.clear();
      });
    }
  }

  @override
  void dispose() {
    _tabController!.removeListener(_handleTabSelection);
    _tabController?.dispose();
    super.dispose();
  }

  Future<void> switchToTab(int tabIndex, String type) async {
    _tabController?.animateTo(tabIndex);
  }

  Future<void> fetchUsers(int page, int size, String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    final String url = 'http://localhost:8080/api/users/get/$type/paged?page=$page&size=$size';
    final response = await get(Uri.parse(url),
        headers: headers);

    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final Map<String, dynamic> data = item['page'];
      List<dynamic> users = data['content'] as List;
      List<UserModel> tmpUsers = <UserModel>[];
      selectedUsers.clear();
        for (var user in users) {
          tmpUsers.add(UserModel(
              id: user['id'], userName: user['username'], role: user['role'], isBlocked: user['isBlocked'], email: user['email']));
        }
      setState(() {
        if(type=="users"){
          players.clear();
          players = List.of(players)..addAll(tmpUsers);
          totalPages['users'] = data['totalPages'];
        }else if(type=="moders"){
          moders.clear();
          moders = List.of(moders)..addAll(tmpUsers);
          totalPages['moders'] = data['totalPages'];
        }else if(type=="admins"){
          admins.clear();
          admins = List.of(admins)..addAll(tmpUsers);
          totalPages['admins'] = data['totalPages'];
        } else if(type=="blockedUsers"){
          blockedUsers.clear();
          blockedUsers = List.of(blockedUsers)..addAll(tmpUsers);
          totalPages['blockedUsers'] = data['totalPages'];
        }
      });
    } else {
      logger.i('fetch users in roles caused error: ${response.body}');
    }
  }

  Future<bool> updateUser(UserModel user, String password) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/admins/edit/${user.id}");
    final Map<String, dynamic> jsonFields = {
      'id': user.id,
      'username': user.userName,
      'password': password,
      'email': user.email,
      'role': user.role
    };
    final editResponse = await put(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if(editResponse.statusCode==200){
      if(editResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${editResponse.headers['authorization']}');
      }
      return true;
    }else{
      logger.i('update user in roles caused error: ${editResponse.body}');
      return false;
    }
  }

  Future<void> deleteUsers(String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/delete/set");

    List<Map<String, dynamic>> listOfObjects = [];

    for (UserModel user in selectedUsers) {
      Map<String, dynamic> u  = {
        'id':user.id
      };
      listOfObjects.add(u);
    }
    final cats = await delete(
      url,
      headers: headers,
      body: jsonEncode(listOfObjects),
    );
    if (cats.statusCode == 200) {
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      setState(() {
        fetchUsers(0, amountPerPage, type);
        if(type!="blockedUsers"){
          fetchUsers(0, amountPerPage, "blockedUsers");
        }
        selectedUsers.clear();
      });
    } else {
      logger.i('delete users in roles caused error: ${cats.body}');
    }
  }

  Future<void> blockUsers(String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/block/set");

    List<Map<String, dynamic>> listOfObjects = [];

    for (UserModel user in selectedUsers) {
      Map<String, dynamic> u  = {
        'id':user.id
      };
      listOfObjects.add(u);
    }
    final cats = await put(
      url,
      headers: headers,
      body: jsonEncode(listOfObjects),
    );
    if (cats.statusCode == 200) {
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      setState(() {
        fetchUsers(0, amountPerPage, type);
        if(type!="blockedUsers"){
          fetchUsers(0, amountPerPage, "blockedUsers");
        }
        selectedUsers.clear();
      });
    } else {
      logger.i('block users in roles caused error: ${cats.body}');
    }
  }

  Future<void> unblockUsers(String type) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/unblock/set");

    List<Map<String, dynamic>> listOfObjects = [];

    for (UserModel user in selectedUsers) {
      Map<String, dynamic> u  = {
        'id':user.id
      };
      listOfObjects.add(u);
    }
    final cats = await put(
      url,
      headers: headers,
      body: jsonEncode(listOfObjects),
    );
    if (cats.statusCode == 200) {
      if(cats.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
      }
      setState(() {
        fetchUsers(0, amountPerPage, "users");
        fetchUsers(0, amountPerPage, "admins");
        fetchUsers(0, amountPerPage, "moders");
        fetchUsers(0, amountPerPage, "blockedUsers");
        selectedUsers.clear();
      });
    } else {
      logger.i('unblock users in roles caused error: ${cats.body}');
    }
  }

  Future<void> registerUser(String type, String username, String email1, String password1) async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse("http://localhost:8080/api/users/$type/new");
    if(type=="users"){
      url = Uri.parse("http://localhost:8080/api/users/new");
    }

    Map<String, dynamic> u  = {
      'username':username,
      'email':email1,
      'password': password1
    };

    final createUser = await post(
      url,
      headers: headers,
      body: jsonEncode(u),
    );
    if(createUser.statusCode==200){
      if(createUser.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${createUser.headers['authorization']}');
      }
      setState(() {
        valueText = "";
        password = "";
        email="";
        userType = "users";
        _emailFieldController = TextEditingController(text:"");
        _textFieldController = TextEditingController(text:"");
        _passFieldController = TextEditingController(text:"");
        fetchUsers(0, amountPerPage, type);
      });
    }else{
      logger.i('create user in roles caused error: ${createUser.body}');
    }
  }

  Widget buildPagination(int currentPage, int totalPages, String type) {
    const int maxVisiblePages = 5;
    List<Widget> pageButtons = [];

    int startPage = max(currentPage - maxVisiblePages ~/ 2, 1);
    int endPage = min(startPage + maxVisiblePages - 1, totalPages);
    startPage = max(endPage - maxVisiblePages + 1, 1);

    if (startPage > 1) {
      pageButtons.add(buildPageButton(1, type));
    }

    if (startPage > 2) {
      pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
    }

    for (int i = startPage; i <= endPage; i++) {
      pageButtons.add(buildPageButton(i, type, isCurrent: i == currentPage));
    }

    if (totalPages > endPage) {
      if (endPage < totalPages - 1) {
        pageButtons.add(const Padding(padding: EdgeInsets.only(left: 5, right: 5), child: Text("...", style: TextStyle(color: Colors.black))));
      }
      pageButtons.add(buildPageButton(totalPages - 1, type));
      pageButtons.add(buildPageButton(totalPages, type));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: pageButtons,
    );
  }

  Widget buildPageButton(int page, String type, {bool isCurrent = false}) {
    return Padding(padding: const EdgeInsets.only(left: 2, right: 2),
      child:
      TextButton(
      onPressed: () {
        setState(() {
          currentPage[type] = page;
        });
        fetchUsers(page-1, amountPerPage, type);
      },
      style: TextButton.styleFrom(
        backgroundColor: isCurrent ? Colors.deepPurple : Colors.deepPurple.shade50,
        foregroundColor: isCurrent ? Colors.white : null,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
      child: Text((page).toString()),
    ));
  }

  Future<void> showEditDialog(int index, List<UserModel> users) async {
    String oldUserName = users[index].userName;
    String oldRole = users[index].role;
    String oldEmail = users[index].email!;
    String newPassword = "";
    TextEditingController usernameController = TextEditingController(text: oldUserName);
    TextEditingController emailController = TextEditingController(text: oldEmail);
    bool usernameEdit = false;
    bool emailEdit = false;
    bool passwordEdit = false;
    bool roleEdit = false;
    bool roleListOpen = false;
    bool roleWasUpdated = false;
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
        return AlertDialog(
          title: const Text('Edit user data'),
          content: Container(
            width: 300,
              child:
          SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                if(!usernameEdit)
                  Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                      child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Current username: '),
                    ElevatedButton(
                        onPressed: () {
                          setState(() {
                            usernameEdit = true;
                          });

                        },
                        child: const Text('Edit username'))
                  ],
                ),),
                if(!usernameEdit)
                  Text(users[index].userName, textScaler: const TextScaler.linear(1.5),),
                if(usernameEdit)
                  Row(
                    children: [
                      Expanded(child:
                      TextField(
                        controller: usernameController,
                        onChanged: (value) {
                          setState(() {
                            users[index].userName = value;
                          });
                        }),),
                      IconButton(
                          onPressed: () {
                            setState( (){
                                users[index].userName = oldUserName;
                                usernameEdit = false;
                                usernameController.text = oldUserName;
                            });
                          },
                          icon: const Icon(Icons.cancel_outlined))
                    ],
                  ),
                if(!emailEdit)
                  Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                      child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Current email: '),
                    ElevatedButton(
                        onPressed: () {
                          setState(() {
                            emailEdit = true;
                          });
                        },
                        child: const Text('Edit email'))
                  ],
                ),),
                if(!emailEdit)
                  Text(users[index].email!, textScaler: const TextScaler.linear(1.5),),
                if(emailEdit)
                  Row(
                    children: [
                      Expanded(child:
                      TextField(
                          controller: emailController,
                          onChanged: (value) {
                            setState(() {
                              users[index].email = value;
                            });
                          }),),
                      IconButton(
                          onPressed: () {
                            setState( (){
                              users[index].email = oldEmail;
                              emailEdit = false;
                              emailController.text = oldEmail;
                            });
                          },
                          icon: const Icon(Icons.cancel_outlined))
                    ],
                  ),
                if(!passwordEdit)
                  Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                  child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              passwordEdit = true;
                            });
                          },
                          child: const Text('Change password'))
                    ],
                  ),),
                if(passwordEdit)
                  Row(
                    children: [
                      Expanded(child:
                      TextField(
                          onChanged: (value) {
                            setState(() {
                              newPassword = value;
                            });
                          }),),
                      IconButton(
                          onPressed: () {
                            setState( (){
                              newPassword = "";
                              passwordEdit = false;
                            });
                          },
                          icon: const Icon(Icons.cancel_outlined))
                    ],
                  ),
                if(!roleEdit)
                  Padding(padding: const EdgeInsets.only(top:3, bottom: 3),
                      child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Current role: ${users[index].role}'),
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              roleEdit = true;
                            });
                          },
                          child: const Text('Edit role'))
                    ],
                  ),),
                if(roleEdit)
                  Row(
                    children: [
                      Expanded(child:
                      ElevatedButton(
                        onPressed: () {
                          setState(() {
                            roleListOpen = !roleListOpen;
                          });
                        },
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          backgroundColor: roleListOpen ? Colors.grey.shade300 : null,
                        ),
                        child: PopupMenuButton<String>(
                          onSelected: (String role) {
                            setState(() {
                              users[index].role = role;
                            });
                          },
                          itemBuilder: (BuildContext context) {
                            return <PopupMenuEntry<String>>[
                              PopupMenuItem<String>(
                                child: ConstrainedBox(
                                  constraints: BoxConstraints(maxHeight: 200.0),
                                  child: const SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        PopupMenuItem<String>(
                                          value: 'USER',
                                          child: Text('user'),
                                        ),
                                        PopupMenuItem<String>(
                                        value: 'MODER',
                                        child: Text('moder'),
                                        ),
                                        PopupMenuItem<String>(
                                        value: 'ADMIN',
                                        child: Text('admin'),
                                        ),
                                      ]),
                                    ),
                                  ),
                                ),
                            ];
                          },
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(users[index].role),
                              Icon(Icons.arrow_drop_down),
                            ],
                          ),
                        ),
                      ),
                      ),
                      IconButton(
                          onPressed: () {
                            setState( (){
                              roleEdit = false;
                            });
                          },
                          icon: const Icon(Icons.check_circle_outline)),
                      IconButton(
                          onPressed: () {
                            setState( (){
                              users[index].role = oldRole;
                              roleEdit = false;
                            });
                          },
                          icon: const Icon(Icons.cancel_outlined))
                    ],
                  ),
              ],
            ),
          ),),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                setState(() {
                  users[index].userName = oldUserName;
                  users[index].email = oldEmail;
                  users[index].role = oldRole;
                  usernameEdit = false;
                  emailEdit = false;
                  passwordEdit = false;
                  roleEdit = false;
                });
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              onPressed: () async {
                if(await updateUser(users[index], newPassword)){
                  if(oldRole!=users[index].role){
                    fetchUsers(0, amountPerPage, "${oldRole.toLowerCase()}s");
                    fetchUsers(0, amountPerPage, "${users[index].role.toLowerCase()}s");
                  }
                  usernameEdit = false;
                  emailEdit = false;
                  passwordEdit = false;
                  roleEdit = false;
                  Navigator.of(context).pop();
                }
              },
              child: const Text('Save'),
            ),
          ],
        );
      },);}
    );
  }


  Widget buildUserList(List<UserModel> users, String type) {
    return Column(
        children:[
          Expanded(child:
          GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: MediaQuery.of(context).size.width>850? 3 : 2,
                  crossAxisSpacing: 0,
                  mainAxisSpacing: 0,
                  childAspectRatio: 4.5
              ),
              itemCount: users.length,
              itemBuilder: (context, index) {
                  bool isChecked = selectedUsers.contains(users[index])
                      ? true
                      : false;
                return Padding(padding: const EdgeInsets.all(5),
                    child:
                    Container(
                    decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                ),
                height: 65,
                width: MediaQuery.of(context).size.width,
                  child:
                  Row(
                    children: [
                      if(type!="blockedUsers" && !users[index].isBlocked!)
                      Checkbox(
                          value: isChecked,
                          onChanged: (newValue){
                            setState(() {
                              isChecked = newValue!;
                              if(isChecked){
                                selectedUsers.add(users[index]);
                              }else if(!isChecked && selectedUsers.contains(users[index])){
                                selectedUsers.remove(users[index]);
                              }
                            });
                          }),
                      if(type=="blockedUsers")
                        Checkbox(
                            value: isChecked,
                            onChanged: (newValue){
                              setState(() {
                                isChecked = newValue!;
                                if(isChecked){
                                  selectedUsers.add(users[index]);
                                }else if(!isChecked && selectedUsers.contains(users[index])){
                                  selectedUsers.remove(users[index]);
                                }
                              });
                            }),
                      if(type!="blockedUsers" && users[index].isBlocked!)
                        const SizedBox(width: 32),
                      FittedBox(
                          alignment: Alignment.center,
                          fit: BoxFit.scaleDown,
                          child: Container(
                              alignment: Alignment.center,
                              child: RawImage(
                                width: 60,
                                height: 60,
                                image: image,
                              ))),
                      SizedBox(width: 8),
                          Expanded(child:
                          GestureDetector(onTap: () async {
                            var result = await Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(userDto: widget.userDto, profileId: users[index].id)));
                            if(result!=null && result==true){
                              fetchUsers(0, amountPerPage, type);
                            }
                          },
                            child:

                                Column(
                                  children:[
                          Text(
                            users[index].userName,
                            textScaler: MediaQuery.of(context).size.width>600? TextScaler.linear(1.5) : TextScaler.linear(1),
                            style: TextStyle(
                                color: users[index].isBlocked! ? Colors.red.shade800 : Colors.black
                            ),
                          ),
                          Text(
                            users[index].email!,
                            style: TextStyle(
                                color: users[index].isBlocked! ? Colors.red.shade800 : Colors.black
                            ),
                            overflow: TextOverflow.ellipsis,
                            ),
                            ],),),
                          ),

                      IconButton(onPressed: (){
                        showEditDialog(index, users).then((_) {
                          setState(() {
                            });
                        });
                      },
                          icon: const Icon(Icons.edit)),
                    ],
                  ),));
              }
          ),),
          if(selectedUsers.isNotEmpty)
            Container(
                height: 60,
                padding: const EdgeInsets.only(bottom: 10),
                child:
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if(type!="blockedUsers")
                    ElevatedButton(
                      onPressed: (){
                        blockUsers(type);
                      },
                      child: const Text('Block selected users'),
                    ),
                    if(type=="blockedUsers")
                      ElevatedButton(
                        onPressed: (){
                          unblockUsers(type);
                        },
                        child: const Text('Unblock selected users'),
                      ),
                    ElevatedButton(
                      onPressed: (){
                        deleteUsers(type);
                      },
                      child: const Text('Delete selected users'),
                    ),
                  ],
                )
            ),
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(bottom: 10),
            child: buildPagination(currentPage[type]!, totalPages[type]!, type),
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    if(players.isEmpty && admins.isEmpty && moders.isEmpty){
      return const CircularProgressIndicator();
    }else {
      return SelectionArea(child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple.shade50,
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          title: AppBarWithBack(
            userDto: widget.userDto,
          ),
        ),
        body: Stack(
          children: [
            Column(
              children: [
                TabBar(
                  controller: _tabController,
                  tabs: const [
                    Tab(text: 'Players'),
                    Tab(text: 'Moderators'),
                    Tab(text: 'Administrators'),
                    Tab(text: 'Blocklist'),
                    Tab(text: 'Register user'),
                  ],
                ),
                Expanded(child:
                TabBarView(
                  controller: _tabController,
                  children: [
                    buildUserList(players, "users"),
                    buildUserList(moders, "moders"),
                    buildUserList(admins, "admins"),
                    buildUserList(blockedUsers, "blockedUsers"),
                    Center(
                        child:
                        Container(
                          width: MediaQuery.of(context).size.width/2,
                          height: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.all(15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                                child:TextField(
                                  onChanged: (value) {
                                    setState(() {
                                      valueText = value;
                                    });
                                  },
                                  controller: _textFieldController,
                                  decoration:
                                  const InputDecoration(hintText: "username"),
                                ),),
                              Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                                child:TextField(
                                  onChanged: (value) {
                                    setState(() {
                                      email = value;
                                    });
                                  },
                                  controller: _emailFieldController,
                                  decoration:
                                  const InputDecoration(hintText: "email"),
                                ),),
                              Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                                child:
                                TextField(
                                  onChanged: (value) {
                                    setState(() {
                                      password = value;
                                    });
                                  },
                                  controller: _passFieldController,
                                  decoration:
                                  const InputDecoration(hintText: "password"),
                                ),),
                              Padding(padding: const EdgeInsets.only(top:15, bottom:15),
                              child:
                              Row(
                                children: [
                                  Expanded(child:
                                  ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        rolesOpen = !rolesOpen;
                                      });
                                    },
                                    style: ElevatedButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      backgroundColor: rolesOpen ? Colors.grey.shade300 : null,
                                    ),
                                    child: PopupMenuButton<String>(
                                      onSelected: (String role) {
                                        setState(() {
                                          userType = "${role.toLowerCase()}s";
                                            });
                                      },
                                      itemBuilder: (BuildContext context) {
                                        return <PopupMenuEntry<String>>[
                                          PopupMenuItem<String>(
                                            child: ConstrainedBox(
                                              constraints: BoxConstraints(maxHeight: 200.0),
                                              child: const SingleChildScrollView(
                                                child: Column(
                                                    children: [
                                                      PopupMenuItem<String>(
                                                        value: 'USER',
                                                        child: Text('user'),
                                                      ),
                                                      PopupMenuItem<String>(
                                                        value: 'MODER',
                                                        child: Text('moder'),
                                                      ),
                                                      PopupMenuItem<String>(
                                                        value: 'ADMIN',
                                                        child: Text('admin'),
                                                      ),
                                                    ]),
                                              ),
                                            ),
                                          ),
                                        ];
                                      },
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(userType.substring(0, userType.length-1).toUpperCase()),
                                          Icon(Icons.arrow_drop_down),
                                        ],
                                      ),
                                    ),
                                  ),
                                  ),
                                ],
                              ),),
                              Padding(padding: const EdgeInsets.only(top: 15, bottom: 15),
                                child:
                                MaterialButton(
                                  color: Colors.deepPurple.shade300,
                                  textColor: Colors.white,
                                  child: const Text('Register'),
                                  onPressed: () {
                                    setState(() {
                                      registerUser(userType, valueText!, email!, password!);
                                      });
                                  },
                                ),),
                            ],
                          ),
                        )
                    ),
                  ],
                ),),
              ],
            ),
            ]
        ),
      ));
    }
  }

}