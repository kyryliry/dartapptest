class ReportModel {
  int id;
  String text;
  String date;
  int? authorId;
  String authorUsername;
  int subjectId;
  String type;
  int? reviewerId;
  String? reviewerUsername;
  bool isOpen;
  String? closeReason;
  String? closeDate;

  ReportModel({required this.id,
    required this.text,
    required this.date,
    this.authorId,
    required this.authorUsername,
    required this.subjectId,
    required this.type,
    this.reviewerId,
    this.reviewerUsername,
    required this.isOpen,
    this.closeReason,
    this.closeDate});
}