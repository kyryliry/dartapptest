import 'dart:ui';

import 'comment_model.dart';

class GalleryImageModel {
  int id;
  int usageId;
  int authorId;
  Image? image;
  String uploadDate;
  String description;
  String? authorName;
  double actualScore;
  List<int> likes;
  List<int> dislikes;
  List<CommentModel>? comments;


  GalleryImageModel({required this.id, required this.usageId, required this.authorId, this.image, required this.uploadDate, required this.description, this.authorName, required this.actualScore, required this.likes, required this.dislikes, this.comments});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['usageId'] = usageId;
    data['authorId'] = authorId;
    data['uploadDate'] = uploadDate;
    data['description'] = description;
    if (image != null) data['image'] = image;
    if (authorName != null) data['authorName'] = authorName;
    data['actualScore'] = actualScore;
    return data;
  }
}