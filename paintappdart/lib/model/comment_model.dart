class CommentModel {
  int id;
  String text;
  int? authorId;
  String? authorUsername;
  String date;
  int subjectId;
  String type;
  List<int>? replies;
  List<int>? likes;
  List<int>? dislikes;
  List<CommentModel>? repliesList;

  CommentModel({required this.id, required this.text, this.authorId, this.authorUsername, required this.date,
  required this.subjectId, required this.type, this.replies, this.likes, this.dislikes, this.repliesList});
}