
class LayerModel {
  int templateId;
  int? id;
  int? attachmentId;
  int strokeSize;
  String color;
  String showColor;
  int number;


  LayerModel({required this.templateId, this.id, this.attachmentId, required this.strokeSize, required this.color, required this.showColor, required this.number});

  factory LayerModel.fromJson(Map<String, dynamic> json) {
    return LayerModel(
      templateId: json['templateId'] as int,
      id: json['id'] != null ? json['id']: null,
      attachmentId: json['attachmentId'] != null ? json['attachmentId']: null,
      strokeSize: json['strokeSize'] as int,
      color: json['color'] as String,
      showColor: json['showColor'] as String,
      // showColor: '#ffff0000',
      number: json['number'] as int,
    );
  }
}