import 'dart:ui';

class UnfinishedUsageModel {
  int id;
  int usageId;
  double? actualScore;
  Image? actualLayerAtch;
  Image? finishedLayerAtch;
  int? actualLayerId;


  UnfinishedUsageModel({required this.id, required this.usageId, required this.actualScore, this.actualLayerAtch, this.finishedLayerAtch, this.actualLayerId});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['usageId'] = usageId;
    if (actualScore != null) data['actualScore'] = actualScore;
    if (actualLayerAtch != null) data['actualLayerAtch'] = actualLayerAtch;
    if (finishedLayerAtch != null) data['finishedLayerAtch'] = finishedLayerAtch;
    if (actualLayerId != null) data['actualLayerId'] = actualLayerId;

    return data;
  }
}