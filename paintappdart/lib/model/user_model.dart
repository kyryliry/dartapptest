class UserModel {
  int id;
  String userName;
  String role;
  bool? isBlocked;
  String? email;
  int? followersCount;
  int? followingCount;
  double? globalPlayerRating;
  double? globalCreatorRating;
  int? numOfTemps;

  UserModel(
      {required this.id, required this.userName, required this.role, this.isBlocked, this.email,
      this.followersCount, this.followingCount, this.globalCreatorRating, this.globalPlayerRating, this.numOfTemps});
}