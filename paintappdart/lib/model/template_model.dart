
import 'dart:ui';

import 'category_model.dart';
import 'comment_model.dart';
import 'gallery_image_model.dart';

class TemplateModel {
  int id;
  int? creatorId;
  String name;
  Image sampleImage;
  List<CategoryModel> categories;
  List<GalleryImageModel>? galleryImages;
  int? layersAmount;
  int downloadsCount;
  String? creatorName;
  String? uploadDate;
  String? description;
  List<int>? likes;
  List<int>? dislikes;
  List<CommentModel>? comments;
  double? maxScore;

  TemplateModel({required this.id, this.creatorId, required this.name, required this.sampleImage, required this.categories, this.galleryImages, this.layersAmount, required this.downloadsCount, this.creatorName, this.uploadDate, this.description, this.likes, this.dislikes, this.comments, this.maxScore});
}