
class TemplateUsageModel {
  int id;
  double? actualScore;
  String dateOfLastPlay;
  int playerId;
  int parentId;
  String state;
  int? unfinishedUsageId;

  TemplateUsageModel({required this.id, required this.actualScore, required this.dateOfLastPlay,
  required this.playerId, required this.parentId, required this.state, this.unfinishedUsageId});

  factory TemplateUsageModel.fromJson(Map<String, dynamic> json) {
    return TemplateUsageModel(
      id: json['id'] as int,
      actualScore: json['actualScore']!=null? json['actualScore'] as double : null,
      dateOfLastPlay: json['dateOfLastPlay'] as String,
      playerId: json['playerId'] as int,
      parentId: json['parentId'] as int,
      state: json['state'] as String,
      unfinishedUsageId: json['unfinishedUsageId'] != null ? json['unfinishedUsageId'] : null,
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'actualScore': actualScore,
    'dateOfLastPlay': dateOfLastPlay,
    'playerId': playerId,
    'parentId': parentId,
    'state': state,
    'unfinishedUsageId': unfinishedUsageId,
  };
}