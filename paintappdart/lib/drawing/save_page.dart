import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/templatesLibrary/library_main_page.dart';
import 'package:paintappdart/model/layer_model.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart' as fc;
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

import '../model/category_model.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class SavePage extends StatefulWidget {
  final Map<LayerModel, ui.Image> layers;
  final ui.Image sampleImage;
  final Function(Map <int, LayerModel> data, List<CategoryModel> cats, List<CategoryModel> selCats) saveReturn;
  final List<CategoryModel> categories;
  final List<CategoryModel> selectedCategories;
  final UserModel userDto;
  const SavePage({Key? key, required this.layers, required this.saveReturn, required this.sampleImage, required this.categories, required this.selectedCategories, required this.userDto}) : super(key: key);

  @override
  _SavePageState createState() => _SavePageState();
}

class _SavePageState extends State<SavePage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  Map<LayerModel, ui.Image> layers = <LayerModel, ui.Image>{};
  List<String> pathes = <String>[];
  List<Uint8List> pathesFile = <Uint8List>[];
  String? templateName;
  Map<int, LayerModel> layersData = <int, LayerModel>{};
  int id = 0;
  bool categoriesListOpen = false;
  CategoryModel? selectedCategory;
  List<CategoryModel> categories = <CategoryModel>[];
  List<CategoryModel> selectedCategories = <CategoryModel>[];
  bool isSended = false;
  double maxScore = 0;
  TextEditingController scoreController = TextEditingController(text: "0");
  String error = "";

  @override
  void initState() {
    super.initState();
    layers = widget.layers;
    categories = widget.categories;
    selectedCategories = widget.selectedCategories;
    maxScore = layers.length.toDouble();
    scoreController = TextEditingController(text: maxScore.toString());
  }



  Future<void> prepareLayersData() async {
    String formattedName = templateName!.replaceAll(RegExp(r'\s'), '_');
    File sampleFile = File("$formattedName.png");
    final sampleData = await widget.sampleImage.toByteData(
        format: ui.ImageByteFormat.png);
    Uint8List? sampleBytes = sampleData?.buffer.asUint8List();

    for(int i=0; i<layers.length; i++){
      MapEntry entry = layers.entries.elementAt(i);
      ui.Image croppedImage = entry.value;

      File file = File("$formattedName$i.png");

      final data = await croppedImage.toByteData(
          format: ui.ImageByteFormat.png);

      Uint8List? bytes = data?.buffer.asUint8List();
      pathes.add(file.path);
      pathesFile.add(bytes!);

    }
    sendPostRequest(sampleBytes!, sampleFile);
  }

  Future<void> sendPostRequest(Uint8List sampleBytes, File sampleFile) async {
    var tmp = "test1";
    if(templateName!=null){
      tmp = templateName!;
    }

    var formData = FormData.fromMap({
      'templateName': tmp,
      'file': MultipartFile.fromBytes(sampleBytes, filename:sampleFile.path),
      'categories': jsonEncode(selectedCategories.map((category) => category.toJson()).toList()),
      'creatorId':widget.userDto.id,
      'state':'PUBLISHED',
      'maxScore':maxScore,
    });


    if(pathes.length==layers.length) {
      for (int i = 0; i < pathes.length; i++) {
        MapEntry entry = layers.entries.elementAt(i);
        LayerModel key = entry.key;
        formData.fields

          ..add(MapEntry('layers[$i].strokeSize', key.strokeSize.toString()))
          ..add(MapEntry('layers[$i].color', key.color))
          ..add(MapEntry('layers[$i].showColor', key.showColor))
          ..add(MapEntry('layers[$i].number', i.toString()));
          formData.files.add(MapEntry(
            'layers[$i].fileL',
            MultipartFile.fromBytes(pathesFile[i], filename: pathes[i]),
          ));
      }
    }


    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var response = await Dio().post("http://localhost:8080/api/template/createWithLayers", data: formData, options: Options(headers: headers));
    if (response.statusCode == 200) {
      if(response.headers.value('authorization')!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers.value('authorization')}');
      }
      isSended = true;
      if(isSended) {
        if(!mounted){
          return;
        }
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => LibraryMainPage(name: widget.userDto!.userName,)),
              (Route<dynamic> route) => false,
        );

      }
    } else {
      logger.i('create template with layers caused error: ${response.data}');
    }
  }

  Future<void> _showAlertDialog() async {
    String e = error; // Capture the initial error message
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // Local state variables for the dialog
        String localError = e;
        TextEditingController _nameController = TextEditingController();

        return StatefulBuilder( // Use StatefulBuilder to manage state inside the dialog
          builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              title: const Text('Enter new template name'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    if (localError.isNotEmpty) // Conditionally display error text
                      Text(localError),
                    TextField(
                      controller: _nameController,
                      onChanged: (value) {
                        templateName = value;
                      },
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    setState(() { // Update the local error message within the dialog
                      error = "";
                    });
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  onPressed: () async {
                    if (await sendGetNameRequest() == true) {
                      prepareLayersData();
                      Navigator.of(context).pop(); // Close the dialog on success
                    } else {
                      setState(() { // Update the local error message within the dialog
                        localError = "This name is not unique!";
                      });
                    }
                  },
                  child: const Text('Save'),
                ),
              ],
            );
          },
        );
      },
    );
  }


  Future<bool> sendGetNameRequest() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/templates/test1");
    if (templateName != null) {
      String name = "${widget.userDto.userName}_${templateName!}";
      apiUrl = Uri.parse("http://localhost:8080/api/templates/$name");
    }
    final response = await http.get(apiUrl, headers: headers);
    if (response.statusCode == 200) {
      var responseData = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      if (responseData['id'] == -1) {
        setState(() {
          error = "";
        });
        return true;
      } else {
        setState(() {
          error = "This name is not unique!";
        });
        return false;
      }
    } else {
      logger.i('get template name caused error: ${response.body}');
    }
    return false;
  }

  Future<void> scoreNullDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Max score must contain value between 1 and 100',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    return SelectionArea(child:Scaffold(
      appBar: AppBar(toolbarHeight: 60, title: const Text('New Template'),
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            widget.saveReturn(layersData, categories, selectedCategories);
            Navigator.pop(context, true);
          },
        ),
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 20),
              child: GestureDetector(
                onTap: () {
                  if(maxScore>0 && maxScore<=100) {
                    _showAlertDialog();
                  } else {
                    scoreNullDialog(context);
                  }
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: const CircleAvatar(
                      radius: 40.0,
                      child: Icon(
                        Icons.save,
                        size: 30.0,
                        color: Colors.white,
                      )),
                ),
              )),
        ],),
      body: Stack(
        children: [

          Padding(
            padding: const EdgeInsets.only(left: 20.0 ,right: 20.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Text('Layers list',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black),
                    ),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Layers order'),
                        Text('Image'),
                        Text('Stroke size'),
                        Text('Draw color'),
                        Text('Show color'),
                      ],
                    ),
                  ),
                  Expanded(
                  child: Container(
                      padding: EdgeInsets.only(left:20, right: 20),
                    height: 400,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black),
                      ),
                  child: ListView.builder(
                    itemCount: layers.length,
                    itemBuilder: (context, index) {
                      MapEntry entry = layers.entries.elementAt(index);
                      LayerModel key = entry.key;
                      ui.Image value = entry.value;
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text('${key.number}'),
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black), // Border color
                            ),
                            width: value.width/5,
                            height: value.height/5,
                            child: RawImage(
                              color: Color(int.parse(key.color.replaceAll('#', '0x'))),
                              image:value,
                              width: value.width/5,
                              height: value.height/5,
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.black),
                            ),
                            width: 70,
                            height: 40,
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      if(key.strokeSize>1) {
                                        key.strokeSize--;
                                        layersData.putIfAbsent(
                                            key.number, () => key);
                                      }
                                    });
                                  },
                                  child: const Icon(
                                        FontAwesomeIcons.minus,
                                        size: 15.0,
                                        color: Colors.black,
                                      ),
                                ),
                                Text('${key.strokeSize}'),
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      if(key.strokeSize<50) {
                                        key.strokeSize++;
                                        layersData.putIfAbsent(
                                            key.number, () => key);
                                      }
                                    });
                                  },
                                  child: const Icon(
                                        FontAwesomeIcons.plus,
                                        size: 15.0,
                                        color: Colors.black,
                                      ),
                                ),
                              ],
                            )
                          ),
                          Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black),
                                color: Color(int.parse(key.color.replaceAll('#', '0x'))), // Border color
                              ),
                              width: 30,
                              height: 30,
                              padding: const EdgeInsets.symmetric(vertical: 8.0),
                          ),
                          GestureDetector(
                              onTap: (){
                                String tmpColor = key.showColor;
                                showDialog<void>(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {

                                    return AlertDialog(
                                      titlePadding: const EdgeInsets.all(0),
                                      contentPadding: const EdgeInsets.all(0),
                                      content: SingleChildScrollView(
                                        child: fc.ColorPicker(
                                          pickerColor: Color(int.parse(key.showColor.replaceAll('#', '0x'))),
                                          onColorChanged: (color) {
                                            setState(() {
                                              key.showColor = '#${color.value.toRadixString(16)}';
                                            });
                                          },
                                          colorPickerWidth: 300,
                                          pickerAreaHeightPercent: 0.7,
                                          enableAlpha: false,
                                          displayThumbColor: true,
                                          showLabel: true,
                                          paletteType: fc.PaletteType.hsv,
                                          pickerAreaBorderRadius: const BorderRadius.only(
                                            topLeft: const Radius.circular(2.0),
                                            topRight: const Radius.circular(2.0),
                                          ),
                                          portraitOnly: true,
                                        ),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          child: const Text('No'),
                                          onPressed: () {
                                            setState(() {
                                              key.showColor = tmpColor;
                                            });
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          onPressed: () async {
                                            setState(() {
                                              layersData.putIfAbsent(key.number, () => key);
                                            });
                                              Navigator.of(context).pop();
                                          },
                                          child: const Text('Yes'),
                                        ),
                                      ],

                                    );},);},
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black), // Border color
                                  color: Color(int.parse(key.showColor.replaceAll('#', '0x'))),
                                ),
                                width: 30,
                                height: 30,
                                padding: const EdgeInsets.symmetric(vertical: 8.0),

                              )
                          )
                        ],
                      );
                    },
                  )
                  )),
                  Row(
                    children: [
                      const Text('Current max possible score: '),
                      Container(
                        width: 60,
                        child:
                      TextField(
                        onChanged: (value) {
                          setState(() {
                            maxScore = double.tryParse(value) ?? 0;
                          });
                        },
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        controller: scoreController,
                        decoration:
                        const InputDecoration(hintText: "Enter reason of your report"),
                      ),),
                    ],
                  ),
                  Container(
                    height: 150,
                    child:
                    Column(
                      children: [
                        Row(
                          children: [
                            const Text('Categories: '),
                      Expanded(
                            child: Container(
                              height: 50,
                            child:
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                            itemCount: selectedCategories.length,
                            itemBuilder: (context, index) {
                              return Row(
                                children: [
                                  Text(selectedCategories[index].name),
                                  IconButton(
                                      onPressed: (){
                                        setState(() {
                                          CategoryModel tmp = selectedCategories[index];
                                          selectedCategories.remove(tmp);
                                          categories.add(tmp);
                                          categories.sort((a, b) => a.name.compareTo(b.name));
                                        });

                                      },
                                      icon: const Icon(Icons.highlight_remove_outlined)
                                  ),
                                ],
                              );
                            }),),)
                          ],
                        ),
                        Row(

                            children: [
                        ElevatedButton(
                          onPressed: () {
                            setState(() {
                              categoriesListOpen = !categoriesListOpen;
                            });
                          },
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            backgroundColor: categoriesListOpen ? Colors.grey.shade300 : null,
                          ),
                          child: PopupMenuButton<CategoryModel>(
                            onSelected: (CategoryModel category) {
                              setState(() {
                                selectedCategory = category;
                              });
                            },
                            itemBuilder: (BuildContext context) {
                              return <PopupMenuEntry<CategoryModel>>[
                                PopupMenuItem<CategoryModel>(
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(maxHeight: 200.0),
                                    child: SingleChildScrollView(
                                      child: Column(
                                        children: categories.map((CategoryModel category) {
                                          return PopupMenuItem<CategoryModel>(
                                            value: category,
                                            child: Text(category.name),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              ];
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(selectedCategory?.name ?? 'select category'),
                                Icon(Icons.arrow_drop_down),
                              ],
                            ),
                          ),
                        ),

                          ElevatedButton(
                          onPressed: ()
                          {
                            if(selectedCategory!=null){
                              setState(() {
                          selectedCategories.add(selectedCategory!);
                          categories.remove(selectedCategory!);
                          selectedCategory = null;
                              });

                            }
                          },
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            backgroundColor: Colors.deepPurple.shade100,
                            foregroundColor: Colors.white,
                          ),
                          child:
                              const Text('add category'),),
                        ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),),
                ],
              )
            ));
  }
    
}
