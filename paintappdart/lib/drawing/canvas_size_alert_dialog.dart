import 'package:cyclop/cyclop.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paintappdart/model/user_model.dart';

import 'draw_mode_page.dart';

class CanvasSizeAlertDialog extends StatefulWidget{
  final UserModel userDto;
  const CanvasSizeAlertDialog({Key? key, required this.userDto}) : super(key: key);

  @override
  CanvasSizeAlertDialogState createState() => CanvasSizeAlertDialogState();

}

 class CanvasSizeAlertDialogState extends State<CanvasSizeAlertDialog>{

  double width = 0;
  double height = 0;
   int selectedOption = 0;
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double widget1Width = 200;
    double remainingWidth = screenWidth - widget1Width -60;
    double widgetHeight = MediaQuery.of(context).size.height - 90;
    return SelectionArea(child:Scaffold(
      appBar: AppBar(
        title: const Text('New Template'),
      ),
      body: Center(
        child: AlertDialog(
          title: Text('Select canvas size'),
          content: Container(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
            ListTile(
            title: Text('Screen size ${remainingWidth.toInt()} x ${widgetHeight.toInt()}'),
            leading: Radio<int>(
              value: 1,
              groupValue: selectedOption,
              onChanged: (value) {
                setState(() {
                  selectedOption = value!;
                  width = remainingWidth;
                  height = widgetHeight;
                });
              },
            ),
          ),
                  ListTile(
                    title: Text('Square ${remainingWidth.toInt()} x ${remainingWidth.toInt()}'),
                    leading: Radio<int>(
                      value: 2,
                      groupValue: selectedOption,
                      onChanged: (value) {
                        setState(() {
                          selectedOption = value!;
                          width = remainingWidth;
                          height = remainingWidth;
                    });
                      },
                    ),
                  ),
      ]
        )),
          actions: <Widget>[
            MaterialButton(
              color: Colors.red,
              textColor: Colors.white,
              child: const Text('CANCEL'),
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
            ),
            MaterialButton(
              color: Colors.green,
              textColor: Colors.white,
              child: const Text('OK'),
              onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => EyeDrop( child: DrawModePage(width: width, height: height, userDto: widget.userDto,))));
              },
            ),
          ],)
      ),
    ));
  }

 }