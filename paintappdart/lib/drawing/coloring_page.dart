import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/drawing/base_page.dart';
import 'package:paintappdart/painters/save_unfinished_painter.dart';
import 'package:paintappdart/templatesLibrary/gallery_image_page.dart';
import 'package:paintappdart/model/gallery_image_model.dart';
import 'package:paintappdart/model/layer_model.dart';
import 'package:paintappdart/utils/stroke_pair.dart';
import 'package:paintappdart/model/template_usage_model.dart';
import 'package:paintappdart/model/unfinished_usage_model.dart';
import 'package:http/http.dart' as http;
import 'package:tuple/tuple.dart';

import '../painters/accuracy_calculation.dart';
import '../painters/layers_painter.dart';
import '../painters/current_layer_painter.dart';
import '../painters/single_line_painter.dart';
import '../utils/session_manager.dart';
import '../utils/stroke.dart';
import '../utils/stroke_options.dart';

class ColoringPage extends BasePage {
  final String? value;
  final ValueNotifier<bool> addLayerAction;
  final ValueNotifier<bool> saveAction;
  final Function() endReached;
  final Function(int) getLayersCount;
  final Function(StrokeOptions) onSizeChanged;
  final resizeFactor;
  final int templateId;
  final ValueNotifier<bool> finishTrigger;
  final ValueNotifier<bool> unfinishedTrigger;
  final ValueNotifier<bool> calculateTrigger;

  const ColoringPage({Key? key,
    required super.userId,
    required super.width,
    required super.height,
    required super.nextOptionsNotifier,
    required super.eraserMode,
    required super.backActionClick,
    required super.forwardActionClick,
    required super.pickerColor,
    required super.zoomLevel,
    required super.valueX,
    required super.valueY,
    required super.clearAction,
    required super.onIndexChanged,
    required super.userDto,
    required this.value,
    required this.addLayerAction,
    required this.saveAction,
    required this.endReached,
    required this.getLayersCount,
    required this.resizeFactor,
    required this.templateId,
    required this.finishTrigger,
    required this.unfinishedTrigger,
    required this.calculateTrigger,
    required this.onSizeChanged})
      : super(key: key);

  @override
  _ColoringPageState createState() => _ColoringPageState();
}

class _ColoringPageState extends BasePageState<ColoringPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  int unfinishedLayerNumber = -1;
  List<LayerModel> dtos = <LayerModel>[];
  String valueT = "test123";
  List<List<Stroke>> allLines = <List<Stroke>>[];
  List<int> strokeSizes = <int>[];

  ui.Image? img;
  int count = 0;

  Uint8List? res;

  List<Future<ui.Image>> imgs = <Future<ui.Image>>[];
  List<ui.Image> imgs2 = <ui.Image>[];
  List<Tuple2<int, double>> accuracyList = [];

  Color showColor = Colors.black;
  double resizeFactor = 1;
  TemplateUsageModel? templateUsageDto;
  UnfinishedUsageModel? unfinishedUsageDto;
  GlobalKey _repaintBoundaryKey = GlobalKey();
  GlobalKey _repaintBoundaryKey2 = GlobalKey();
  bool finishExecuted = false;
  Map<int, List<Offset>> allPixels = {};
  AccuracyCalculator? calculator;
  double? layerAccuracy;
  bool isCalculationAccuracy = false;
  Offset pointerPosition = Offset.zero;
  double maxScore = 1;
  double finAverage = 0.0;

  @override
  void initState() {
    super.initState();
    valueT = widget.value!;
    nextOptions = widget.nextOptionsNotifier.value;
    eraserMode = widget.eraserMode.value;
    zoomLevel = widget.zoomLevel.value;
    widget.onIndexChanged(selectedIndex!);
    valueX = widget.valueX.value;
    valueY = widget.valueY.value;
    widget.addLayerAction.addListener(_executeAddLayerAction);
    widget.finishTrigger.addListener(_executeFinishUsage);
    widget.unfinishedTrigger.addListener(_executeUnfinishedUsage);
    widget.calculateTrigger.addListener(_executeCalculateAccuracy);
    resizeFactor = widget.resizeFactor;
    setState(() {
      templateUsage();
      _getImage;
    });
  }

  void _executeCalculateAccuracy(){
    if(count<dtos.length) {
      setState(() {
        layerAccuracy = null;
        isCalculationAccuracy = true;
      });
      showLoadingDialog(context, "Calculating");
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Future.delayed(Duration(milliseconds: 100), () {
          if (isCalculationAccuracy && mounted) {
            calculateAccuracyForLayer(true);
          }
        });
      });
    }
  }

  Future<void> calculateAccuracyForLayer(bool isNotNext) async {
    if(mounted) {
      Navigator.pop(context);
    }
    calculator = AccuracyCalculator(currentColor: currentColor, optionList: optionList, lines: lines,
        options: options, colors: colors, allPixels: allPixels, count: count, len: img!.width*img!.height, resizeFactor: resizeFactor!=1? resizeFactor:null, img: imgs2[count], isCancelled: false,
      backgroundImage: unfinishedUsageDto?.actualLayerAtch!=null && count==unfinishedLayerNumber? unfinishedUsageDto!.actualLayerAtch: null,
    );
    Stopwatch stopwatch = Stopwatch();

    stopwatch.start();
    double? accuracy = await calculator!.calculate(canvasWidth, canvasHeight, MediaQuery.of(context).devicePixelRatio);
    stopwatch.stop();
    setState(() {
      isCalculationAccuracy = false;
      if(accuracy!=null) {
        accuracy = double.parse(accuracy!.toStringAsFixed(2));
        layerAccuracy = accuracy;
      }
    });
    if(accuracy!=null) {
      accuracy = double.parse(accuracy!.toStringAsFixed(2));
      if (!isNotNext) {
        accuracyList.add(Tuple2(count, accuracy!));
      }
      if (isNotNext) {
        if (mounted) {
          showAccuracyDialog(context);
        }
      }
    }
  }

  void cancelCalculation() {
    if(calculator!=null) {
      calculator!.cancel();
    }
  }

  void showLoadingDialog(BuildContext context, String text) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: Text(text),
          content: const Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Please wait..."),
            ],
          ),
        );
      },
    );
  }
  void showAccuracyDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: const Text("Current layer accuracy"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [Text("${layerAccuracy!}%")],
          ),
          actions: <Widget>[
            ElevatedButton(
              child: const Text("Ok"),
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
            ),
          ],
        );
      },
    );
  }


  Future<void> _executeUnfinishedUsage() async {
    var average = 0.0;
    if (count < dtos.length) {
        if (accuracyList.length > 1) {
          average = accuracyList.map((t) => t.item2).reduce((a,
              b) => a + b) / accuracyList.length;
        } else if (accuracyList.length == 1) {
          average = accuracyList[0].item2;
        }
        if (everyLineHasOption.isNotEmpty && unfinishedUsageDto != null) {
        showLoadingDialog(context, "Loading");
        setState(() {
          layerAccuracy = null;
          isCalculationAccuracy = true;
        });
        await Future.delayed(Duration(milliseconds: 100));
        if (mounted && isCalculationAccuracy) {
          await calculateAccuracyForLayer(false);
        }
        var actual = 0.0;
        if (accuracyList.length > 1) {
          actual = accuracyList.map((t) => t.item2).reduce((a,
              b) => a + b);
        } else if (accuracyList.length == 1) {
          actual = accuracyList[0].item2;
        }
        var amount = dtos.length;
        if(count<dtos.length){
          amount = dtos.length-(dtos.length-(count+1));
        }
        average = (unfinishedUsageDto!.actualScore!+actual)/amount;
      }
    }
    average = double.parse(average.toStringAsFixed(2));
    saveAsUnfinishedUsage(average);
  }

  void _executeFinishUsage(){
    setState(() {
      finishExecuted = true;
    });
    prepareResultImage();
  }

  Future<void> _executeAddLayerAction() async {
    if(everyLineHasOption.isEmpty){
      alertDialog(context);
      return;
    }
    double average = 0.0;
    if(count<dtos.length) {
      if (count + 1 != accuracyList.length && everyLineHasOption.isNotEmpty &&
          unfinishedUsageDto == null) {
        showLoadingDialog(context, "Loading");
        setState(() {
          layerAccuracy = null;
          isCalculationAccuracy = true;
        });
        await Future.delayed(Duration(milliseconds: 100));
        if (mounted && isCalculationAccuracy) {
          await calculateAccuracyForLayer(false);
        }
        if (accuracyList.length > 1) {
          average = accuracyList.map((t) => t.item2).reduce((a,
              b) => a + b) / dtos.length;
        } else if (accuracyList.length == 1) {
          average = accuracyList[0].item2/dtos.length;
        }
      } else if (unfinishedUsageDto?.actualLayerAtch != null) {
        showLoadingDialog(context, "Loading");
        setState(() {
          layerAccuracy = null;
          isCalculationAccuracy = true;
        });
        await Future.delayed(Duration(milliseconds: 100));
        if (mounted && isCalculationAccuracy) {
          await calculateAccuracyForLayer(false);
        }
        var actual = 0.0;
        if (accuracyList.length > 1) {
          actual = accuracyList.map((t) => t.item2).reduce((a,
              b) => a + b);
        } else if (accuracyList.length == 1) {
          actual = accuracyList[0].item2;
        }
        var amount = dtos.length;
        if (count < dtos.length) {
          amount = dtos.length - (dtos.length - (count + 1));
        }
        average = (unfinishedUsageDto!.actualScore! + actual) / amount;
      }
      setState(() {
        finAverage = average;
      });
    }
    if(layerAccuracy!=null){
      next();
    }

  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Layer may not be empty, please draw something',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> next() async {
    setState(() {
      count++;
    });

      List<StrokePair> tmp = <StrokePair>[];
      for (var i in everyLineHasOption) {
        StrokePair newS = StrokePair(i.stroke, i.options);
        tmp.add(newS);
      }
      everyLineHasOptionListOfAll = List.from(everyLineHasOptionListOfAll)
        ..add(tmp);
      everyLineHasOption.clear();
      if (count < imgs2.length) {
        img = imgs2[count];
      } else {
        img = null;
      }
      setState(() {
        lines = [];
        line = null;
        optionList = [];
        layersColors = List.from(layersColors)
          ..add(currentColor);
        if (count < dtos.length) {
          currentColor =
              Color(int.parse(dtos[count].color.replaceAll('#', '0x')));
          options = StrokeOptions();
          options.size = dtos[count].strokeSize.toDouble();
          widget.onSizeChanged(StrokeOptions(size: options.size));
          showColor =
              Color(int.parse(dtos[count].showColor.replaceAll('#', '0x')));
          pickerColor = currentColor;
        }
        else {
          widget.endReached();
        }

        selectedIndex = selectedIndex! + 1;
      widget.onIndexChanged(selectedIndex!);
      });
  }

  Future<void> showFinishDialog(BuildContext context, ui.Image image, double average) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
            const Text('You finished redrawing template.',
              overflow: TextOverflow.clip,
            textAlign: TextAlign.center,),),
            content: Container(
          width: 300,
          height: 200,
          child:
          Column(
              children: [
                Expanded(child:
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),

                child: FittedBox(
                  alignment: Alignment.center,
                  fit: BoxFit.scaleDown,
                child:
                RawImage(
                  image: image,
                  width: image.width.toDouble(),
                  height: image.height.toDouble(),
                ),),),),
                Text('Your will get $average points',
                  overflow: TextOverflow.clip,
                  textAlign: TextAlign.center,),
                const Text('Do you want to add your result image to gallery of this template?',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),
              ],
            ),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('Cancel'),
                onPressed: () {
                  setState(() {
                    finishExecuted = false;
                    if(count<dtos.length && everyLineHasOption.isNotEmpty) {
                      accuracyList.removeLast();
                    }
                  });
                  Navigator.pop(context);
                },
              ),
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('Add and close'),
                onPressed: () async {
                  await finishTemplateUsage(average);
                  addNewGalleryImage(image, average);
                },
              ),
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('Dont add and close'),
                onPressed: () {
                  Navigator.pop(context);
                  finishTemplateUsage(average);
                },
              ),
            ],
          );
        });
  }

  addNewGalleryImage(ui.Image image, double average) async {
    String formattedName = valueT!.replaceAll(RegExp(r'\s'), '_');
    File imageFile = File("$formattedName.png");
    final imageData = await image.toByteData(
        format: ui.ImageByteFormat.png);
    Uint8List? imageBytes = imageData?.buffer.asUint8List();

    var formData = FormData.fromMap({
      'authorId': userDto!.id,
      'usageId': templateUsageDto!.id,
      'description':"",
      'image': MultipartFile.fromBytes(imageBytes!, filename:imageFile.path),
      'actualScore': average,
    });

    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var response = await Dio().post("http://localhost:8080/api/templates/${templateUsageDto!.parentId}/images/new", data: formData, options: Options(headers: headers));
    if(response.statusCode==200){
      final io = await response.data;
      if(response.headers.value('authorization')!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers.value('authorization')}');
      }
      String i = io['image'];
      var iBytes = base64Decode(i);
      ui.Codec codec1 = await ui.instantiateImageCodec(iBytes);
      ui.FrameInfo frame1 = await codec1.getNextFrame();
      List<dynamic> likes = io['likes'] as List;
      List<dynamic> dislikes = io['dislikes'] as List;
      GalleryImageModel galleryImageDto = GalleryImageModel(id: io['id'], description: io['description'], usageId: io['usageId'], authorId: io['authorId'],
          authorName: io['authorName'], image: frame1.image, uploadDate: io['uploadDate'], actualScore: io['actualScore'], likes: likes.cast<int>(), dislikes: dislikes.cast<int>());
      if (!mounted) return;
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => GalleryImagePage(galleryImageDto: galleryImageDto, userDto: userDto!, templateId: templateUsageDto!.parentId, fromTemplate: false)),
            (Route<dynamic> route) => false,
      );
    }else{
      logger.i('create gallery image caused error: ${response.data}');
    }
  }


  Future<ui.Image> combineImages(ui.Image image1, ui.Image image2) async {

    ui.PictureRecorder recorder = ui.PictureRecorder();
    ui.Canvas canvas = ui.Canvas(recorder);
    canvas.drawImageRect(image1, Rect.fromLTRB(0, 0, image1!.width.toDouble(), image1!.height.toDouble()), Rect.fromLTRB(0, 0, image1!.width.toDouble(), image1!.height.toDouble()), Paint());
    canvas.drawImageRect(image2, Rect.fromLTRB(0, 0, image1!.width.toDouble(), image1!.height.toDouble()), Rect.fromLTRB(0, 0, image1!.width.toDouble(), image1!.height.toDouble()), Paint());
    ui.Picture picture = recorder.endRecording();
    return picture.toImage(image1.width, image1.height);
  }

  Future<void> prepareResultImage() async {
    RenderRepaintBoundary boundary = _repaintBoundaryKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    ui.Image image = await boundary.toImage(pixelRatio: MediaQuery.of(context).devicePixelRatio); // Adjust the pixel ratio to your needs
    ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);

    RenderRepaintBoundary boundary2 = _repaintBoundaryKey2.currentContext!.findRenderObject() as RenderRepaintBoundary;
    ui.Image image2 = await boundary2.toImage(pixelRatio: MediaQuery.of(context).devicePixelRatio);


    Uint8List pngBytes = byteData!.buffer.asUint8List();
    ui.Image i = await combineImages(image, image2);

    if (!mounted) return;

    double average = 0.0;
    if(count<dtos.length) {
      if (count + 1 != accuracyList.length &&
          unfinishedUsageDto == null) {
        showLoadingDialog(context, "Loading");
        setState(() {
          layerAccuracy = null;
          isCalculationAccuracy = true;
        });
        await Future.delayed(Duration(milliseconds: 100));
        if (mounted && isCalculationAccuracy) {
          await calculateAccuracyForLayer(false);
        }
        if (accuracyList.length > 1) {
          average = accuracyList.map((t) => t.item2).reduce((a,
              b) => a + b) / dtos.length;
        } else if (accuracyList.length == 1) {
          average = accuracyList[0].item2/dtos.length;
        }
      } else if (unfinishedUsageDto?.actualLayerAtch != null) {
        showLoadingDialog(context, "Loading");
        setState(() {
          layerAccuracy = null;
          isCalculationAccuracy = true;
        });
        await Future.delayed(Duration(milliseconds: 100));
        if (mounted && isCalculationAccuracy) {
          await calculateAccuracyForLayer(false);
        }
        var actual = 0.0;
        if (accuracyList.length > 1) {
          actual = accuracyList.map((t) => t.item2).reduce((a,
              b) => a + b);
        } else if (accuracyList.length == 1) {
          actual = accuracyList[0].item2;
        }
        var amount = dtos.length;
        if (count < dtos.length) {
          amount = dtos.length - (dtos.length - (count + 1));
        }
        average = (unfinishedUsageDto!.actualScore! + actual) / amount;
      }
      setState(() {
        finAverage = average;
      });
    }
    if(finAverage!=average){
      average = finAverage;
    }
    average = (maxScore*average)/100;
    average = double.parse(average.toStringAsFixed(2));
    if(mounted) {
      showFinishDialog(context, i, average);
    }
  }

  Future<void> finishTemplateUsage(double average) async{
    final Map<String, dynamic> jsonFields = {
      'id': templateUsageDto!.id,
      'actualScore': average,
    };
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var url = Uri.parse(
        "http://localhost:8080/api/users/usage/finish");
    final finishResponse = await http.put(
      url,
      headers: headers,
      body: jsonEncode(jsonFields),
    );
    if (finishResponse.statusCode == 200) {
      if(finishResponse.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${finishResponse.headers['authorization']}');
      }
    }else{
      logger.i('finish template usage caused error: ${finishResponse.body}');
    }
    setState(() {
      Navigator.pop(context);
    });
  }

  Future<void> saveAsUnfinishedUsage(double average) async{
    UnfinishedUsageModel ufDto = UnfinishedUsageModel(id: unfinishedUsageDto?.id!=null? unfinishedUsageDto!.id : 0,
        usageId: templateUsageDto!.id,
        actualScore: templateUsageDto!.actualScore,
        actualLayerId: count<dtos.length && count>=0 && dtos[count].id!=null?dtos[count].id!:null);
    SaveUnfinishedPainter(
      everyLineHasOptionListOfAll: everyLineHasOptionListOfAll,
      layersColors: layersColors,
        resizeFactor: resizeFactor,
      currentColor: currentColor,
      optionList: optionList,
      lines: lines,
      options: options,
      colors: colors,
      unfinishedUsageDto: ufDto,
      templateName: valueT,
        backgroundImage: unfinishedUsageDto?.actualLayerAtch!=null ? unfinishedUsageDto!.actualLayerAtch: null,
        layerNumber: unfinishedLayerNumber!=-1?unfinishedLayerNumber:null,
        isSameLayer: unfinishedLayerNumber==count || (unfinishedLayerNumber==-1 && count==dtos.length)? true: false,
      oldFinImage: unfinishedUsageDto?.finishedLayerAtch!=null ? unfinishedUsageDto!.finishedLayerAtch: null,
      accuracy: average,
    ).save(canvasWidth, canvasHeight, MediaQuery.of(context).devicePixelRatio);
  }

  Future<void> templateUsage() async{
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse(
        "http://localhost:8080/api/users/usage/checkExist/${userDto!.id}/${widget.templateId}");
    final response = await http
        .get(apiUrl, headers: headers);
    if(response.statusCode==200){
      if(response.body==""){
        final Map<String, dynamic> jsonFields = {
          'playerId': userDto!.id,
          'parentId': widget.templateId,
        };
        String? jsessionId1 = SessionManager().getJsessionId();
        Map<String, String>? headers1 = {'Content-Type': 'application/json'};
        if(jsessionId1!=null){
          headers1.putIfAbsent('Authorization', () => jsessionId1);
        }
        var url = Uri.parse(
            "http://localhost:8080/api/users/usage/new");
        final createResponse = await http.post(
          url,
          headers: headers1,
          body: jsonEncode(jsonFields),
        );
        if (createResponse.statusCode == 200) {
          if(createResponse.headers['authorization']!=null) {
            SessionManager().setJsessionId('Bearer ${createResponse.headers['authorization']}');
          }
          templateUsage();
        }else{
          logger.i('create template usage caused error: ${createResponse.body}');
        }
      }
      else{
        var item = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }
          templateUsageDto = TemplateUsageModel.fromJson(item);

        if(templateUsageDto?.unfinishedUsageId!=null){
          String? jsessionId1 = SessionManager().getJsessionId();
          Map<String, String>? headers1 = {'Content-Type': 'application/json'};
          if(jsessionId1!=null){
            headers1.putIfAbsent('Authorization', () => jsessionId1);
          }
          var apiUrl1 = Uri.parse(
              "http://localhost:8080/api/users/usage/unfinished/${templateUsageDto?.unfinishedUsageId}");
          final getUnfUsage = await http
              .get(apiUrl1, headers: headers1);
          if(getUnfUsage.statusCode==200){
            final unf = json.decode(getUnfUsage.body);
            if(getUnfUsage.headers['authorization']!=null) {
              SessionManager().setJsessionId('Bearer ${getUnfUsage.headers['authorization']}');
            }
            ui.Image? actualLayerAtch;
            if(unf['actualLayerAtch']!="") {
              final String actualLayerString = unf['actualLayerAtch'];
              var actualLayerBytes = base64Decode(actualLayerString);
              ui.Codec codec = await ui.instantiateImageCodec(
                  actualLayerBytes!);
              ui.FrameInfo frame = await codec.getNextFrame();
              actualLayerAtch = frame.image;
            }
            ui.Image? finishedLayerAtch;
            if(unf['finishedLayerAtch']!="") {
              final String finishedLayerString = unf['finishedLayerAtch'];
              var finishedLayerBytes = base64Decode(finishedLayerString);
              ui.Codec codec1 = await ui.instantiateImageCodec(
                  finishedLayerBytes!);
              ui.FrameInfo frame1 = await codec1.getNextFrame();
              finishedLayerAtch = frame1.image;
            }

            unfinishedUsageDto = UnfinishedUsageModel(id: unf['id'],
                usageId: unf['usageId'],
                actualScore: unf['actualScore']);
            if(actualLayerAtch!=null){
              unfinishedUsageDto!.actualLayerAtch = actualLayerAtch as ui.Image?;
            }
            if(finishedLayerAtch!=null){
              unfinishedUsageDto!.finishedLayerAtch = finishedLayerAtch as ui.Image?;
            }
            if(unf['actualLayerId']!=""){
              unfinishedUsageDto!.actualLayerId = unf['actualLayerId'];
            }
          }
        }

        String? jsessionId2 = SessionManager().getJsessionId();
        Map<String, String>? headers2 = {'Content-Type': 'application/json'};
        if(jsessionId2!=null){
          headers2.putIfAbsent('Authorization', () => jsessionId2);
        }
        final Map<String, dynamic> jsonFields = {
          'id': templateUsageDto!.id,
          'actualState': templateUsageDto!.actualScore,
        };
        var url = Uri.parse(
            "http://localhost:8080/api/users/usage/actualize");
        final actualizeResponse = await http.put(
          url,
          headers: headers2,
          body: jsonEncode(jsonFields),
        );

        if (actualizeResponse.statusCode == 200) {
          if(actualizeResponse.headers['authorization']!=null) {
            SessionManager().setJsessionId('Bearer ${actualizeResponse.headers['authorization']}');
          }
        }else{
          logger.i('actualize template usage caused error: ${actualizeResponse.body}');
        }
      }
    }

  }

  Future<List<ui.Image>> get _getImage async {
    if (imgs2.isEmpty) {
      String? jsessionId = SessionManager().getJsessionId();
      Map<String, String>? headers = {'Content-Type': 'application/json'};
      if(jsessionId!=null){
        headers.putIfAbsent('Authorization', () => jsessionId);
      }
      var apiUrl = Uri.parse(
          "http://localhost:8080/api/templates/${valueT}/getallatts");
      final response = await http.get(apiUrl, headers: headers);
      if (response.statusCode == 200) {
        var responseData = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }
        var ids = responseData['ids'] as List;

        if (ids != null) {
          for (int i = 0; i < ids.length; i++) {
            var response1 = await http.get(
                Uri.parse("http://localhost:8080/api/templates/get/${ids[i]}"));
            ui.Codec codec =
                await ui.instantiateImageCodec(response1.bodyBytes);
            ui.FrameInfo frame = await codec.getNextFrame();
            if (imgs2.isEmpty) {
              res = response1.bodyBytes;
            }
            imgs2.add(frame.image);
          }
          img = imgs2[0];
        }
      } else {
        logger.i('get layers attachments caused error: ${response.body}');
      }
      String? jsessionId1 = SessionManager().getJsessionId();
      Map<String, String>? headers1 = {'Content-Type': 'application/json'};
      if(jsessionId1!=null){
        headers1.putIfAbsent('Authorization', () => jsessionId1);
      }
      var apiUrl1 = Uri.parse(
          "http://localhost:8080/api/templates/${valueT}/getlayersinfo");
      final response1 = await http.get(apiUrl1, headers: headers1);
      try {
        final item = json.decode(response1.body);
        if(response1.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response1.headers['authorization']}');
        }
        setState(() {
          maxScore = item['maxScore'];
        });
        var list = item['layers']
            .map((data) => LayerModel.fromJson(data))
            .toList();
        var tmp = -1;
        for(var item in list){
          if(unfinishedUsageDto!=null && unfinishedUsageDto!.actualLayerId==item.id){
            tmp = item.number;
          }
          dtos.add(LayerModel(templateId: item.templateId,
              id:item.id, attachmentId: item.attachmentId, strokeSize: item.strokeSize, color: item.color, showColor: item.showColor, number: item.number));
        }
        if(unfinishedUsageDto!=null && unfinishedUsageDto!.finishedLayerAtch!=null && tmp==-1){
          tmp =dtos.length;
        }
        if(tmp!=-1 && tmp<dtos.length){
          currentColor = Color(int.parse(dtos[tmp].color.replaceAll('#', '0x')));
          pickerColor = currentColor;
          options.size = dtos[tmp].strokeSize.toDouble();
          widget.onSizeChanged(StrokeOptions(size: options.size));
          showColor = Color(int.parse(dtos[tmp].showColor.replaceAll('#', '0x')));
          img = imgs2[tmp];
          count = tmp;
          unfinishedLayerNumber = tmp;
          selectedIndex = tmp+1;
          widget.onIndexChanged(selectedIndex!);
        }else if(tmp==dtos.length){
          currentColor = Colors.black;
          pickerColor = currentColor;
          count = tmp;
          widget.endReached();
          unfinishedLayerNumber = tmp;
          selectedIndex = tmp+1;
          img = null;
          widget.onIndexChanged(selectedIndex!);
        } else {
          currentColor = Color(int.parse(dtos[0].color.replaceAll('#', '0x')));
          pickerColor = currentColor;
          options.size = dtos[0].strokeSize.toDouble();
          widget.onSizeChanged(StrokeOptions(size: options.size));
          showColor = Color(int.parse(dtos[0].showColor.replaceAll('#', '0x')));
        }
      } catch (e) {
        logger.i('parse json caused error: $e');
      }
      widget.getLayersCount(imgs2.length);
    }
    return imgs2;
  }

  Future<void> getPixelsPosition(List<ui.Image> images) async {
    for(ui.Image image in images){
      ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.rawRgba);
      Uint8List? pixels = byteData?.buffer.asUint8List();

      List<Offset> pixelPositions = [];

      for (int y = 0; y < image.height; y++) {
        for (int x = 0; x < image.width; x++) {
          int index = (y * image.width + x) * 4;

          int alpha = pixels![index + 3];

          if (alpha == 255) {
            pixelPositions.add(Offset(x.toDouble(), y.toDouble()));
          }
        }
      }
      setState(() {
        allPixels.putIfAbsent(images.indexOf(image), () => pixelPositions);
      });
    }
  }



  @override
  void onPointerUp(PointerUpEvent details) {
    if (backActionMap.isNotEmpty) {
      backActionMap.clear();
    }
    lines = List.from(lines)..add(line!);
    var gifts = StrokePair(line!, options);
    everyLineHasOption = List.from(everyLineHasOption)..add(gifts);

    optionList = List.from(optionList)..add(options);
    if(count>=dtos.length) {
      colors.add(pickerColor);
    }
    linesStreamController.add(lines);
  }

  @override
  Widget buildCurrentPath(BuildContext context) {
    var color = pickerColor;
    if(colors.isNotEmpty){
      color = currentColor;
    }
    return ClipRect(
      child: Transform.scale(
        scale: zoomLevel.toDouble(),
        alignment: Alignment(valueX, valueY),
        child: Listener(
          onPointerDown: onPointerDown,
          onPointerMove: onPointerMove,
          onPointerUp: onPointerUp,
          child: RepaintBoundary(
            child: Container(
                color: Colors.transparent,
                width: canvasWidth,
                height: canvasHeight,
                child: StreamBuilder<Stroke>(
                    stream: currentLineStreamController.stream,
                    builder: (context, snapshot) {
                      return CustomPaint(
                        painter: SingleLinePainter(
                          currentColor: color,
                          line: line == null ? null : line!,
                          options: options,
                        ),
                      );
                    })),
          ),
        ),
      ),
    );
  }

  @override
  Widget buildAllPaths(BuildContext context) {
    return ClipRect(
      child: Transform.scale(
        scale: zoomLevel.toDouble(),
        alignment: Alignment(valueX, valueY),
        child: RepaintBoundary(
          child: SizedBox(
            width: canvasWidth,
            height: canvasHeight,
            child: StreamBuilder<List<Stroke>>(
              stream: linesStreamController.stream,
              builder: (context, snapshot) {
                return CustomPaint(
                  painter: CurrentLayerPainter(
                    currentColor: Color.fromARGB(255, pickerColor.red,
                        pickerColor.green, pickerColor.blue),
                    optionList: optionList,
                    lines: lines,
                    options: options,
                    colors: colors,
                    backgroundImage: unfinishedUsageDto?.actualLayerAtch!=null && count==unfinishedLayerNumber? unfinishedUsageDto!.actualLayerAtch: null,
                    resizeFactor: resizeFactor!=1? resizeFactor:null,
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget buildAllLayers(BuildContext context) {
    return ClipRect(
      child: Transform.scale(
        scale: zoomLevel.toDouble(),
        alignment: Alignment(valueX, valueY),
        child: RepaintBoundary(
          child: SizedBox(
            width: canvasWidth,
            height: canvasHeight,
            child: StreamBuilder<List<Stroke>>(
              stream: linesStreamController.stream,
              builder: (context, snapshot) {
                return CustomPaint(
                  painter: LayersPainter(
                    layersColors: layersColors,
                    everyLineHasOptionListOfAll: everyLineHasOptionListOfAll,
                      backgroundImage: unfinishedUsageDto?.actualLayerAtch!=null && count>unfinishedLayerNumber? unfinishedUsageDto!.actualLayerAtch: null,
                      resizeFactor: resizeFactor!=1? resizeFactor:null,
                      layerNumber: unfinishedLayerNumber!=-1?unfinishedLayerNumber:null
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget buildFinishedBackground(BuildContext context) {
    if(unfinishedUsageDto!=null && unfinishedUsageDto!.finishedLayerAtch!=null) {
      return ClipRect(
          child: Transform.scale(
              scale: zoomLevel.toDouble(),
              alignment: Alignment(valueX, valueY),
              child: RepaintBoundary(
                  child: SizedBox(
                      width: canvasWidth,
                      height: canvasHeight,
                      child: CustomPaint(
                              painter: ImageLoader(
                                  img: unfinishedUsageDto!.finishedLayerAtch,
                                  resizeFactor: resizeFactor
                              ))
                  ))));
    }
    return Container();
  }

  Widget buildActualBackground(BuildContext context) {
    if(unfinishedUsageDto!=null && unfinishedUsageDto!.actualLayerAtch!=null) {
      return ClipRect(
          child: Transform.scale(
              scale: zoomLevel.toDouble(),
              alignment: Alignment(valueX, valueY),
              child: RepaintBoundary(
                  child: SizedBox(
                      width: canvasWidth,
                      height: canvasHeight,
                      child: CustomPaint(
                          painter: ImageLoader(
                              img: unfinishedUsageDto!.actualLayerAtch,
                              resizeFactor: resizeFactor
                          ))
                  ))));
    }
    return Container();
  }

  Widget buildBackground(BuildContext context) {
    if(templateUsageDto!=null && templateUsageDto!.unfinishedUsageId!=null && unfinishedUsageDto==null){
      return const CircularProgressIndicator();
    }
    if(imgs2.isNotEmpty){
      return ClipRect(
          child: Transform.scale(
              scale: zoomLevel.toDouble(),
              alignment: Alignment(valueX, valueY),
              child: RepaintBoundary(
                  child: SizedBox(
                      width: canvasWidth,
                      height: canvasHeight,
                      child: ColorFiltered(
              colorFilter: ColorFilter.mode(showColor, BlendMode.srcIn),
              child:
                      CustomPaint(
                          painter: ImageLoader(
                              img: img,
                              resizeFactor: resizeFactor
                          ))
                      )))));
    }else{
      return const CircularProgressIndicator();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.value != null) {
      valueT = widget.value!;
    }
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Listener(
          onPointerMove: (event) {
    setState(() {
    pointerPosition = event.localPosition;
    });
    },
      onPointerDown: (event) {
        setState(() {
          pointerPosition = event.localPosition;
        });
        },
      onPointerUp: (event) {
        setState(() {
          pointerPosition = event.localPosition;
        });
      },
          child: MouseRegion(
            cursor: SystemMouseCursors.none,
            onHover: (event) {
              setState(() {
                pointerPosition = event.localPosition;
              });
            },

            child: Stack(
              children: [
                RepaintBoundary(
                  key: _repaintBoundaryKey,
                  child: Stack(
                    children: [
                      buildFinishedBackground(context),
                      buildAllLayers(context),
                    ],
                  ),
                ),

                buildBackground(context),
                RepaintBoundary(
                  key: _repaintBoundaryKey2,
                  child: Stack(
                    children: [
                      buildAllPaths(context),
                      buildCurrentPath(context),
                    ],
                  ),
                ),
              ],
            ),
          ),),
          Overlay(
            initialEntries: [
              OverlayEntry(
                builder: (context) => Positioned(
                  left: pointerPosition.dx-5,
                  top: pointerPosition.dy-5,
                  child: IgnorePointer(
                    child: CustomPaint(
                      painter: PointerIndicatorPainter((nextOptions.size/2)*zoomLevel),
                      size: Size(10, 10),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
    // }
  }

  @override
  void dispose() {
    widget.addLayerAction.removeListener(_executeAddLayerAction);
    widget.finishTrigger.removeListener(_executeFinishUsage);
    widget.unfinishedTrigger.removeListener(_executeUnfinishedUsage);
    super.dispose();
  }
}

class ImageLoader extends CustomPainter {
  final ui.Image? img;
  final double resizeFactor;

  ImageLoader({required this.img, required this.resizeFactor});

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    Paint paint = Paint();
    if (img != null) {
      canvas.saveLayer(Rect.fromLTRB(0, 0, img!.width.toDouble()*resizeFactor, img!.height.toDouble()*resizeFactor), Paint());
      canvas.drawImageRect(img!, Rect.fromLTRB(0, 0, img!.width.toDouble(), img!.height.toDouble()), Rect.fromLTRB(0, 0, img!.width.toDouble()*resizeFactor, img!.height.toDouble()*resizeFactor), paint);
      canvas.restore();
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class PointerIndicatorPainter extends CustomPainter {
  double radius;

  PointerIndicatorPainter(this.radius);
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.grey
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;

    canvas.drawCircle(Offset(size.width / 2, size.height / 2), radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
