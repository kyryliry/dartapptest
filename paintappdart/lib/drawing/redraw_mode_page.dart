import 'dart:convert';

import 'package:cyclop/cyclop.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart' as fc;
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:paintappdart/drawing/coloring_page.dart';
import 'package:paintappdart/utils/stroke_options.dart';
import 'dart:ui' as ui;

import 'package:paintappdart/model/user_model.dart';

import '../utils/session_manager.dart';

class RedrawModePage extends HookWidget {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  int templateId = 0;
  final String valueText;
  double width = 0;
  double height = 0;
  final UserModel userDto;

  RedrawModePage({super.key, required this.valueText, required this.userDto});

  Future<Size> getTemplate() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if (jsessionId != null) {
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    Size size = Size.zero;
    final res = await get(
        Uri.parse("http://localhost:8080/api/templates/${valueText}"),
        headers: headers);
    var id = 0;
    if (res.statusCode == 200) {
      var responseData = json.decode(res.body);
      if(res.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${res.headers['authorization']}');
      }
      id = responseData['id'];
    }
    String? jsessionId1 = SessionManager().getJsessionId();
    Map<String, String>? headers1 = {'Content-Type': 'application/json'};
    if (jsessionId1 != null) {
      headers1.putIfAbsent('Authorization', () => jsessionId1);
    }
    final response = await get(
        Uri.parse("http://localhost:8080/api/templates/getTwSI/${id}"),
        headers: headers1);

    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      final String base64Image = item['image'];
      final id = item['id'];
      templateId = id;
      final name = item['name'];
      var imageBytes = base64Decode(base64Image);
      ui.Codec codec = await ui.instantiateImageCodec(imageBytes!);
      ui.FrameInfo frame = await codec.getNextFrame();
      var image = frame.image;
      width = image.width.toDouble();
      height = image.height.toDouble();
      size = Size(width, height);
    }
    return size;
  }

  @override
  Widget build(BuildContext context) {
    final isEndReached = useState(false);
    final selectedIndexNotifier = useState(1);
    final pointerPosition = useState(Offset.zero);
    void onIndexChanged(int newValue) {
      selectedIndexNotifier.value = newValue;
    }

    void endReached() {
      isEndReached.value = true;
    }

    useEffect(() {
      return null;
    }, [pointerPosition.value]);

    useEffect(() {
      return null;
    }, [isEndReached.value]);

    useEffect(() {
      return null;
    }, [selectedIndexNotifier.value]);
    final size = useState<Size>(Size.zero);
    useEffect(() {
      Future<void> loadTemplate() async {
        try {
          var data = await getTemplate();
          size.value = data;
        } catch (error) {
          logger.i("redraw mode page caused error: $error");
        }
      }

      loadTemplate();

      return null;
    }, []);
    double screenWidth = MediaQuery.of(context).size.width;
    double widget1Width = 200;
    double remainingWidth = screenWidth - widget1Width;
    double widgetHeight = MediaQuery.of(context).size.height - 50;
    ValueNotifier<StrokeOptions> nextOptionsNotifier =
        useState<StrokeOptions>(StrokeOptions());
    ValueNotifier<bool> eraserMode = useState(false);
    ValueNotifier<bool> strokeOptionsOpen = useState(false);
    ValueNotifier<bool> colorOptionsOpen = useState(false);
    ValueNotifier<bool> backAction = useState(false);
    ValueNotifier<bool> forwardAction = useState(false);
    ValueNotifier<Color> pickerColor = useState(Colors.black);
    ValueNotifier<double> zoomLevel = useState(1);
    ValueNotifier<double> valueX = useState(-1);
    ValueNotifier<double> valueY = useState(-1);
    ValueNotifier<bool> clearActionTrigger = useState(false);
    ValueNotifier<bool> addLayerActionTrigger = useState(false);
    ValueNotifier<bool> saveActionTrigger = useState(false);
    ValueNotifier<int> layersCount = useState(0);
    ValueNotifier<bool> unfinishedTrigger = useState(false);
    ValueNotifier<bool> finishTrigger = useState(false);
    ValueNotifier<bool> calculateTrigger = useState(false);
    void onSizeChanged(StrokeOptions newValue) {
      nextOptionsNotifier.value = newValue;
    }

    double canvasWidth = size.value.width;
    double canvasHeight = size.value.height;
    double resizeFactor = 1;
    if (width > height && width > MediaQuery.of(context).size.width - 260) {
      resizeFactor = (MediaQuery.of(context).size.width - 260) / width;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height > width &&
        height > MediaQuery.of(context).size.height - 90) {
      resizeFactor = (MediaQuery.of(context).size.height - 90) / height;
      canvasWidth = width * resizeFactor;
      canvasHeight = height * resizeFactor;
    } else if (height == width &&
        (width > MediaQuery.of(context).size.width - 260 ||
            height > MediaQuery.of(context).size.height - 90)) {
      if (MediaQuery.of(context).size.width - 260 >
          MediaQuery.of(context).size.height - 90) {
        resizeFactor = (MediaQuery.of(context).size.height - 90) / height;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      } else {
        resizeFactor = (MediaQuery.of(context).size.width - 260) / width;
        canvasWidth = width * resizeFactor;
        canvasHeight = height * resizeFactor;
      }
    }
    void getLayersCount(int newValue) {
      layersCount.value = newValue;
    }

    if (size.value == Size.zero) {
      return CircularProgressIndicator();
    } else {
      return Scaffold(
          appBar: AppBar(
            toolbarHeight: 60,
            title: const Text('Template Redraw'),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                  onPressed: () => finishTrigger.value = !finishTrigger.value,
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    backgroundColor: Colors.deepPurple.shade100,
                  ),
                  child: const Text('Finish'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                  onPressed: () =>
                      unfinishedTrigger.value = !unfinishedTrigger.value,
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    backgroundColor: Colors.deepPurple.shade100,
                  ),
                  child: const Text('Save as unfinished'),
                ),
              ),
            ],
          ),
          body: Container(
              color: Colors.grey,
              child: Stack(
                children: [
                  Positioned(
                      right: 0.0,
                      width: 200,
                      child: Container(
                          height: widgetHeight,
                          child: ListView(children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Layer: ${selectedIndexNotifier.value}/${layersCount.value}',
                                    textAlign: TextAlign.start,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      if (!isEndReached.value) {
                                        addLayerActionTrigger.value =
                                            !addLayerActionTrigger.value;
                                      }
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: CircleAvatar(
                                        backgroundColor: !isEndReached.value
                                            ? null
                                            : Colors.grey.shade400,
                                        child: const Icon(
                                          Icons.navigate_next,
                                          size: 20.0,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const Padding(
                                    padding: EdgeInsets.only(bottom: 8),
                                    child: Text(
                                      'Move to Next Layer',
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 20),
                                    child: ElevatedButton(
                                      onPressed: () => calculateTrigger.value =
                                          !calculateTrigger.value,
                                      style: ElevatedButton.styleFrom(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        backgroundColor:
                                            Colors.deepPurple.shade100,
                                      ),
                                      child: const Text('Get accuracy'),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8, bottom: 8),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        ElevatedButton(
                                          onPressed: () => strokeOptionsOpen
                                              .value = !strokeOptionsOpen.value,
                                          style: ElevatedButton.styleFrom(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            backgroundColor:
                                                strokeOptionsOpen.value
                                                    ? Colors.grey.shade300
                                                    : null,
                                          ),
                                          child: const Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Text('Stroke Options'),
                                              Icon(Icons.arrow_drop_down),
                                            ],
                                          ),
                                        ),
                                        if (strokeOptionsOpen.value)
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Divider(),
                                              const Text(
                                                'Size',
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12),
                                              ),
                                              Slider(
                                                value: nextOptionsNotifier
                                                    .value.size,
                                                min: 1,
                                                max: 50,
                                                divisions: 100,
                                                label: nextOptionsNotifier
                                                    .value.size
                                                    .round()
                                                    .toString(),
                                                onChanged: (double newValue) {
                                                  nextOptionsNotifier.value =
                                                      nextOptionsNotifier.value
                                                          .copyWithoutSize(
                                                              size: newValue);
                                                },
                                              ),
                                              const Text(
                                                'Thinning',
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12),
                                              ),
                                              Slider(
                                                value: nextOptionsNotifier
                                                    .value.thinning,
                                                min: -1,
                                                max: 1,
                                                divisions: 100,
                                                label: nextOptionsNotifier
                                                    .value.thinning
                                                    .toStringAsFixed(2),
                                                onChanged: (double newValue) {
                                                  nextOptionsNotifier.value =
                                                      nextOptionsNotifier.value
                                                          .copyWithoutThinning(
                                                              thinning:
                                                                  newValue);
                                                },
                                              ),
                                              const Text(
                                                'Smoothing',
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12),
                                              ),
                                              Slider(
                                                value: nextOptionsNotifier
                                                    .value.smoothing,
                                                min: 0,
                                                max: 1,
                                                divisions: 100,
                                                label: nextOptionsNotifier
                                                    .value.smoothing
                                                    .toStringAsFixed(2),
                                                onChanged: (double newValue) {
                                                  nextOptionsNotifier.value =
                                                      nextOptionsNotifier.value
                                                          .copyWithoutSmoothing(
                                                              smoothing:
                                                                  newValue);
                                                },
                                              ),
                                              const Text(
                                                'Taper Start',
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12),
                                              ),
                                              Slider(
                                                value: nextOptionsNotifier
                                                    .value.taperStart,
                                                min: 0,
                                                max: 100,
                                                divisions: 100,
                                                label: nextOptionsNotifier
                                                    .value.taperStart
                                                    .toStringAsFixed(2),
                                                onChanged: (double newValue) {
                                                  nextOptionsNotifier.value =
                                                      nextOptionsNotifier.value
                                                          .copyWithoutTaperStart(
                                                              taperStart:
                                                                  newValue);
                                                },
                                              ),
                                              const Text(
                                                'Taper End',
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12),
                                              ),
                                              Slider(
                                                value: nextOptionsNotifier
                                                    .value.taperEnd,
                                                min: 0,
                                                max: 100,
                                                divisions: 100,
                                                label: nextOptionsNotifier
                                                    .value.taperEnd
                                                    .toStringAsFixed(2),
                                                onChanged: (double newValue) {
                                                  nextOptionsNotifier.value =
                                                      nextOptionsNotifier.value
                                                          .copyWithoutTaperEnd(
                                                              taperEnd:
                                                                  newValue);
                                                },
                                              ),
                                            ],
                                          ),
                                      ],
                                    ),
                                  ),
                                  if (isEndReached.value)
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 8, bottom: 8),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          ElevatedButton(
                                            onPressed: () =>
                                                colorOptionsOpen.value =
                                                    !colorOptionsOpen.value,
                                            style: ElevatedButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              backgroundColor:
                                                  colorOptionsOpen.value
                                                      ? Colors.grey.shade300
                                                      : null,
                                            ),
                                            child: const Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Text('Color Settings'),
                                                Icon(Icons.arrow_drop_down),
                                              ],
                                            ),
                                          ),
                                          if (colorOptionsOpen.value)
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Divider(),
                                                Padding(
                                                  padding: EdgeInsets.all(10),
                                                  child: fc.ColorPicker(
                                                    pickerColor:
                                                        pickerColor.value,
                                                    onColorChanged: (color) =>
                                                        pickerColor.value =
                                                            color,
                                                    pickerAreaHeightPercent:
                                                        0.6,
                                                    enableAlpha: false,
                                                    displayThumbColor: true,
                                                    showLabel: true,
                                                    paletteType:
                                                        fc.PaletteType.hsv,
                                                    pickerAreaBorderRadius:
                                                        const BorderRadius.only(
                                                      topLeft:
                                                          const Radius.circular(
                                                              2.0),
                                                      topRight:
                                                          const Radius.circular(
                                                              2.0),
                                                    ),
                                                    portraitOnly: true,
                                                  ),
                                                ),
                                              ],
                                            ),
                                        ],
                                      ),
                                    ),
                                ])
                          ]))),
                  Positioned(
                    top: 0,
                    left: 30,
                    width: canvasWidth,
                    height: canvasHeight,
                    child: ColoringPage(
                      userId: userDto!.id,
                      value: valueText,
                      width: canvasWidth,
                      height: canvasHeight,
                      nextOptionsNotifier: nextOptionsNotifier,
                      eraserMode: eraserMode,
                      backActionClick: backAction,
                      forwardActionClick: forwardAction,
                      pickerColor: pickerColor,
                      zoomLevel: zoomLevel,
                      valueX: valueX,
                      valueY: valueY,
                      clearAction: clearActionTrigger,
                      onIndexChanged: onIndexChanged,
                      userDto: userDto,
                      addLayerAction: addLayerActionTrigger,
                      saveAction: saveActionTrigger,
                      endReached: endReached,
                      getLayersCount: getLayersCount,
                      resizeFactor: resizeFactor,
                      templateId: templateId,
                      finishTrigger: finishTrigger,
                      unfinishedTrigger: unfinishedTrigger,
                      calculateTrigger: calculateTrigger,
                      onSizeChanged: onSizeChanged,
                    ),
                  ),
                  Positioned(
                      top: 0,
                      left: 0,
                      width: 30,
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () => eraserMode.value = false,
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: CircleAvatar(
                                backgroundColor:
                                    !eraserMode.value ? Colors.blueGrey : null,
                                child: const Icon(
                                  Icons.brush,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => eraserMode.value = true,
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: CircleAvatar(
                                backgroundColor:
                                    eraserMode.value ? Colors.blueGrey : null,
                                child: const Icon(
                                  FontAwesomeIcons.eraser,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              backAction.value = !backAction.value;
                            },
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: const CircleAvatar(
                                child: Icon(
                                  Icons.arrow_back_rounded,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              forwardAction.value = !forwardAction.value;
                            },
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: const CircleAvatar(
                                child: Icon(
                                  Icons.arrow_forward_rounded,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          if (isEndReached.value)
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: CircleAvatar(
                                child: EyedropperButton(
                              onColor: (value) => pickerColor.value = value,
                              icon: Icons.colorize_rounded,
                              iconColor: Colors.white,
                            )),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (zoomLevel.value < 5) {
                                zoomLevel.value = zoomLevel.value + 1;
                              }
                            },
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: const CircleAvatar(
                                child: Icon(
                                  Icons.zoom_in,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (zoomLevel.value > 1) {
                                zoomLevel.value = zoomLevel.value - 1;
                              }
                            },
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: const CircleAvatar(
                                child: Icon(
                                  Icons.zoom_out,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              clearActionTrigger.value =
                                  !clearActionTrigger.value;
                            },
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: const CircleAvatar(
                                child: Icon(
                                  Icons.clear,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )),
                  if (zoomLevel.value > 1)
                    Positioned(
                      bottom: 0,
                      height: 30,
                      left: 30,
                      width: remainingWidth - 30,
                      child: Slider(
                          value: valueX.value,
                          min: -1,
                          max: 1,
                          label: valueX.value.toStringAsFixed(2),
                          onChanged: (double value1) {
                            if (zoomLevel.value > 1) {
                              valueX.value = value1;
                            }
                          }),
                    ),
                  if (zoomLevel.value > 1)
                    Positioned(
                      width: 30,
                      height: widgetHeight - 30,
                      right: 200,
                      child: RotatedBox(
                        quarterTurns: 1,
                        child: Slider(
                            value: valueY.value,
                            min: -1,
                            max: 1,
                            label: valueY.value.toStringAsFixed(2),
                            onChanged: (double value1) {
                              if (zoomLevel.value > 1) {
                                valueY.value = value1;
                              }
                            }),
                      ),
                    ),
                ],
              )));
    }
  }
}
