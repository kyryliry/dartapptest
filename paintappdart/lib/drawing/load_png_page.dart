import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:ui' as ui;
import 'package:flutter_colorpicker/flutter_colorpicker.dart' as fc;
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:tuple/tuple.dart';

import '../templatesLibrary/library_main_page.dart';
import '../model/category_model.dart';
import '../model/layer_model.dart';
import '../utils/session_manager.dart';
import '../model/user_model.dart';

class LoadPngPage extends StatefulWidget {
  final UserModel userDto;
  const LoadPngPage({Key? key, required this.userDto}) : super(key: key);

  @override
  LoadPngPageState createState() => LoadPngPageState();
}

class LoadPngPageState extends State<LoadPngPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<Tuple2<LayerModel, ui.Image>> layers = <Tuple2<LayerModel, ui.Image>>[];
  List<CategoryModel> categories = <CategoryModel>[];
  List<CategoryModel> selectedCategories = <CategoryModel>[];
  ui.Image? sampleImage;
  late UserModel userDto;
  String? templateName;
  bool categoriesListOpen = false;
  CategoryModel? selectedCategory;
  Map<int, TextEditingController> showColorControllers = {};
  Map<int, TextEditingController> brushColorControllers = {};
  List<String> pathes = <String>[];
  List<Uint8List> pathesFile = <Uint8List>[];
  bool isSended = false;
  double maxScore = 0;
  TextEditingController scoreController = TextEditingController(text: "0");
  String error = "";

  @override
  void initState(){
    super.initState();
    userDto = widget.userDto;
    getCategories();
  }

  @override
  void dispose(){
    showColorControllers.forEach((_, controller) => controller.dispose());
    brushColorControllers.forEach((_, controller) => controller.dispose());
    super.dispose();


  }

  Future<void> _showAlertDialog() async {
    String e = error; // Capture the initial error message
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // Local state variables for the dialog
        String localError = e;
        TextEditingController _nameController = TextEditingController();

        return StatefulBuilder( // Use StatefulBuilder to manage state inside the dialog
          builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              title: const Text('Enter new template name'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    if (localError.isNotEmpty) // Conditionally display error text
                      Text(localError),
                    TextField(
                      controller: _nameController,
                      onChanged: (value) {
                        templateName = value;
                      },
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    setState(() { // Update the local error message within the dialog
                      error = "";
                    });
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  onPressed: () async {
                    if (await sendGetNameRequest() == true) {
                      prepareLayersData();
                      Navigator.of(context).pop(); // Close the dialog on success
                    } else {
                      setState(() { // Update the local error message within the dialog
                        localError = "This name is not unique!";
                      });
                    }
                  },
                  child: const Text('Save'),
                ),
              ],
            );
          },
        );
      },
    );
  }

  Future<void> getCategories() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/categories");

    final cats = await http.get(apiUrl, headers: headers);
    if (cats.statusCode == 200) {
      setState(() {
        final item = json.decode(cats.body);
        if(cats.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${cats.headers['authorization']}');
        }

        var list = item['categories'] as List;
        for(var item in list){
          categories.add(CategoryModel.fromJson(item));
        }
      });
    } else {
      logger.i('get categories in load png page caused error: ${cats.body}');
    }
  }

  Future<Color> findColorInImage(ui.Image croppedImage, bool isWeb) async {
    ByteData? byteData = await croppedImage.toByteData(format: ui.ImageByteFormat.rawUnmodified);
    Uint8List? pixels = byteData?.buffer.asUint8List();

    if (pixels != null) {
      for (int i = 0; i < pixels.length; i += 4) {
        int blue = pixels[i];
        int green = pixels[i + 1];
        int red = pixels[i + 2];
        int alpha = pixels[i + 3];
        if (alpha == 255) {
          if(isWeb) {
            return Color.fromARGB(alpha, red, green, blue);
          }else{
            return Color.fromARGB(alpha, blue, green, red);
          }
        }
      }
    }
    return Colors.transparent;
  }

  Future<void> pickFiles() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: ['png'],
    );

    if (result != null) {
      List<PlatformFile> files = result.files;
      int i=0;
      int count = layers.length+1;

      for (var file in files) {

      Uint8List? fileBytes;
      bool isWeb = false;
      if(!kIsWeb && file.path !=null){
        fileBytes = await File(file.path!).readAsBytes();
        isWeb = false;
      }else if(kIsWeb || file.bytes !=null){
        fileBytes = file.bytes;
        isWeb = true;
      }

      if (fileBytes != null) {
        final ui.Codec codec = await ui.instantiateImageCodec(fileBytes);
        final ui.FrameInfo frameInfo = await codec.getNextFrame();

        Color color = await findColorInImage(frameInfo.image, isWeb);
        if(color!=Colors.transparent){
            LayerModel l = LayerModel(templateId: 0, strokeSize: 16, color: '#${color.value.toRadixString(16)}', showColor: '#${invert(color).value.toRadixString(16)}', number: count+i);
          setState(() {
            layers.add(Tuple2(l, frameInfo.image));
          });
        }else{
          logger.i('error while parsing image file: color is not detected');
        }

        i++;
      }
      }
      setState(() {
        maxScore = layers.length.toDouble();
        scoreController = TextEditingController(text: maxScore.toString());
      });
    } else {
      logger.i("Error: No files selected");
    }
  }

  Color invert(Color color) {
    if(color==Colors.black){
      return Color.fromARGB((color.opacity * 255).round(), 210, 100, 100);
    }
    final r = 255 - color.red;
    final g = 255 - color.green;
    final b = 255 - color.blue;

    return Color.fromARGB((color.opacity * 255).round(), r, g, b);
  }


  Future<void> generateSampleImage(double sizex, double sizey) async {
    PictureRecorder pR = PictureRecorder();
    Canvas wholeCanvas = Canvas(pR);
    Paint paint = Paint();
    for(var entry in layers) {
      ui.Image img = entry.item2;
      LayerModel key = entry.item1;
      paint.colorFilter = ColorFilter.mode(Color(int.parse(key.color.replaceAll('#', '0x'))), BlendMode.srcIn);
      wholeCanvas.drawImageRect(img,
          Rect.fromLTRB(0, 0, img.width.toDouble(), img.height.toDouble()),
          Rect.fromLTRB(0, 0, img.width.toDouble(), img.height.toDouble()), paint);
    }
    Picture p = pR.endRecording();

    ui.Image img = await p.toImage(
        (sizex).toInt(),
        (sizey).toInt());
    setState(() {
      sampleImage = img;
      showSampleImageDialog(context, sampleImage);
    });
  }

  Future<void> showSampleImageDialog(BuildContext context, ui.Image? image) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Sample image of your template',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            content: Container(
              width: 300,
              height: 200,
              child:
                  Container(
                    child: FittedBox(
                      alignment: Alignment.center,
                      fit: BoxFit.scaleDown,
                      child:
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                            ),
                            child: RawImage(
                              image: image!,
                              width: image!.width/10,
                              height: image!.height/10,
                            ),
                          )
                      ),),
                ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('Ok'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<bool> sendGetNameRequest() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/templates/test1");
    if (templateName != null) {
      String name = "${widget.userDto.userName}_${templateName!}";
      apiUrl = Uri.parse("http://localhost:8080/api/templates/$name");
    }
    final response = await http.get(apiUrl, headers: headers);
    if (response.statusCode == 200) {
      var responseData = json.decode(response.body);
      if(response.headers['authorization']!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
      }
      if (responseData['id'] == -1) {
        setState(() {
          error = "";
        });
        return true;
      } else {
        setState(() {
          error = "This name is not unique!";
        });
        return false;
      }
    } else {
      logger.i('get template name caused error: ${response.body}');
    }
    return false;
  }

  Future<void> prepareLayersData() async {
    String formattedName = templateName!.replaceAll(RegExp(r'\s'), '_');
    File sampleFile = File("$formattedName.png");
    await generateSampleImage(layers.first.item2.width.toDouble(), layers.first.item2.height.toDouble());
    final sampleData = await sampleImage!.toByteData(
        format: ui.ImageByteFormat.png);
    Uint8List? sampleBytes = sampleData?.buffer.asUint8List();

    for(int i=0; i<layers.length; i++){
      var entry = layers[i];
      ui.Image croppedImage = entry.item2;

      File file = File("$formattedName$i.png");

      final data = await croppedImage.toByteData(
          format: ui.ImageByteFormat.png);

      Uint8List? bytes = data?.buffer.asUint8List();
      pathes.add(file.path);
      pathesFile.add(bytes!);
    }
    sendPostRequest(sampleBytes!, sampleFile);
  }

  Future<void> sendPostRequest(Uint8List sampleBytes, File sampleFile) async {
    var tmp = "test1";
    if(templateName!=null){
      tmp = templateName!;
    }

    var formData = FormData.fromMap({
      'templateName': tmp,
      'file': MultipartFile.fromBytes(sampleBytes, filename:sampleFile.path),
      'categories': jsonEncode(selectedCategories.map((category) => category.toJson()).toList()),
      'creatorId':widget.userDto.id,
      'state':'PUBLISHED',
      'maxScore':maxScore,
    });


    if(pathes.length==layers.length) {
      for (int i = 0; i < pathes.length; i++) {
        var entry = layers[i];
        LayerModel key = entry.item1;

        formData.fields

          ..add(MapEntry('layers[$i].strokeSize', key.strokeSize.toString()))
          ..add(MapEntry('layers[$i].color', key.color))
          ..add(MapEntry('layers[$i].showColor', key.showColor))
          ..add(MapEntry('layers[$i].number', i.toString()));
        formData.files.add(MapEntry(
          'layers[$i].fileL',
          await MultipartFile.fromBytes(pathesFile[i], filename: pathes[i]), // Assuming the filename or you can use obj['filename'] if available
        ));
      }
    }

    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }

    var response = await Dio().post("http://localhost:8080/api/template/createWithLayers", data: formData, options: Options(headers: headers));
    if (response.statusCode == 200) {
      if(response.headers.value('authorization')!=null) {
        SessionManager().setJsessionId('Bearer ${response.headers.value('authorization')}');
      }
      isSended = true;
      if(isSended) {
        if(!mounted){
          return;
        }
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => LibraryMainPage(name: widget.userDto.userName,)),
              (Route<dynamic> route) => false,
        );

      }
    } else {
      logger.i('create template with layers caused error: ${response.data}');
    }
  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Template may not be empty, please add something',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> scoreNullDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Max score must contain value between 1 and 100',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return SelectionArea(child:Scaffold(
        appBar: AppBar(toolbarHeight: 60, title: const Text('New Template'),
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context, true);
            },
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.only(right: 20),
                child: GestureDetector(
                  onTap: () {
                    if(layers.isNotEmpty && maxScore>0 && maxScore<=100) {
                          _showAlertDialog();
                    } else if(maxScore<=0 || maxScore>100){
                      scoreNullDialog(context);
                    }else{
                      alertDialog(context);
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: const CircleAvatar(
                        radius: 40.0,
                        child: Icon(
                          Icons.save,
                          size: 30.0,
                          color: Colors.white,
                        )),
                  ),
                )),
          ],),
        body: Stack(
          children: [

            Padding(
              padding: const EdgeInsets.only(left: 20.0 ,right: 20.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Loaded ${layers.length} layers',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
                        const Padding(padding: EdgeInsets.only(left: 10),
                        child:
                        Text('(max 100)',
                          textAlign: TextAlign.left,),),
                        Padding(padding: const EdgeInsets.only(left:10),
                        child:
                        ElevatedButton(
                            onPressed: (){
                              if(layers.length<100) {
                                pickFiles();
                              }
                            },
                            style: ButtonStyle(
                              backgroundColor:
                              MaterialStateProperty
                                  .resolveWith<
                                  Color?>(
                                    (Set<MaterialState>
                                states) {
                                  return layers.length<100? null : Colors
                                      .grey.shade400;
                                },
                              ),
                            ),
                            child: const Text('Load png files'))),
                        if(layers.isNotEmpty)
                        Padding(padding: const EdgeInsets.only(left:10),
                            child:
                            ElevatedButton(
                                onPressed: () async {
                                  generateSampleImage(layers.first.item2.width.toDouble(), layers.first.item2.height.toDouble());
                                  },
                                child: const Text('Show sample image'))),
                      ],
                    ),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black),
                      ),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        // crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text('Layers order'),
                          Text('Image'),
                          Text('Stroke size'),
                          Text('Draw color'),
                          Text('Show color'),
                        ],
                      ),
                    ),
                    Expanded(
                        child: Container(
                            padding: EdgeInsets.only(left:20, right: 20),
                            height: 400,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black), // Border color
                            ),
                            child: ListView.builder(
                              itemCount: layers.length,
                              itemBuilder: (context, index) {
                                var entry = layers[index];
                                LayerModel key = entry.item1;
                                ui.Image value = entry.item2;
                                int d = value.width~/1000;
                                double divider = d.toDouble();
                                if(value.width<=500 || value.height<=500){
                                  divider = 0.2;
                                }else if((value.width>500 && value.width<=650) || ( value.height>500 && value.height<=650)){
                                  divider = 0.5;
                                }else if((value.width>650 && value.width<=800) || ( value.height>650 && value.height<=800)){
                                  divider = 0.7;
                                }else if((value.width>800 && value.width<=1000) || ( value.height>800 && value.height<=1000)){
                                  divider = 0.85;
                                }
                                showColorControllers[index] = showColorControllers[index] ?? TextEditingController(text: key.showColor);
                                brushColorControllers[index] = brushColorControllers[index] ?? TextEditingController(text: key.color);
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Container(
                                        decoration: BoxDecoration(
                                          borderRadius:BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Colors.black),
                                        ),
                                        width: 70,
                                        height: 40,
                                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: [
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  if(key.number<layers.length-1 && layers.length>1) {
                                                    setState(() {
                                                      final temp = layers[index];
                                                      layers[index] = layers[index+1];
                                                      layers[index+1] = temp;
                                                      layers[index].item1.number --;
                                                      layers[index+1].item1.number ++;
                                                    });
                                                  }
                                                });
                                              },
                                              child: const Icon(
                                                FontAwesomeIcons.arrowDown,
                                                size: 15.0,
                                                color: Colors.black,
                                              ),
                                            ),
                                            Text('${key.number}'),
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  if(key.number>1 && layers.length>1) {
                                                    setState(() {
                                                      final temp = layers[index];
                                                      layers[index] = layers[index-1];
                                                      layers[index-1] = temp;
                                                      layers[index].item1.number ++;
                                                      layers[index-1].item1.number --;
                                                    });
                                                  }
                                                });
                                              },
                                              child: const Icon(
                                                FontAwesomeIcons.arrowUp,
                                                size: 15.0,
                                                color: Colors.black,
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  layers.removeAt(index);
                                                  for(int i=index; i<layers.length; i++){
                                                    layers[i].item1.number = i+1;
                                                  }
                                                });
                                              },
                                              child: const Icon(
                                                Icons.delete,
                                                size: 15.0,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ],
                                        )
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.black), // Border color
                                      ),
                                      width: value.width/(5*divider),
                                      height: value.height/(5*divider),
                                      child: RawImage(
                                        color: Color(int.parse(key.color.replaceAll('#', '0x'))),
                                        image:value,
                                        width: value.width/(5*divider),
                                        height: value.height/(5*divider),
                                      ),
                                    ),
                                    Container(
                                        decoration: BoxDecoration(
                                          borderRadius:BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Colors.black),
                                        ),
                                        width: 70,
                                        height: 40,
                                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: [
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  if(key.strokeSize>1) {
                                                    key.strokeSize--;
                                                  }
                                                });
                                              },
                                              child: const Icon(
                                                FontAwesomeIcons.minus,
                                                size: 15.0,
                                                color: Colors.black,
                                              ),
                                            ),
                                            // GestureDetector(
                                            //   child: Container(
                                            //     width: 30,
                                            //   height: 40,
                                            //   child: TextFormField(
                                            //     initialValue: '${key.strokeSize}',
                                            //     onChanged: (String value){
                                            //       if(int.tryParse(value)!=null){
                                            //         setState(() {
                                            //           int size = int.parse(value);
                                            //           if(size>1 && size<50) {
                                            //             key.strokeSize = size;
                                            //           }
                                            //         });
                                            //       }
                                            //     },
                                            //   ),),
                                            // ),
                                            Text('${key.strokeSize}'),
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  if(key.strokeSize<50) {
                                                    key.strokeSize++;
                                                  }
                                                });
                                              },
                                              child: const Icon(
                                                FontAwesomeIcons.plus,
                                                size: 15.0,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ],
                                        )
                                    ),
                                    GestureDetector(
                                        onTap: (){
                                          String tmpColor = key.color;
                                          showDialog<void>(
                                            context: context,
                                            barrierDismissible: false, // user must tap button!
                                            builder: (BuildContext context) {

                                              return AlertDialog(
                                                titlePadding: const EdgeInsets.all(0),
                                                contentPadding: const EdgeInsets.all(0),
                                                content: SingleChildScrollView(
                                                  child: fc.ColorPicker(
                                                    pickerColor: Color(int.parse(key.color.replaceAll('#', '0x'))),
                                                    onColorChanged: (color) {
                                                      setState(() {
                                                        key.color = '#${color.value.toRadixString(16)}';
                                                      });
                                                    },
                                                    colorPickerWidth: 300,
                                                    pickerAreaHeightPercent: 0.7,
                                                    enableAlpha: false,
                                                    displayThumbColor: true,
                                                    showLabel: true,
                                                    paletteType: fc.PaletteType.hsv,
                                                    pickerAreaBorderRadius: const BorderRadius.only(
                                                      topLeft: const Radius.circular(2.0),
                                                      topRight: const Radius.circular(2.0),
                                                    ),
                                                    portraitOnly: true,
                                                    hexInputBar: true,
                                                    hexInputController: brushColorControllers[index],
                                                  ),
                                                ),
                                                actions: <Widget>[
                                                  TextButton(
                                                    child: const Text('No'),
                                                    onPressed: () {
                                                      setState(() {
                                                        key.color = tmpColor;
                                                      });
                                                      Navigator.of(context).pop();
                                                    },
                                                  ),
                                                  TextButton(
                                                    onPressed: () async {
                                                      // setState(() {
                                                      //   layersData.putIfAbsent(key.number, () => key);
                                                      // });
                                                      Navigator.of(context).pop();
                                                    },
                                                    child: const Text('Yes'),
                                                  ),
                                                ],

                                              );},);},
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black), // Border color
                                            color: Color(int.parse(key.color.replaceAll('#', '0x'))),
                                          ),
                                          width: 30,
                                          height: 30,
                                          padding: const EdgeInsets.symmetric(vertical: 8.0),

                                        )
                                    ),
                                    GestureDetector(
                                        onTap: (){
                                          String tmpColor = key.showColor;
                                          showDialog<void>(
                                            context: context,
                                            barrierDismissible: false, // user must tap button!
                                            builder: (BuildContext context) {

                                              return AlertDialog(
                                                titlePadding: const EdgeInsets.all(0),
                                                contentPadding: const EdgeInsets.all(0),
                                                content: SingleChildScrollView(
                                                  child: fc.ColorPicker(
                                                    pickerColor: Color(int.parse(key.showColor.replaceAll('#', '0x'))),
                                                    onColorChanged: (color) {
                                                      setState(() {
                                                        key.showColor = '#${color.value.toRadixString(16)}';
                                                      });
                                                    },
                                                    colorPickerWidth: 300,
                                                    pickerAreaHeightPercent: 0.7,
                                                    enableAlpha: false,
                                                    displayThumbColor: true,
                                                    showLabel: true,
                                                    paletteType: fc.PaletteType.hsv,
                                                    pickerAreaBorderRadius: const BorderRadius.only(
                                                      topLeft: const Radius.circular(2.0),
                                                      topRight: const Radius.circular(2.0),
                                                    ),
                                                    portraitOnly: true,
                                                    hexInputBar: true,
                                                    hexInputController: showColorControllers[index],
                                                  ),
                                                ),
                                                actions: <Widget>[
                                                  TextButton(
                                                    child: const Text('No'),
                                                    onPressed: () {
                                                      setState(() {
                                                        key.showColor = tmpColor;
                                                      });
                                                      Navigator.of(context).pop();
                                                    },
                                                  ),
                                                  TextButton(
                                                    onPressed: () async {
                                                      // setState(() {
                                                      //   layersData.putIfAbsent(key.number, () => key);
                                                      // });
                                                      Navigator.of(context).pop();
                                                    },
                                                    child: const Text('Yes'),
                                                  ),
                                                ],

                                              );},);},
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black), // Border color
                                            color: Color(int.parse(key.showColor.replaceAll('#', '0x'))),
                                          ),
                                          width: 30,
                                          height: 30,
                                          padding: const EdgeInsets.symmetric(vertical: 8.0),

                                        )
                                    )
                                  ],
                                );
                              },
                            )
                        )),
                    Row(
                      children: [
                        const Text('Current max possible score: '),
                        Container(
                          width: 60,
                          child:
                          TextField(
                            onChanged: (value) {
                              setState(() {
                                maxScore = double.tryParse(value) ?? 0;
                              });
                            },
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly, // Restricts input to digits only
                            ],
                            controller: scoreController,
                            decoration:
                            const InputDecoration(hintText: "Enter reason of your report"),
                          ),),
                      ],
                    ),
                    Container(
                      height: 150,
                      child:
                      Column(
                        children: [
                          Row(
                            children: [
                              const Text('Categories: '),
                              Expanded(
                                child: Container(
                                  height: 50,
                                  child:
                                  ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: selectedCategories.length,
                                      itemBuilder: (context, index) {
                                        return Row(
                                          children: [
                                            Text(selectedCategories[index].name),
                                            IconButton(
                                                onPressed: (){
                                                  setState(() {
                                                    CategoryModel tmp = selectedCategories[index];
                                                    selectedCategories.remove(tmp);
                                                    categories.add(tmp);
                                                    categories.sort((a, b) => a.name.compareTo(b.name));
                                                  });

                                                },
                                                icon: const Icon(Icons.highlight_remove_outlined)
                                            ),
                                          ],
                                        );
                                      }),),)
                            ],
                          ),
                          Row(
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      categoriesListOpen = !categoriesListOpen;
                                    });
                                  },
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    backgroundColor: categoriesListOpen ? Colors.grey.shade300 : null,
                                  ),
                                  child: PopupMenuButton<CategoryModel>(
                                    onSelected: (CategoryModel category) {
                                      // Handle the category selection

                                      setState(() {
                                        selectedCategory = category;
                                      });
                                    },
                                    itemBuilder: (BuildContext context) {
                                      return <PopupMenuEntry<CategoryModel>>[
                                        PopupMenuItem<CategoryModel>(
                                          child: ConstrainedBox(
                                            constraints: BoxConstraints(maxHeight: 200.0), // For example, 5 items * 40.0px each
                                            child: SingleChildScrollView(
                                              child: Column(
                                                children: categories.map((CategoryModel category) {
                                                  return PopupMenuItem<CategoryModel>(
                                                    value: category,
                                                    child: Text(category.name),
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ];
                                    },
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(selectedCategory?.name ?? 'select category'),
                                        Icon(Icons.arrow_drop_down),
                                      ],
                                    ),
                                  ),
                                ),

                                ElevatedButton(
                                  onPressed: ()
                                  {
                                    if(selectedCategory!=null){
                                      setState(() {
                                        selectedCategories.add(selectedCategory!);
                                        categories.remove(selectedCategory!);
                                        selectedCategory = null;
                                      });

                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    backgroundColor: Colors.deepPurple.shade100,
                                    foregroundColor: Colors.white,
                                  ),
                                  child:
                                  const Text('add category'),),
                              ]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),),
          ],
        )
    ));
  }
}