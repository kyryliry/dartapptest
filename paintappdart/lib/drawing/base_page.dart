import 'dart:async';
import 'dart:ui';

import 'package:paintappdart/painters/layers_painter.dart';
import 'package:flutter/material.dart';
import 'package:paintappdart/painters/single_line_painter.dart';
import 'package:paintappdart/utils/stroke_pair.dart';
import 'package:perfect_freehand/perfect_freehand.dart';

import '../../painters/current_layer_painter.dart';
import '../../utils/stroke.dart';
import '../../utils/stroke_options.dart';
import '../model/user_model.dart';

class BasePage extends StatefulWidget {
  final int userId;
  final double width;
  final double height;
  final ValueNotifier<StrokeOptions> nextOptionsNotifier;
  final ValueNotifier<bool> eraserMode;
  final ValueNotifier<bool> backActionClick;
  final ValueNotifier<bool> forwardActionClick;
  final ValueNotifier<Color> pickerColor;
  final ValueNotifier<double> zoomLevel;
  final ValueNotifier<double> valueX;
  final ValueNotifier<double> valueY;
  final ValueNotifier<bool> clearAction;
  final Function(int) onIndexChanged;
  final UserModel userDto;


  const BasePage(
      {Key? key,
        required this.userId,
        required this.width,
        required this.height,
        required this.nextOptionsNotifier,
        required this.eraserMode,
        required this.backActionClick,
        required this.forwardActionClick,
        required this.pickerColor,
        required this.zoomLevel,
        required this.valueX,
        required this.valueY,
        required this.clearAction,
        required this.onIndexChanged,
        required this.userDto})
      : super(key: key);

  @override
  BasePageState createState() => BasePageState();
}

class BasePageState<T extends BasePage> extends State<T> {
  List<Stroke> lines = <Stroke>[];
  List<StrokeOptions> optionList = <StrokeOptions>[];
  List<Color> layersColors = <Color>[];
  Stroke? line;
  String? valueText;

  StrokeOptions options = StrokeOptions();
  StrokeOptions nextOptions = StrokeOptions();

  Color pickerColor = Color.fromARGB(255, 0, 0, 0);
  Color currentColor = Color(0x000000);
  HSVColor currentHsvColor = HSVColor.fromColor(Colors.black);

  StreamController<Stroke> currentLineStreamController =
  StreamController<Stroke>.broadcast();
  StreamController<List<Stroke>> linesStreamController =
  StreamController<List<Stroke>>.broadcast();

  List<List<StrokePair>> everyLineHasOptionListOfAll = <List<StrokePair>>[];
  List<StrokePair> everyLineHasOption = <StrokePair>[];
  List<StrokePair> backActionMap = <StrokePair>[];
  bool eraserMode = false;
  bool clearBackupMode = false;

  Alignment position = Alignment.topLeft;
  double valueX = -1;
  double valueY = -1;
  bool isZoomMode = false;
  double zoomLevel = 1;
  int? selectedIndex = 1;
  int newValue = 1;

  List<Color> colors = <Color>[];

  double canvasWidth = 0;
  double canvasHeight = 0;

  UserModel? userDto;

  @override
  void initState() {
    super.initState();
    everyLineHasOptionListOfAll.add([]);
    layersColors.add(Colors.black);
    nextOptions = widget.nextOptionsNotifier.value;
    eraserMode = widget.eraserMode.value;
    pickerColor = widget.pickerColor.value;
    zoomLevel = widget.zoomLevel.value;
    valueX = widget.valueX.value;
    valueY = widget.valueY.value;
    widget.onIndexChanged(selectedIndex!);
    widget.nextOptionsNotifier.addListener(_updateOptions);
    widget.eraserMode.addListener(_updateMode);
    widget.backActionClick.addListener(_updateBack);
    widget.forwardActionClick.addListener(_updateForward);
    widget.pickerColor.addListener(_updateColor);
    widget.zoomLevel.addListener(_updateZoom);
    widget.valueX.addListener(_updateValueX);
    widget.valueY.addListener(_updateValueY);
    widget.clearAction.addListener(_executeClearAction);
    canvasWidth = widget.width;
    canvasHeight = widget.height;
    userDto = widget.userDto;
  }

  void _executeClearAction() {
    clear();
  }

  void _updateValueX() {
    setState(() {
      valueX = widget.valueX.value;
    });
  }

  void _updateValueY() {
    setState(() {
      valueY = widget.valueY.value;
    });
  }

  void _updateColor() {
    setState(() {
      changeColor(widget.pickerColor.value);
    });
  }

  void _updateZoom() {
    setState(() {
      zoomLevel = widget.zoomLevel.value;
      if (zoomLevel == 1) {
        isZoomMode = false;
      } else {
        isZoomMode = true;
      }
    });
  }

  void _updateBack() {
    setState(() {
      backAction();
    });
  }

  void _updateForward() {
    setState(() {
      forwardAction();
    });
  }

  void _updateOptions() {
    setState(() {
      nextOptions = widget.nextOptionsNotifier.value;
    });
  }

  void _updateMode() {
    setState(() {
      eraserMode = widget.eraserMode.value;
    });
  }

  Future<void> clear() async {
    setState(() {
      if (everyLineHasOption.isNotEmpty) {
        backActionMap.clear();
        backActionMap = List.from(backActionMap)..addAll(everyLineHasOption);
        clearBackupMode = true;
      }
      lines = [];
      optionList = [];
      line = null;
      everyLineHasOption.clear();
    });
  }

  Future<void> updateSizeOption(double size) async {
    setState(() {
      nextOptions.size = size;
    });
  }

  Future<void> changeColor(Color color) async {
    setState(() {
      pickerColor = color;
    });
  }

  void onPointerDown(PointerDownEvent details) {
    setState(() {
      currentColor = pickerColor;
    });

    options = StrokeOptions(
      size: nextOptions.size,
      thinning: nextOptions.thinning,
      streamline: nextOptions.streamline,
      smoothing: nextOptions.smoothing,
      taperStart: nextOptions.taperStart,
      taperEnd: nextOptions.taperEnd,
      simulatePressure: details.kind != PointerDeviceKind.stylus,
    );
    var offset = Offset(0, 0);
    var slope = (zoomLevel.toDouble() - 1.0) / (1.0 + 1.0);
    var value1 = 1.0 + slope * (valueX + 1.0);
    var value2 = 1.0 + slope * (valueY + 1.0);
    final box = context.findRenderObject() as RenderBox;
    final scaledWidth = box.size.width / zoomLevel;
    final scaledHeight = box.size.height / zoomLevel;

    final scaledOffset = Offset(
      details.localPosition.dx,
      details.localPosition.dy,
    );

    final clampedOffset = Offset(
      scaledOffset.dx.clamp(scaledWidth * (value1 - 1), scaledWidth * (value1)),
      scaledOffset.dy
          .clamp(scaledHeight * (value2 - 1), scaledHeight * (value2)),
    );
    offset = clampedOffset;
    late final Point point;
    if (details.kind == PointerDeviceKind.stylus) {
      point = Point(
        offset.dx,
        offset.dy,
        (details.pressure - details.pressureMin) /
            (details.pressureMax - details.pressureMin),
      );
    } else {
      point = Point(offset.dx, offset.dy);
    }
    final points = [point];
    line = Stroke(points, eraserMode);
    currentLineStreamController.add(line!);
  }

  void onPointerMove(PointerMoveEvent details) {
    var offset = Offset(0, 0);
    var slope = (zoomLevel.toDouble() - 1.0) / (1.0 + 1.0);
    var value1 = 1.0 + slope * (valueX + 1.0);
    var value2 = 1.0 + slope * (valueY + 1.0);
    final box = context.findRenderObject() as RenderBox;
    final scaledWidth = box.size.width / zoomLevel;
    final scaledHeight = box.size.height / zoomLevel;

    final scaledOffset = Offset(
      details.localPosition.dx,
      details.localPosition.dy,
    );

    final clampedOffset = Offset(
      scaledOffset.dx.clamp(scaledWidth * (value1 - 1), scaledWidth * (value1)),
      scaledOffset.dy
          .clamp(scaledHeight * (value2 - 1), scaledHeight * (value2)),
    );
    offset = clampedOffset;
    late final Point point;
    if (offset.dx <= canvasWidth && offset.dy <= canvasHeight) {
      point = setPoint(offset.dx, offset.dy, details);
    } else if (offset.dx > canvasWidth && offset.dy <= canvasHeight) {
      point = setPoint(canvasWidth, offset.dy, details);
    } else if (offset.dx <= canvasWidth && offset.dy > canvasHeight) {
      point = setPoint(offset.dx, canvasHeight, details);
    } else if (offset.dx > canvasWidth && offset.dy > canvasHeight) {
      point = setPoint(canvasWidth, canvasHeight, details);
    }
    final points = [...line!.points, point];
    line = Stroke(points, eraserMode);
    currentLineStreamController.add(line!);
  }

  Point setPoint(double x, double y, PointerMoveEvent details) {
    if (details.kind == PointerDeviceKind.stylus) {
      return Point(
        x,
        y,
        (details.pressure - details.pressureMin) /
            (details.pressureMax - details.pressureMin),
      );
    } else {
      return Point(x, y);
    }
  }

  void onPointerUp(PointerUpEvent details) {
    if (backActionMap.isNotEmpty) {
      backActionMap.clear();
    }
    lines = List.from(lines)..add(line!);
    var gifts = StrokePair(line!, options);
    everyLineHasOption = List.from(everyLineHasOption)..add(gifts);

    optionList = List.from(optionList)..add(options);
    linesStreamController.add(lines);
  }

  Future<void> backAction() async {
    if (everyLineHasOption.isNotEmpty) {
      setState(() {
        backActionMap = List.from(backActionMap)..add(everyLineHasOption.last);
        everyLineHasOption.removeLast();
        lines.removeLast();
        optionList.removeLast();
        line = null;
      });
    } else if (everyLineHasOption.isEmpty &&
        clearBackupMode &&
        backActionMap.isNotEmpty) {
      setState(() {
        everyLineHasOption = List.of(everyLineHasOption)..addAll(backActionMap);
        for (var item in backActionMap) {
          lines.add(item.stroke);
          optionList.add(item.options);
        }
        line = lines.last;
        backActionMap.clear();
        clearBackupMode = false;
      });
    }
  }

  Future<void> forwardAction() async {
    if (backActionMap.isNotEmpty && !clearBackupMode) {
      setState(() {
        everyLineHasOption.add(backActionMap.last);
        lines.add(backActionMap.last.stroke);
        optionList.add(backActionMap.last.options);
        backActionMap.removeLast();
        line = lines.last;
      });
    }
  }

  Widget buildCurrentPath(BuildContext context) {
    return ClipRect(
      child: Transform.scale(
        scale: zoomLevel.toDouble(),
        alignment: Alignment(valueX, valueY),
        child: Listener(
          onPointerDown: onPointerDown,
          onPointerMove: onPointerMove,
          onPointerUp: onPointerUp,
          child: RepaintBoundary(
            child: Container(
                color: Colors.transparent,
                width: canvasWidth,
                height: canvasHeight,
                child: StreamBuilder<Stroke>(
                    stream: currentLineStreamController.stream,
                    builder: (context, snapshot) {
                      return CustomPaint(
                        painter: SingleLinePainter(
                          currentColor: pickerColor,
                          line: line == null ? null : line!,
                          options: options,
                        ),
                      );
                    })),
          ),
        ),
      ),
    );
  }

  Widget buildAllPaths(BuildContext context) {
    return ClipRect(
      child: Transform.scale(
        scale: zoomLevel.toDouble(),
        alignment: Alignment(valueX, valueY),
        child: RepaintBoundary(
          child: SizedBox(
            width: canvasWidth,
            height: canvasHeight,
            child: StreamBuilder<List<Stroke>>(
              stream: linesStreamController.stream,
              builder: (context, snapshot) {
                return CustomPaint(
                  painter: CurrentLayerPainter(
                    currentColor: Color.fromARGB(255, pickerColor.red,
                        pickerColor.green, pickerColor.blue),
                    optionList: optionList,
                    lines: lines,
                    options: options,
                    colors: colors,
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget buildAllLayers(BuildContext context) {
    return ClipRect(
      child: Transform.scale(
        scale: zoomLevel.toDouble(),
        alignment: Alignment(valueX, valueY),
        child: RepaintBoundary(
          child: SizedBox(
            width: canvasWidth,
            height: canvasHeight,
            child: StreamBuilder<List<Stroke>>(
              stream: linesStreamController.stream,
              builder: (context, snapshot) {
                return CustomPaint(
                  painter: LayersPainter(
                    layersColors: layersColors,
                    everyLineHasOptionListOfAll: everyLineHasOptionListOfAll,
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          buildAllLayers(context),
          buildAllPaths(context),
          buildCurrentPath(context)
        ],
      ),
    );
  }

  @override
  void dispose() {
    linesStreamController.close();
    currentLineStreamController.close();
    widget.nextOptionsNotifier.removeListener(_updateOptions);
    widget.eraserMode.removeListener(_updateMode);
    widget.backActionClick.removeListener(_updateBack);
    widget.forwardActionClick.removeListener(_updateForward);
    widget.pickerColor.removeListener(_updateColor);
    widget.zoomLevel.removeListener(_updateZoom);
    widget.valueX.removeListener(_updateValueX);
    widget.valueY.removeListener(_updateValueY);
    widget.clearAction.removeListener(_executeClearAction);
    super.dispose();
  }
}
