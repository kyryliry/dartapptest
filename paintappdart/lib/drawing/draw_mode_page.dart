import 'package:cyclop/cyclop.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart' as fc;
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:paintappdart/drawing/drawing_page.dart';
import 'package:paintappdart/utils/stroke_options.dart';

import '../model/user_model.dart';

class DrawModePage extends HookWidget {
  final selectedIndexNotifier = ValueNotifier<int>(1);
  final double width;
  final double height;
  final UserModel userDto;
  final alertIndex = ValueNotifier<int>(0);

  DrawModePage({super.key, required this.width, required this.height, required this.userDto});

  void onIndexChanged(int newValue) {
    selectedIndexNotifier.value = newValue;
  }
  void alertTrigger(int value){
    alertIndex.value = value;
  }

  @override
  Widget build(BuildContext context) {
    final indexes = useState<List<int>>([]);
    void onListChanged(List<int> newList) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        indexes.value = List.from(newList);
      });
    }
    double screenWidth = MediaQuery.of(context).size.width;
    double widget1Width = 200;
    double remainingWidth = screenWidth - widget1Width;
    double widgetHeight = MediaQuery.of(context).size.height - 90;
    double canvasWidth = width;
    double canvasHeight = height;
    double resizeFactor = 1;
    if(width>height && width>MediaQuery.of(context).size.width-260){
      resizeFactor = (MediaQuery.of(context).size.width-260)/width;
      canvasWidth = width*resizeFactor;
      canvasHeight = height*resizeFactor;

    }else if(height>width && height>MediaQuery.of(context).size.height-90){
      resizeFactor = (MediaQuery.of(context).size.height-90)/height;
      canvasWidth = width*resizeFactor;
      canvasHeight = height*resizeFactor;
    }else if(height==width && (width>MediaQuery.of(context).size.width-260 ||
        height>MediaQuery.of(context).size.height-90)){
      if(MediaQuery.of(context).size.width-260>MediaQuery.of(context).size.height-90){
        resizeFactor = (MediaQuery.of(context).size.height-90)/height;
        canvasWidth = width*resizeFactor;
        canvasHeight = height*resizeFactor;
      }else{
        resizeFactor = (MediaQuery.of(context).size.width-260)/width;
        canvasWidth = width*resizeFactor;
        canvasHeight = height*resizeFactor;
      }
    }
    ValueNotifier<StrokeOptions> nextOptionsNotifier =
        useState<StrokeOptions>(StrokeOptions());
    ValueNotifier<bool> eraserMode = useState(false);
    ValueNotifier<bool> strokeOptionsOpen = useState(false);
    ValueNotifier<bool> layersListOpen = useState(false);
    ValueNotifier<bool> colorOptionsOpen = useState(false);
    ValueNotifier<bool> backAction = useState(false);
    ValueNotifier<bool> forwardAction = useState(false);
    ValueNotifier<Color> pickerColor = useState(Colors.black);
    ValueNotifier<double> zoomLevel = useState(1);
    ValueNotifier<double> valueX = useState(-1);
    ValueNotifier<double> valueY = useState(-1);
    ValueNotifier<int> selectedIndex = useState(1);
    ValueNotifier<bool> clearActionTrigger = useState(false);
    ValueNotifier<bool> addLayerActionTrigger = useState(false);
    ValueNotifier<bool> saveActionTrigger = useState(false);
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 60,
          title: const Text('New Template'),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.only(right: 20),
                child: GestureDetector(
                  onTap: () {
                    saveActionTrigger.value = !saveActionTrigger.value;
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: const CircleAvatar(
                        radius: 40.0,
                        child: Icon(
                          Icons.save,
                          size: 30.0,
                          color: Colors.white,
                        )),
                  ),
                )),
          ],
        ),
        body: Container(
          color: Colors.grey,
          child: Stack(
          children: [
            Positioned(
                right: 0.0,
                width: 200,
                child: Container(
                    height: widgetHeight,
                    child: ListView(
                        children: [
                          Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                      Text(
                        'Current layer: ${alertIndex.value==0? selectedIndexNotifier.value: alertIndex.value}',
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      GestureDetector(
                        onTap: () {
                          if(selectedIndexNotifier.value<100){
                          addLayerActionTrigger.value =
                            !addLayerActionTrigger.value;}
                          },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: CircleAvatar(
                            backgroundColor: selectedIndexNotifier.value<100? null: Colors.grey.shade400,
                              child: const Icon(
                            Icons.plus_one,
                            size: 20.0,
                            color: Colors.white,
                          )),
                        ),
                      ),
                      const Text(
                        'Add New Layer',
                        textAlign: TextAlign.center,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 8),
                        child: Text(
                          '(max 100 layers)',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: () =>
                                  layersListOpen.value = !layersListOpen.value,
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                backgroundColor: layersListOpen.value
                                    ? Colors.grey.shade300
                                    : null,
                              ),
                              child: const Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text('Layers List'),
                                  Icon(Icons.arrow_drop_down),
                                ],
                              ),
                            ),
                            if (layersListOpen.value)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Divider(),
                                  Container(
                                    height: 150,
                                    width: 150,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(color: Colors.grey),
                                    ),
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: indexes.value.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        bool isSelected = false;
                                        if((alertIndex.value==0 && selectedIndexNotifier.value ==
                                            indexes.value[index]) || alertIndex.value==indexes.value[index]) {
                                          isSelected = true;
                                        }
                                        return SizedBox(
                                            height: 30,
                                            child: Container(
                                                child: ListTile(
                                              textColor: isSelected
                                                  ? Colors.black
                                                  : Colors.grey.shade400,
                                              title: Text(
                                                'Layer ${indexes.value[index]}',
                                                textAlign: TextAlign.center,
                                              ),
                                              onTap: () {
                                                if (selectedIndex.value !=
                                                    indexes.value[index] && alertIndex.value==0) {
                                                  selectedIndex.value =
                                                      indexes.value[index];
                                                  selectedIndexNotifier.value =
                                                      selectedIndex.value;
                                                }else if(alertIndex.value==indexes.value[index]){
                                                  selectedIndex.value =
                                                  alertIndex.value;
                                                  selectedIndexNotifier.value =
                                                      selectedIndex.value;
                                                }
                                              },
                                            )));
                                      },
                                    ),
                                  )
                                ],
                              ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8, bottom: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: () => strokeOptionsOpen.value =
                                  !strokeOptionsOpen.value,
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                backgroundColor: strokeOptionsOpen.value
                                    ? Colors.grey.shade300
                                    : null,
                              ),
                              child: const Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text('Stroke Options'),
                                  Icon(Icons.arrow_drop_down),
                                ],
                              ),
                            ),
                            if (strokeOptionsOpen.value)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Divider(),
                                  const Text(
                                    'Size',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12),
                                  ),
                                  Slider(
                                    value: nextOptionsNotifier.value.size,
                                    min: 1,
                                    max: 50,
                                    divisions: 100,
                                    label: nextOptionsNotifier.value.size
                                        .round()
                                        .toString(),
                                    onChanged: (double newValue) {
                                      nextOptionsNotifier.value =
                                          nextOptionsNotifier.value
                                              .copyWithoutSize(size: newValue);
                                    },
                                  ),
                                  const Text(
                                    'Thinning',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12),
                                  ),
                                  Slider(
                                    value: nextOptionsNotifier.value.thinning,
                                    min: -1,
                                    max: 1,
                                    divisions: 100,
                                    label: nextOptionsNotifier.value.thinning
                                        .toStringAsFixed(2),
                                    onChanged: (double newValue) {
                                      nextOptionsNotifier.value =
                                          nextOptionsNotifier.value
                                              .copyWithoutThinning(
                                                  thinning: newValue);
                                    },
                                  ),
                                  const Text(
                                    'Smoothing',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12),
                                  ),
                                  Slider(
                                    value: nextOptionsNotifier.value.smoothing,
                                    min: 0,
                                    max: 1,
                                    divisions: 100,
                                    label: nextOptionsNotifier.value.smoothing
                                        .toStringAsFixed(2),
                                    onChanged: (double newValue) {
                                      nextOptionsNotifier.value =
                                          nextOptionsNotifier.value
                                              .copyWithoutSmoothing(
                                                  smoothing: newValue);
                                    },
                                  ),
                                  const Text(
                                    'Taper Start',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12),
                                  ),
                                  Slider(
                                    value: nextOptionsNotifier.value.taperStart,
                                    min: 0,
                                    max: 100,
                                    divisions: 100,
                                    label: nextOptionsNotifier.value.taperStart
                                        .toStringAsFixed(2),
                                    onChanged: (double newValue) {
                                      nextOptionsNotifier.value =
                                          nextOptionsNotifier.value
                                              .copyWithoutTaperStart(
                                                  taperStart: newValue);
                                    },
                                  ),
                                  const Text(
                                    'Taper End',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12),
                                  ),
                                  Slider(
                                    value: nextOptionsNotifier.value.taperEnd,
                                    min: 0,
                                    max: 100,
                                    divisions: 100,
                                    label: nextOptionsNotifier.value.taperEnd
                                        .toStringAsFixed(2),
                                    onChanged: (double newValue) {
                                      nextOptionsNotifier.value =
                                          nextOptionsNotifier.value
                                              .copyWithoutTaperEnd(
                                                  taperEnd: newValue);
                                    },
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8, bottom: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: () => colorOptionsOpen.value =
                                  !colorOptionsOpen.value,
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                backgroundColor: colorOptionsOpen.value
                                    ? Colors.grey.shade300
                                    : null,
                              ),
                              child: const Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text('Color Settings'),
                                  Icon(Icons.arrow_drop_down),
                                ],
                              ),
                            ),
                            if (colorOptionsOpen.value)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Divider(),
                                  Padding(
                                    padding: EdgeInsets.all(10),
                                    child: fc.ColorPicker(
                                      pickerColor: pickerColor.value,
                                      onColorChanged: (color) =>
                                          pickerColor.value = color,
                                      pickerAreaHeightPercent: 0.6,
                                      enableAlpha: false,
                                      displayThumbColor: true,
                                      showLabel: true,
                                      paletteType: fc.PaletteType.hsv,
                                      pickerAreaBorderRadius:
                                          const BorderRadius.only(
                                        topLeft: const Radius.circular(2.0),
                                        topRight: const Radius.circular(2.0),
                                      ),
                                      portraitOnly: true,
                                    ),
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ])]))),
            Positioned(
              top: 0,
              left: 30,
              width: canvasWidth,
              height: canvasHeight,
              child: DrawingPage(
                  userId: userDto!.id,
                  width: canvasWidth,
                  height: canvasHeight,
                  nextOptionsNotifier: nextOptionsNotifier,
                  eraserMode: eraserMode,
                  backActionClick: backAction,
                  forwardActionClick: forwardAction,
                  pickerColor: pickerColor,
                  zoomLevel: zoomLevel,
                  valueX: valueX,
                  valueY: valueY,
                  onListChanged: onListChanged,
                  indexParent: selectedIndex,
                  onIndexChanged: onIndexChanged,
                  userDto: userDto,
                  clearAction: clearActionTrigger,
                  addLayerAction: addLayerActionTrigger,
                  saveAction: saveActionTrigger,
                  resizeFactor: resizeFactor,
                  alertTrigger: alertTrigger),
            ),
            Positioned(
                top: 0,
                left: 0,
                width: 30,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () => eraserMode.value = false,
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: CircleAvatar(
                          backgroundColor:
                              !eraserMode.value ? Colors.blueGrey : null,
                          child: const Icon(
                            Icons.brush,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => eraserMode.value = true,
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: CircleAvatar(
                          backgroundColor:
                              eraserMode.value ? Colors.blueGrey : null,
                          child: const Icon(
                            FontAwesomeIcons.eraser,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        backAction.value = !backAction.value;
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.arrow_back_rounded,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        forwardAction.value = !forwardAction.value;
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.arrow_forward_rounded,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: CircleAvatar(
                          child: EyedropperButton(
                        onColor: (value) => pickerColor.value = value,
                        icon: Icons.colorize_rounded,
                        iconColor: Colors.white,
                      )),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (zoomLevel.value < 5) {
                          zoomLevel.value++;
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.zoom_in,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (zoomLevel.value > 1) {
                          zoomLevel.value--;
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.zoom_out,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        clearActionTrigger.value = !clearActionTrigger.value;
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.clear,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
            if (zoomLevel.value > 1)
              Positioned(
                bottom: 0,
                height: 30,
                left: 30,
                width: remainingWidth - 30,
                child: Slider(
                    value: valueX.value,
                    min: -1,
                    max: 1,
                    label: valueX.value.toStringAsFixed(2),
                    onChanged: (double value1) {
                      if (zoomLevel.value > 1) {
                        valueX.value = value1;
                      }
                    }),
              ),
            if (zoomLevel.value > 1)
              Positioned(
                width: 30,
                height: widgetHeight - 30,
                right: 200,
                child: RotatedBox(
                  quarterTurns: 1,
                  child: Slider(
                      value: valueY.value,
                      min: -1,
                      max: 1,
                      label: valueY.value.toStringAsFixed(2),
                      onChanged: (double value1) {
                        if (zoomLevel.value > 1) {
                          valueY.value = value1;
                        }
                      }),
                ),
              ),
          ],
        )));
  }
}
