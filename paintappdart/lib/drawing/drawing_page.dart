import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

import 'package:paintappdart/drawing/base_page.dart';
import 'package:flutter/material.dart';
import 'package:paintappdart/drawing/save_page.dart';
import 'package:paintappdart/model/layer_model.dart';
import 'package:paintappdart/utils/stroke_pair.dart';
import 'package:paintappdart/painters/save_painter.dart';

import '../model/category_model.dart';
import '../utils/session_manager.dart';
import 'coloring_page.dart';

class DrawingPage extends BasePage {

  final Function(List<int>) onListChanged;
  final ValueNotifier<int> indexParent;
  final ValueNotifier<bool> addLayerAction;
  final ValueNotifier<bool> saveAction;
  final double resizeFactor;
  final Function(int) alertTrigger;

  DrawingPage({Key? key,
    required super.userId,
    required super.width,
    required super.height,
    required super.nextOptionsNotifier,
    required super.eraserMode,
    required super.backActionClick,
    required super.forwardActionClick,
    required super.pickerColor,
    required super.zoomLevel,
    required super.valueX,
    required super.valueY,
    required super.clearAction,
    required super.onIndexChanged,
    required super.userDto,
    required this.onListChanged,
    required this.indexParent,
    required this.addLayerAction,
    required this.saveAction,
    required this.resizeFactor,
    required this.alertTrigger,
  }
  ): super(key: key);

  @override
  _DrawingPageState createState() => _DrawingPageState();
}

class _DrawingPageState extends BasePageState<DrawingPage> {
  final logger = Logger(
    printer: SimplePrinter(),
  );
  List<StrokePair> selectedLayer = <StrokePair>[];
  List<int> indexes = [];
  List<CategoryModel> categories = <CategoryModel>[];
  List<CategoryModel> selectedCategories = <CategoryModel>[];

  Map<int, LayerModel> layersData = <int, LayerModel>{};
  Offset pointerPosition = Offset.zero;

  @override
  void initState() {
    super.initState();
    indexes.add(1);
    newValue = widget.indexParent.value;
    widget.onListChanged(indexes);
    widget.indexParent.addListener(_updateSelectedIndex);
    widget.addLayerAction.addListener(_executeAddLayerAction);
    widget.saveAction.addListener(_executeSaveAction);
  }

  void _executeSaveAction() {
    save();
  }

  void _executeAddLayerAction() {
    layer();
  }

  Future<void> alertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            alignment: Alignment.center,
            actionsAlignment: MainAxisAlignment.spaceAround,
            title:  Container(
              width: 200,
              child:
              const Text('Layer may not be empty, please draw something',
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,),),
            actions: <Widget>[
              MaterialButton(
                color: Colors.deepPurple.shade300,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  void _updateSelectedIndex() {
    if(everyLineHasOption.isEmpty && everyLineHasOptionListOfAll[selectedIndex! - 1].isEmpty){
      alertDialog(context);
      widget.alertTrigger(selectedIndex!);
      return;
    }
    newValue = widget.indexParent.value;
    if (selectedIndex != newValue) {
      if ((selectedIndex!) != everyLineHasOptionListOfAll.length) {
        setState(() {
          everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
          everyLineHasOptionListOfAll[selectedIndex! - 1]
              .addAll(everyLineHasOption);

          currentColor = Color.fromARGB(
              255, pickerColor.red, pickerColor.green, pickerColor.blue);
          layersColors[selectedIndex! - 1] = currentColor;
          everyLineHasOption.clear();
          lines = [];
          line = null;
          optionList = [];
        });
      } else {
        setState(() {
          everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
          everyLineHasOptionListOfAll[selectedIndex! - 1]
              .addAll(everyLineHasOption);
          currentColor = Color.fromARGB(
              255, pickerColor.red, pickerColor.green, pickerColor.blue);
          layersColors[selectedIndex! - 1] = currentColor;
          everyLineHasOption.clear();
          lines = [];
          line = null;
          optionList = [];
        });
      }
      setState(() {
        selectedIndex = newValue;
        layerAction();
      });
    }
  }

  Future<void> layerAction() async {
    setState(() {
      everyLineHasOption = List.of(everyLineHasOption)
        ..addAll(everyLineHasOptionListOfAll[selectedIndex! - 1]);
      everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
      changeColor(layersColors[selectedIndex! - 1]);
      for (var item in everyLineHasOption) {
        lines.add(item.stroke);
        optionList.add(item.options);
      }
      if (lines.isNotEmpty) {
        line = lines.last;
      }
    });
  }

  Future<void> layer() async {
    if(everyLineHasOption.isEmpty){
      alertDialog(context);
      widget.alertTrigger(selectedIndex!);
      return;
    }
    if ((selectedIndex!) != everyLineHasOptionListOfAll.length) {
      setState(() {
        everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
        everyLineHasOptionListOfAll[selectedIndex! - 1]
            .addAll(everyLineHasOption);
        currentColor = Color.fromARGB(
            255, pickerColor.red, pickerColor.green, pickerColor.blue);
        layersColors[selectedIndex! - 1] = currentColor;
        everyLineHasOption.clear();
        lines = [];
        line = null;
        optionList = [];
        selectedIndex = selectedIndex! + 1;
        if (indexes.contains(selectedIndex)) {
          indexes.insert(selectedIndex! - 1, selectedIndex!);
          for (int i = selectedIndex!; i < indexes.length; i++) {
            indexes[i] = indexes[i] + 1;
          }
          widget.onListChanged(indexes);
          widget.onIndexChanged(selectedIndex!);
          widget.alertTrigger(0);
          everyLineHasOptionListOfAll.insert(selectedIndex! - 1, []);
          layersColors.insert(selectedIndex! - 1, currentColor);
        }
      });
    } else {
      everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
      everyLineHasOptionListOfAll[selectedIndex! - 1]
          .addAll(everyLineHasOption);
      everyLineHasOptionListOfAll.add([]);
      indexes.add(everyLineHasOptionListOfAll.length);
      widget.onListChanged(indexes);
      currentColor = Color.fromARGB(
          255, pickerColor.red, pickerColor.green, pickerColor.blue);
      layersColors[selectedIndex! - 1] = currentColor;
      layersColors.add(currentColor);
      selectedIndex = indexes.length;
      widget.onIndexChanged(selectedIndex!);
      widget.alertTrigger(0);
      everyLineHasOption.clear();
      setState(() {
        lines = [];
        line = null;
        optionList = [];
      });
    }
  }

  @override
  void onPointerUp(PointerUpEvent details) {
    if (backActionMap.isNotEmpty) {
      backActionMap.clear();
    }
    lines = List.from(lines)..add(line!);
    var gifts = StrokePair(line!, options);
    everyLineHasOption = List.from(everyLineHasOption)..add(gifts);
    widget.alertTrigger(0);

    optionList = List.from(optionList)..add(options);
    linesStreamController.add(lines);
  }

  Future<void> getAllCategories() async {
    String? jsessionId = SessionManager().getJsessionId();
    Map<String, String>? headers = {'Content-Type': 'application/json'};
    if(jsessionId!=null){
      headers.putIfAbsent('Authorization', () => jsessionId);
    }
    var apiUrl = Uri.parse("http://localhost:8080/api/categories");

    final response = await http
        .get(apiUrl, headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        final item = json.decode(response.body);
        if(response.headers['authorization']!=null) {
          SessionManager().setJsessionId('Bearer ${response.headers['authorization']}');
        }

        var list = item['categories'] as List;
        for(var item in list){
          categories.add(CategoryModel.fromJson(item));
        }
      });
    } else {
      logger.i('get categories in drawing page caused error: ${response.body}');
    }
  }

  void saveReturn(Map <int, LayerModel> data, List<CategoryModel> cats, List<CategoryModel> selCats){
      setState(() {
        layerAction();
        layersData.clear();
        layersData = data;
        categories = cats;
        selectedCategories = selCats;
      });
  }

  Future<void> save() async {
    if ((selectedIndex!) != everyLineHasOptionListOfAll.length) {
      everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
      everyLineHasOptionListOfAll[selectedIndex! - 1]
          .addAll(everyLineHasOption);
      currentColor = Color.fromARGB(
          255, pickerColor.red, pickerColor.green, pickerColor.blue);
      layersColors[selectedIndex! - 1] = currentColor;
      everyLineHasOption.clear();
      setState(() {
        lines = [];
        line = null;
        optionList = [];
      });
    } else {
      everyLineHasOptionListOfAll[selectedIndex! - 1].clear();
      everyLineHasOptionListOfAll[selectedIndex! - 1]
          .addAll(everyLineHasOption);
      currentColor = Color.fromARGB(
          255, pickerColor.red, pickerColor.green, pickerColor.blue);
      layersColors[selectedIndex! - 1] = currentColor;
      everyLineHasOption.clear();
      setState(() {
        lines = [];
        line = null;
        optionList = [];
      });
    }
    if(categories.isEmpty) {
      await getAllCategories();
    }
    if(mounted) {
      SavePaint(
          everyLineHasOptionListOfAll: everyLineHasOptionListOfAll,
          options: options,
          layersColors: layersColors,
          onMapGenerated: (layers, sampleImage) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        SavePage(layers: layers,
                          saveReturn: saveReturn,
                          sampleImage: sampleImage,
                          categories: categories,
                          selectedCategories: selectedCategories,
                          userDto: userDto!,)));
          },
          layersData: layersData,
          resizeFactor: widget.resizeFactor).save(
          widget.width, widget.height, MediaQuery
          .of(context)
          .devicePixelRatio);
    }
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Listener(
            onPointerMove: (event) {
              setState(() {
                pointerPosition = event.localPosition;
              });
            },
            onPointerDown: (event) {
              setState(() {
                pointerPosition = event.localPosition;
              });
            },
            onPointerUp: (event) {
              setState(() {
                pointerPosition = event.localPosition;
              });
            },
            child: MouseRegion(
              cursor: SystemMouseCursors.none,
              onHover: (event) {
                setState(() {
                  pointerPosition = event.localPosition;
                });
              },
              child: Stack(
                children: [
                  buildAllLayers(context),
                  buildAllPaths(context),
                  buildCurrentPath(context)
                ],
              ),
            ),
          ),
          Overlay(
            initialEntries: [
              OverlayEntry(
                builder: (context) => Positioned(
                  left: pointerPosition.dx - 5,
                  top: pointerPosition.dy - 5,
                  child: IgnorePointer(
                    child: CustomPaint(
                      painter: PointerIndicatorPainter(
                          (nextOptions.size / 2) * zoomLevel),
                      size: Size(10, 10),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    widget.indexParent.removeListener(_updateSelectedIndex);
    widget.addLayerAction.removeListener(_executeAddLayerAction);
    widget.saveAction.removeListener(_executeSaveAction);
    super.dispose();
  }
}
