
import 'package:flutter/material.dart';
import 'package:paintappdart/templatesLibrary/library_main_page.dart';
import 'package:paintappdart/templatesLibrary/template_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Drawing App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LibraryMainPage(),
      onGenerateRoute: (settings) {
        if (settings.name == '/template') {
          final args = settings.arguments as Map;
          final id = args['id'];
          final userDto = args['userDto'];
          return MaterialPageRoute(
            builder: (context) {
              return TemplatePage(templateId: id, userDto: userDto,);
            },
          );
        }
        return null;
      },
    );
  }
}
